// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.0.1.0_GSC_DN/sbtest/ain_buf_clear.c $
// $Rev: 17643 $
// $Date: 2012-08-20 17:06:51 -0500 (Mon, 20 Aug 2012) $

#include <errno.h>
#include <stdio.h>
#include <unistd.h>

#include "main.h"
#include "24dsi_dsl.h"



//*****************************************************************************
static int _service_test(int fd)
{
	// There are no persistent bits to test with this service.
	return(0);
}



//*****************************************************************************
static int _function_test(int fd)
{
	#define	THRESHOLD_FLAG	(1 << 14)

	static const service_data_t	list[]	=
	{
		{				// Wait for the buffer to fill.
			/* service	*/	SERVICE_SLEEP,
			/* cmd		*/	0,
			/* arg		*/	6,	// Let the buffer fill. We may need more time.
			/* reg		*/	-1,
			/* mask		*/	0,
			/* value	*/	0
		},
		{				// Verify that we got lots of input.
			/* service	*/	SERVICE_REG_TEST,
			/* cmd		*/	0,
			/* arg		*/	0,
			/* reg		*/	DSI_GSC_IBCR,
			/* mask		*/	THRESHOLD_FLAG,
			/* value	*/	THRESHOLD_FLAG
		},
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_AIN_BUF_INPUT,
			/* arg		*/	DSI_AIN_BUF_INPUT_DISABLE,
			/* reg		*/	-1,
			/* mask		*/	0,
			/* value	*/	0
		},
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_AIN_BUF_CLEAR,
			/* arg		*/	0,
			/* reg		*/	DSI_GSC_BSR,
			/* mask		*/	0x3FFFF,
			/* value	*/	0x00000
		},

		{ SERVICE_END_LIST }
	};

	int	errs	= 0;

	errs	+= dsi_initialize(fd, -1, 0);

	errs	+= service_ioctl_set_reg_list(fd, list);

	return(errs);
}



/******************************************************************************
*
*	Function:	ain_buf_clear_test
*
*	Purpose:
*
*		Perform a test of the IOCTL service DSI_IOCTL_AIN_BUF_CLEAR.
*
*	Arguments:
*
*		fd		The handle for the board to access.
*
*	Returned:
*
*		>= 0	The number of errors encounterred.
*
******************************************************************************/

int ain_buf_clear_test(int fd)
{
	int	errs	= 0;

	gsc_label("DSI_IOCTL_AIN_BUF_CLEAR");
	errs	+= _service_test(fd);
	errs	+= _function_test(fd);

	if (errs == 0)
		printf("PASS\n");

	return(errs);
}


