// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.0.1.0_GSC_DN/sbtest/rate_gen_a_nrate.c $
// $Rev: 17643 $
// $Date: 2012-08-20 17:06:51 -0500 (Mon, 20 Aug 2012) $

#include <stdio.h>

#include "main.h"



//*****************************************************************************
static int _service_subtest(
	int		fd,
	__u32	reg,
	__s32	min,
	__s32	max,
	__s32	nrate)
{
	#undef	MASK
	#define	MASK	0x1FFFF

	service_data_t	list[]	=
	{
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_RATE_GEN_A_NRATE,
			/* arg		*/	min,
			/* reg		*/	reg,
			/* mask		*/	MASK,
			/* value	*/	min
		},
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_RATE_GEN_A_NRATE,
			/* arg		*/	nrate,
			/* reg		*/	reg,
			/* mask		*/	MASK,
			/* value	*/	nrate
		},
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_RATE_GEN_A_NRATE,
			/* arg		*/	max,
			/* reg		*/	reg,
			/* mask		*/	MASK,
			/* value	*/	max
		},

		{ SERVICE_END_LIST }
	};

	int	errs	= 0;

	errs	+= service_ioctl_set_reg_list(fd, list);
	errs	+= service_ioctl_reg_get_list(fd, list);

	return(errs);
}



//*****************************************************************************
static int _service_test(int fd, __u32 reg, __s32 min, __s32 max)
{
	int errs	= 0;

	errs	+= dsi_initialize(fd, -1, 0);

	errs	+= _service_subtest(fd, reg, min, max, min + 1);
	errs	+= _service_subtest(fd, reg, min, max, (min + max) / 2);
	errs	+= _service_subtest(fd, reg, min, max, max - 1);

	return(errs);
}



//*****************************************************************************
static int _function_test(int fd)
{
	// TBD
	return(0);
}



/******************************************************************************
*
*	Function:	rate_gen_a_nrate_test
*
*	Purpose:
*
*		Perform a test of the IOCTL service DSI_IOCTL_RATE_GEN_A_NRATE.
*
*	Arguments:
*
*		fd		The handle for the board to access.
*
*	Returned:
*
*		>= 0	The number of errors encounterred.
*
******************************************************************************/

int rate_gen_a_nrate_test(int fd)
{
	__s32	rcabr;
	int		errs	= 0;
	__s32	max;
	__s32	min;
	__s32	pll		= 0;
	__s32	qty		= 0;
	__u32	reg;

	gsc_label("DSI_IOCTL_RATE_GEN_A_NRATE");
	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_NRATE_MAX, &max);
	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_NRATE_MIN, &min);
	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_PLL_PRESENT, &pll);
	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_RATE_GEN_QTY, &qty);
	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_RCAR_RCBR, &rcabr);

	if (errs)
	{
	}
	else if ((pll) || (qty < 1))
	{
		printf("SKIPPED  (Not supported on this board.)\n");
	}
	else
	{
		reg		= rcabr ? DSI_GSC_RCAR : DSI_GSC_RCR;
		errs	+= _service_test(fd, reg, min, max);
		errs	+= _function_test(fd);

		if (errs == 0)
			printf("PASS\n");
	}

	return(errs);
}


