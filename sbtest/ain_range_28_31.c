// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.0.1.0_GSC_DN/sbtest/ain_range_28_31.c $
// $Rev: 17643 $
// $Date: 2012-08-20 17:06:51 -0500 (Mon, 20 Aug 2012) $

#include <stdio.h>

#include "main.h"



//*****************************************************************************
static int _service_test(int fd)
{
	static const service_data_t	list[]	=
	{
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_AIN_RANGE_28_31,
			/* arg		*/	DSI_AIN_RANGE_2_5V,
			/* reg		*/	DSI_GSC_RFCR,
			/* mask		*/	0xC0000000,
			/* value	*/	0x40000000
		},
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_AIN_RANGE_28_31,
			/* arg		*/	DSI_AIN_RANGE_5V,
			/* reg		*/	DSI_GSC_RFCR,
			/* mask		*/	0xC0000000,
			/* value	*/	0x80000000
		},
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_AIN_RANGE_28_31,
			/* arg		*/	DSI_AIN_RANGE_10V,
			/* reg		*/	DSI_GSC_RFCR,
			/* mask		*/	0xC0000000,
			/* value	*/	0xC0000000
		},

		{ SERVICE_END_LIST }
	};

	int errs	= 0;

	errs	+= dsi_initialize(fd, -1, 0);

	errs	+= service_ioctl_set_get_list(fd, list);

	errs	+= service_ioctl_set_get_list(fd, list);

	return(errs);
}



//*****************************************************************************
static int _function_test(int fd)
{
	// TBD
	return(0);
}



/******************************************************************************
*
*	Function:	ain_range_28_31_test
*
*	Purpose:
*
*		Perform a test of the IOCTL service DSI_IOCTL_AIN_RANGE_28_31
*
*	Arguments:
*
*		fd		The handle for the board to access.
*
*	Returned:
*
*		>= 0	The number of errors encounterred.
*
******************************************************************************/

int ain_range_28_31_test(int fd)
{
	int		errs	= 0;
	__s32	qty;
	__s32	rfcr;

	gsc_label("DSI_IOCTL_AIN_RANGE_28_31");
	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_CHANNEL_QTY, &qty);
	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_RFCR, &rfcr);

	if (errs)
	{
	}
	else if ((rfcr == 0) || (qty <= 31))
	{
		printf("SKIPPED  (Not supported on this board.)\n");
	}
	else
	{
		errs	+= _service_test(fd);
		errs	+= _function_test(fd);

		if (errs == 0)
			printf("PASS\n");
	}

	return(errs);
}


