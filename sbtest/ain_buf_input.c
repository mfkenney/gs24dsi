// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.0.1.0_GSC_DN/sbtest/ain_buf_input.c $
// $Rev: 17643 $
// $Date: 2012-08-20 17:06:51 -0500 (Mon, 20 Aug 2012) $

#include <errno.h>
#include <stdio.h>
#include <unistd.h>

#include "main.h"
#include "24dsi_dsl.h"



//*****************************************************************************
static int _service_test(int fd)
{
	#define	CONTROL_BIT		(1 << 18)

	static const service_data_t	list[]	=
	{
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_AIN_BUF_INPUT,
			/* arg		*/	DSI_AIN_BUF_INPUT_DISABLE,
			/* reg		*/	DSI_GSC_IBCR,
			/* mask		*/	CONTROL_BIT,
			/* value	*/	CONTROL_BIT
		},
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_AIN_BUF_INPUT,
			/* arg		*/	DSI_AIN_BUF_INPUT_ENABLE,
			/* reg		*/	DSI_GSC_IBCR,
			/* mask		*/	CONTROL_BIT,
			/* value	*/	0
		},

		{ SERVICE_END_LIST }
	};

	int errs	= 0;

	errs	+= dsi_initialize(fd, -1, 0);

	errs	+= service_ioctl_set_get_list(fd, list);

	errs	+= service_ioctl_set_get_list(fd, list);

	return(errs);
}



//*****************************************************************************
static int _function_test(int fd)
{
	// TBD
	return(0);
}



/******************************************************************************
*
*	Function:	ain_buf_input_test
*
*	Purpose:
*
*		Perform a test of the IOCTL service DSI_IOCTL_AIN_BUF_INPUT.
*
*	Arguments:
*
*		fd		The handle for the board to access.
*
*	Returned:
*
*		>= 0	The number of errors encounterred.
*
******************************************************************************/

int ain_buf_input_test(int fd)
{
	int	errs	= 0;

	gsc_label("DSI_IOCTL_AIN_BUF_INPUT");
	errs	+= _service_test(fd);
	errs	+= _function_test(fd);

	if (errs == 0)
		printf("PASS\n");

	return(errs);
}


