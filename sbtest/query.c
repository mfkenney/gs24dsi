// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.0.1.0_GSC_DN/sbtest/query.c $
// $Rev: 17643 $
// $Date: 2012-08-20 17:06:51 -0500 (Mon, 20 Aug 2012) $

#include <stdio.h>

#include "main.h"



/******************************************************************************
*
*	Function:	query_test
*
*	Purpose:
*
*		Perform a test of the IOCTL service DSI_IOCTL_QUERY. We test only the
*		overall service, not the values returned.
*
*	Arguments:
*
*		fd		The handle for the board to access.
*
*	Returned:
*
*		>= 0	The number of errors encounterred.
*
******************************************************************************/

int query_test(int fd)
{
	__s32	data;
	__s32	count;
	int		errs;
	int		i;

	gsc_label("DSI_IOCTL_QUERY");
	errs	= dsi_query(fd, -1, 0, DSI_QUERY_COUNT, &count);

	if (count != (DSI_IOCTL_QUERY_LAST + 1))
	{
		errs	= 1;
		printf(	"FAIL <---  (expected %ld, got %ld, rebuild application)\n",
				(long) DSI_IOCTL_QUERY_LAST + 1,
				(long) count);
	}

	for (i = 0; (errs == 0) && (i <= DSI_IOCTL_QUERY_LAST); i++)
		errs	= dsi_query(fd, -1, 0, i, &data);

	if (errs == 0)
		printf("PASS\n");

	return(errs);
}


