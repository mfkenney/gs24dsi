// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.0.1.0_GSC_DN/sbtest/channel_order.c $
// $Rev: 17643 $
// $Date: 2012-08-20 17:06:51 -0500 (Mon, 20 Aug 2012) $

#include <stdio.h>

#include "main.h"



static int _service_test(int fd, __s32 sync_1)
{
	static const service_data_t	list_sync_0[]	=
	{
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_CHANNEL_ORDER,
			/* arg		*/	DSI_CHANNEL_ORDER_ASYNC,
			/* reg		*/	DSI_GSC_BCTLR,
			/* mask		*/	0x10000,
			/* value	*/	0x10000
		},
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_CHANNEL_ORDER,
			/* arg		*/	DSI_CHANNEL_ORDER_SYNC,
			/* reg		*/	DSI_GSC_BCTLR,
			/* mask		*/	0x10000,
			/* value	*/	0x00000
		},

		{ SERVICE_END_LIST }
	};

	static const service_data_t	list_sync_1[]	=
	{
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_CHANNEL_ORDER,
			/* arg		*/	DSI_CHANNEL_ORDER_ASYNC,
			/* reg		*/	DSI_GSC_BCTLR,
			/* mask		*/	0x10000,
			/* value	*/	0x00000
		},
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_CHANNEL_ORDER,
			/* arg		*/	DSI_CHANNEL_ORDER_SYNC,
			/* reg		*/	DSI_GSC_BCTLR,
			/* mask		*/	0x10000,
			/* value	*/	0x10000
		},

		{ SERVICE_END_LIST }
	};

	int						errs	= 0;
	const service_data_t*	list;

	list	= sync_1 ? list_sync_1 : list_sync_0;

	errs	+= dsi_initialize(fd, -1, 0);

	errs	+= service_ioctl_set_get_list(fd, list);
	errs	+= service_ioctl_set_get_list(fd, list);

	return(errs);
}



//*****************************************************************************
static int _function_test(int fd)
{
	// TBD
	return(0);
}



/******************************************************************************
*
*	Function:	channel_order_test
*
*	Purpose:
*
*		Perform a test of the IOCTL service DSI_IOCTL_CHANNEL_ORDER.
*
*	Arguments:
*
*		fd		The handle for the board to access.
*
*	Returned:
*
*		>= 0	The number of errors encounterred.
*
******************************************************************************/

int channel_order_test(int fd)
{
	int		errs;
	__s32	sync_1;

	gsc_label("DSI_IOCTL_CHANNEL_ORDER");
	errs	= dsi_query(fd, -1, 0, DSI_QUERY_D16_SYNC_1, &sync_1);

	if (errs == 0)
	{
		errs	+= _service_test(fd, sync_1);
		errs	+= _function_test(fd);

		if (errs == 0)
			printf("PASS\n");
	}

	return(errs);
}


