// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.0.1.0_GSC_DN/sbtest/ain_mode.c $
// $Rev: 17643 $
// $Date: 2012-08-20 17:06:51 -0500 (Mon, 20 Aug 2012) $

#include <stdio.h>

#include "main.h"



//*****************************************************************************
static int _service_test(int fd)
{
	static const service_data_t	list[]	=
	{
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_AIN_MODE,
			/* arg		*/	DSI_AIN_MODE_VREF,
			/* reg		*/	DSI_GSC_BCTLR,
			/* mask		*/	0x3,
			/* value	*/	0x3
		},
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_AIN_MODE,
			/* arg		*/	DSI_AIN_MODE_ZERO,
			/* reg		*/	DSI_GSC_BCTLR,
			/* mask		*/	0x3,
			/* value	*/	0x2
		},
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_AIN_MODE,
			/* arg		*/	DSI_AIN_MODE_DIFF,
			/* reg		*/	DSI_GSC_BCTLR,
			/* mask		*/	0x3,
			/* value	*/	0x0
		},

		{ SERVICE_END_LIST }
	};

	int errs	= 0;

	errs	+= dsi_initialize(fd, -1, 0);

	errs	+= service_ioctl_set_get_list(fd, list);

	errs	+= service_ioctl_set_get_list(fd, list);

	return(errs);
}



//*****************************************************************************
static int _function_test(int fd)
{
	// TBD
	return(0);
}



/******************************************************************************
*
*	Function:	ain_mode_test
*
*	Purpose:
*
*		Perform a test of the IOCTL service DSI_IOCTL_AIN_MODE.
*
*	Arguments:
*
*		fd		The handle for the board to access.
*
*	Returned:
*
*		>= 0	The number of errors encounterred.
*
******************************************************************************/

int ain_mode_test(int fd)
{
	__s32	d0_1;
	int		errs	= 0;

	gsc_label("DSI_IOCTL_AIN_MODE");
	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_D0_1_IN_MODE, &d0_1);

	if (errs)
	{
	}
	else if (d0_1 == 0)
	{
		printf("SKIPPED  (Not supported on this board.)\n");
	}
	else
	{
		errs	+= _service_test(fd);
		errs	+= _function_test(fd);

		if (errs == 0)
			printf("PASS\n");
	}


	return(errs);
}


