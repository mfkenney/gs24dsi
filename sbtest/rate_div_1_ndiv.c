// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.0.1.0_GSC_DN/sbtest/rate_div_1_ndiv.c $
// $Rev: 17643 $
// $Date: 2012-08-20 17:06:51 -0500 (Mon, 20 Aug 2012) $

#include <stdio.h>

#include "main.h"



//*****************************************************************************
static int _service_subtest(
	int		fd,
	int		shift,
	__s32	mask,
	__s32	min,
	__s32	max,
	__s32	ndiv)
{
	#define	MAX		(max << shift)
	#define	MIN		(min << shift)
	#define	MASK	(mask << shift)
	#define	NDIV	(ndiv << shift)

	service_data_t	list[]	=
	{
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_RATE_DIV_1_NDIV,
			/* arg		*/	min,
			/* reg		*/	DSI_GSC_RDR,
			/* mask		*/	MASK,
			/* value	*/	MIN
		},
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_RATE_DIV_1_NDIV,
			/* arg		*/	ndiv,
			/* reg		*/	DSI_GSC_RDR,
			/* mask		*/	MASK,
			/* value	*/	NDIV
		},
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_RATE_DIV_1_NDIV,
			/* arg		*/	max,
			/* reg		*/	DSI_GSC_RDR,
			/* mask		*/	MASK,
			/* value	*/	MAX
		},

		{ SERVICE_END_LIST }
	};

	int	errs	= 0;

	errs	+= service_ioctl_set_reg_list(fd, list);
	errs	+= service_ioctl_reg_get_list(fd, list);

	return(errs);
}



//*****************************************************************************
static int _service_test(int fd, int shift, __s32 mask, __s32 min, __s32 max)
{
	int errs	= 0;

	errs	+= dsi_initialize(fd, -1, 0);

	errs	+= _service_subtest(fd, shift, mask, min, max, min + 1);
	errs	+= _service_subtest(fd, shift, mask, min, max, (min + max) / 2);
	errs	+= _service_subtest(fd, shift, mask, min, max, max - 1);

	return(errs);
}



//*****************************************************************************
static int _function_test(int fd)
{
	// TBD
	return(0);
}



/******************************************************************************
*
*	Function:	rate_div_1_ndiv_test
*
*	Purpose:
*
*		Perform a test of the IOCTL service DSI_IOCTL_RATE_DIV_1_NDIV.
*
*	Arguments:
*
*		fd		The handle for the board to access.
*
*	Returned:
*
*		>= 0	The number of errors encounterred.
*
******************************************************************************/

int rate_div_1_ndiv_test(int fd)
{
	int		errs	= 0;
	__s32	qty;
	__s32	mask;
	__s32	max;
	__s32	min;

	gsc_label("DSI_IOCTL_RATE_DIV_1_NDIV");
	errs	= dsi_query(fd, -1, 0, DSI_QUERY_RATE_DIV_QTY, &qty);
	errs	= dsi_query(fd, -1, 0, DSI_QUERY_NDIV_MASK, &mask);
	errs	= dsi_query(fd, -1, 0, DSI_QUERY_NDIV_MAX, &max);
	errs	= dsi_query(fd, -1, 0, DSI_QUERY_NDIV_MIN, &min);

	if (errs)
	{
	}
	else if (qty < 2)
	{
		printf("SKIPPED  (Not supported on this board.)\n");
	}
	else
	{
		errs	+= _service_test(fd, 8, mask, min, max);
		errs	+= _function_test(fd);

		if (errs == 0)
			printf("PASS\n");
	}

	return(errs);
}


