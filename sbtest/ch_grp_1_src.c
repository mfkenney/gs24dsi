// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.0.1.0_GSC_DN/sbtest/ch_grp_1_src.c $
// $Rev: 17643 $
// $Date: 2012-08-20 17:06:51 -0500 (Mon, 20 Aug 2012) $

#include <stdio.h>

#include "main.h"



//*****************************************************************************
static int _service_test(int fd, __s32 gens, __s32 rar0)
{
	static const service_data_t	list_a[]	=
	{
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_CH_GRP_1_SRC,
			/* arg		*/	DSI_CH_GRP_SRC_GEN_A,
			/* reg		*/	DSI_GSC_RAR,
			/* mask		*/	0xF0,
			/* value	*/	0x00
		},
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_CH_GRP_1_SRC,
			/* arg		*/	DSI_CH_GRP_SRC_EXTERN,
			/* reg		*/	DSI_GSC_RAR,
			/* mask		*/	0xF0,
			/* value	*/	0x40
		},
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_CH_GRP_1_SRC,
			/* arg		*/	DSI_CH_GRP_SRC_DIR_EXTERN,
			/* reg		*/	DSI_GSC_RAR,
			/* mask		*/	0xF0,
			/* value	*/	0x50
		},
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_CH_GRP_1_SRC,
			/* arg		*/	DSI_CH_GRP_SRC_DISABLE,
			/* reg		*/	DSI_GSC_RAR,
			/* mask		*/	0xF0,
			/* value	*/	0x60
		},

		{ SERVICE_END_LIST }
	};

	static const service_data_t	list_ab[]	=
	{
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_CH_GRP_1_SRC,
			/* arg		*/	DSI_CH_GRP_SRC_GEN_A,
			/* reg		*/	DSI_GSC_RAR,
			/* mask		*/	0xF0,
			/* value	*/	0x00
		},
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_CH_GRP_1_SRC,
			/* arg		*/	DSI_CH_GRP_SRC_GEN_B,
			/* reg		*/	DSI_GSC_RAR,
			/* mask		*/	0xF0,
			/* value	*/	0x10
		},
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_CH_GRP_1_SRC,
			/* arg		*/	DSI_CH_GRP_SRC_EXTERN,
			/* reg		*/	DSI_GSC_RAR,
			/* mask		*/	0xF0,
			/* value	*/	0x40
		},
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_CH_GRP_1_SRC,
			/* arg		*/	DSI_CH_GRP_SRC_DIR_EXTERN,
			/* reg		*/	DSI_GSC_RAR,
			/* mask		*/	0xF0,
			/* value	*/	0x50
		},
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_CH_GRP_1_SRC,
			/* arg		*/	DSI_CH_GRP_SRC_DISABLE,
			/* reg		*/	DSI_GSC_RAR,
			/* mask		*/	0xF0,
			/* value	*/	0x60
		},

		{ SERVICE_END_LIST }
	};

	static const service_data_t	list_alt[]	=
	{
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_CH_GRP_1_SRC,
			/* arg		*/	DSI_CH_GRP_SRC_ENABLE,
			/* reg		*/	DSI_GSC_RAR,
			/* mask		*/	0xF0,
			/* value	*/	0x50
		},
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_CH_GRP_1_SRC,
			/* arg		*/	DSI_CH_GRP_SRC_DISABLE,
			/* reg		*/	DSI_GSC_RAR,
			/* mask		*/	0xF0,
			/* value	*/	0x60
		},

		{ SERVICE_END_LIST }
	};

	int errs	= 0;

	errs	+= dsi_initialize(fd, -1, 0);

	if (rar0)
		errs	+= service_ioctl_set_reg_list(fd, list_alt);
	else if (gens == 1)
		errs	+= service_ioctl_set_reg_list(fd, list_a);
	else
		errs	+= service_ioctl_reg_get_list(fd, list_ab);

	return(errs);
}



//*****************************************************************************
static int _function_test(int fd)
{
	// TBD
	return(0);
}



/******************************************************************************
*
*	Function:	ch_grp_1_src_test
*
*	Purpose:
*
*		Perform a test of the IOCTL service DSI_IOCTL_CH_GRP_1_SRC.
*
*	Arguments:
*
*		fd		The handle for the board to access.
*
*	Returned:
*
*		>= 0	The number of errors encounterred.
*
******************************************************************************/

int ch_grp_1_src_test(int fd)
{
	int		errs	= 0;
	__s32	gens;
	__s32	groups;
	__s32	rar0;

	gsc_label("DSI_IOCTL_CH_GRP_1_SRC");
	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_CHANNEL_GPS, &groups);
	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_RATE_GEN_QTY, &gens);
	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_CH_GRP_0_RAR, &rar0);

	if (errs)
	{
	}
	else if (groups < 2)
	{
		printf("SKIPPED  (Not supported on this board.)\n");
	}
	else
	{
		errs	+= _service_test(fd, gens, rar0);
		errs	+= _function_test(fd);

		if (errs == 0)
			printf("PASS\n");
	}

	return(errs);
}


