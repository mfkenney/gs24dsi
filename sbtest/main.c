// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.0.1.0_GSC_DN/sbtest/main.c $
// $Rev: 16259 $
// $Date: 2012-05-10 15:14:07 -0500 (Thu, 10 May 2012) $

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "main.h"



// variables	***************************************************************

static	int		_continuous		= 0;
static	int		_ignore_errors	= 0;
static	int		_index			= 0;
static	long	_minute_limit	= 0;
static	int		_test_limit		= -1;



/******************************************************************************
*
*	Function:	_parse_args
*
*	Purpose:
*
*		Parse the command line arguments.
*
*	Arguments:
*
*		argc	The number of command line arguments given.
*
*		argv	The list of command line arguments given.
*
*	Returned:
*
*		>= 0	The number of errors encounterred.
*
******************************************************************************/

static int _parse_args(int argc, char** argv)
{
	char	c;
	int		errs	= 0;
	int		i;
	int		j;
	int		k;

	printf("USAGE: sbtest <-c> <-C> <-m#> <-n#> <index>\n");
	printf("   -c     Continue testing until an error occurs.\n");
	printf("   -C     Continue testing even if errors occur.\n");
	printf("   -m#    Run for at most # minutes (a decimal number).\n");
	printf("   -n#    Repeat test at most # times (a decimal number).\n");
	printf("   index  The index of the board to access.\n");
	printf(" NOTE: Hit Ctrl-C to abort continuous testing.\n");

	for (i = 1; i < argc; i++)
	{
		if (strcmp(argv[i], "-c") == 0)
		{
			_continuous		= 1;
			_ignore_errors	= 0;
			continue;
		}

		if (strcmp(argv[i], "-C") == 0)
		{
			_continuous		= 1;
			_ignore_errors	= 1;
			continue;
		}

		if ((argv[i][0] == '-') && (argv[i][1] == 'm') && (argv[i][2]))
		{
			j	= sscanf(&argv[i][2], "%d%c", &k, &c);

			if ((j == 1) && (k > 0))
			{
				_minute_limit	= k;
				continue;
			}

			errs	= 1;
			printf("ERROR: invalid argument: %s\n", argv[i]);
			break;
		}

		if ((argv[i][0] == '-') && (argv[i][1] == 'n') && (argv[i][2]))
		{
			j	= sscanf(&argv[i][2], "%d%c", &k, &c);

			if ((j == 1) && (k > 0))
			{
				_test_limit	= k;
				continue;
			}

			errs	= 1;
			printf("ERROR: invalid argument: %s\n", argv[i]);
			break;
		}

		j	= sscanf(argv[i], "%d%c", &k, &c);

		if ((j == 1) && (k >= 0))
		{
			_index	= k;
			continue;
		}
		else
		{
			errs	= 1;
			printf("ERROR: invalid board selection: %s\n", argv[i]);
			break;
		}
	}

	return(errs);
}



/******************************************************************************
*
*	Function:	_perform_tests
*
*	Purpose:
*
*		Perform the appropriate testing.
*
*	Arguments:
*
*		fd		The handle for the board to access.
*
*	Returned:
*
*		>= 0	The number of errors encounterred.
*
******************************************************************************/

static int _perform_tests(int fd)
{
	int			errs	= 0;
	const char*	psz;
	struct tm*	stm;
	time_t		tt;

	time(&tt);
	stm	= localtime(&tt);
	psz	= asctime(stm);
	gsc_label("Performing Test");
	printf("%s", psz);

	errs	+= gsc_id_driver(DSI_BASE_NAME);
	errs	+= dsi_id_board(fd, -1, NULL);

	errs	+= reg_read_test(fd);				// DSI_IOCTL_REG_READ
	errs	+= reg_write_test(fd);				// DSI_IOCTL_REG_WRITE
	errs	+= reg_mod_test(fd);				// DSI_IOCTL_REG_MOD
	errs	+= query_test(fd);					// DSI_IOCTL_QUERY
	errs	+= initialize_test(fd);				// DSI_IOCTL_INITIALIZE
	errs	+= auto_calibrate_test(fd);			// DSI_IOCTL_AUTO_CALIBRATE

	errs	+= rx_io_abort_test(fd);			// DSI_IOCTL_RX_IO_ABORT
	errs	+= rx_io_mode_test(fd);				// DSI_IOCTL_RX_IO_MODE
	errs	+= rx_io_overflow_test(fd);			// DSI_IOCTL_RX_IO_OVERFLOW
	errs	+= rx_io_timeout_test(fd);			// DSI_IOCTL_RX_IO_TIMEOUT
	errs	+= rx_io_underflow_test(fd);		// DSI_IOCTL_RX_IO_UNDERFLOW

	errs	+= ain_buf_clear_test(fd);			// DSI_IOCTL_AIN_BUF_CLEAR
	errs	+= ain_buf_input_test(fd);			// DSI_IOCTL_AIN_BUF_INPUT
	errs	+= ain_buf_overflow_test(fd);		// DSI_IOCTL_AIN_BUF_OVERFLOW
	errs	+= ain_buf_thresh_test(fd);			// DSI_IOCTL_AIN_BUF_THRESH
	errs	+= ain_buf_underflow_test(fd);		// DSI_IOCTL_AIN_BUF_UNDERFLOW

	errs	+= ain_filter_test(fd);				// DSI_IOCTL_AIN_FILTER
	errs	+= ain_filter_sel_test(fd);			// DSI_IOCTL_AIN_FILTER_SEL
	errs	+= ain_filter_00_03_test(fd);		// DSI_IOCTL_AIN_FILTER_00_03
	errs	+= ain_filter_04_07_test(fd);		// DSI_IOCTL_AIN_FILTER_04_07
	errs	+= ain_filter_08_11_test(fd);		// DSI_IOCTL_AIN_FILTER_08_11
	errs	+= ain_filter_12_15_test(fd);		// DSI_IOCTL_AIN_FILTER_12_15
	errs	+= ain_filter_16_19_test(fd);		// DSI_IOCTL_AIN_FILTER_16_19
	errs	+= ain_filter_20_23_test(fd);		// DSI_IOCTL_AIN_FILTER_20_23
	errs	+= ain_filter_24_27_test(fd);		// DSI_IOCTL_AIN_FILTER_24_27
	errs	+= ain_filter_28_31_test(fd);		// DSI_IOCTL_AIN_FILTER_28_31

	errs	+= ain_mode_test(fd);				// DSI_IOCTL_AIN_MODE

	errs	+= ain_range_test(fd);				// DSI_IOCTL_AIN_RANGE
	errs	+= ain_range_sel_test(fd);			// DSI_IOCTL_AIN_RANGE_SEL
	errs	+= ain_range_00_03_test(fd);		// DSI_IOCTL_AIN_RANGE_00_03
	errs	+= ain_range_04_07_test(fd);		// DSI_IOCTL_AIN_RANGE_04_07
	errs	+= ain_range_08_11_test(fd);		// DSI_IOCTL_AIN_RANGE_08_11
	errs	+= ain_range_12_15_test(fd);		// DSI_IOCTL_AIN_RANGE_12_15
	errs	+= ain_range_16_19_test(fd);		// DSI_IOCTL_AIN_RANGE_16_19
	errs	+= ain_range_20_23_test(fd);		// DSI_IOCTL_AIN_RANGE_20_23
	errs	+= ain_range_24_27_test(fd);		// DSI_IOCTL_AIN_RANGE_24_27
	errs	+= ain_range_28_31_test(fd);		// DSI_IOCTL_AIN_RANGE_28_31

	errs	+= channel_order_test(fd);			// DSI_IOCTL_CHANNEL_ORDER
	errs	+= data_format_test(fd);			// DSI_IOCTL_DATA_FORMAT
	errs	+= data_width_test(fd);				// DSI_IOCTL_DATA_WIDTH
	errs	+= init_mode_test(fd);				// DSI_IOCTL_INIT_MODE
	errs	+= sw_sync_test(fd);				// DSI_IOCTL_SW_SYNC
	errs	+= sw_sync_mode_test(fd);			// DSI_IOCTL_SW_SYNC_MODE
	errs	+= pll_ref_freq_hz_test(fd);		// DSI_IOCTL_PLL_REF_FREQ_HZ

	errs	+= rate_gen_a_nvco_test(fd);		// DSI_IOCTL_RATE_GEN_A_NVCO
	errs	+= rate_gen_b_nvco_test(fd);		// DSI_IOCTL_RATE_GEN_B_NVCO
	errs	+= rate_gen_a_nref_test(fd);		// DSI_IOCTL_RATE_GEN_A_NREF
	errs	+= rate_gen_b_nref_test(fd);		// DSI_IOCTL_RATE_GEN_B_NREF
	errs	+= rate_gen_a_nrate_test(fd);		// DSI_IOCTL_RATE_GEN_A_NRATE
	errs	+= rate_gen_b_nrate_test(fd);		// DSI_IOCTL_RATE_GEN_B_NRATE
	errs	+= rate_div_0_ndiv_test(fd);		// DSI_IOCTL_RATE_DIV_0_NDIV
	errs	+= rate_div_1_ndiv_test(fd);		// DSI_IOCTL_RATE_DIV_1_NDIV

	errs	+= ch_grp_0_src_test(fd);			// DSI_IOCTL_CH_GRP_0_SRC
	errs	+= ch_grp_1_src_test(fd);			// DSI_IOCTL_CH_GRP_1_SRC
	errs	+= ch_grp_2_src_test(fd);			// DSI_IOCTL_CH_GRP_2_SRC
	errs	+= ch_grp_3_src_test(fd);			// DSI_IOCTL_CH_GRP_3_SRC

	errs	+= ext_clk_src_test(fd);			// DSI_IOCTL_EXT_CLK_SRC
	errs	+= ext_trig_test(fd);				// DSI_IOCTL_EXT_TRIG
	errs	+= thres_flag_cbl_test(fd);			// DSI_IOCTL_THRES_FLAG_CBL
	errs	+= xcvr_type_test(fd);				// DSI_IOCTL_XCVR_TYPE

	errs	+= gps_enable_test(fd);				// DSI_IOCTL_GPS_ENABLE
	errs	+= gps_locked_test(fd);				// DSI_IOCTL_GPS_LOCKED
	errs	+= gps_polarity_test(fd);			// DSI_IOCTL_GPS_POLARITY
	errs	+= gps_rate_locked_test(fd);		// DSI_IOCTL_GPS_RATE_LOCKED
	errs	+= gps_target_rate_test(fd);		// DSI_IOCTL_GPS_TARGET_RATE
	errs	+= gps_tolerance_test(fd);			// DSI_IOCTL_GPS_TOLERANCE

	errs	+= ain_coupling_test(fd);			// DSI_IOCTL_AIN_COUPLING
	errs	+= adc_rate_out_test(fd);			// DSI_IOCTL_ADC_RATE_OUT
	errs	+= rx_io_abort_test(fd);			// DSI_IOCTL_RX_IO_ABORT
	errs	+= irq_sel_test(fd);				// DSI_IOCTL_IRQ_SEL

	errs	+= wait_event_test(fd);				// DSI_IOCTL_WAIT_EVENT
	errs	+= wait_cancel_test(fd);			// DSI_IOCTL_WAIT_CANCEL
	errs	+= wait_status_test(fd);			// DSI_IOCTL_WAIT_STATUS

	errs	+= voltage_test(fd);

	return(errs);
}



/******************************************************************************
*
*	Function:	main
*
*	Purpose:
*
*		Control the overall flow of the application.
*
*	Arguments:
*
*		argc			The number of command line arguments.
*
*		argv			The list of command line arguments.
*
*	Returned:
*
*		EXIT_SUCCESS	We tested a device.
*		EXIT_FAILURE	We didn't test a device.
*
******************************************************************************/

int main(int argc, char** argv)
{
	int		errs;
	time_t	exec		= time(NULL);
	long	failures	= 0;
	int		fd			= 0;
	long	hours;
	long	mins;
	time_t	now;
	long	passes		= 0;
	int		qty;
	int		ret			= EXIT_FAILURE;
	long	secs;
	time_t	t_limit;
	time_t	test;
	long	tests		= 0;

	gsc_label_init(27);

	for (;;)
	{
		test	= time(NULL);
		printf("sbtest - Single Board Test Application (Version %s)\n", VERSION);
		errs	= _parse_args(argc, argv);

		if (errs)
			break;

		gsc_id_host();
		t_limit	= exec + (_minute_limit * 60);
		qty		= gsc_count_boards(DSI_BASE_NAME);
		errs	= gsc_select_1_board(qty, &_index);

		if ((qty <= 0) || (errs))
			break;

		gsc_label("Testing Board Index");
		printf("%d\n", _index);
		fd	= gsc_dev_open(_index, DSI_BASE_NAME);

		if (fd == -1)
		{
			errs	= 1;
			printf(	"ERROR: Unable to access device %d.", _index);
		}

		if (errs == 0)
		{
			ret		= EXIT_SUCCESS;
			errs	= _perform_tests(fd);
		}

		gsc_dev_close(_index, fd);

		now	= time(NULL);
		tests++;

		if (errs)
		{
			failures++;
			printf(	"\nRESULTS: FAIL <---  (%d error%s)",
					errs,
					(errs == 1) ? "" : "s");
		}
		else
		{
			passes++;
			printf("\nRESULTS: PASS");
		}

		secs	= now - test;
		hours	= secs / 3600;
		secs	= secs % 3600;
		mins	= secs / 60;
		secs	= secs % 60;
		printf(" (duration %ld:%ld:%02ld)\n", hours, mins, secs);

		secs	= now - exec;
		hours	= secs / 3600;
		secs	= secs % 3600;
		mins	= secs / 60;
		secs	= secs % 60;
		printf(	"SUMMARY: tests %ld, pass %ld, fail %ld"
				" (duration %ld:%ld:%02ld)\n\n",
				tests,
				passes,
				failures,
				hours,
				mins,
				secs);

		if ((_test_limit > 0) && (_test_limit <= tests))
			break;

		if (_continuous == 0)
			break;

		if ((_ignore_errors == 0) && (errs))
			break;

		if ((_minute_limit) && (now >= t_limit))
			break;
	}

	return(ret);
}



