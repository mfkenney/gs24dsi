// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.0.1.0_GSC_DN/sbtest/main.h $
// $Rev: 17643 $
// $Date: 2012-08-20 17:06:51 -0500 (Mon, 20 Aug 2012) $

#ifndef __MAIN_H__
#define __MAIN_H__

#include "24dsi.h"
#include "24dsi_dsl.h"
#include "24dsi_utils.h"
#include "gsc_utils.h"



// #defines	*******************************************************************

#define	VERSION				"1.11"
// 1.11	Added a "verbose" argument to the utility services.
// 1.10	Updated the parameter list to gsc_id_driver().
// 1.9	Changed PCI Device ID Reg read expectations. Could be 0x9080 or 0x9056.
// 1.8	Split the utility code into two libraries: common and device specific.
//		Updated and fixed a few tests.
// 1.7	Removed a compiler warning under Fedora 15.
// 1.6	Corrected display output text in rx_io_abort.c.
//		Updated test code per changes in the set of IOCTL services.
// 1.5	Updated the driver for the current common code base.
//		Added Rx I/O Abort support.
//		Added WAIT services support.
//		Updated to work with the driver's explicit local interrupt support.
//		Updated test source to follow now common format.
//		Improved testing of some services.
// 1.4	Modified Ain Buf Overflow test to disable input before final test.
// 1.3	Fixed some tests for the 24DSI32 boards.
// 1.2	Updated some PLX register names.
//		Updated "not supported" messages to a common format.
// 1.1	Added support for the 24DSI6LN.
// 1.0	initial release.

#define	ARRAY_ELEMENTS(a)	(sizeof((a))/sizeof((a)[0]))



// data types	***************************************************************

typedef enum
{
	SERVICE_END_LIST,	// Ends list of service_data_t structures.
	SERVICE_NORMAL,
	SERVICE_REG_MOD,
	SERVICE_REG_READ,
	SERVICE_REG_SHOW,
	SERVICE_REG_TEST,
	SERVICE_SLEEP,
	SERVICE_IOCTL_GET,
	SERVICE_IOCTL_SET
} service_t;

typedef struct
{
	service_t		service;
	unsigned long	cmd;	// IOCTL code
	unsigned long	arg;	// The IOCTL data argument.
	__u32			reg;	// The register to access. Use -1 to ignore.
	__u32			mask;	// The register bits of interest.
	__u32			value;	// The value expected for the bits of interest.
} service_data_t;



// prototypes	***************************************************************

int	adc_rate_out_test(int fd);
int	ain_buf_clear_test(int fd);
int	ain_buf_input_test(int fd);
int	ain_buf_overflow_test(int fd);
int	ain_buf_thresh_test(int fd);
int	ain_buf_underflow_test(int fd);
int	ain_coupling_test(int fd);
int	ain_filter_00_03_test(int fd);
int	ain_filter_04_07_test(int fd);
int	ain_filter_08_11_test(int fd);
int	ain_filter_12_15_test(int fd);
int	ain_filter_16_19_test(int fd);
int	ain_filter_20_23_test(int fd);
int	ain_filter_24_27_test(int fd);
int	ain_filter_28_31_test(int fd);
int	ain_filter_sel_test(int fd);
int	ain_filter_test(int fd);
int	ain_mode_test(int fd);
int	ain_range_sel_test(int fd);
int	ain_range_test(int fd);
int	ain_range_00_03_test(int fd);
int	ain_range_04_07_test(int fd);
int	ain_range_08_11_test(int fd);
int	ain_range_12_15_test(int fd);
int	ain_range_16_19_test(int fd);
int	ain_range_20_23_test(int fd);
int	ain_range_24_27_test(int fd);
int	ain_range_28_31_test(int fd);
int	auto_calibrate_test(int fd);

int	ch_grp_0_src_test(int fd);
int	ch_grp_1_src_test(int fd);
int	ch_grp_2_src_test(int fd);
int	ch_grp_3_src_test(int fd);
int	channel_order_test(int fd);

int	data_format_test(int fd);
int	data_width_test(int fd);

int	ext_clk_src_test(int fd);
int	ext_trig_test(int fd);

int	gps_enable_test(int fd);
int	gps_locked_test(int fd);
int	gps_polarity_test(int fd);
int	gps_rate_locked_test(int fd);
int	gps_target_rate_test(int fd);
int	gps_tolerance_test(int fd);

int	init_mode_test(int fd);
int	initialize_test(int fd);
int	irq_sel_test(int fd);

int	pll_ref_freq_hz_test(int fd);

int	query_test(int fd);

int	rate_div_0_ndiv_test(int fd);
int	rate_div_1_ndiv_test(int fd);
int	rate_gen_a_nrate_test(int fd);
int	rate_gen_a_nref_test(int fd);
int	rate_gen_a_nvco_test(int fd);
int	rate_gen_b_nrate_test(int fd);
int	rate_gen_b_nref_test(int fd);
int	rate_gen_b_nvco_test(int fd);
int	reg_mod_test(int fd);
int	reg_read_test(int fd);
int	reg_write_test(int fd);
int	rx_io_abort_test(int fd);
int	rx_io_mode_test(int fd);
int	rx_io_overflow_test(int fd);
int	rx_io_timeout_test(int fd);
int	rx_io_underflow_test(int fd);

int	service_ioctl_reg_get_list(int fd, const service_data_t* list);
int	service_ioctl_set_get_list(int fd, const service_data_t* list);
int	service_ioctl_set_reg_list(int fd, const service_data_t* list);
int	sw_sync_mode_test(int fd);
int	sw_sync_test(int fd);

int	thres_flag_cbl_test(int fd);

int	voltage_test(int fd);

int	wait_cancel_test(int fd);
int	wait_event_test(int fd);
int	wait_status_test(int fd);

int	wait_cancel_test(int fd);		// DSI_IOCTL_WAIT_CANCEL
int	wait_event_test(int fd);		// DSI_IOCTL_WAIT_EVENT
int	wait_status_test(int fd);		// DSI_IOCTL_WAIT_STATUS

int	xcvr_type_test(int fd);



#endif
