// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.0.1.0_GSC_DN/sbtest/rate_gen_a_nref.c $
// $Rev: 17643 $
// $Date: 2012-08-20 17:06:51 -0500 (Mon, 20 Aug 2012) $

#include <stdio.h>

#include "main.h"



//*****************************************************************************
static int _service_subtest(
	int		fd,
	__u32	reg,
	int		shift,
	__s32	min,
	__s32	max,
	__s32	nref)
{
	#undef	MASK
	#define	MASK	(0x3FF << SHIFT)
	#undef	SHIFT
	#define	SHIFT	shift

	service_data_t	list[]	=
	{
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_RATE_GEN_A_NREF,
			/* arg		*/	min,
			/* reg		*/	reg,
			/* mask		*/	MASK,
			/* value	*/	min << shift
		},
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_RATE_GEN_A_NREF,
			/* arg		*/	nref,
			/* reg		*/	reg,
			/* mask		*/	MASK,
			/* value	*/	nref << shift
		},
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_RATE_GEN_A_NREF,
			/* arg		*/	max,
			/* reg		*/	reg,
			/* mask		*/	MASK,
			/* value	*/	max << shift
		},

		{ SERVICE_END_LIST }
	};

	int	errs	= 0;

	errs	+= service_ioctl_set_reg_list(fd, list);
	errs	+= service_ioctl_reg_get_list(fd, list);

	return(errs);
}



//*****************************************************************************
static int _service_test(int fd, __u32 reg, int shift, __s32 min, __s32 max)
{
	int errs	= 0;

	errs	+= dsi_initialize(fd, -1, 0);

	errs	+= _service_subtest(fd, reg, shift, min, max, min + 1);
	errs	+= _service_subtest(fd, reg, shift, min, max, (min + max) / 2);
	errs	+= _service_subtest(fd, reg, shift, min, max, max - 1);

	return(errs);
}



//*****************************************************************************
static int _function_test(int fd)
{
	// TBD
	return(0);
}



/******************************************************************************
*
*	Function:	rate_gen_a_nref_test
*
*	Purpose:
*
*		Perform a test of the IOCTL service DSI_IOCTL_RATE_GEN_A_NREF.
*
*	Arguments:
*
*		fd		The handle for the board to access.
*
*	Returned:
*
*		>= 0	The number of errors encounterred.
*
******************************************************************************/

int rate_gen_a_nref_test(int fd)
{
	int		errs	= 0;
	__s32	qty		= 0;
	__s32	max;
	__s32	min;
	__s32	pll		= 0;
	__s32	rcabr;
	__u32	reg;
	int		shift;

	gsc_label("DSI_IOCTL_RATE_GEN_A_NREF");
	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_NREF_MAX, &max);
	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_NREF_MIN, &min);
	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_PLL_PRESENT, &pll);
	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_RATE_GEN_QTY, &qty);
	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_RCAR_RCBR, &rcabr);

	if (errs)
	{
	}
	else if ((pll == 0) || (qty < 1))
	{
		printf("SKIPPED  (Not supported on this board.)\n");
	}
	else
	{
		reg		= rcabr ? DSI_GSC_RCAR : DSI_GSC_NREFCR;
		shift	= rcabr ? 16 : 0;
		errs	+= _service_test(fd, reg, shift, min, max);
		errs	+= _function_test(fd);

		if (errs == 0)
			printf("PASS\n");
	}

	return(errs);
}


