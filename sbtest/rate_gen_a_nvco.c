// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.0.1.0_GSC_DN/sbtest/rate_gen_a_nvco.c $
// $Rev: 17643 $
// $Date: 2012-08-20 17:06:51 -0500 (Mon, 20 Aug 2012) $

#include <stdio.h>

#include "main.h"



//*****************************************************************************
static int _service_subtest(
	int		fd,
	__u32	reg,
	__s32	min,
	__s32	max,
	__s32	nvco)
{
	service_data_t	list[]	=
	{
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_RATE_GEN_A_NVCO,
			/* arg		*/	min,
			/* reg		*/	reg,
			/* mask		*/	0x3FF,
			/* value	*/	min
		},
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_RATE_GEN_A_NVCO,
			/* arg		*/	nvco,
			/* reg		*/	reg,
			/* mask		*/	0x3FF,
			/* value	*/	nvco
		},
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_RATE_GEN_A_NVCO,
			/* arg		*/	max,
			/* reg		*/	reg,
			/* mask		*/	0x3FF,
			/* value	*/	max
		},

		{ SERVICE_END_LIST }
	};

	int	errs	= 0;

	errs	+= service_ioctl_set_reg_list(fd, list);
	errs	+= service_ioctl_reg_get_list(fd, list);

	return(errs);
}



//*****************************************************************************
static int _service_test(int fd, __u32 reg, __s32 min, __s32 max)
{
	int errs	= 0;

	errs	+= dsi_initialize(fd, -1, 0);

	errs	+= _service_subtest(fd, reg, min, max, min + 1);
	errs	+= _service_subtest(fd, reg, min, max, max - 1);
	errs	+= _service_subtest(fd, reg, min, max, (min + max) / 2);

	return(errs);
}



//*****************************************************************************
static int _function_test(int fd)
{
	// TBD
	return(0);
}



/******************************************************************************
*
*	Function:	rate_gen_a_nvco_test
*
*	Purpose:
*
*		Perform a test of the IOCTL service DSI_IOCTL_RATE_GEN_A_NVCO.
*
*	Arguments:
*
*		fd		The handle for the board to access.
*
*	Returned:
*
*		>= 0	The number of errors encounterred.
*
******************************************************************************/

int rate_gen_a_nvco_test(int fd)
{
	int		errs	= 0;
	__s32	max;
	__s32	min;
	__s32	pll		= 0;
	__s32	qty		= 0;
	__s32	rcabr;
	__u32	reg;

	gsc_label("DSI_IOCTL_RATE_GEN_A_NVCO");
	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_NVCO_MAX, &max);
	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_NVCO_MIN, &min);
	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_PLL_PRESENT, &pll);
	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_RATE_GEN_QTY, &qty);
	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_RCAR_RCBR, &rcabr);

	if (errs)
	{
	}
	else if ((pll == 0) || (qty < 1))
	{
		printf("SKIPPED  (Not supported on this board.)\n");
	}
	else
	{
		reg		= rcabr ? DSI_GSC_RCAR : DSI_GSC_NVCOCR;
		errs	+= _service_test(fd, reg, min, max);
		errs	+= _function_test(fd);

		if (errs == 0)
			printf("PASS\n");
	}

	return(errs);
}


