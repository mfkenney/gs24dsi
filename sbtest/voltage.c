// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.0.1.0_GSC_DN/sbtest/voltage.c $
// $Rev: 17643 $
// $Date: 2012-08-20 17:06:51 -0500 (Mon, 20 Aug 2012) $

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "main.h"



// variables	***************************************************************

static __u32	_data[4096];



/******************************************************************************
*
*	Function:	_v_test
*
*	Purpose:
*
*		Perform ADC voltage accuracy tests.
*
*	Arguments:
*
*		fd			The handle for the board to access.
*
*		title		The title to use for this test.
*
*		ain_mode	The analog input mode to select for the test.
*
*		ain_range	The input range to select for the test.
*
*		range		The positive full scale voltage range.
*
*		acc			The expected accuracy.
*
*		frac		The fraction of the full range to test for.
*
*	Returned:
*
*		>= 0		The number of errors encounterred.
*
******************************************************************************/

static int _v_test(
	int			fd,
	const char*	title,
	__s32		ain_mode,
	__s32		ain_range,
	float		range,
	float		acc,
	float		frac)
{
	service_data_t	list_1[]	=
	{
		{
			/* service	*/	SERVICE_IOCTL_SET,
			/* cmd		*/	DSI_IOCTL_INITIALIZE,
			/* arg		*/	0,
			/* reg		*/	-1,
			/* mask		*/	0,
			/* value	*/	0
		},
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_RX_IO_MODE,
			/* arg		*/	GSC_IO_MODE_PIO,
			/* reg		*/	-1,
			/* mask		*/	0,
			/* value	*/	0
		},
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_RX_IO_OVERFLOW,
			/* arg		*/	DSI_IO_UNDERFLOW_IGNORE,
			/* reg		*/	-1,
			/* mask		*/	0,
			/* value	*/	0
		},
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_RX_IO_UNDERFLOW,
			/* arg		*/	DSI_IO_OVERFLOW_IGNORE,
			/* reg		*/	-1,
			/* mask		*/	0,
			/* value	*/	0
		},
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_RX_IO_TIMEOUT,
			/* arg		*/	10,
			/* reg		*/	-1,
			/* mask		*/	0,
			/* value	*/	0
		},
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_DATA_FORMAT,
			/* arg		*/	DSI_DATA_FORMAT_OFF_BIN,
			/* reg		*/	-1,
			/* mask		*/	0,
			/* value	*/	0
		},
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_AIN_MODE,
			/* arg		*/	ain_mode,
			/* reg		*/	-1,
			/* mask		*/	0,
			/* value	*/	0
		},
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_AIN_RANGE,
			/* arg		*/	ain_range,
			/* reg		*/	-1,
			/* mask		*/	0,
			/* value	*/	0
		},
		{
			/* service	*/	SERVICE_SLEEP,
			/* cmd		*/	0,
			/* arg		*/	3,	// wait for test voltage settling
			/* reg		*/	-1,
			/* mask		*/	0,
			/* value	*/	0
		},
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_AUTO_CALIBRATE,
			/* arg		*/	0,
			/* reg		*/	-1,
			/* mask		*/	0,
			/* value	*/	0
		},
		{
			/* service	*/	SERVICE_SLEEP,
			/* cmd		*/	0,
			/* arg		*/	1,
			/* reg		*/	-1,
			/* mask		*/	0,
			/* value	*/	0
		},
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_AIN_BUF_CLEAR,
			/* arg		*/	0,
			/* reg		*/	-1,
			/* mask		*/	0,
			/* value	*/	0
		},
		{
			/* service	*/	SERVICE_SLEEP,
			/* cmd		*/	0,
			/* arg		*/	1,
			/* reg		*/	-1,
			/* mask		*/	0,
			/* value	*/	0
		},

		{ SERVICE_END_LIST }
	};

	static const service_data_t	list_2[]	=
	{
		{
			/* service	*/	SERVICE_IOCTL_SET,
			/* cmd		*/	DSI_IOCTL_INITIALIZE,
			/* arg		*/	0,
			/* reg		*/	-1,
			/* mask		*/	0,
			/* value	*/	0
		},

		{ SERVICE_END_LIST },
	};

	char	buf[64];
	int		bytes;
	int		chan;
	__s32	d_adc;
	int		errs	= 0;
	int		i;
	__s32	mode;
	float	v_adc;
	float	v_avg;
	int		v_count[32];	// 16 channels max
	float	v_delta;
	float	v_dif;
	float	v_lsb;
	float	v_sum[32];		// 16 channels max
	float	v_targ;

	gsc_label(title);
	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_D0_1_IN_MODE, &mode);

	if (errs)
	{
	}
	else if (mode)
	{
		errs	= service_ioctl_set_reg_list(fd, list_1);

		if (errs == 0)
		{
			bytes	= (int) sizeof(_data);
			i		= read(fd, _data, bytes);
			errs	= service_ioctl_reg_get_list(fd, list_2);

			if (errs)
			{
			}
			else if (i == -1)
			{
				errs	= 1;
				printf("FAIL <---  (read(), errno %d)\n", errno);
			}
			else if (i != bytes)
			{
				errs	= 1;
				printf(	"FAIL <---  (requested %d bytes, got only %d)\n",
						bytes,
						i);
			}
		}

		if (errs == 0)
			printf("\n");

		// Calculate the numbers we'll use for testing.
		v_lsb	= range / 32768;
		v_targ	= range * frac;
		v_dif	= acc + v_lsb;

		memset(&v_count, 0, sizeof(v_count));
		memset(&v_sum, 0, sizeof(v_sum));

		if (errs == 0)
		{
			// Go through the data and sum the content for averaging.

			for (i = 0; i < ARRAY_ELEMENTS(_data); i++)
			{
				chan		= (_data[i] & 0xF000000) >> 24;
				d_adc		= _data[i] & 0x3FFFF;
				v_adc		= range / 32768 * (d_adc - 0x8000);
				v_delta		= v_adc - (range * frac);
				v_sum[chan]	+= v_adc;
				v_count[chan]++;
			}

			gsc_label_level_inc();

			for (i = 0; i < 32; i++)
			{
				if (v_count[i] == 0)
					continue;

				sprintf(buf, "Channel %d", i);
				gsc_label(buf);
				v_avg	= v_sum[i] / v_count[i];
				v_delta	= v_targ - v_avg;

				if (v_delta < -v_dif)
				{
					errs	= 1;
					printf(	"FAIL <---  (low average: got %+.2f mv, min is %+.2f mv)\n",
							v_delta * 1000,
							-v_dif * 1000);
				}
				else if (v_delta > v_dif)
				{
					errs	= 1;
					printf(	"FAIL <---  (high average: got %+.2f mv, max is %+.2f mv)\n",
							v_delta * 1000,
							v_dif * 1000);
				}
				else
				{
					printf("PASS  (ave difference %+.2f mv)\n", v_delta * 1000);
				}
			}

			gsc_label_level_dec();
		}
	}
	else
	{
		printf("SKIPPED  (Not supported on this board.\n");
	}

	return(errs);
}



/******************************************************************************
*
*	Function:	voltage_test
*
*	Purpose:
*
*		Perform ADC voltage accuracy tests.
*
*	Arguments:
*
*		fd		The handle for the board to access.
*
*	Returned:
*
*		>= 0	The number of errors encounterred.
*
******************************************************************************/

int voltage_test(int fd)
{
	__u32	bcfgr;
	int		errs	= 0;
	__s32	fixed;
	__s32	low_power;
	int		test_25	= 0;
	int		test_5	= 0;
	int		test_10	= 0;
	__s32	type;
	float	v25;
	float	v5;
	float	v10;

	gsc_label("ADC Voltage Tests");
	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_DEVICE_TYPE, &type);
	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_LOW_POWER, &low_power);
	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_D17_18_RANGE, &fixed);
	errs	+= dsi_reg_read(fd, DSI_GSC_BCFGR, &bcfgr);

	if (low_power)
	{
		test_25	= 1;
		test_5	= 1;
	}
	else if (fixed)
	{
		switch (bcfgr & 0x60000)
		{
			default:		// just in case
			case 0x60000:	// reserved
			case 0x00000:	test_10	= 1;	break;
			case 0x20000:	test_5	= 1;	break;
			case 0x40000:	test_25	= 1;	break;
		}
	}
	else
	{
		test_25	= 1;
		test_5	= 1;
		test_10	= 1;
	}

	if (errs == 0)
	{
		printf("\n");

		if (type == GSC_DEV_TYPE_24DSI12)
		{
			// 24DSI12: defaults: 16-bit resolution, 10K S/S
			// At the default sample rate the full scale accuracy is .3%.
			v10	= 10.0 * 0.003;
			v5	=  5.0 * 0.003;
			v25	=  2.5 * 0.003;
		}
		else
		{
			// 24DSI32: defaults: 16-bit resolution, 12.8K S/S
			// At the default sample rate the full scale accuracy is .5%.
			v10	= 10.0 * 0.005;
			v5	=  5.0 * 0.005;
			v25	=  2.5 * 0.005;
		}

		gsc_label_level_inc();

		if (test_10)
			errs	+= _v_test(fd, "Vref Test @ +-10v",		DSI_AIN_MODE_VREF, DSI_AIN_RANGE_10V,	10.0,	v10,	0.99);

		if (test_5)
			errs	+= _v_test(fd, "Vref Test @ +-5v",		DSI_AIN_MODE_VREF, DSI_AIN_RANGE_5V,	 5.0,	v5,		0.99);

		if (test_25)
			errs	+= _v_test(fd, "Vref Test @ +-2.5v",	DSI_AIN_MODE_VREF, DSI_AIN_RANGE_2_5V,	 2.5,	v25,	0.99);

		if (test_10)
			errs	+= _v_test(fd, "Zero Test @ +-10v",		DSI_AIN_MODE_ZERO, DSI_AIN_RANGE_10V,	10.0,	.0005,	0.00);

		if (test_5)
			errs	+= _v_test(fd, "Zero Test @ +-5v",		DSI_AIN_MODE_ZERO, DSI_AIN_RANGE_5V,	 5.0,	.0003,	0.00);

		if (test_25)
			errs	+= _v_test(fd, "Zero Test @ +-2.5v",	DSI_AIN_MODE_ZERO, DSI_AIN_RANGE_2_5V,	 2.5,	.0001,	0.00);

		gsc_label_level_dec();
	}

	return(errs);
}


