// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.0.1.0_GSC_DN/sbtest/gps_locked.c $
// $Rev: 17643 $
// $Date: 2012-08-20 17:06:51 -0500 (Mon, 20 Aug 2012) $

#include <stdio.h>

#include "main.h"
#include "24dsi_dsl.h"



//*****************************************************************************
static int _service_test(int fd)
{
	int		errs	= 0;
	__u32	gscr;
	__s32	locked;

	errs	+= dsi_initialize(fd, -1, 0);
	errs	+= dsi_dsl_ioctl(fd, DSI_IOCTL_GPS_LOCKED, &locked);
	errs	+= dsi_reg_read(fd, DSI_GSC_GSR, &gscr);

	if (errs)
	{
	}
	else if (locked == DSI_GPS_LOCKED_YES)
	{
		// GPS should not be locked.
		errs	= 1;
		printf("FAIL <---  (driver reports locked status)\n");
	}
	else if (locked != DSI_GPS_LOCKED_NO)
	{
		// There are only two options.
		errs	= 1;
		printf("FAIL <---  (invalid return value: %ld)\n", (long) locked);
	}
	else if (gscr & 0x1000000)
	{
		// The status should be UNLOCKED.
		errs	= 1;
		printf(	"FAIL <---  (GSCR indicates locked status: 0x%lX)\n", (long) gscr);
	}

	return(errs);
}



//*****************************************************************************
static int _function_test(int fd)
{
	// TBD
	return(0);
}



/******************************************************************************
*
*	Function:	gps_locked_test
*
*	Purpose:
*
*		Perform a test of the IOCTL service DSI_IOCTL_GPS_LOCKED.
*
*	Arguments:
*
*		fd		The handle for the board to access.
*
*	Returned:
*
*		>= 0	The number of errors encounterred.
*
******************************************************************************/

int gps_locked_test(int fd)
{
	int		errs;
	__s32	gps;

	gsc_label("DSI_IOCTL_GPS_LOCKED");
	errs	= dsi_query(fd, -1, 0, DSI_QUERY_GPS_PRESENT, &gps);

	if (errs)
	{
	}
	else if (gps == 0)
	{
		printf("SKIPPED  (Not supported on this board.)\n");
	}
	else
	{
		errs	+= _service_test(fd);
		errs	+= _function_test(fd);

		if (errs == 0)
			printf("PASS\n");
	}

	return(errs);
}


