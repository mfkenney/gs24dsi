// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.0.1.0_GSC_DN/sbtest/gps_target_rate.c $
// $Rev: 17643 $
// $Date: 2012-08-20 17:06:51 -0500 (Mon, 20 Aug 2012) $

#include <errno.h>
#include <stdio.h>
#include <unistd.h>

#include "main.h"
#include "24dsi_dsl.h"



//*****************************************************************************
static int _service_test(int fd)
{
	#define	MASK	0xFFFFF

	static const service_data_t	list[]	=
	{
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_GPS_TARGET_RATE,
			/* arg		*/	0x00000,
			/* reg		*/	DSI_GSC_GSR,
			/* mask		*/	MASK,
			/* value	*/	0x00000
		},
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_GPS_TARGET_RATE,
			/* arg		*/	0x0000F,
			/* reg		*/	DSI_GSC_GSR,
			/* mask		*/	MASK,
			/* value	*/	0x0000F
		},
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_GPS_TARGET_RATE,
			/* arg		*/	0x000F0,
			/* reg		*/	DSI_GSC_GSR,
			/* mask		*/	MASK,
			/* value	*/	0x000F0
		},
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_GPS_TARGET_RATE,
			/* arg		*/	0x00F00,
			/* reg		*/	DSI_GSC_GSR,
			/* mask		*/	MASK,
			/* value	*/	0x00F00
		},
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_GPS_TARGET_RATE,
			/* arg		*/	0x0F000,
			/* reg		*/	DSI_GSC_GSR,
			/* mask		*/	MASK,
			/* value	*/	0x0F000
		},
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_GPS_TARGET_RATE,
			/* arg		*/	0xF0000,
			/* reg		*/	DSI_GSC_GSR,
			/* mask		*/	MASK,
			/* value	*/	0xF0000
		},
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_GPS_TARGET_RATE,
			/* arg		*/	0x80001,
			/* reg		*/	DSI_GSC_GSR,
			/* mask		*/	MASK,
			/* value	*/	0x80001
		},
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_GPS_TARGET_RATE,
			/* arg		*/	0xFFFFF,
			/* reg		*/	DSI_GSC_GSR,
			/* mask		*/	MASK,
			/* value	*/	0xFFFFF
		},

		{ SERVICE_END_LIST }
	};

	int errs	= 0;

	errs	+= dsi_initialize(fd, -1, 0);

	errs	+= service_ioctl_set_get_list(fd, list);

	errs	+= service_ioctl_set_get_list(fd, list);

	return(errs);
}



//*****************************************************************************
static int _function_test(int fd)
{
	// TBD
	return(0);
}



/******************************************************************************
*
*	Function:	gps_target_rate_test
*
*	Purpose:
*
*		Perform a test of the IOCTL service DSI_IOCTL_GPS_TARGET_RATE.
*
*	Arguments:
*
*		fd		The handle for the board to access.
*
*	Returned:
*
*		>= 0	The number of errors encounterred.
*
******************************************************************************/

int gps_target_rate_test(int fd)
{
	int		errs;
	__s32	gps;

	gsc_label("DSI_IOCTL_GPS_TARGET_RATE");
	errs	= dsi_query(fd, -1, 0, DSI_QUERY_GPS_PRESENT, &gps);

	if (errs)
	{
	}
	else if (gps == 0)
	{
		printf("SKIPPED  (Not supported on this board.)\n");
	}
	else
	{
		errs	+= _service_test(fd);
		errs	+= _function_test(fd);

		if (errs == 0)
			printf("PASS\n");
	}

	return(errs);
}


