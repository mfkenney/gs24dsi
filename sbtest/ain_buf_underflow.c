// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.0.1.0_GSC_DN/sbtest/ain_buf_underflow.c $
// $Rev: 17643 $
// $Date: 2012-08-20 17:06:51 -0500 (Mon, 20 Aug 2012) $

#include <errno.h>
#include <stdio.h>
#include <unistd.h>

#include "main.h"
#include "24dsi_dsl.h"



//*****************************************************************************
static int _service_test(int fd)
{
	// There are no persistent bits to test with this service.
	return(0);
}



//*****************************************************************************
static int _function_test(int fd)
{
	#undef	MASK
	#define	MASK	0x2000000

	service_data_t	list_set[]	=
	{
		{
			/* service	*/	SERVICE_IOCTL_SET,
			/* cmd		*/	DSI_IOCTL_AIN_BUF_INPUT,
			/* arg		*/	DSI_AIN_BUF_INPUT_DISABLE,
			/* reg		*/	-1,
			/* mask		*/	0,
			/* value	*/	0
		},
		{
			/* service	*/	SERVICE_IOCTL_SET,
			/* cmd		*/	DSI_IOCTL_AIN_BUF_CLEAR,
			/* arg		*/	0,
			/* reg		*/	-1,
			/* mask		*/	0,
			/* value	*/	0
		},
		{
			/* service	*/	SERVICE_REG_READ,
			/* cmd		*/	-1,
			/* arg		*/	-1,
			/* reg		*/	DSI_GSC_IDBR,
			/* mask		*/	0,
			/* value	*/	0
		},
		{
			/* service	*/	SERVICE_REG_TEST,
			/* cmd		*/	-1,
			/* arg		*/	-1,
			/* reg		*/	DSI_GSC_IBCR,
			/* mask		*/	MASK,
			/* value	*/	MASK
		},

		{ SERVICE_END_LIST }
	};

	service_data_t	list_clear[]	=
	{
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_AIN_BUF_UNDERFLOW,
			/* arg		*/	DSI_AIN_BUF_UNDERFLOW_CLEAR,
			/* reg		*/	DSI_GSC_IBCR,
			/* mask		*/	MASK,
			/* value	*/	0
		},

		{ SERVICE_END_LIST }
	};

	int		errs	= 0;
	__s32	val;
	int		ret;

	errs	+= dsi_initialize(fd, -1, 0);

	for (;;)	// A convenience loop.
	{
		// Verify that there isn't an underflow already.
		val	= DSI_AIN_BUF_UNDERFLOW_TEST;
		ret	= dsi_dsl_ioctl(fd, DSI_IOCTL_AIN_BUF_UNDERFLOW, &val);

		if (ret == -1)
		{
			errs	= 1;
			printf("FAIL <---  (ioctl() error, errno %d)\n", errno);
			break;
		}
		else if (val)
		{
			errs	= 1;
			printf("FAIL <---  (underflow already occurred.)\n");
			break;
		}

		// Now induce an underflow.
		errs	= service_ioctl_reg_get_list(fd, list_set);

		if (errs)
			break;

		// Verify that there is an underflow already.
		val	= DSI_AIN_BUF_UNDERFLOW_TEST;
		ret	= dsi_dsl_ioctl(fd, DSI_IOCTL_AIN_BUF_UNDERFLOW, &val);

		if (ret == -1)
		{
			errs	= 1;
			printf("FAIL <---  (ioctl() error, errno %d)\n", errno);
			break;
		}
		else if (val == 0)
		{
			errs	= 1;
			printf("FAIL <---  (underflow not detected.)\n");
			break;
		}

		// Now clear the underflow.
		errs	= service_ioctl_set_reg_list(fd, list_clear);
		break;
	}

	return(errs);
}



/******************************************************************************
*
*	Function:	ain_buf_underflow_test
*
*	Purpose:
*
*		Perform a test of the IOCTL service DSI_IOCTL_AIN_BUF_UNDERFLOW.
*
*	Arguments:
*
*		fd		The handle for the board to access.
*
*	Returned:
*
*		>= 0	The number of errors encounterred.
*
******************************************************************************/

int ain_buf_underflow_test(int fd)
{
	int	errs	= 0;

	gsc_label("DSI_IOCTL_AIN_BUF_UNDERFLOW");
	errs	+= _service_test(fd);
	errs	+= _function_test(fd);

	if (errs == 0)
		printf("PASS\n");

	return(errs);
}


