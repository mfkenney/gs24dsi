// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.0.1.0_GSC_DN/sbtest/data_format.c $
// $Rev: 17643 $
// $Date: 2012-08-20 17:06:51 -0500 (Mon, 20 Aug 2012) $

#include <stdio.h>

#include "main.h"



//*****************************************************************************
static int _service_test(int fd)
{
	static const service_data_t	list[]	=
	{
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_DATA_FORMAT,
			/* arg		*/	DSI_DATA_FORMAT_2S_COMP,
			/* reg		*/	DSI_GSC_BCTLR,
			/* mask		*/	0x10,
			/* value	*/	0x00
		},
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_DATA_FORMAT,
			/* arg		*/	DSI_DATA_FORMAT_OFF_BIN,
			/* reg		*/	DSI_GSC_BCTLR,
			/* mask		*/	0x10,
			/* value	*/	0x10
		},

		{ SERVICE_END_LIST }
	};

	int errs	= 0;

	errs	+= dsi_initialize(fd, -1, 0);

	errs	+= service_ioctl_set_get_list(fd, list);

	errs	+= service_ioctl_set_get_list(fd, list);

	return(errs);
}



//*****************************************************************************
static int _function_test(int fd)
{
	// TBD
	return(0);
}



/******************************************************************************
*
*	Function:	data_format_test
*
*	Purpose:
*
*		Perform a test of the IOCTL service DSI_IOCTL_DATA_FORMAT.
*
*	Arguments:
*
*		fd		The handle for the board to access.
*
*	Returned:
*
*		>= 0	The number of errors encounterred.
*
******************************************************************************/

int data_format_test(int fd)
{
	int	errs	= 0;

	gsc_label("DSI_IOCTL_DATA_FORMAT");
	errs	+= _service_test(fd);
	errs	+= _function_test(fd);

	if (errs == 0)
		printf("PASS\n");

	return(errs);
}


