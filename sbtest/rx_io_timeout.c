// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.0.1.0_GSC_DN/sbtest/rx_io_timeout.c $
// $Rev: 5781 $
// $Date: 2010-06-02 16:35:06 -0500 (Wed, 02 Jun 2010) $

#include <stdio.h>

#include "main.h"



//*****************************************************************************
static int _service_test(int fd)
{
	static const service_data_t	list[]	=
	{
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_RX_IO_TIMEOUT,
			/* arg		*/	DSI_IO_TIMEOUT_MAX,
			/* reg		*/	-1,
			/* mask		*/	0,
			/* value	*/	0
		},
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_RX_IO_TIMEOUT,
			/* arg		*/	DSI_IO_TIMEOUT_MIN,
			/* reg		*/	-1,
			/* mask		*/	0,
			/* value	*/	0
		},
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_RX_IO_TIMEOUT,
			/* arg		*/	DSI_IO_TIMEOUT_MAX,
			/* reg		*/	-1,
			/* mask		*/	0,
			/* value	*/	0
		},
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_RX_IO_TIMEOUT,
			/* arg		*/	DSI_IO_TIMEOUT_NO_SLEEP,
			/* reg		*/	-1,
			/* mask		*/	0,
			/* value	*/	0
		},
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_RX_IO_TIMEOUT,
			/* arg		*/	DSI_IO_TIMEOUT_DEFAULT,
			/* reg		*/	-1,
			/* mask		*/	0,
			/* value	*/	0
		},

		{ SERVICE_END_LIST }
	};

	int errs	= 0;

	errs	= service_ioctl_set_get_list(fd, list);

	errs	= service_ioctl_set_get_list(fd, list);

	return(errs);
}



//*****************************************************************************
static int _function_test(int fd)
{
	// TBD
	return(0);
}



/******************************************************************************
*
*	Function:	rx_io_timeout_test
*
*	Purpose:
*
*		Perform a test of the IOCTL service DSI_IOCTL_RX_IO_TIMEOUT.
*
*	Arguments:
*
*		fd		The handle for the board to access.
*
*	Returned:
*
*		>= 0	The number of errors encounterred.
*
******************************************************************************/

int rx_io_timeout_test(int fd)
{
	int	errs	= 0;

	gsc_label("DSI_IOCTL_RX_IO_TIMEOUT");
	errs	+= _service_test(fd);
	errs	+= _function_test(fd);

	if (errs == 0)
		printf("PASS\n");

	return(errs);
}


