// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.0.1.0_GSC_DN/sbtest/wait_cancel.c $
// $Rev: 17643 $
// $Date: 2012-08-20 17:06:51 -0500 (Mon, 20 Aug 2012) $

#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/ioctl.h>

#include "main.h"



// #defines	*******************************************************************

#define	_15_SEC		(15L * 1000L)



// variables	***************************************************************

static	const gsc_wait_t	wait_src[]	=
{
	// flags			main								...			timeout_ms
	{ 0,				GSC_WAIT_MAIN_DMA0,					0, 0, 0,	_15_SEC	},
	{ 0,				GSC_WAIT_MAIN_DMA1,					0, 0, 0,	_15_SEC	},
	{ 0,				GSC_WAIT_MAIN_GSC,					0, 0, 0,	_15_SEC	},

	// flags ...		gsc									...			timeout_ms
	{ 0, 0,				DSI_WAIT_GSC_INIT_DONE,				0, 0,		_15_SEC	},
	{ 0, 0,				DSI_WAIT_GSC_AUTO_CAL_DONE,			0, 0,		_15_SEC	},
	{ 0, 0,				DSI_WAIT_GSC_CHAN_READY,			0, 0,		_15_SEC	},
	{ 0, 0,				DSI_WAIT_GSC_AIN_BUF_THRESH_L2H,	0, 0,		_15_SEC	},
	{ 0, 0,				DSI_WAIT_GSC_AIN_BUF_THRESH_H2L,	0, 0,		_15_SEC	},

	// flags ...		io												timeout_ms
	{ 0, 0, 0, 0,		GSC_WAIT_IO_RX_DONE,				 			_15_SEC	},
	{ 0, 0, 0, 0,		GSC_WAIT_IO_RX_ERROR,				 			_15_SEC	},
	{ 0, 0, 0, 0,		GSC_WAIT_IO_RX_TIMEOUT,							_15_SEC	},
	{ 0, 0, 0, 0,		GSC_WAIT_IO_RX_ABORT,							_15_SEC	},

	// terminate list
	{ 1	}
};

static	gsc_wait_t		wait_dst[ARRAY_ELEMENTS(wait_src)];
static	int				wait_fd;
static	gsc_thread_t	wait_thread[ARRAY_ELEMENTS(wait_src)];



//*****************************************************************************
static int _service_test(int fd)
{
	// There are no settings bits to test here.
	return(0);
}



//*****************************************************************************
static int _wait_thread(void* arg)
{
	int	i		= (int) (long) arg;
	int	status;

	status		= ioctl(wait_fd, DSI_IOCTL_WAIT_EVENT, &wait_dst[i]);

	if (status == -1)
		wait_dst[i].flags	= 0xFFFFFFFF;

	return(0);
}


//*****************************************************************************
static int _create_thread_1(int index)
{
	char		buf[64];
	int			errs	= 0;
	int			i;
	int			status;
	gsc_wait_t	wait;

	wait_dst[index]	= wait_src[index];
	sprintf(buf, "wait cancel %d", index);
	errs	+= gsc_thread_create(	&wait_thread[index],
									buf,
									_wait_thread,
									(void*) (long) index);

	// Wait for the thread to become waiting.

	if (errs == 0)
	{
		wait	= wait_dst[index];

		for (i = 0; i < 100; i++)
		{
			status		= ioctl(wait_fd, DSI_IOCTL_WAIT_STATUS, &wait);

			if (status == -1)
			{
				printf(	"FAIL <---  (%d. status request %d: errno %d)\n",
						__LINE__,
						index,
						errno);
				errs++;
				break;
			}

			if (wait.count == 1)
				break;

			usleep(100000);
		}

		if ((errs == 0) && (wait.count != 1))
		{
			printf(	"FAIL <---  (%d. count: expect 1, got %ld)\n",
					__LINE__,
					(long) wait.count);
			errs++;
		}
	}

	return(errs);
}



//*****************************************************************************
static int _create_thread_2(int index)
{
	char		buf[64];
	int			errs	= 0;
	int			i;
	int			status;
	gsc_wait_t	wait;

	wait_dst[index]	= wait_src[0];
	sprintf(buf, "wait cancel %d", index);
	errs	+= gsc_thread_create(	&wait_thread[index],
									buf,
									_wait_thread,
									(void*) (long) index);

	// Wait for the thread to become waiting.

	if (errs == 0)
	{
		wait	= wait_dst[index];

		for (i = 0; i < 100; i++)
		{
			status		= ioctl(wait_fd, DSI_IOCTL_WAIT_STATUS, &wait);

			if (status == -1)
			{
				printf(	"FAIL <---  (%d. status request %d: errno %d)\n",
						__LINE__,
						index,
						errno);
				errs++;
				break;
			}

			if (wait.count == (index + 1))
				break;

			usleep(100000);
		}

		if ((errs == 0) && (wait.count != index + 1))
		{
			printf(	"FAIL <---  (%d. count: expect %d, got %ld)\n",
					__LINE__,
					index + 1,
					(long) wait.count);
			errs++;
		}
	}

	return(errs);
}



//*****************************************************************************
static int _delete_thread_1(int index)
{
	int			errs	= 0;
	int			status;
	gsc_wait_t	wait;

	if (wait_dst[index].flags != 0xFFFFFFFF)
	{
		wait	= wait_dst[index];

		status	= ioctl(wait_fd, DSI_IOCTL_WAIT_CANCEL, &wait);

		if (status == -1)
		{
			printf(	"FAIL <---  (%d. calcel request %d: errno %d)\n",
					__LINE__,
					index,
					errno);
			errs++;
		}

		if ((errs == 0) && (wait.count != 1))
		{
			printf(	"FAIL <---  (%d. count: expect 1, got %ld)\n",
					__LINE__,
					(long) wait.count);
			errs++;
		}

		errs	+= gsc_thread_destroy(&wait_thread[index]);
	}

	return(errs);
}



//*****************************************************************************
static int _delete_thread_2(int count)
{
	int			errs	= 0;
	int			i;
	int			status;
	gsc_wait_t	wait;

	wait	= wait_dst[0];
	status	= ioctl(wait_fd, DSI_IOCTL_WAIT_CANCEL, &wait);

	if (status == -1)
	{
		printf(	"FAIL <---  (%d. calcel request: errno %d)\n",
				__LINE__,
				errno);
		errs++;
	}

	if ((errs == 0) && (wait.count != count))
	{
		printf(	"FAIL <---  (%d. count: expect %d, got %ld)\n",
				__LINE__,
				count,
				(long) wait.count);
		errs++;
	}

	for (i = 0; i < count; i++)
	{
		if (wait_dst[i].flags != 0xFFFFFFFF)
			errs	+= gsc_thread_destroy(&wait_thread[i]);
	}

	return(errs);
}



//*****************************************************************************
static int _function_test(int fd)
{
	int		errs	= 0;
	int		i;

	errs	+= dsi_initialize(fd, -1, 0);

	wait_fd	= fd;
	memset(&wait_dst, 0, sizeof(wait_dst));
	memset(&wait_thread, 0, sizeof(wait_thread));

	// Create threads awaiting each of the source crtiteria.

	for (i = 0; (errs == 0) && (wait_src[i].flags == 0) ; i++)
		errs	+= _create_thread_1(i);

	// Cancel each wait and delete the thread.

	for (i = 0; wait_src[i].flags == 0 ; i++)
		errs	+= _delete_thread_1(i);

	// Create multiple threads awaiting the same criteria.

	for (i = 0; (errs == 0) && (wait_src[i].flags == 0) ; i++)
		errs	+= _create_thread_2(i);

	// Cancel each wait and delete the thread.
	errs	+= _delete_thread_2(ARRAY_ELEMENTS(wait_src) - 1);

	return(errs);
}



/******************************************************************************
*
*	Function:	wait_cancel_test
*
*	Purpose:
*
*		Perform a test of the IOCTL service DSI_IOCTL_WAIT_CANCEL.
*
*	Arguments:
*
*		fd		The handle for the board to access.
*
*	Returned:
*
*		>= 0	The number of errors encounterred.
*
******************************************************************************/

int wait_cancel_test(int fd)
{
	int	errs	= 0;

	gsc_label("DSI_IOCTL_WAIT_CANCEL");
	errs	+= _service_test(fd);
	errs	+= _function_test(fd);

	if (errs == 0)
		printf("PASS\n");

	return(errs);
}


