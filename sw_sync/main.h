// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.0.1.0_GSC_DN/sw_sync/main.h $
// $Rev: 22613 $
// $Date: 2013-07-16 15:25:27 -0500 (Tue, 16 Jul 2013) $

#ifndef __MAIN_H__
#define __MAIN_H__

#include "24dsi.h"
#include "24dsi_dsl.h"
#include "24dsi_utils.h"
#include "gsc_utils.h"



// #defines	*******************************************************************

#define	VERSION			"1.5"
// 1.5	Corrected the text of an output message.
// 1.4	Added a "verbose" argument to the utility services.
// 1.3	Updated the parameter list to gsc_id_driver().
// 1.2	Split the utility code into two libraries: common and device specific.
// 1.1	Corrected output spelling.
// 1.0	initial release.



// prototypes	***************************************************************

int	sw_sync(int fd);



#endif
