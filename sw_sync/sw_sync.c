// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.0.1.0_GSC_DN/sw_sync/sw_sync.c $
// $Rev: 17641 $
// $Date: 2012-08-20 17:05:08 -0500 (Mon, 20 Aug 2012) $

#include <errno.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/ioctl.h>

#include "main.h"



/******************************************************************************
*
*	Function:	sw_sync
*
*		Repetatively generate a SW Sync pulse.
*
*	Arguments:
*
*		fd		The handle to the board to access.
*
*	Returned:
*
*		>= 0	The number of errors encounterred.
*
******************************************************************************/

int sw_sync(int fd)
{
	int	errs	= 0;
	int	i;
	int	qty		= 10;

	for (i = 0; (i < qty) && (errs == 0); i++)
		errs	+= dsi_sw_sync(fd, -1, 1);

	return(errs);
}



