#!/bin/bash

### BEGIN INIT INFO
# Provides:  24dsi
# Required-Start: $local_fs
# Required-Stop: $local_fs
# Default-Start: 2 3 4 5
# Default-Stop:
# Short-Description: load 24DSI driver module
# Description: load and configure the driver module for
#              a General Standards 24DSI A/D board.
### END INIT INFO
 
PATH=/sbin:/usr/sbin:/usr/local/sbin:/bin:/usr/bin
export PATH

NAME="24dsi"
DEFAULTS="/etc/default/${NAME}"
MODULE="/lib/modules/`uname -r`/misc/${NAME}.ko"

test -f $MODULE || exit 0
test -s $DEFAULTS && . $DEFAULTS

. /lib/lsb/init-functions

if [ "$GS24DSI_ENABLE" != "1" ]
then
    log_warning_msg "24dsi disabled"
    exit 0
fi

do_start ()
{
    log_daemon_msg "Loading 24DSI driver"
    modprobe -r 24dsi || true
    rm -f /dev/24dsi*
    modprobe 24dsi
    major=`grep 24dsi /proc/devices|cut -f1 -d' '`
    boards=`grep -i boards: /proc/24dsi|cut -f2 -d' '`
    minor=0
    while [ $minor -lt $boards ]
    do
	mknod -m 0666 /dev/24dsi${minor} c $major $minor
        minor=`expr $minor + 1`
	log_progress_msg "/dev/24dsi${minor}"
    done
    log_end_msg $?
}

do_stop ()
{
    log_daemon_msg "Unloading 24DSI driver"
    modprobe -r -q 24dsi
    log_end_msg $?
}


case "$1" in
    start)
	do_start
	;;
    stop)
	do_stop
	;;
    restart)
	do_stop
	do_start
	;;
    reload|force-reload)
	log_warning_msg "Reload not supported"
	;;
    *)
	echo "Usage: $(basename $0) start|stop|status" 1>&2
	exit 1
	;;
esac

exit 0
