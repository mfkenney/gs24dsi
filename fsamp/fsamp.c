// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.0.1.0_GSC_DN/fsamp/fsamp.c $
// $Rev: 17647 $
// $Date: 2012-08-20 17:10:05 -0500 (Mon, 20 Aug 2012) $

#include <stdio.h>
#include <string.h>

#include "main.h"



/******************************************************************************
*
*	Function:	fsamp_compute
*
*	Purpose:
*
*		Configure the board, then capture data.
*
*	Arguments:
*
*		fd		The handle for the board to access.
*
*		fref	The Fref value. This is ignored if this is a legacy board.
*
*		fsamp	The requested sample rate.
*
*		options	Display all computed options within this range of the requested
*				sample rate. This is supressed in quiet mode.
*
*	Returned:
*
*		>= 0	The number of errors encounterred.
*
******************************************************************************/

int fsamp_compute(int fd, __s32 fref, __s32 fsamp, __s32 options)
{
	int		errs	= 0;
	__s32	tmp;

	gsc_label("Fsamp Computation");
	printf("\n");
	gsc_label_level_inc();
	errs	+= dsi_query_model				(fd, -1, 1, &tmp);
	errs	+= dsi_query_frequency_control	(fd, -1, 1, &tmp);
	errs	+= dsi_fref_compute				(fd, -1, 1, &fref);
	errs	+= dsi_query_fgen_range			(fd, -1, 1, &tmp, &tmp);
	errs	+= dsi_query_nvco_range			(fd, -1, 1, &tmp, &tmp);
	errs	+= dsi_query_nref_range			(fd, -1, 1, &tmp, &tmp);
	errs	+= dsi_query_nrate_range		(fd, -1, 1, &tmp, &tmp);
	errs	+= dsi_query_ndiv_range			(fd, -1, 1, &tmp, &tmp);
	errs	+= dsi_query_fsamp_range		(fd, -1, 1, &tmp, &tmp);
	errs	+= dsi_fsamp_validate			(fd, -1, 1, &fsamp);
	errs	+= dsi_fsamp_compute			(fd, -1, 1, options, fref, &fsamp, &tmp, &tmp, &tmp, &tmp);

	gsc_label_level_dec();
	return(errs);
}


