// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.0.1.0_GSC_DN/fsamp/main.h $
// $Rev: 17647 $
// $Date: 2012-08-20 17:10:05 -0500 (Mon, 20 Aug 2012) $

#ifndef __MAIN_H__
#define __MAIN_H__

#include "24dsi.h"
#include "24dsi_dsl.h"
#include "24dsi_utils.h"
#include "gsc_utils.h"



// #defines	*******************************************************************

#define	VERSION				"1.4"
// 1.4	Added a "verbose" argument to the utility services.
// 1.3	Updated the parameter list to gsc_id_driver().
// 1.2	Split the utility code into two libraries: common and device specific.
// 1.1	Fixed a bug in parsing the "index" command line argument.
// 1.0	initial release.



// prototypes	***************************************************************

int	fsamp_compute(int fd, __s32 nref, __s32 fsamp, __s32 options);
int	fref_derive(int fd, __s32* nref);



#endif
