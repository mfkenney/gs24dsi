// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.0.1.0_GSC_DN/savedata/main.h $
// $Rev: 21126 $
// $Date: 2013-04-19 13:06:16 -0500 (Fri, 19 Apr 2013) $

#ifndef __MAIN_H__
#define __MAIN_H__

#include "24dsi.h"
#include "24dsi_dsl.h"
#include "24dsi_utils.h"
#include "gsc_utils.h"



// #defines	*******************************************************************

#define	VERSION			"1.5"
// 1.5	Added error message to the saved text file.
// 1.4	Added a "verbose" argument to the utility services.
// 1.3	Updated the parameter list to gsc_id_driver().
// 1.2	Split the utility code into two libraries: common and device specific.
// 1.1	Report overflow and underflow status after reading data.
// 1.0	initial release.



// prototypes	***************************************************************

int	save_data(int fd);



#endif
