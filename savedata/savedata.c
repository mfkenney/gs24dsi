// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.0.1.0_GSC_DN/savedata/savedata.c $
// $Rev: 21126 $
// $Date: 2013-04-19 13:06:16 -0500 (Fri, 19 Apr 2013) $

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "main.h"



// #defines	*******************************************************************

#define	_1M		(1024L * 1024L)



// variables	***************************************************************

static	__u32	_buffer[_1M];



/******************************************************************************
*
*	Function:	_channels
*
*	Purpose:
*
*		Check to see how many channels the board has.
*
*	Arguments:
*
*		fd		The handle for the board to access.
*
*		chan	Report the number of channels here.
*
*	Returned:
*
*		>= 0	The number of errors encounterred.
*
******************************************************************************/

static int _channels(int fd, __s32* chans)
{
	int	errs;

	gsc_label("Input Channels");
	errs	= dsi_query(fd, -1, 0, DSI_QUERY_CHANNEL_QTY, chans);

	if (errs == 0)
		printf("%ld Channels\n", (long) chans[0]);

	return(errs);
}



/******************************************************************************
*
*	Function:	_read_data
*
*	Purpose:
*
*		Read data into the buffer.
*
*	Arguments:
*
*		fd		The handle for the board to access.
*
*	Returned:
*
*		>= 0	The number of errors encounterred.
*
******************************************************************************/

static int _read_data(int fd)
{
	int		errs;
	long	get		= sizeof(_buffer) / 4;
	int		got;

	gsc_label("Reading");
	got	= dsi_dsl_read(fd, _buffer, get);

	if (got < 0)
	{
		errs	= 1;
	}
	else if (got != get)
	{
		errs	= 1;
		printf(	"FAIL <---  (got %ld samples, requested %ld)\n",
				(long) got,
				(long) get);
	}
	else
	{
		errs	= 0;
		printf(	"PASS  (%ld samples)\n",
				(long) get);
	}

	return(errs);
}



/******************************************************************************
*
*	Function:	_save_data
*
*	Purpose:
*
*		Save the read data to a text file.
*
*	Arguments:
*
*		fd		The handle for the board to access.
*
*		chans	The number of channels.
*
*		errs	have there been any errors so far?
*
*	Returned:
*
*		>= 0	The number of errors encounterred.
*
******************************************************************************/

static int _save_data(int fd, int chans, int errs)
{
	FILE*		file;
	int			i;
	int			i_expected	= 0;
	int			i_got;
	int			in_sync		= 1;
	long		l;
	const char*	name		= "data.txt";
	long		samples		= sizeof(_buffer) / 4;
	int			sync_errs	= 0;

	gsc_label("Saving");

	for (;;)
	{
		if (errs)
		{
			printf("SKIPPED  (errors)\n");
			errs	= 0;
			break;
		}

		file	= fopen(name, "w+b");

		if (file == NULL)
		{
			printf("FAIL <---  (unable to create %s)\n", name);
			errs	= 1;
			break;
		}

		for (l = 0; l < samples; l++)
		{
			i_got	= (_buffer[l] >> 24) & 0x1F;

			if (in_sync)
			{
				if (i_got == i_expected)
				{
					// We're still in sync.
					i_expected	= (i_expected + 1) % chans;
					i			= fprintf(file, "  %08lX", (long) _buffer[l]);

					if (i != 10)
					{
						printf("FAIL <---  (fprintf() failure to %s)\n", name);
						errs	= 1;
						break;
					}

					if (i_expected == 0)
					{
						i	= fprintf(file, "\r\n");

						if (i != 2)
						{
							printf("FAIL <---  (fprintf() failure to %s)\n", name);;
							errs	= 1;
							break;
						}
					}
				}
				else
				{
					// We're now out of sync.
					sync_errs++;
					i_expected	= 0;

					if (i_got == 0)
					{
						i	= fprintf(file, "  <--- OUT OF SYNC\r\n");

						if (i != 20)
						{
							printf("FAIL <---  (fprintf() failure to %s)\n", name);;
							errs	= 1;
							break;
						}

						l--;
					}
					else
					{
						in_sync	= 0;
						i		= fprintf(file, "  %08lX", (long) _buffer[l]);

						if (i != 10)
						{
							printf("FAIL <---  (fprintf() failure to %s)\n", name);
							errs	= 1;
							break;
						}
					}
				}
			}
			else // in_sync == 0
			{
				if (i_got)
				{
					// We're still out of sync.
					i	= fprintf(file, "  %08lX", (long) _buffer[l]);

					if (i != 10)
					{
						printf("FAIL <---  (fprintf() failure to %s)\n", name);
						errs	= 1;
						break;
					}
				}
				else
				{
					// We're back in sync.
					i	= fprintf(file, "  <--- OUT OF SYNC\r\n");

					if (i != 20)
					{
						printf("FAIL <---  (fprintf() failure to %s)\n", name);;
						errs	= 1;
						break;
					}

					in_sync		= 1;
					i_expected	= 1;
					i			= fprintf(file, "  %08lX", (long) _buffer[l]);

					if (i != 10)
					{
						printf("FAIL <---  (fprintf() failure to %s)\n", name);
						errs	= 1;
						break;
					}
				}
			}
		}

		fclose(file);

		if (errs)
		{
		}
		else if (sync_errs)
		{
			errs++;
			printf("FAIL  (%s, %d sync errors)\n", name, sync_errs);
		}
		else
		{
			printf("PASS  (%s)\n", name);
		}

		break;
	}

	return(errs);
}



/******************************************************************************
*
*	Function:	save_data
*
*	Purpose:
*
*		Configure the board, then capture data to a file.
*
*	Arguments:
*
*		fd		The handle for the board to access.
*
*	Returned:
*
*		>= 0	The number of errors encounterred.
*
******************************************************************************/

int save_data(int fd)
{
	__s32	chans	= 32;
	int		errs	= 0;
	__s32	fref	= -1;

	gsc_label("Capture & Save");
	printf("\n");
	gsc_label_level_inc();

	errs	+= dsi_config_ai(fd, -1, 1, fref, 10000);
	errs	+= _channels(fd, &chans);
	errs	+= _read_data(fd);
	errs	+= dsi_ain_buf_overflow(fd, -1, 1, DSI_AIN_BUF_OVERFLOW_TEST, NULL);
	errs	+= dsi_ain_buf_underflow(fd, -1, 1, DSI_AIN_BUF_UNDERFLOW_TEST, NULL);
	errs	+= _save_data(fd, chans, errs);

	gsc_label_level_dec();
	return(errs);
}



