// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.0.1.0_GSC_DN/id/main.h $
// $Rev: 17646 $
// $Date: 2012-08-20 17:09:37 -0500 (Mon, 20 Aug 2012) $

#ifndef __MAIN_H__
#define __MAIN_H__

#include "24dsi.h"
#include "24dsi_utils.h"
#include "gsc_utils.h"



// #defines	*******************************************************************

#define	VERSION			"1.8"
// 1.8	Added a "verbose" argument to the utility services.
// 1.7	Updated the parameter list to gsc_id_driver().
// 1.6	Added support for the PCIE104-24DSI12.
// 1.5	Split the utility code into two libraries: common and device specific.
//		Updated the register output data.
// 1.4	Corrected output spelling.
// 1.3	Updated the register data output code.
// 1.2	Updated various Auto Cal related strings and macros for consistency.
// 1.1	Added support for the 24DSI6LN.
// 1.0	initial release.



// prototypes	***************************************************************


int	id_device(int fd);



#endif
