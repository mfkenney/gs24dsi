// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.0.1.0_GSC_DN/docsrc/24dsi_dsl.h $
// $Rev: 1814 $
// $Date: 2009-04-22 17:13:15 -0500 (Wed, 22 Apr 2009) $

#ifndef __24DSI_DSL_H__
#define __24DSI_DSL_H__

#include "24dsi.h"



// prototypes	***************************************************************

int	dsi_dsl_close(int fd);
int	dsi_dsl_ioctl(int fd, int request, void *arg);
int	dsi_dsl_open(unsigned int board);
int	dsi_dsl_read(int fd, __u32 *buf, size_t samples);



#endif
