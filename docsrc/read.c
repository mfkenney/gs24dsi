#include <errno.h>
#include <stdio.h>
#include <unistd.h>

#include "24dsi_dsl.h"

int dsi_dsl_read(int fd, __u32 *buf, size_t samples)
{
	size_t	bytes;
	int		status;

	bytes	= samples * 4;
	status	= read(fd, buf, bytes);

	if (status == -1)
		printf("ERROR: read() failure, errno = %d\n", errno);
	else
		status	/= 4;

	return(status);
}
