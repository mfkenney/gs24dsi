#include <errno.h>
#include <stdio.h>
#include <unistd.h>

#include "24dsi_dsl.h"

int dsi_dsl_close(int fd)
{
	int	err;
	int	status;

	status	= close(fd);

	if (status == -1)
		printf("ERROR: close() failure, errno = %d\n", errno);

	err	= (status == -1) ? 1 : 0;
	return(err);
}
