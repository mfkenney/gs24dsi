// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.0.1.0_GSC_DN/regs/main_menu.c $
// $Rev: 16259 $
// $Date: 2012-05-10 15:14:07 -0500 (Thu, 10 May 2012) $

#include <stdio.h>

#include "main.h"



/*****************************************************************************
*
*	Function:	_dump_everything
*
*	Purpose:
*
*		Dump all available data to the screen.
*
*	Arguments:
*
*		fd		The handle for the board to access.
*
*	Returned:
*
*		None.
*
*****************************************************************************/

static void _dump_everything(int fd)
{
	__u32	didr;

	dsi_reg_read(fd, GSC_PCI_9080_DIDR, &didr);

	gsc_id_host();
	printf("\n");
	gsc_id_driver(DSI_BASE_NAME);
	printf("\n");
	dsi_id_board(fd, -1, NULL);
	printf("\n");

	if (didr == 0x9080)
		gsc_reg_plx9080_list_pci(fd, dsi_reg_read);
	else
		gsc_reg_plx9056_list_pci(fd, dsi_reg_read);

	printf("\n");

	if (didr == 0x9080)
		gsc_reg_plx9080_list_plx(fd, dsi_reg_read);
	else
		gsc_reg_plx9056_list_plx(fd, dsi_reg_read);

	printf("\n");
	dsi_reg_list(fd, 1);
}



/*****************************************************************************
*
*	Function:	_dump_gsc
*
*	Purpose:
*
*		Dump the GSC registers.
*
*	Arguments:
*
*		fd		The handle for the board to access.
*
*	Returned:
*
*		None.
*
*****************************************************************************/

static void _dump_gsc(int fd)
{
	dsi_reg_list(fd, 0);
}



/*****************************************************************************
*
*	Function:	_dump_gsc_detail
*
*	Purpose:
*
*		Dump the GSC register details.
*
*	Arguments:
*
*		fd		The handle for the board to access.
*
*	Returned:
*
*		None.
*
*****************************************************************************/

static void _dump_gsc_detail(int fd)
{
	dsi_reg_list(fd, 1);
}



/*****************************************************************************
*
*	Function:	_dump_pci
*
*	Purpose:
*
*		Dump the PCI registers.
*
*	Arguments:
*
*		fd		The handle for the board to access.
*
*	Returned:
*
*		None.
*
*****************************************************************************/

static void _dump_pci(int fd)
{
	__u32	didr;

	dsi_reg_read(fd, GSC_PCI_9080_DIDR, &didr);

	if (didr == 0x9080)
		gsc_reg_plx9080_list_pci(fd, dsi_reg_read);
	else
		gsc_reg_plx9056_list_pci(fd, dsi_reg_read);
}



/*****************************************************************************
*
*	Function:	_dump_plx
*
*	Purpose:
*
*		Dump the PLX registers.
*
*	Arguments:
*
*		fd		The handle for the board to access.
*
*	Returned:
*
*		None.
*
*****************************************************************************/

static void _dump_plx(int fd)
{
	__u32	didr;

	dsi_reg_read(fd, GSC_PCI_9080_DIDR, &didr);

	if (didr == 0x9080)
		gsc_reg_plx9080_list_plx(fd, dsi_reg_read);
	else
		gsc_reg_plx9056_list_plx(fd, dsi_reg_read);
}



/*****************************************************************************
*
*	Function:	_id_board
*
*	Purpose:
*
*		Identify the device being accessed.
*
*	Arguments:
*
*		fd		The handle for the board to access.
*
*	Returned:
*
*		None.
*
*****************************************************************************/

static void _id_board(int fd)
{
	dsi_id_board(fd, -1, NULL);
}



/*****************************************************************************
*
*	Function:	_id_driver
*
*	Purpose:
*
*		Identify the device driver.
*
*	Arguments:
*
*		fd		The handle for the board to access.
*
*	Returned:
*
*		None.
*
*****************************************************************************/

static void _id_driver(int fd)
{
	gsc_id_driver(DSI_BASE_NAME);
}



/*****************************************************************************
*
*	Function:	_id_host
*
*	Purpose:
*
*		Identify the host OS.
*
*	Arguments:
*
*		fd		The handle for the board to access.
*
*	Returned:
*
*		None.
*
*****************************************************************************/

static void _id_host(int fd)
{
	gsc_id_host();
}



/*****************************************************************************
*
*	Function:	main_menu
*
*	Purpose:
*
*		Present the main menu.
*
*	Arguments:
*
*		fd		The handle for the board to access.
*
*	Returned:
*
*		None.
*
*****************************************************************************/

void main_menu(int fd)
{
	static const menu_item_t	list[]	=
	{
		// name								func
		{ "Dump Everything",				_dump_everything	},
		{ "Host OS Identification",			_id_host			},
		{ "Driver Identification",			_id_driver			},
		{ "Board Identification",			_id_board			},
		{ "PCI Register Dump",				_dump_pci			},
		{ "PLX Register Dump",				_dump_plx			},
		{ "GSC Register Dump",				_dump_gsc			},
		{ "GSC Register Detail Dump",		_dump_gsc_detail	},
		{ "Edit GSC Register By Name",		reg_mod_by_name		},
		{ "Edit GSC Register By Offset",	reg_mod_by_offset	},
		{ NULL,								NULL				}
	};

	static const menu_t	menu	=
	{
		/* title	*/	"Main Menu",
		/* list		*/	list
	};

	printf("\n\n");
	printf("Register Access Application (version %s)\n", VERSION);
	menu_call(fd, &menu);
}



