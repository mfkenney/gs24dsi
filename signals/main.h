// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.0.1.0_GSC_DN/signals/main.h $
// $Rev: 17642 $
// $Date: 2012-08-20 17:06:01 -0500 (Mon, 20 Aug 2012) $

#ifndef __MAIN_H__
#define __MAIN_H__

#include "24dsi.h"
#include "24dsi_dsl.h"
#include "24dsi_utils.h"
#include "gsc_utils.h"



// #defines	*******************************************************************

#define	VERSION				"1.3"
// 1.3	Added a "verbose" argument to the utility services.
// 1.2	Updated the parameter list to gsc_id_driver().
// 1.1	Split the utility code into two libraries: common and device specific.
// 1.0	Initial release.



// prototypes	***************************************************************

int	signals(int fd, int seconds);



#endif
