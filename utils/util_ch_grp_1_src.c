// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.0.1.0_GSC_DN/utils/util_ch_grp_1_src.c $
// $Rev: 17640 $
// $Date: 2012-08-20 16:58:49 -0500 (Mon, 20 Aug 2012) $

#include <stdio.h>
#include <string.h>

#include "24dsi_dsl.h"
#include "24dsi_utils.h"
#include "gsc_utils.h"



/******************************************************************************
*
*	Function:	dsi_ch_grp_1_src
*
*	Purpose:
*
*		Provide a visual wrapper for the DSI_IOCTL_CH_GRP_1_SRC IOCTL service.
*
*	Arguments:
*
*		fd		The handle to use to access the driver.
*
*		index	The index of the board to access. Ignore if < 0.
*
*		verbose	Work verbosely? If not, then quietly.
*
*		set		The setting to apply.
*
*		get		The current setting is recorded here, if not NULL.
*
*	Returned:
*
*		>= 0	The number of errors encountered here.
*
******************************************************************************/

int	dsi_ch_grp_1_src(int fd, int index, int verbose, __s32 set, __s32* get)
{
	#define	GROUP	1

	char	buf[128];
	int		errs	= 0;
	__s32	groups;
	__s32	pri;

	if (verbose)
		gsc_label_index("Channel Group 1 Source", index);

	errs	+= dsi_query(fd, index, 0, DSI_QUERY_CHANNEL_GPS, &groups);
	errs	+= dsi_query(fd, index, 0, DSI_QUERY_CH_GRP_0_RAR, &pri);

	if (errs)
	{
	}
	else if (groups < 1)
	{
		if (verbose)
		{
			printf(	"SKIPPED  "
					"(channel group %ld not supported on this board)\n",
					(long) GROUP);
		}
	}
	else
	{
		if ((GROUP) && (pri))
		{
			set		= (set == DSI_CH_GRP_SRC_DISABLE)
					? DSI_CH_GRP_SRC_DISABLE
					: DSI_CH_GRP_SRC_ENABLE;
		}

		errs	= dsi_dsl_ioctl(fd, DSI_IOCTL_CH_GRP_1_SRC, &set);

		switch (set)
		{
			default:

				sprintf(buf, "INVALID: %ld", (long) set);
				break;

			case DSI_CH_GRP_SRC_GEN_A:

				strcpy(buf, "Rate Generator A");
				break;

			case DSI_CH_GRP_SRC_GEN_B:

				strcpy(buf, "Rate Generator B");
				break;
			case DSI_CH_GRP_SRC_EXTERN:

				strcpy(buf, "External");
				break;

			case DSI_CH_GRP_SRC_DIR_EXTERN:

				strcpy(buf, "Direct External");
				break;

			case DSI_CH_GRP_SRC_DISABLE:

				strcpy(buf, "Disable");
				break;

			case DSI_CH_GRP_SRC_ENABLE:

				strcpy(buf, "Enable");
				break;
		}

		if (verbose)
			printf("%s  (%s)\n", errs ? "FAIL <---" : "PASS", buf);
	}

	if (get)
		get[0]	= set;

	return(errs);
}


