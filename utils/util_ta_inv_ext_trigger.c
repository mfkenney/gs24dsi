// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.0.1.0_GSC_DN/utils/util_ta_inv_ext_trigger.c $
// $Rev: 17640 $
// $Date: 2012-08-20 16:58:49 -0500 (Mon, 20 Aug 2012) $

#include <stdio.h>
#include <string.h>

#include "24dsi_dsl.h"
#include "24dsi_utils.h"
#include "gsc_utils.h"



/******************************************************************************
*
*	Function:	dsi_tx_inv_ext_trigger
*
*	Purpose:
*
*		Provide a visual wrapper for the DSI_IOCTL_TA_INV_EXT_TRIGGER IOCTL service.
*
*	Arguments:
*
*		fd		The handle to use to access the driver.
*
*		index	The index of the board to access. Ignore if < 0.
*
*		verbose	Work verbosely? If not, then quietly.
*
*		set		The setting to apply.
*
*		get		The current setting is recorded here, if not NULL.
*
*	Returned:
*
*		>= 0	The number of errors encountered here.
*
******************************************************************************/

int	dsi_tx_inv_ext_trigger(int fd, int index, int verbose, __s32 set, __s32* get)
{
	char	buf[128];
	int		errs;

	if (verbose)
		gsc_label_index("TA Invert External Trigger", index);

	errs	= dsi_dsl_ioctl(fd, DSI_IOCTL_TA_INV_EXT_TRIGGER, &set);

	switch (set)
	{
		default:

			sprintf(buf, "INVALID: %ld", (long) set);
			break;

		case DSI_TA_INV_EXT_TRIGGER_NO:

			strcpy(buf, "No (Rising Edge)");
			break;

		case DSI_TA_INV_EXT_TRIGGER_YES:

			strcpy(buf, "Yes (Falling Edge)");
			break;
	}

	if (verbose)
		printf("%s  (%s)\n", errs ? "FAIL <---" : "PASS", buf);

	if (get)
		get[0]	= set;

	return(errs);
}


