// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.0.1.0_GSC_DN/utils/util_reg_read.c $
// $Rev: 13534 $
// $Date: 2011-12-22 13:04:11 -0600 (Thu, 22 Dec 2011) $

#include <stdio.h>
#include <unistd.h>
#include <sys/ioctl.h>

#include "24dsi.h"
#include "24dsi_dsl.h"
#include "24dsi_utils.h"
#include "gsc_utils.h"



/*******************************************************************************
*
*	Function:	dsi_reg_read
*
*	Purpose:
*
*		Provide a silent wrapper for the DSI_IOCTL_REG_READ IOCTL service.
*
*	Arguments:
*
*		fd		The handles to access all four channels.
*
*		reg		The register to access.
*
*		value	The value read goes here.
*
*	Returned:
*
*		>= 0	The number of errors encountered here.
*
*******************************************************************************/

int dsi_reg_read(int fd, __u32 reg, __u32* value)
{
	int			errs;
	int			i;
	gsc_reg_t	parm;

	parm.reg	= reg;
	parm.value	= 0xDEADBEEF;
	parm.mask	= 0;	// Unused here.
	i			= ioctl(fd, DSI_IOCTL_REG_READ, &parm);

	if (value)
		value[0]	= parm.value;

	errs	= (i == -1) ? 1 : 0;
	return(errs);
}


