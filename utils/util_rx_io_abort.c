// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.0.1.0_GSC_DN/utils/util_rx_io_abort.c $
// $Rev: 17640 $
// $Date: 2012-08-20 16:58:49 -0500 (Mon, 20 Aug 2012) $

#include <stdio.h>
#include <unistd.h>
#include <sys/ioctl.h>

#include "24dsi.h"
#include "24dsi_dsl.h"
#include "24dsi_utils.h"
#include "gsc_utils.h"



/******************************************************************************
*
*	Function:	dsi_rx_io_abort
*
*	Purpose:
*
*		Provide a visual wrapper for the DSI_IOCTL_RX_IO_ABORT IOCTL service.
*
*	Arguments:
*
*		fd		The handle to use to access the driver.
*
*		index	The index of the board to access. Ignore if < 0.
*
*		verbose	Work verbosely? If not, then quietly.
*
*		get		The results are reported here. This may be NULL.
*
*	Returned:
*
*		>= 0	The number of errors encounterred.
*
******************************************************************************/

int dsi_rx_io_abort(int fd, int index, int verbose, __s32* get)
{
	char		buf[128];
	int			errs;
	const char*	ptr;
	__s32		set;

	if (verbose)
		gsc_label_index("Rx I/O Abort", index);

	errs	= dsi_dsl_ioctl(fd, DSI_IOCTL_RX_IO_ABORT, &set);

	switch (set)
	{
		default:

			errs++;
			ptr	= buf;
			sprintf(buf, "Unrecognized option: 0x%lX", (long) set);
			break;

		case DSI_IO_ABORT_NO:

			ptr	= "No (Not Aborted)";
			break;

		case DSI_IO_ABORT_YES:

			ptr	= "Yes (Aborted)";
			break;
	}

	if (verbose)
		printf("%s  (%s)\n", errs ? "FAIL <---" : "PASS", ptr);

	if (get)
		get[0]	= set;

	return(errs);
}


