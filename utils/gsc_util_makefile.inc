# $URL: http://subversion:8080/svn/gsc/trunk/drivers/gsc_common/linux/utils/gsc_util_makefile.inc $
# $Rev: 21125 $
# $Date: 2013-04-19 13:01:39 -0500 (Fri, 19 Apr 2013) $

.PHONY: all clean default makefile release

GSC_TARGET			= gsc_utils.a

GSC_UTIL_SOURCES	:=							\
					./gsc_util_close.c			\
					./gsc_util_count.c			\
					./gsc_util_id.c				\
					./gsc_util_kbhit.c			\
					./gsc_util_label.c			\
					./gsc_util_open.c			\
					./gsc_util_reg.c			\
					./gsc_util_reg_pex8111.c	\
					./gsc_util_reg_pex8112.c	\
					./gsc_util_reg_plx9056.c	\
					./gsc_util_reg_plx9060es.c	\
					./gsc_util_reg_plx9080.c	\
					./gsc_util_reg_plx9656.c	\
					./gsc_util_select.c			\
					./gsc_util_thread.c			\
					./gsc_utils.h

GSC_OBJ_FILES		:=							\
					./gsc_util_close.o			\
					./gsc_util_count.o			\
					./gsc_util_id.o				\
					./gsc_util_kbhit.o			\
					./gsc_util_label.o			\
					./gsc_util_open.o			\
					./gsc_util_reg.o			\
					./gsc_util_reg_pex8111.o	\
					./gsc_util_reg_pex8112.o	\
					./gsc_util_reg_plx9056.o	\
					./gsc_util_reg_plx9060es.o	\
					./gsc_util_reg_plx9080.o	\
					./gsc_util_reg_plx9656.o	\
					./gsc_util_select.o			\
					./gsc_util_thread.o

ECHO	:= ${shell ls /bin/echo 2>/dev/null | wc -l}
ifeq ("${ECHO}","1")
ECHO	:= /bin/echo
else
ECHO	:= echo
endif



${GSC_TARGET}: ${GSC_UTIL_SOURCES} ${GSC_OBJ_FILES}
	@echo ==== Linking: $@
	@ar -rcs $@ ${GSC_OBJ_FILES}
	#@ld -r -o $@ ${GSC_OBJ_FILES}

ifeq ("${GSC_COMMON}","1")

./gsc_util_close.c: ./../../../gsc_common/linux/utils/gsc_util_close.c
	@echo == Getting: $@
	@rm -f $@
	@cp $< ./
	@chmod 444 $@

./gsc_util_count.c: ./../../../gsc_common/linux/utils/gsc_util_count.c
	@echo == Getting: $@
	@rm -f $@
	@cp $< ./
	@chmod 444 $@

./gsc_util_id.c: ./../../../gsc_common/linux/utils/gsc_util_id.c
	@echo == Getting: $@
	@rm -f $@
	@cp $< ./
	@chmod 444 $@

./gsc_util_kbhit.c: ./../../../gsc_common/linux/utils/gsc_util_kbhit.c
	@echo == Getting: $@
	@rm -f $@
	@cp $< $@
	@chmod 444 $@

./gsc_util_label.c: ./../../../gsc_common/linux/utils/gsc_util_label.c
	@echo == Getting: $@
	@rm -f $@
	@cp $< $@
	@chmod 444 $@

./gsc_util_open.c: ./../../../gsc_common/linux/utils/gsc_util_open.c
	@echo == Getting: $@
	@rm -f $@
	@cp $< $@
	@chmod 444 $@

./gsc_util_reg.c: ./../../../gsc_common/linux/utils/gsc_util_reg.c
	@echo == Getting: $@
	@rm -f $@
	@cp $< $@
	@chmod 444 $@

./gsc_util_reg_pex8111.c: ./../../../gsc_common/linux/utils/gsc_util_reg_pex8111.c
	@echo == Getting: $@
	@rm -f $@
	@cp $< $@
	@chmod 444 $@

./gsc_util_reg_pex8112.c: ./../../../gsc_common/linux/utils/gsc_util_reg_pex8112.c
	@echo == Getting: $@
	@rm -f $@
	@cp $< $@
	@chmod 444 $@

./gsc_util_reg_plx9056.c: ./../../../gsc_common/linux/utils/gsc_util_reg_plx9056.c
	@echo == Getting: $@
	@rm -f $@
	@cp $< $@
	@chmod 444 $@

./gsc_util_reg_plx9060es.c: ./../../../gsc_common/linux/utils/gsc_util_reg_plx9060es.c
	@echo == Getting: $@
	@rm -f $@
	@cp $< $@
	@chmod 444 $@

./gsc_util_reg_plx9080.c: ./../../../gsc_common/linux/utils/gsc_util_reg_plx9080.c
	@echo == Getting: $@
	@rm -f $@
	@cp $< $@
	@chmod 444 $@

./gsc_util_reg_plx9656.c: ./../../../gsc_common/linux/utils/gsc_util_reg_plx9656.c
	@echo == Getting: $@
	@rm -f $@
	@cp $< $@
	@chmod 444 $@

./gsc_util_select.c: ./../../../gsc_common/linux/utils/gsc_util_select.c
	@echo == Getting: $@
	@rm -f $@
	@cp $< $@
	@chmod 444 $@

./gsc_util_thread.c: ./../../../gsc_common/linux/utils/gsc_util_thread.c
	@echo == Getting: $@
	@rm -f $@
	@cp $< $@
	@chmod 444 $@

./gsc_utils.h: ./../../../gsc_common/linux/utils/gsc_utils.h
	@echo == Getting: $@
	@rm -f $@
	@cp $< $@
	@chmod 444 $@

endif
