// $URL: http://subversion:8080/svn/gsc/trunk/drivers/gsc_common/linux/utils/gsc_util_thread.c $
// $Rev: 21556 $
// $Date: 2013-05-10 17:35:42 -0500 (Fri, 10 May 2013) $

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "gsc_utils.h"



/* data types	*************************************************************/

typedef struct
{
	int		(*function)(void* arg);
	void*	arg;
} _thread_data_t;



/*****************************************************************************
*
*	Function:	_thread_func
*
*	Purpose:
*
*		This is an OS specific thread entry function, whose purpose is to
*		call the application's OS independent thread entry function.
*
*	Arguments:
*
*		pvArg	The thread's entry arguemtn.
*
*	Returned:
*
*		The thread exit code, which we ignore.
*
*****************************************************************************/

static void* _thread_func(void* arg)
{
	_thread_data_t*	data	= (_thread_data_t*) arg;
	int				ret;
	_thread_data_t	tmp		= data[0];

	free(arg);
	ret	= (tmp.function)(tmp.arg);
	pthread_exit((void*) (unsigned long) ret);
	return((void*) (unsigned long) ret);
}



/*****************************************************************************
*
*	Function:	gsc_thread_create
*
*	Purpose:
*
*		Create a thread whose effective entry point and argument are as
*		given.
*
*	Arguments:
*
*		thread		The OS specific thread data is stored here.
*
*		name		A name to associate with the thread.
*
*		function	The effective entry point.
*
*		arg			The entry argument.
*
*	Returned:
*
*		>= 0		The number of errors encountered.
*
*****************************************************************************/

int gsc_thread_create(	gsc_thread_t*	thread,
						const char*		name,
						int				(*function)(void* arg),
						void*			arg)
{
	_thread_data_t*	data;
	int				errs	= 0;
	int				i;

	for (;;)	// A convenience loop.
	{
		if ((thread == NULL) || (name == NULL) || (function == NULL))
		{
			printf("FAIL <---  (invalid arguments)\n");
			errs++;
			break;
		}

		memset(thread, 0, sizeof(gsc_thread_t));
		data	= malloc(sizeof(_thread_data_t));

		if (data == NULL)
		{
			printf("FAIL <---  (malloc failed)\n");
			errs++;
			break;
		}

		data->function	= function;
		data->arg		= arg;
		i				= pthread_create(	&thread->thread,
											NULL,
											_thread_func,
											data);

		if (i)
		{
			printf("FAIL <---  (thread creation failed)\n");
			free(data);
			errs++;
			break;
		}

		strncpy(thread->name, name, sizeof(thread->name));
		thread->name[sizeof(thread->name) - 1]	= 0;
		break;
	}

	return(errs);
}



/*****************************************************************************
*
*	Function:	gsc_thread_destroy
*
*	Purpose:
*
*		End a thread in a well behaved manner.
*
*	Arguments:
*
*		thread		The thread to end.
*
*	Returned:
*
*		>= 0		The number of errors encountered.
*
*****************************************************************************/

int gsc_thread_destroy(gsc_thread_t* thread)
{
	int	errs	= 0;

	if (thread)
	{
		pthread_join(thread->thread, NULL);
		memset(thread, 0, sizeof(gsc_thread_t));
	}
	else
	{
		printf("FAIL <---  (invalid arguments)\n");
		errs++;
	}

	return(errs);
}


