// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.0.1.0_GSC_DN/utils/util_config.c $
// $Rev: 17640 $
// $Date: 2012-08-20 16:58:49 -0500 (Mon, 20 Aug 2012) $

#include <stdio.h>
#include <string.h>

#include "24dsi_dsl.h"
#include "24dsi_utils.h"
#include "gsc_utils.h"



/******************************************************************************
*
*	Function:	dsi_config_ai
*
*	Purpose:
*
*		Configure the given board using common defaults.
*
*	Arguments:
*
*		fd		The handle to use to access the driver.
*
*		index	The index of the board to access. Ignore if < 0.
*
*		verbose	Work verbosely? If not, then work quietly.
*
*		fref	This is the PLL Fref value, or -1 to use the default.
*
*		fsamp	This is the desired Fsamp rate, of -1 to use the default.
*
*	Returned:
*
*		>= 0	The number of errors encountered here.
*
******************************************************************************/

int	dsi_config_ai(int fd, int index, int verbose, __s32 fref, __s32 fsamp)
{
	int		errs	= 0;
	__s32	ndiv;
	__s32	nrate;	// Legacy
	__s32	nref;	// PLL
	__s32	nvco;	// PLL

	errs	+= dsi_initialize				(fd, index, verbose);
	errs	+= dsi_rx_io_mode				(fd, index, verbose, GSC_IO_MODE_DMA, NULL);
	errs	+= dsi_rx_io_overflow			(fd, index, verbose, DSI_IO_OVERFLOW_CHECK, NULL);
	errs	+= dsi_rx_io_timeout			(fd, index, verbose, 30, NULL);
	errs	+= dsi_rx_io_underflow			(fd, index, verbose, DSI_IO_UNDERFLOW_CHECK, NULL);
	errs	+= dsi_ain_mode					(fd, index, verbose, DSI_AIN_MODE_DIFF, NULL);
	errs	+= dsi_ain_range				(fd, index, verbose, DSI_AIN_RANGE_5V, NULL);
	errs	+= dsi_ain_buf_input			(fd, index, verbose, DSI_AIN_BUF_INPUT_ENABLE, NULL);
	errs	+= dsi_ain_buf_threshold		(fd, index, verbose, 128L * 1024, NULL);
	errs	+= dsi_sw_sync_mode				(fd, index, verbose, DSI_SW_SYNC_MODE_CLR_BUF, NULL);
	errs	+= dsi_channel_order			(fd, index, verbose, DSI_CHANNEL_ORDER_SYNC, NULL);
	errs	+= dsi_data_format				(fd, index, verbose, DSI_DATA_FORMAT_OFF_BIN, NULL);
	errs	+= dsi_data_width				(fd, index, verbose, DSI_DATA_WIDTH_16, NULL);
	errs	+= dsi_init_mode				(fd, index, verbose, DSI_INIT_MODE_INITIATOR, NULL);
	errs	+= dsi_external_clock_source	(fd, index, verbose, DSI_EXT_CLK_SRC_GRP_0, NULL);
	errs	+= dsi_fref_compute				(fd, index, verbose, &fref);
	errs	+= dsi_channel_group_source_all	(fd, index, verbose, DSI_CH_GRP_SRC_GEN_A);
	errs	+= dsi_fsamp_compute			(fd, index, verbose, 0, fref, &fsamp, &nvco, &nref, &nrate, &ndiv);
	errs	+= dsi_rate_gen_x_nvco_all		(fd, index, verbose, nvco);
	errs	+= dsi_rate_gen_x_nref_all		(fd, index, verbose, nref);
	errs	+= dsi_rate_gen_x_nrate_all		(fd, index, verbose, nrate);
	errs	+= dsi_rate_gen_x_ndiv_all		(fd, index, verbose, ndiv);
	errs	+= dsi_fsamp_report_all			(fd, index, verbose, &fref);
	errs	+= dsi_xcvr_type				(fd, index, verbose, DSI_XCVR_TYPE_LVDS, NULL);
	errs	+= dsi_auto_calibrate			(fd, index, verbose);
	errs	+= dsi_ain_buf_clear_at_boundary(fd, index, verbose);
	errs	+= dsi_ain_buf_overflow			(fd, index, verbose, DSI_AIN_BUF_OVERFLOW_CLEAR, NULL);
	errs	+= dsi_ain_buf_underflow		(fd, index, verbose, DSI_AIN_BUF_UNDERFLOW_CLEAR, NULL);

	return(errs);
}


