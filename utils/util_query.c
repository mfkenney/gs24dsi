// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.0.1.0_GSC_DN/utils/util_query.c $
// $Rev: 17640 $
// $Date: 2012-08-20 16:58:49 -0500 (Mon, 20 Aug 2012) $

#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <sys/ioctl.h>

#include "24dsi_dsl.h"
#include "24dsi_utils.h"
#include "gsc_utils.h"



/******************************************************************************
*
*	Function:	dsi_query
*
*	Purpose:
*
*		Provide a silent wrapper for the DSI_IOCTL_QUERY IOCTL service.
*
*	Arguments:
*
*		fd	The handle to use to access the driver.
*
*		index	The index of the board to access. Ignore if < 0.
*
*		verbose	Work verbosely? If not, then work quietly.
*
*		query	The option bein asked about.
*
*		get		The response goes here.
*
*	Returned:
*
*		>= 0	The number of errors encountered here.
*
******************************************************************************/

int	dsi_query(int fd, int index, int verbose, __s32 query, __s32* get)
{
	int	errs	= 0;
	int	i;

	if (get)
	{
		get[0]	= query;
		i		= ioctl(fd, DSI_IOCTL_QUERY, get);

		if (i == -1)
		{
			errs	= 1;
			printf("ERROR: ioctl() failure, errno = %d\n", errno);
		}
	}
	else
	{
		errs	= 1;
		printf("FAIL: internal error\n");
	}

	return(errs);
}



/******************************************************************************
*
*	Function:		dsi_query_fgen_range
*
*	Purpose:
*
*		Read and report the board's Fgen range.
*
*	Arguments:
*
*		fd		The handle for the board to access.
*
*		index	The index of the board to access. Ignore if < 0.
*
*		verbose	Work verbosely? If not, then work quietly.
*
*		min		The minimum value is reported here.
*
*		max		The maximum value is reported here.
*
*	Returned:
*
*		>= 0	The number of errors encounterred.
*
******************************************************************************/

int dsi_query_fgen_range(int fd, int index, int verbose, __s32* min, __s32* max)
{
	int		errs		= 0;
	__s32	range_max;
	__s32	range_min;

	if (verbose)
		gsc_label_index("Fgen Range", index);

	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_FGEN_MIN, &range_min);
	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_FGEN_MAX, &range_max);

	if ((errs == 0) && (verbose))
	{
		gsc_label_long_comma(range_min);
		printf(" to ");
		gsc_label_long_comma(range_max);
		printf(" Hz\n");
	}

	min[0]	= range_min;
	max[0]	= range_max;
	return(errs);
}



/******************************************************************************
*
*	Function:		dsi_query_frequency_control
*
*	Purpose:
*
*		Read and report the board's frequency control mechanism.
*
*	Arguments:
*
*		fd		The handle for the board to access.
*
*		index	The index of the board to access. Ignore if < 0.
*
*		verbose	Work verbosely? If not, then work quietly.
*
*		pll		Does the board support a PLL?
*
*	Returned:
*
*		>= 0	The number of errors encounterred.
*
******************************************************************************/

int dsi_query_frequency_control(int fd, int index, int verbose, __s32* pll)
{
	int	errs;

	if (verbose)
		gsc_label_index("Frequency Control", index);

	errs	= dsi_query(fd, index, 0, DSI_QUERY_PLL_PRESENT, pll);

	if ((errs) || (verbose == 0))
		;
	else if (pll)
		printf("PLL\n");
	else
		printf("Legacy\n");

	return(errs);
}



/******************************************************************************
*
*	Function:		dsi_query_model
*
*	Purpose:
*
*		Read and report the board model.
*
*	Arguments:
*
*		fd		The handle for the board to access.
*
*		index	The index of the board to access. Ignore if < 0.
*
*		verbose	Work verbosely? If not, then work quietly.
*
*		model	The model is reported here.
*
*	Returned:
*
*		>= 0	The number of errors encounterred.
*
******************************************************************************/

int	dsi_query_model(int fd, int index, int verbose, __s32* model)
{
	int			errs	= 0;
	const char*	name;

	if (verbose)
		gsc_label_index("Model", index);

	errs	+= dsi_query(fd, index, 0, DSI_QUERY_DEVICE_TYPE, model);

	if (errs == 0)
	{
		switch (model[0])
		{
			default:

				errs	= 1;
				name	= NULL;
				printf(	"FAIL <---  (unrecognized model: %ld)\n",
						(long) model[0]);
				break;

			case GSC_DEV_TYPE_24DSI6:	name	= "24DSI6";		break;
			case GSC_DEV_TYPE_24DSI12:	name	= "24DSI12";	break;
			case GSC_DEV_TYPE_24DSI32:	name	= "24DSI32";	break;
		}

		if ((errs == 0) && (verbose) && (name))
			printf("%s\n", name);
	}

	return(errs);
}



/******************************************************************************
*
*	Function:		dsi_query_fref_default
*
*	Purpose:
*
*		Read and report the board's default Fref value.
*
*	Arguments:
*
*		fd		The handle for the board to access.
*
*		index	The index of the board to access. Ignore if < 0.
*
*		verbose	Work verbosely? If not, then work quietly.
*
*		def		The default value is reported here.
*
*	Returned:
*
*		>= 0	The number of errors encounterred.
*
******************************************************************************/

int dsi_query_fref_default(int fd, int index, int verbose, __s32* def)
{
	int	errs;

	if (verbose)
		gsc_label_index("Fref Default", index);

	errs	= dsi_query(fd, -1, 0, DSI_QUERY_FREF_DEFAULT, def);

	if ((errs == 0) && (verbose))
	{
		gsc_label_long_comma(def[0]);
		printf(" Hz\n");
	}

	return(errs);
}



/******************************************************************************
*
*	Function:		dsi_query_fsamp_default
*
*	Purpose:
*
*		Read and report the board's default Fsamp value.
*
*	Arguments:
*
*		fd		The handle for the board to access.
*
*		index	The index of the board to access. Ignore if < 0.
*
*		verbose	Work verbosely? If not, then work quietly.
*
*		def		The default value is reported here.
*
*	Returned:
*
*		>= 0	The number of errors encounterred.
*
******************************************************************************/

int dsi_query_fsamp_default(int fd, int index, int verbose, __s32* def)
{
	int	errs;

	if (verbose)
		gsc_label_index("Fsamp Default", index);

	errs	= dsi_query(fd, -1, 0, DSI_QUERY_FSAMP_DEFAULT, def);

	if ((errs == 0) && (verbose))
	{
		gsc_label_long_comma(def[0]);
		printf(" S/S\n");
	}

	return(errs);
}



/******************************************************************************
*
*	Function:		dsi_query_fsamp_range
*
*	Purpose:
*
*		Read and report the board's Fsamp range.
*
*	Arguments:
*
*		fd		The handle for the board to access.
*
*		index	The index of the board to access. Ignore if < 0.
*
*		verbose	Work verbosely? If not, then work quietly.
*
*		min		The minimum value is reported here.
*
*		max		The maximum value is reported here.
*
*	Returned:
*
*		>= 0	The number of errors encounterred.
*
******************************************************************************/

int dsi_query_fsamp_range(int fd, int index, int verbose, __s32* min, __s32* max)
{
	int		errs		= 0;
	__s32	range_max;
	__s32	range_min;

	if (verbose)
		gsc_label_index("Fsamp Range", index);

	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_FSAMP_MIN, &range_min);
	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_FSAMP_MAX, &range_max);

	if ((errs == 0) && (verbose))
	{
		gsc_label_long_comma(range_min);
		printf(" to ");
		gsc_label_long_comma(range_max);
		printf(" S/S\n");
	}

	min[0]	= range_min;
	max[0]	= range_max;
	return(errs);
}



/******************************************************************************
*
*	Function:		dsi_query_ndiv_range
*
*	Purpose:
*
*		Read and report the board's Ndiv range.
*
*	Arguments:
*
*		fd		The handle for the board to access.
*
*		index	The index of the board to access. Ignore if < 0.
*
*		verbose	Work verbosely? If not, then work quietly.
*
*		min		The minimum value is reported here.
*
*		max		The maximum value is reported here.
*
*	Returned:
*
*		>= 0	The number of errors encounterred.
*
******************************************************************************/

int dsi_query_ndiv_range(int fd, int index, int verbose, __s32* min, __s32* max)
{
	int		errs		= 0;
	__s32	range_max;
	__s32	range_min;

	if (verbose)
		gsc_label_index("Ndiv Range", index);

	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_NDIV_MIN, &range_min);
	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_NDIV_MAX, &range_max);

	if ((errs == 0) && (verbose))
	{
		gsc_label_long_comma(range_min);
		printf(" to ");
		gsc_label_long_comma(range_max);
		printf("\n");
	}

	min[0]	= range_min;
	max[0]	= range_max;
	return(errs);
}



/******************************************************************************
*
*	Function:		dsi_query_nrate_range
*
*	Purpose:
*
*		Read and report the board's Nrate range.
*
*	Arguments:
*
*		fd		The handle for the board to access.
*
*		index	The index of the board to access. Ignore if < 0.
*
*		verbose	Work verbosely? If not, then work quietly.
*
*		min		The minimum value is reported here.
*
*		max		The maximum value is reported here.
*
*	Returned:
*
*		>= 0	The number of errors encounterred.
*
******************************************************************************/

int dsi_query_nrate_range(int fd, int index, int verbose, __s32* min, __s32* max)
{
	int		errs		= 0;
	__s32	pll;
	__s32	range_max;
	__s32	range_min;

	if (verbose)
		gsc_label_index("Legacy Nrate Range", index);

	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_PLL_PRESENT, &pll);
	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_NRATE_MIN, &range_min);
	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_NRATE_MAX, &range_max);

	if ((errs) || (verbose == 0))
	{
	}
	else if (pll)
	{
		printf("SKIPPED  (Not supported on this board.)\n");
	}
	else
	{
		gsc_label_long_comma(range_min);
		printf(" to ");
		gsc_label_long_comma(range_max);
		printf("\n");
	}

	min[0]	= range_min;
	max[0]	= range_max;
	return(errs);
}



/******************************************************************************
*
*	Function:		dsi_query_nref_range
*
*	Purpose:
*
*		Read and report the board's Nref range.
*
*	Arguments:
*
*		fd		The handle for the board to access.
*
*		index	The index of the board to access. Ignore if < 0.
*
*		verbose	Work verbosely? If not, then work quietly.
*
*		min		The minimum value is reported here.
*
*		max		The maximum value is reported here.
*
*	Returned:
*
*		>= 0	The number of errors encounterred.
*
******************************************************************************/

int dsi_query_nref_range(int fd, int index, int verbose, __s32* min, __s32* max)
{
	int		errs		= 0;
	__s32	pll;
	__s32	range_max;
	__s32	range_min;

	if (verbose)
		gsc_label_index("PLL Nref Range", index);

	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_PLL_PRESENT, &pll);
	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_NREF_MIN, &range_min);
	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_NREF_MAX, &range_max);

	if ((errs) || (verbose == 0))
	{
	}
	else if (pll == 0)
	{
		printf("SKIPPED  (Not supported on this board.)\n");
	}
	else
	{
		gsc_label_long_comma(range_min);
		printf(" to ");
		gsc_label_long_comma(range_max);
		printf("\n");
	}

	min[0]	= range_min;
	max[0]	= range_max;
	return(errs);
}



/******************************************************************************
*
*	Function:		dsi_query_nvco_range
*
*	Purpose:
*
*		Read and report the board's Nvco range.
*
*	Arguments:
*
*		fd		The handle for the board to access.
*
*		index	The index of the board to access. Ignore if < 0.
*
*		verbose	Work verbosely? If not, then work quietly.
*
*		min		The minimum value is reported here.
*
*		max		The maximum value is reported here.
*
*	Returned:
*
*		>= 0	The number of errors encounterred.
*
******************************************************************************/

int dsi_query_nvco_range(int fd, int index, int verbose, __s32* min, __s32* max)
{
	int		errs		= 0;
	__s32	pll;
	__s32	range_max;
	__s32	range_min;

	if (verbose)
		gsc_label_index("PLL Nvco Range", index);

	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_PLL_PRESENT, &pll);
	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_NVCO_MIN, &range_min);
	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_NVCO_MAX, &range_max);

	if ((errs) || (verbose == 0))
	{
	}
	else if (pll == 0)
	{
		printf("SKIPPED  (Not supported on this board.)\n");
	}
	else
	{
		gsc_label_long_comma(range_min);
		printf(" to ");
		gsc_label_long_comma(range_max);
		printf("\n");
	}

	min[0]	= range_min;
	max[0]	= range_max;
	return(errs);
}


