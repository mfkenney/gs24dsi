// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.0.1.0_GSC_DN/utils/util_rate_gen_x_nvco.c $
// $Rev: 17640 $
// $Date: 2012-08-20 16:58:49 -0500 (Mon, 20 Aug 2012) $

#include <stdio.h>
#include <string.h>

#include "24dsi_dsl.h"
#include "24dsi_utils.h"
#include "gsc_utils.h"


// variables	***************************************************************

static const int	_nvco_ioctl[]	=
{
	DSI_IOCTL_RATE_GEN_A_NVCO,
	DSI_IOCTL_RATE_GEN_B_NVCO
};



//*****************************************************************************
int dsi_rate_gen_x_nvco(int fd, int index, int verbose, int gen, __s32 set, __s32* get)
{
	char	buf[128];
	int		cmd;
	int		errs	= 0;
	__s32	gen_qty;
	__s32	max;
	__s32	min;
	__s32	pll;

	if (verbose)
	{
		sprintf(buf, "Rate Gen %c Nvco", 'A' + gen);
		gsc_label_index(buf, index);
	}

	for (;;)	// A convenience loop.
	{
		if ((gen < 0) || (gen > 1))
		{
			errs	= 1;

			if (verbose)
				printf("FAIL <---  (invalid rate generator index: %d)\n", gen);

			break;
		}

		cmd		= _nvco_ioctl[gen];
		errs	+= dsi_query(fd, -1, 0, DSI_QUERY_PLL_PRESENT, &pll);
		errs	+= dsi_query(fd, -1, 0, DSI_QUERY_RATE_GEN_QTY, &gen_qty);
		errs	+= dsi_query(fd, -1, 0, DSI_QUERY_NVCO_MAX, &max);
		errs	+= dsi_query(fd, -1, 0, DSI_QUERY_NVCO_MIN, &min);

		if (errs)
		{
			break;
		}
		else if (gen >= gen_qty)
		{
			printf("SKIPPED  (Not supported on this board.)\n");
			break;
		}
		else if (pll == 0)
		{
			if (verbose)
				printf("SKIPPED  (Not supported on this board.)\n");

			break;
		}
		else
		{

			set		= (set == -1) ? -1 : (set < min) ? min : ((set > max) ? max : set);
			errs	= dsi_dsl_ioctl(fd, cmd, &set);

			if (verbose)
				printf("%s  (Nvco = %ld)\n", errs ? "FAIL <---" : "PASS", (long) set);
		}

		break;
	}

	if (get)
		get[0]	= set;

	return(errs);
}


//*****************************************************************************
int dsi_rate_gen_x_nvco_all(int fd, int index, int verbose, __s32 set)
{
	int		errs	= 0;
	int		gen;
	__s32	gen_qty;
	__s32	pll;

	if (verbose)
		gsc_label_index("Nvco", index);

	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_RATE_GEN_QTY, &gen_qty);
	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_PLL_PRESENT, &pll);

	if (errs)
	{
	}
	else if (pll == 0)
	{
		if (verbose)
			printf("SKIPPED  (Not supported on this board.)\n");
	}
	else
	{
		if (verbose)
			printf("\n");

		gsc_label_level_inc();

		for (gen = 0; gen < gen_qty; gen++)
			errs	+= dsi_rate_gen_x_nvco(fd, -1, verbose, gen, set, NULL);

		gsc_label_level_dec();
	}

	return(errs);
}


