// $URL: http://subversion:8080/svn/gsc/trunk/drivers/gsc_common/linux/utils/gsc_utils.h $
// $Rev: 21125 $
// $Date: 2013-04-19 13:01:39 -0500 (Fri, 19 Apr 2013) $

#ifndef __GSC_UTILS_H__
#define __GSC_UTILS_H__

#include <pthread.h>
#include <asm/types.h>



// data types	***************************************************************

typedef struct		// For listing register values.
{
	const char*	name;
	int			reg;
	int			err;
	__u32		value;
	int			ask_support;	// Call below function for this.
	int			(*decode)(int fd, int supported, __u32 value, int width);
	const char*	desc;	// Description
} gsc_reg_def_t;

typedef struct
{
	pthread_t	thread;
	char		name[64];
} gsc_thread_t;



// prototypes	***************************************************************

int			gsc_count_boards(const char* base_name);

void		gsc_dev_close(unsigned int index, int fd);
int			gsc_dev_open(unsigned int index, const char* base_name);

int			gsc_id_driver(const char* base_name);
void		gsc_id_host(void);

void		gsc_kbd_close(void);
int			gsc_kbd_hit(void);
void		gsc_kbd_open(void);
int			gsc_kbd_read(void);

void		gsc_label(const char* label);
void		gsc_label_init(int width);
int			gsc_label_indent(int delta);
void		gsc_label_index(const char* label, int index);
void		gsc_label_level_dec(void);
void		gsc_label_level_inc(void);
void		gsc_label_long_comma(long long value);
void		gsc_label_suffix(const char* label, const char* suffix);

const char*	gsc_reg_pex8111_get_desc(unsigned long reg);
const char*	gsc_reg_pex8111_get_name(unsigned long reg);
int			gsc_reg_pex8111_list_pci(int fd, int (reg_read)(int fd, __u32 reg, __u32* value));
int			gsc_reg_pex8111_list_plx(int fd, int (reg_read)(int fd, __u32 reg, __u32* value));

const char*	gsc_reg_pex8112_get_desc(unsigned long reg);
const char*	gsc_reg_pex8112_get_name(unsigned long reg);
int			gsc_reg_pex8112_list_pci(int fd, int (reg_read)(int fd, __u32 reg, __u32* value));
int			gsc_reg_pex8112_list_plx(int fd, int (reg_read)(int fd, __u32 reg, __u32* value));

const char*	gsc_reg_plx9056_get_desc(unsigned long reg);
const char*	gsc_reg_plx9056_get_name(unsigned long reg);
int			gsc_reg_plx9056_list_pci(int fd, int (reg_read)(int fd, __u32 reg, __u32* value));
int			gsc_reg_plx9056_list_plx(int fd, int (reg_read)(int fd, __u32 reg, __u32* value));

const char*	gsc_reg_plx9060es_get_desc(unsigned long reg);
const char*	gsc_reg_plx9060es_get_name(unsigned long reg);
int			gsc_reg_plx9060es_list_pci(int fd, int (reg_read)(int fd, __u32 reg, __u32* value));
int			gsc_reg_plx9060es_list_plx(int fd, int (reg_read)(int fd, __u32 reg, __u32* value));

const char*	gsc_reg_plx9080_get_desc(unsigned long reg);
const char*	gsc_reg_plx9080_get_name(unsigned long reg);
int			gsc_reg_plx9080_list_pci(int fd, int (reg_read)(int fd, __u32 reg, __u32* value));
int			gsc_reg_plx9080_list_plx(int fd, int (reg_read)(int fd, __u32 reg, __u32* value));

const char*	gsc_reg_plx9656_get_desc(unsigned long reg);
const char*	gsc_reg_plx9656_get_name(unsigned long reg);
int			gsc_reg_plx9656_list_pci(int fd, int (reg_read)(int fd, __u32 reg, __u32* value));
int			gsc_reg_plx9656_list_plx(int fd, int (reg_read)(int fd, __u32 reg, __u32* value));

int			gsc_reg_list(
				int				fd,
				gsc_reg_def_t*	list,
				int				detail,
				int				(reg_read)(int fd, __u32 reg, __u32* value));

void		gsc_reg_field_show(
				int				label_width,
				int				name_width,
				__u32			value,
				int				hi_bit,
				int				low_bit,
				int				eol,
				const char**	list,
				const char*		name);

int			gsc_select_1_board(int qty, int* index);
int			gsc_select_2_boards(int qty, int* index_1, int* index_2);

int			gsc_thread_create(gsc_thread_t* thread, const char* name, int (*function)(void* arg), void* arg);
int			gsc_thread_destroy(gsc_thread_t* thread);



#endif
