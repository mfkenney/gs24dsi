// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.0.1.0_GSC_DN/utils/util_wait_event.c $
// $Rev: 11306 $
// $Date: 2011-07-26 09:57:37 -0500 (Tue, 26 Jul 2011) $

#include <stdio.h>
#include <unistd.h>
#include <sys/ioctl.h>

#include "24dsi.h"
#include "24dsi_dsl.h"
#include "24dsi_utils.h"
#include "gsc_utils.h"



/******************************************************************************
*
*	Function:	dsi_wait_event
*
*	Purpose:
*
*		Provide a wrapper for the DSI_IOCTL_WAIT_EVENT service.
*
*	Arguments:
*
*		fd		The handle to use to access the driver.
*
*		wait	This is the crieteria to use.
*
*	Returned:
*
*		>= 0	The number of errors encounterred.
*
******************************************************************************/

int dsi_wait_event(int fd, gsc_wait_t* wait)
{
	int	errs;

	errs	= dsi_dsl_ioctl(fd, DSI_IOCTL_WAIT_EVENT, wait);
	return(errs);
}



