// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.0.1.0_GSC_DN/utils/util_rate_gen_b_nrate.c $
// $Rev: 17640 $
// $Date: 2012-08-20 16:58:49 -0500 (Mon, 20 Aug 2012) $

#include <stdio.h>
#include <string.h>

#include "24dsi_dsl.h"
#include "24dsi_utils.h"
#include "gsc_utils.h"



//*****************************************************************************
int dsi_rate_gen_b_nrate(int fd, int index, int verbose, __s32 set, __s32* get)
{
	int		errs	= 0;
	__s32	gen_qty;
	__s32	max;
	__s32	min;
	__s32	pll;

	if (verbose)
		gsc_label_index("Rate Gen B Nrate", index);

	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_PLL_PRESENT, &pll);
	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_RATE_GEN_QTY, &gen_qty);
	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_NRATE_MAX, &max);
	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_NRATE_MIN, &min);

	if (errs)
	{
	}
	else if ((gen_qty < 2) || (pll == 0))
	{
		if (verbose)
			printf("SKIPPED  (Not supported on this board.)\n");
	}
	else
	{
		set		= (set == -1) ? -1 : (set < min) ? min : ((set > max) ? max : set);
		errs	= dsi_dsl_ioctl(fd, DSI_IOCTL_RATE_GEN_B_NRATE, &set);

		if (verbose)
			printf("%s  (Nrate = %ld)\n", errs ? "FAIL <---" : "PASS", (long) set);
	}

	if (get)
		get[0]	= set;

	return(errs);
}


