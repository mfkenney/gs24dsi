// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.0.1.0_GSC_DN/utils/util_id.c $
// $Rev: 17640 $
// $Date: 2012-08-20 16:58:49 -0500 (Mon, 20 Aug 2012) $

#include <stdio.h>

#include "24dsi_dsl.h"
#include "24dsi_utils.h"
#include "gsc_utils.h"



/******************************************************************************
*
*	Function:	_id_board_pci
*
*	Purpose:
*
*		Identify the board using the PCI registers.
*
*	Arguments:
*
*		fd		The handle used to access the device.
*
*	Returned:
*
*		>= 0	The number of errors encountered here.
*
******************************************************************************/

static int _id_board_pci(int fd)
{
	int		errs	= 0;
	__u32	reg;

	gsc_label("Vendor ID");
	errs	+= dsi_reg_read(fd, GSC_PCI_9080_VIDR, &reg);
	printf("0x%04lX      ", (long) reg);

	if (reg == 0x10B5)
	{
		printf("(PLX)\n");
	}
	else
	{
		errs++;
		printf("(UNKNOWN) FAIL <---\n");
	}

	gsc_label("Device ID");
	errs	+= dsi_reg_read(fd, GSC_PCI_9080_DIDR, &reg);
	printf("0x%04lX      ", (long) reg);

	if (reg == 0x9080)
	{
		printf("(PCI9080)\n");
	}
	else if (reg == 0x9056)
	{
		printf("(PCI9056)\n");
	}
	else
	{
		errs++;
		printf("(UNKNOWN) FAIL <---\n");
	}

	gsc_label("Sub Vendor ID");
	errs	+= dsi_reg_read(fd, GSC_PCI_9080_SVID, &reg);
	printf("0x%04lX      ", (long) reg);

	if (reg == 0x10B5)
	{
		printf("(PLX)\n");
	}
	else
	{
		errs++;
		printf("(UNKNOWN) FAIL <---\n");
	}

	gsc_label("Subsystem ID");
	errs	+= dsi_reg_read(fd, GSC_PCI_9080_SID, &reg);
	printf("0x%04lX      ", (long) reg);

	if (reg == 0x3100)
	{
		printf("(24DSI12 or 24DSI6)\n");
	}
	else if (reg == 0x3540)
	{
		printf("(24DSI12)\n");
	}
	else if (reg == 0x2974)
	{
		printf("(24DSI32)\n");
	}
	else
	{
		errs++;
		printf("(UNKNOWN) FAIL <---\n");
	}

	return(errs);
}



/******************************************************************************
*
*	Function:	dsi_id_board
*
*	Purpose:
*
*		Identify the board.
*
*	Arguments:
*
*		fd		The handle to use to access the board.
*
*		index	The index of the board or -1 if we don't care.
*
*		desc	A description of the board or NULL if we don't care.
*
*	Returned:
*
*		>= 0	The number of errors encountered here.
*
******************************************************************************/

int dsi_id_board(int fd, int index, const char* desc)
{
	__s32	device_type;
	int		errs	= 0;

	gsc_label_index("Board", index);

	if (desc == NULL)
	{
		errs	= dsi_query(fd, -1, 0, DSI_QUERY_DEVICE_TYPE, &device_type);

		if (errs == 0)
		{
			switch (device_type)
			{
				default:

					errs	= 1;
					printf(	"FAIL <---  (unexpected device type: %ld)",
							(long) device_type);
					break;

				case GSC_DEV_TYPE_24DSI6:

					desc	= "24DSI6";
					break;

				case GSC_DEV_TYPE_24DSI12:

					desc	= "24DSI12";
					break;

				case GSC_DEV_TYPE_24DSI32:

					desc	= "24DSI32";
					break;
			}
		}
	}

	printf("%s\n", desc ? desc : "");
	gsc_label_level_inc();

	errs	+= _id_board_pci(fd);

	gsc_label_level_dec();

	return(errs);
}



