// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.0.1.0_GSC_DN/utils/util_rate_div_x_ndiv.c $
// $Rev: 17640 $
// $Date: 2012-08-20 16:58:49 -0500 (Mon, 20 Aug 2012) $

#include <stdio.h>
#include <string.h>

#include "24dsi_dsl.h"
#include "24dsi_utils.h"
#include "gsc_utils.h"



// variables	***************************************************************

static const int	_ndiv_ioctl[]	=
{
	DSI_IOCTL_RATE_DIV_0_NDIV,
	DSI_IOCTL_RATE_DIV_1_NDIV
};



//*****************************************************************************
int dsi_rate_gen_x_ndiv(int fd, int index, int verbose, int divider, __s32 set, __s32* get)
{
	char	buf[128];
	int		cmd;
	int		errs	= 0;
	__s32	groups;
	__s32	max;
	__s32	min;

	if (verbose)
	{
		sprintf(buf, "Rate Divider %d Ndiv", divider);
		gsc_label_index(buf, index);
	}

	for (;;)	// A convenience loop.
	{
		if ((divider < 0) || (divider > 1))
		{
			errs	= 1;

			if (verbose)
				printf("FAIL <---  (invalid rate divider index: %d)\n", divider);

			break;
		}

		cmd		= _ndiv_ioctl[divider];
		errs	+= dsi_query(fd, -1, 0, DSI_QUERY_CHANNEL_GPS, &groups);
		errs	+= dsi_query(fd, -1, 0, DSI_QUERY_NDIV_MAX, &max);
		errs	+= dsi_query(fd, -1, 0, DSI_QUERY_NDIV_MIN, &min);

		if (errs)
		{
			break;
		}
		else if (divider >= groups)
		{
			if (verbose)
				printf("SKIPPED  (Not supported on this board.)\n");

			break;
		}
		else
		{
			set		= (set == -1) ? -1 : (set < min) ? min : ((set > max) ? max : set);
			errs	= dsi_dsl_ioctl(fd, cmd, &set);

			if (verbose)
				printf("%s  (Ndiv = %ld)\n", errs ? "FAIL <---" : "PASS", (long) set);
		}

		break;
	}

	if (get)
		get[0]	= set;

	return(errs);
}



//*****************************************************************************
int dsi_rate_gen_x_ndiv_all(int fd, int index, int verbose, __s32 set)
{
	int		errs	= 0;
	int		divider;
	__s32	qty;

	if (verbose)
			gsc_label_index("Ndiv", index);

	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_RATE_DIV_QTY, &qty);

	if (errs)
	{
	}
	else
	{
		if (verbose)
			printf("\n");

		gsc_label_level_inc();

		for (divider = 0; divider < qty; divider++)
			errs	+= dsi_rate_gen_x_ndiv(fd, index, verbose, divider, set, NULL);

		gsc_label_level_dec();
	}

	return(errs);
}


