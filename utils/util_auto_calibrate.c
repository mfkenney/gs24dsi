// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.0.1.0_GSC_DN/utils/util_auto_calibrate.c $
// $Rev: 17640 $
// $Date: 2012-08-20 16:58:49 -0500 (Mon, 20 Aug 2012) $

#include <stdio.h>

#include "24dsi_dsl.h"
#include "24dsi_utils.h"
#include "gsc_utils.h"



/******************************************************************************
*
*	Function:	dsi_auto_calibrate
*
*	Purpose:
*
*		Provide a visual wrapper for the DSI_IOCTL_AUTO_CALIBRATE IOCTL service.
*
*	Arguments:
*
*		fd		The handle to use to access the driver.
*
*		index	The index of the board to access. Ignore if < 0.
*
*		verbose	Work verbosely? If not, then quietly.
*
*	Returned:
*
*		>= 0	The number of errors encountered here.
*
******************************************************************************/

int	dsi_auto_calibrate(int fd, int index, int verbose)
{
	int		errs	= 0;
	__s32	query;

	if (verbose)
		gsc_label_index("Auto-Calibrate", index);

	errs	+= dsi_query(fd, index, 0, DSI_QUERY_AUTO_CAL_MS, &query);

	if (errs)
	{
	}
	else if (query)
	{
		errs	= dsi_dsl_ioctl(fd, DSI_IOCTL_AUTO_CALIBRATE, NULL);

		if (verbose)
			printf("%s\n", errs ? "FAIL <---" : "PASS");
	}
	else
	{
		if (verbose)
			printf("SKIPPED  (Not supported on this board.)\n");
	}

	return(errs);
}


