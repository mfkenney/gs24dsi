// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.0.1.0_GSC_DN/utils/util_fref.c $
// $Rev: 17640 $
// $Date: 2012-08-20 16:58:49 -0500 (Mon, 20 Aug 2012) $

#include <stdio.h>
#include <string.h>

#include "24dsi_dsl.h"
#include "24dsi_utils.h"
#include "gsc_utils.h"



/******************************************************************************
*
*	Function:	dsi_fref_compute
*
*	Purpose:
*
*		Configure the board, then capture data.
*
*	Arguments:
*
*		fd		The handle for the board to access.
*
*		index	The index of the board being accessed or -1 to ignore.
*
*		verbose	Work verbosely? If not, then work quietly.
*
*		fref	The computed Fref value is put here. If this is >= 0 it is
*				untouched.
*
*	Returned:
*
*		>= 0	The number of errors encounterred.
*
******************************************************************************/

int dsi_fref_compute(int fd, int index, int verbose, __s32* fref)
{
	__s32	def;
	float	delta;
	int		errs	= 0;
	__s32	pll;

	if (verbose)
		gsc_label("Fref");

	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_PLL_PRESENT, &pll);
	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_FREF_DEFAULT, &def);

	if (errs)
	{
		fref[0]	= 0;
	}
	else if (pll == 0)
	{
		if (verbose)
			printf("SKIPPED  (Legacy model)\n");

		fref[0]	= 0;
	}
	else if (fref[0] >= 0)
	{
		if (verbose)
		{
			gsc_label_long_comma(fref[0]);
			printf(" Hz\n");
		}
	}
	else
	{
		errs	= dsi_dsl_ioctl(fd, DSI_IOCTL_PLL_REF_FREQ_HZ, fref);

		if (errs == 0)
		{
			delta	= def * 0.0002;
			delta	= (delta >= 0) ? delta : -delta;

			if (((def - delta) <= fref[0])	||
				((def + delta) >= fref[0]))
			{
				fref[0]	= def;

				if (verbose)
				{
					gsc_label_long_comma(fref[0]);
					printf(" Hz  (default)\n");
				}
			}
			else
			{
				if (verbose)
				{
					printf("~");
					gsc_label_long_comma(fref[0]);
					printf(" Hz  (default)\n");
				}
			}

		}
	}

	return(errs);
}


