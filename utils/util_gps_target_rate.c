// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.0.1.0_GSC_DN/utils/util_gps_target_rate.c $
// $Rev: 17640 $
// $Date: 2012-08-20 16:58:49 -0500 (Mon, 20 Aug 2012) $

#include <stdio.h>
#include <string.h>

#include "24dsi_dsl.h"
#include "24dsi_utils.h"
#include "gsc_utils.h"



/******************************************************************************
*
*	Function:	dsi_gps_target_rate
*
*	Purpose:
*
*		Provide a visual wrapper for the DSI_IOCTL_GPS_TARGET_RATE IOCTL service.
*
*	Arguments:
*
*		fd		The handle to use to access the driver.
*
*		index	The index of the board to access. Ignore if < 0.
*
*		verbose	Work verbosely? If not, then work quietly.
*
*		set		The setting to apply.
*
*		get		The current setting is recorded here, if not NULL.
*
*	Returned:
*
*		>= 0	The number of errors encountered here.
*
******************************************************************************/

int dsi_gps_target_rate(int fd, int index, int verbose, __s32 set, __s32* get)
{
	int		errs	= 0;

	if (verbose)
		gsc_label_index("GPS Target Rate", index);

	errs	= dsi_dsl_ioctl(fd, DSI_IOCTL_GPS_TARGET_RATE, &set);

	if (verbose)
	{
		printf("%s  (", errs ? "FAIL <---" : "PASS");
		gsc_label_long_comma(set);
		printf(" Hz)\n");
	}

	if (get)
		get[0]	= set;

	return(errs);
}


