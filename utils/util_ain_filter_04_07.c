// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.0.1.0_GSC_DN/utils/util_ain_filter_04_07.c $
// $Rev: 17640 $
// $Date: 2012-08-20 16:58:49 -0500 (Mon, 20 Aug 2012) $

#include <stdio.h>
#include <string.h>

#include "24dsi_dsl.h"
#include "24dsi_utils.h"
#include "gsc_utils.h"



/******************************************************************************
*
*	Function:	dsi_ain_filter_04_07
*
*	Purpose:
*
*		Provide a visual wrapper for the DSI_IOCTL_AIN_FILTER_04_07 IOCTL service.
*
*	Arguments:
*
*		fd		The handle to use to access the driver.
*
*		index	The index of the board to access. Ignore if < 0.
*
*		verbose	Work verbosely? If not, then quietly.
*
*		set		The setting to apply.
*
*		get		The current setting is recorded here, if not NULL.
*
*	Returned:
*
*		>= 0	The number of errors encountered here.
*
******************************************************************************/

int	dsi_ain_filter_04_07(int fd, int index, int verbose, __s32 set, __s32* get)
{
	char	buf[128];
	int		errs;

	if (verbose)
		gsc_label_index("Input 04-07 Filter Range", index);

	errs	= dsi_dsl_ioctl(fd, DSI_IOCTL_AIN_FILTER_04_07, &set);

	switch (set)
	{
		default:

			sprintf(buf, "INVALID: %ld", (long) set);
			break;

		case DSI_AIN_FILTER_HI:

			strcpy(buf, "Hi");
			break;

		case DSI_AIN_FILTER_LOW:

			strcpy(buf, "Low");
			break;
	}

	if (verbose)
		printf("%s  (%s)\n", errs ? "FAIL <---" : "PASS", buf);

	if (get)
		get[0]	= set;

	return(errs);
}


