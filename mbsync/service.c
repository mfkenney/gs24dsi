// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.0.1.0_GSC_DN/mbsync/service.c $
// $Rev: 17650 $
// $Date: 2012-08-20 17:14:06 -0500 (Mon, 20 Aug 2012) $

#include <errno.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/ioctl.h>

#include "main.h"



// #defines *******************************************************************

#define	IDBR_CHANNEL_TAG_MASK	0x0F000000

#define	ZERO_V					(0xFFFF/2)
#define	POS_CUTOFF				(0x8CCC)	// +1V on 10V range, +2V on 5V range
#define	NEG_CUTOFF				(0x7333)	// -1V on 10V range, -2V on 5V range
#define	FREQ_MIN				(INPUT_FREQUENCY - 250)
#define	FREQ_MAX				(INPUT_FREQUENCY + 250)



// data types *****************************************************************

typedef struct
{
	int			arg;
	const char*	desc;
} arg_t;



// variables ******************************************************************

static	long	_cycles_min			= (long) (CYCLES_MIN * 0.9);
static	long	_period_min			= (long) (PERIOD_MIN * 0.9);
static	long	_samples_needed		= (long) (SAMPLES_NEEDED * 0.9);



//*****************************************************************************
static void _show_field(int width, const char* title)
{
	static char	buf[1024];

	int	i;
	int	len;

	if (width >= (sizeof(buf) - 2))
		width	= sizeof(buf) - 2;

	if (title == NULL)
		title	= "";

	strncpy(buf, title, sizeof(buf));
	buf[sizeof(buf) - 1]	= 0;
	buf[sizeof(buf) - 2]	= 0;

	if (strcmp(title, "=") == 0)
	{
		// Make the title equal a bunch of '=' chars.

		for (i = 0; i < width; i++)
			buf[i]	= '=';

		buf[i]	= 0;
	}

	len	= strlen(buf);

	for (; len < width; len++)
		strcat(buf, " ");

	printf("%s  ", buf);
	fflush(stdout);
}



//*****************************************************************************
static void _service_title(const char* title)
{
	gsc_label(title);
	fflush(stdout);
}



//*****************************************************************************
static void _service_subtitle(const char* subtitle)
{
	_show_field(15, subtitle);
	fflush(stdout);
}



//*****************************************************************************
static void _service_data(const char* data)
{
	_show_field(10, data);
	fflush(stdout);
}



//*****************************************************************************
static const char* _data_get_desc(const arg_t* list, int arg)
{
	int			i;
	const char*	desc	= NULL;

	for (i = 0; list[i].desc; i++)
	{
		if (list[i].arg == arg)
		{
			desc	= list[i].desc;
			break;
		}
	}

	return(desc);
}



//*****************************************************************************
static int _service_ioctl_data(int fd, long cmd, int arg)
{
	int		errs;
	int		sts;
	__s32	val	= arg;

	sts	= ioctl(fd, cmd, &val);

	if (sts == -1)
	{
		errs	= 1;
		_service_data("FAIL <---");
	}
	else
	{
		errs	= 0;
		_service_data("PASS");
	}

	return(errs);
}



//*****************************************************************************
static int _service_ioctl(
	int				qty,
	const int*		fd_list,
	int				init,
	int				targ,
	const char*		title,
	long			cmd,
	const arg_t*	list)
{
	int			errs		= 0;
	int			i;
	const char*	sub_init	= _data_get_desc(list, init);
	const char*	sub_targ	= _data_get_desc(list, targ);

	if ((init == -1) && (targ == -1))
	{
		// Do nothing.
	}
	else if ((sub_init) && (init != targ))
	{
		// Set the initiator, but not the targets.
		_service_title		(title);
		_service_subtitle	(sub_init);
		errs	+= _service_ioctl_data(fd_list[0], cmd, init);

		for (i = 1; i < qty; i++)
			_service_data("----");

		printf("\n");
	}
	else if ((sub_targ) && (init != targ))
	{
		// Set the targets, but not the initiator.
		_service_title		(title);
		_service_subtitle	(sub_targ);
		_service_data		("----");

		for (i = 1; i < qty; i++)
			errs	+= _service_ioctl_data(fd_list[i], cmd, targ);

		printf("\n");
	}
	else if ((sub_init) && (sub_targ))
	{
		// Set the initiator and the targets.
		_service_title		(title);
		_service_subtitle	(sub_init);

		for (i = 0; i < qty; i++)
			errs	+= _service_ioctl_data(fd_list[i], cmd, targ);

		printf("\n");
	}
	else
	{
		errs++;
		_service_title		(title);
		_service_subtitle	("ERROR");

		if (init == -1)
			_service_data("----");
		else if (sub_init == NULL)
			_service_data("FAIL <---");
		else
			errs	+= _service_ioctl_data(fd_list[0], cmd, init);

		if (targ == -1)
		{
			for (i = 1; i < qty; i++)
				_service_data("----");
		}
		else if (sub_targ == NULL)
		{
			for (i = 1; i < qty; i++)
				_service_data("FAIL <---");
		}
		else
		{
			for (i = 1; i < qty; i++)
				errs	+= _service_ioctl_data(fd_list[i], cmd, targ);
		}

		printf("\n");
	}

	return(errs);
}



//*****************************************************************************
static int _service_query(int qty, const int* fd_list, const char* title, int query, __s32* arg)
{
	char	buf[32];
	int		errs	= 0;
	int		i;
	int		sts;

	_service_title		(title);
	_service_subtitle	("Query");

	for (i = 0; i < qty; i++)
	{
		arg[i]	= query;
		sts		= ioctl(fd_list[i], DSI_IOCTL_QUERY, &arg[i]);

		if (sts == -1)
		{
			errs	= 1;
			_service_data("FAIL <---");
		}
		else
		{
			errs	= 0;
			sprintf(buf, "PASS (%ld)", (long) arg[i]);
			_service_data(buf);
		}
	}

	printf("\n");
	return(errs);
}



//*****************************************************************************
void service_list_init(int qty, const int* i_list)
{
	char	buf[64];
	int		i;

	printf("\n");

	// Field Titles ***************************************
	_service_title		("Setting");
	_service_subtitle	("Value");
	_service_data		("Initiator");

	for (i = 1; i < qty; i++)
		_service_data("Target");

	printf("\n");

	// Field Underlines ***********************************
	_service_title		("=======");
	_service_subtitle	("=");

	for (i = 0; i < qty; i++)
		_service_data("=");

	printf("\n");

	// Board Index Numbers ********************************
	_service_title		("Boards Under Test");
	_service_subtitle	("Index #'s");

	for (i = 0; i < qty; i++)
	{
		sprintf(buf, "%d", i_list[i]);
		_service_data(buf);
	}

	printf("\n");
}



//*****************************************************************************
int service_open(int qty, const int* i_list, int* fd_list)
{
	int		errs	= 0;
	int		i;
	char	name[64];

	_service_title		("Opening Boards");
	_service_subtitle	("");

	for (i = 0; i < qty; i++)
	{
		sprintf(name, "/dev/%s%u", DSI_BASE_NAME, i_list[i]);
		fd_list[i]	= open(name, O_RDWR);

		if (fd_list[i] == -1)
		{
			errs++;
			_service_data("FAIL <---");
		}
		else
		{
			_service_data("PASS");
		}
	}

	printf("\n");
	return(errs);
}



//*****************************************************************************
int service_close(int qty, int* fd_list)
{
	int	errs	= 0;
	int	fd;
	int	i;
	int	sts;

	_service_title		("Closing Boards");
	_service_subtitle	("");

	for (i = 0; i < qty; i++)
	{
		fd			= fd_list[i];
		fd_list[i]	= -1;

		sts	= close(fd);

		if (sts == -1)
		{
			errs++;
			_service_data("FAIL <---");
		}
		else
		{
			_service_data("PASS");
		}
	}

	printf("\n");
	return(errs);
}



//*****************************************************************************
int service_config(int qty, const int* fd_list, __s32 fsamp)
{
	int		err;
	int		errs	= 0;
	int		i;
	char	subtitle[64];

	_service_title		("Basic Configure");
	sprintf(subtitle, "%ld S/S", (long) fsamp);
	_service_subtitle	(subtitle);

	for (i = 0; i < qty; i++)
	{
		err	= dsi_config_ai(fd_list[i], -1, 0, -1, fsamp);

		if (err)
		{
			errs++;
			_service_data("FAIL <---");
		}
		else
		{
			_service_data("PASS");
		}
	}

	printf("\n");
	return(errs);
}



//*****************************************************************************
int service_init_mode(int qty, const int* fd_list, int init, int targ)
{
	static const arg_t	list[]	=
	{
		// arg						desc
		{ DSI_INIT_MODE_TARGET,		"Target"	},
		{ DSI_INIT_MODE_INITIATOR,	"Initiator"	},
		{ -1, NULL }
	};

	int			errs	= 0;
	const char*	title	= "Initiator Mode";

	if (init == targ)
	{
		errs	+= _service_ioctl(qty, fd_list, init, targ, title, DSI_IOCTL_INIT_MODE, list);
	}
	else
	{
		errs	+= _service_ioctl(qty, fd_list, init, -1, title, DSI_IOCTL_INIT_MODE, list);
		errs	+= _service_ioctl(qty, fd_list, -1, targ, title, DSI_IOCTL_INIT_MODE, list);
	}

	return(errs);
}



//*****************************************************************************
int service_channel_group_source(int qty, const int* fd_list, int init, int targ)
{
	static const arg_t	list[]	=
	{
		// arg							desc
		{ DSI_CH_GRP_SRC_GEN_A,			"Rate-A Gen"	},
		{ DSI_CH_GRP_SRC_GEN_B,			"Rate-B Gen"	},
		{ DSI_CH_GRP_SRC_EXTERN,		"External"		},
		{ DSI_CH_GRP_SRC_DIR_EXTERN,	"Direct Extern"	},
		{ DSI_CH_GRP_SRC_DISABLE,		"Disable"		},
		{ DSI_CH_GRP_SRC_ENABLE,		"Enable"		},
		{ -1, NULL }
	};

	static const int	cmd[]	=
	{
		DSI_IOCTL_CH_GRP_0_SRC,
		DSI_IOCTL_CH_GRP_1_SRC,
		DSI_IOCTL_CH_GRP_2_SRC,
		DSI_IOCTL_CH_GRP_3_SRC
	};

	int			errs	= 0;
	__s32		groups;
	int			i;
	char		title[64];

	errs	+= dsi_query(fd_list[0], -1, 0, DSI_QUERY_CHANNEL_GPS, &groups);


	for (i = 0; i < groups; i++)
	{
		sprintf(title, "Channel Group %d Source", i);

		if (init == targ)
		{
			errs	+= _service_ioctl(qty, fd_list, init, targ, title, cmd[i], list);
		}
		else
		{
			errs	+= _service_ioctl(qty, fd_list, init, -1, title, cmd[i], list);
			errs	+= _service_ioctl(qty, fd_list, -1, targ, title, cmd[i], list);
		}
	}

	return(errs);
}



//*****************************************************************************
int service_extern_clock_source(int qty, const int* fd_list, int init, int targ)
{
	static const arg_t	list[]	=
	{
		// arg						desc
		{ DSI_EXT_CLK_SRC_GRP_0,	"Group 0"		},
		{ DSI_EXT_CLK_SRC_GEN_A,	"Rate-A Gen"	},
		{ -1, NULL }
	};

	int			errs	= 0;
	const char*	title	= "Ext Clock Source";

	if (init == targ)
	{
		errs	+= _service_ioctl(qty, fd_list, init, targ, title, DSI_IOCTL_EXT_CLK_SRC, list);
	}
	else
	{
		errs	+= _service_ioctl(qty, fd_list, init, -1, title, DSI_IOCTL_EXT_CLK_SRC, list);
		errs	+= _service_ioctl(qty, fd_list, -1, targ, title, DSI_IOCTL_EXT_CLK_SRC, list);
	}

	return(errs);
}



//*****************************************************************************
int service_sw_sync_mode(int qty, const int* fd_list, int init, int targ)
{
	static const arg_t	list[]	=
	{
		// arg						desc
		{ DSI_SW_SYNC_MODE_SW_SYNC,	"SW Sync"		},
		{ DSI_SW_SYNC_MODE_CLR_BUF,	"Clear Buffer"	},
		{ -1, NULL }
	};

	int			errs	= 0;
	const char*	title	= "SW Sync Mode";

	if (init == targ)
	{
		errs	+= _service_ioctl(qty, fd_list, init, targ, title, DSI_IOCTL_SW_SYNC_MODE, list);
	}
	else
	{
		errs	+= _service_ioctl(qty, fd_list, init, -1, title, DSI_IOCTL_SW_SYNC_MODE, list);
		errs	+= _service_ioctl(qty, fd_list, -1, targ, title, DSI_IOCTL_SW_SYNC_MODE, list);
	}

	return(errs);
}



//*****************************************************************************
int service_sw_sync(int qty, const int* fd_list, int init, int targ)
{
	static const arg_t	list[]	=
	{
		// arg	desc
		{ 0,	""	},
		{ -1, NULL }
	};

	int			errs	= 0;
	const char*	title	= "SW Sync";

	if (init == targ)
	{
		errs	+= _service_ioctl(qty, fd_list, init, targ, title, DSI_IOCTL_SW_SYNC, list);
	}
	else
	{
		errs	+= _service_ioctl(qty, fd_list, init, -1, title, DSI_IOCTL_SW_SYNC, list);
		errs	+= _service_ioctl(qty, fd_list, -1, targ, title, DSI_IOCTL_SW_SYNC, list);
	}

	return(errs);
}



//*****************************************************************************
int service_channel_ready(int qty, const int* fd_list, int init, int targ)
{
	static const arg_t	list[]	=
	{
		// arg	desc
		{ 0,	""	},
		{ -1, NULL }
	};

	int			errs	= 0;
	const char*	title	= "Channels Ready";

	if (init == targ)
	{
		errs	+= _service_ioctl(qty, fd_list, init, targ, title, DSI_IOCTL_CHANNELS_READY, list);
	}
	else
	{
		errs	+= _service_ioctl(qty, fd_list, init, -1, title, DSI_IOCTL_CHANNELS_READY, list);
		errs	+= _service_ioctl(qty, fd_list, -1, targ, title, DSI_IOCTL_CHANNELS_READY, list);
	}

	return(errs);
}



//*****************************************************************************
int service_read(int qty, const int* fd_list, read_data_t* data)
{
	long		bytes;
	int			errs	= 0;
	const char*	factor;
	int			i;
	long		l;
	long		size;
	char		subtitle[64];
	const char*	title	= "Read Data";

	factor	= "";
	size	= sizeof(data[0].data) / 4;

	if ((size % 1024) == 0)
	{
		size	= size / 1024;
		factor	= "K";
	}

	if ((size % 1024) == 0)
	{
		size	= size / 1024;
		factor	= "M";
	}

	sprintf(subtitle, "%ld%s samples", size, factor);

	_service_title		(title);
	_service_subtitle	(subtitle);
	bytes	= sizeof(data[0].data);

	for (i = 0; i < qty; i++)
	{
		l	= read(fd_list[i], &data[i].data, bytes);

		if (l == bytes)
			_service_data("PASS");
		else
			_service_data("FAIL <---");
	}

	printf("\n");
	return(errs);
}



//*****************************************************************************
int service_rx_io_overflow(int qty, const int* fd_list, int init, int targ)
{
	static const arg_t	list[]	=
	{
		// arg						desc
		{ DSI_IO_OVERFLOW_IGNORE,	"Ignore"	},
		{ DSI_IO_OVERFLOW_CHECK,	"Check"		},
		{ -1, NULL }
	};

	int			errs	= 0;
	const char*	title	= "Rx I/O Overflow";

	if (init == targ)
	{
		errs	+= _service_ioctl(qty, fd_list, init, targ, title, DSI_IOCTL_RX_IO_OVERFLOW, list);
	}
	else
	{
		errs	+= _service_ioctl(qty, fd_list, init, -1, title, DSI_IOCTL_RX_IO_OVERFLOW, list);
		errs	+= _service_ioctl(qty, fd_list, -1, targ, title, DSI_IOCTL_RX_IO_OVERFLOW, list);
	}

	return(errs);
}



//*****************************************************************************
int service_low_power(int qty, const int* fd_list, int* lp)
{
	__s32		args[8];
	int			errs	= 0;
	int			i;
	const char*	title	= "Low Power";

	errs	+= _service_query(qty, fd_list, title, DSI_QUERY_D20_LOW_POWER, args);
	lp[0]	= 0;

	for (i = 0; i < qty; i++)
	{
		if (args[i])
			lp[0]	= 1;
	}

	return(errs);
}



//*****************************************************************************
int service_ain_range(int qty, const int* fd_list, int arg)
{
	static const arg_t	list[]	=
	{
		// arg					desc
		{ DSI_AIN_RANGE_2_5V,	"+-2.5 Volts"	},
		{ DSI_AIN_RANGE_5V,		"+-5 Volts"		},
		{ DSI_AIN_RANGE_10V,	"+-10 Volts"	},
		{ -1, NULL }
	};

	int			errs	= 0;
	const char*	title	= "AI Range";

	errs	+= _service_ioctl(qty, fd_list, arg, arg, title, DSI_IOCTL_AIN_RANGE, list);
	return(errs);
}



//*****************************************************************************
int service_data_width(int qty, const int* fd_list, int arg)
{
	static const arg_t	list[]	=
	{
		// arg					desc
		{ DSI_DATA_WIDTH_16,	"16-bits"	},
		{ DSI_DATA_WIDTH_18,	"18-bits"	},
		{ DSI_DATA_WIDTH_20,	"20-bits"	},
		{ DSI_DATA_WIDTH_24,	"24-bits"	},
		{ -1, NULL }
	};

	int			errs	= 0;
	const char*	title	= "Data Width";

	errs	+= _service_ioctl(qty, fd_list, arg, arg, title, DSI_IOCTL_DATA_WIDTH, list);
	return(errs);
}



//*****************************************************************************
int service_auto_cal(int qty, const int* fd_list)
{
	static const arg_t	list[]	=
	{
		// arg	desc
		{ 0,	""	},
		{ -1, NULL }
	};

	int			errs	= 0;
	const char*	title	= "Auto-Calibration";

	errs	+= _service_ioctl(qty, fd_list, 0, 0, title, DSI_IOCTL_AUTO_CALIBRATE, list);
	return(errs);
}



//*****************************************************************************
int service_clear(int qty, const int* fd_list)
{
	static const arg_t	list[]	=
	{
		// arg	desc
		{ 0,	""	},
		{ -1, NULL }
	};

	int			errs	= 0;
	const char*	title	= "Clear Buffer";

	errs	+= _service_ioctl(qty, fd_list, 0, 0, title, DSI_IOCTL_AIN_BUF_CLEAR, list);
	return(errs);
}



//*****************************************************************************
int service_settling(int qty)
{
	char	buf[64];
	int		i;
	int		time	= 2;

	_service_title		("Settling");
	sprintf(buf, "%d Second%s", time, (time == 1) ? "" : "s");
	_service_subtitle	(buf);
	fflush(stdout);
	sleep(time);

	for (i = 0; i < qty; i++)
		_service_data("----");

	printf("\n");
	return(0);
}



//*****************************************************************************
int service_data_format(int qty, const int* fd_list, int arg)
{
	static const arg_t	list[]	=
	{
		// arg						desc
		{ DSI_DATA_FORMAT_2S_COMP,	"Twos Compliment"	},
		{ DSI_DATA_FORMAT_OFF_BIN,	"Offset Binary"		},
		{ -1, NULL }
	};

	int			errs	= 0;
	const char*	title	= "Data Format";

	errs	+= _service_ioctl(qty, fd_list, arg, arg, title, DSI_IOCTL_DATA_FORMAT, list);
	return(errs);
}



//*****************************************************************************
int service_channel_order(int qty, const int* fd_list, int arg)
{
	static const arg_t	list[]	=
	{
		// arg						desc
		{ DSI_CHANNEL_ORDER_SYNC,	"Synchronous"	},
		{ DSI_CHANNEL_ORDER_ASYNC,	"Asynchronous"	},
		{ -1, NULL }
	};

	int			errs	= 0;
	const char*	title	= "Channel Order";

	errs	+= _service_ioctl(qty, fd_list, arg, arg, title, DSI_IOCTL_CHANNEL_ORDER, list);
	return(errs);
}



//*****************************************************************************
int service_voltage_range_examine(
	int					qty,
	const read_data_t*	data,
	volt_data_t*		v_data,
	int					lp)		// Is a Low Power board present?
{
	__u16	dmax;
	__u16	dmin;
	int		errs		= 0;
	int		i;
	int		s;
	int		samples;
	int		size		= ARRAY_ELEMENTS(data->data);
	__u16	value;
	float	vmax;
	float	vmin;

	_service_title		("Examining Data");
	_service_subtitle	("Voltage Levels");

	for (i = 0; i < qty; i++)
	{
		samples	= 0;

		for (s = EXAMINE_OFFSET; s < size; s++)
		{
			if (data[i].data[s] & IDBR_CHANNEL_TAG_MASK)
			{
				// We only look at channel 0.
				continue;
			}

			samples++;
			value	= data[i].data[s] & 0xFFFF;;

			if (samples == 1)
				dmax	= value;
			else if (dmax < value)
				dmax	= value;

			if (samples == 1)
				dmin	= value;
			else if (dmin > value)
				dmin	= value;
		}

		v_data[i].ch_0_samples	= samples;

		if (lp)
		{
			vmax	= 10.0 / 0x10000 * dmax - 5.0;
			vmin	= 10.0 / 0x10000 * dmin - 5.0;
		}
		else
		{
			vmax	= 20.0 / 0x10000 * dmax - 10.0;
			vmin	= 20.0 / 0x10000 * dmin - 10.0;
		}

		v_data[i].vmax	= vmax;
		v_data[i].vmin	= vmin;
		_service_data("Done");
	}

	printf("\n");
	return(errs);
}



//*****************************************************************************
int service_voltage_range_samples(int qty, volt_data_t* v_data)
{
	char	buf[64];
	int		errs		= 0;
	int		i;

	_service_title("Min Samples In Data Set");
	sprintf(buf, ">= %ld", _samples_needed);
	_service_subtitle(buf);

	for (i = 0; i < qty; i++)
	{
		if (v_data[i].ch_0_samples >= _samples_needed)
		{
			sprintf(buf, "PASS %ld", (long) v_data[i].ch_0_samples);
			_service_data(buf);
		}
		else
		{
			errs++;
			v_data[i].ignore	= 1;
			sprintf(buf, "FAIL %ld", (long) v_data[i].ch_0_samples);
			_service_data(buf);
		}
	}

	printf("\n");
	return(errs);
}



//*****************************************************************************
int service_voltage_range_vmax(int qty, volt_data_t* v_data, int lp)
{
	char	buf[64];
	int		errs		= 0;
	int		i;
	float	limit_max	= lp ? +4.9 : +9.5;
	float	limit_min	= lp ? +4.5 : +9.0;

	_service_title("Max Voltage Level");
	sprintf(buf, "%.1f to %.1f", limit_min, limit_max);
	_service_subtitle(buf);

	for (i = 0; i < qty; i++)
	{
		if (v_data[i].vmax > limit_max)
		{
			errs++;
			v_data[i].ignore	= 1;
			sprintf(buf, "FAIL %.2f", v_data[i].vmax);
			_service_data(buf);
		}
		else if (v_data[i].vmax < limit_min)
		{
			errs++;
			v_data[i].ignore	= 1;
			sprintf(buf, "FAIL %.2f", v_data[i].vmax);
			_service_data(buf);
		}
		else
		{
			sprintf(buf, "PASS %.2f", v_data[i].vmax);
			_service_data(buf);
		}
	}

	printf("\n");
	return(errs);
}



//*****************************************************************************
int service_voltage_range_vmin(int qty, volt_data_t* v_data, int lp)
{
	char	buf[64];
	int		errs		= 0;
	int		i;
	float	limit_max	= lp ? -4.5 : -9.0;
	float	limit_min	= lp ? -4.9 : -9.5;

	_service_title("Min Voltage Level");
	sprintf(buf, "%.1f to %.1f", limit_min, limit_max);
	_service_subtitle(buf);

	for (i = 0; i < qty; i++)
	{
		if (v_data[i].vmin > limit_max)
		{
			errs++;
			v_data[i].ignore	= 1;
			sprintf(buf, "FAIL %.2f", v_data[i].vmin);
			_service_data(buf);
		}
		else if (v_data[i].vmin < limit_min)
		{
			errs++;
			v_data[i].ignore	= 1;
			sprintf(buf, "FAIL %.2f", v_data[i].vmin);
			_service_data(buf);
		}
		else
		{
			sprintf(buf, "PASS %.2f", v_data[i].vmin);
			_service_data(buf);
		}
	}

	printf("\n");
	return(errs);
}



//*****************************************************************************
int service_data_rate_examine(int qty, const read_data_t* data, volt_data_t* v_data)
{
	enum
	{
		UNKNOWN,
		POSITIVE,
		NEGATIVE
	} state	= UNKNOWN;

	long	cycles;
	int		errs		= 0;
	int		i;
	float	period;
	int		s;
	long	samples;
	int		size		= ARRAY_ELEMENTS(data->data);
	long	start;
	long	sum;
	__u32	value;

	_service_title		("Examining Data");
	_service_subtitle	("Rate Content");

	for (i = 0; i < qty; i++)
	{
		cycles	= 0;
		samples	= 0;
		start	= 0;
		sum		= 0;

		for (s = EXAMINE_OFFSET; s < size; s++)
		{
			if (data[i].data[s] & IDBR_CHANNEL_TAG_MASK)
			{
				// We only look at channel 0.
				continue;
			}

			samples++;
			value	= data[i].data[s] & 0xFFFF;;

			if (state == UNKNOWN)
			{
				if (value > POS_CUTOFF)
					state	= POSITIVE;
				else if (value < NEG_CUTOFF)
					state	= NEGATIVE;
			}
			else if (state == POSITIVE)
			{
				if (value < NEG_CUTOFF)
					state	= NEGATIVE;
			}
			else // state == NEGATIVE
			{
				if (value > POS_CUTOFF)
				{
					state	= POSITIVE;

					if (start == 0)
					{
						start	= samples;
					}
					else
					{
						sum		+= samples - start;
						start	= samples;
						cycles++;
					}
				}
			}
		}

		if (cycles)
			period				= sum / cycles;
		else
			period				= 0;

		v_data[i].ch_0_cycles	= cycles;
		v_data[i].ch_0_period	= period;
		_service_data("Done");
	}

	printf("\n");
	return(errs);
}



//*****************************************************************************
int service_data_rate_cycles(int qty, volt_data_t* v_data)
{
	char	buf[64];
	int		errs		= 0;
	int		i;

	_service_title("Min Cycles In Data Set");
	sprintf(buf, ">= %ld", _cycles_min);
	_service_subtitle(buf);

	for (i = 0; i < qty; i++)
	{
		if (v_data[i].ch_0_cycles >= _cycles_min)
		{
			sprintf(buf, "PASS %ld", v_data[i].ch_0_cycles);
			_service_data(buf);
		}
		else
		{
			errs++;
			v_data[i].ignore	= 1;
			sprintf(buf, "FAIL %ld", v_data[i].ch_0_cycles);
			_service_data(buf);
		}
	}

	printf("\n");
	return(errs);
}



//*****************************************************************************
int service_data_rate_period(int qty, volt_data_t* v_data)
{
	char	buf[64];
	int		errs		= 0;
	int		i;

	_service_title("Min Samples Per Cycle");
	sprintf(buf, ">= %ld", _period_min);
	_service_subtitle(buf);

	for (i = 0; i < qty; i++)
	{
		if (v_data[i].ch_0_period >= _period_min)
		{
			sprintf(buf, "PASS %.2f", v_data[i].ch_0_period);
			_service_data(buf);
		}
		else
		{
			errs++;
			v_data[i].ignore	= 1;
			sprintf(buf, "FAIL %.2f", v_data[i].ch_0_period);
			_service_data(buf);
		}
	}

	printf("\n");
	return(errs);
}



//*****************************************************************************
int service_data_rate_fsamp(int qty, const int* fd_list, volt_data_t* v_data)
{
	char	buf[64];
	int		err;
	int		errs		= 0;
	__s32	fref;
	__s32	fsamp;
	int		i;
	int		sts;

	_service_title		("Sample Rate");
	_service_subtitle	("Calculated");

	for (i = 0; i < qty; i++)
	{
		fref	= DSI_QUERY_FREF_DEFAULT;
		sts		= ioctl(fd_list[i], DSI_IOCTL_QUERY, &fref);
		err		= sts ? 1 : 0;
		err		+= dsi_fsamp_report(fd_list[i], i, 0, 0, &fref, &fsamp);

		if (err)
		{
			errs++;
			_service_data("FAIL <---");
		}
		else
		{
			v_data[i].fref	= fref;
			v_data[i].fsamp	= fsamp;
			sprintf(buf, "%ld", (long) fsamp);
			_service_data(buf);
		}
	}

	printf("\n");
	return(errs);
}



//*****************************************************************************
int service_data_rate_frequency(int qty, const int* fd_list, volt_data_t* v_data)
{
	char	buf[64];
	int		errs		= 0;
	int		i;

	_service_title		("Frequency");
	sprintf(buf, "%ld to %ld", (long) FREQ_MIN, (long) FREQ_MAX);
	_service_subtitle	(buf);

	for (i = 0; i < qty; i++)
	{
		v_data[i].freq	= v_data[i].ch_0_period ? (v_data[i].fsamp / v_data[i].ch_0_period) : 0;

		if ((v_data[i].freq < FREQ_MIN) || (v_data[i].freq > FREQ_MAX))
		{
			errs++;
			sprintf(buf, "FAIL %ld", (long) v_data[i].freq);
			_service_data(buf);
		}
		else
		{
			sprintf(buf, "PASS %ld", (long) v_data[i].freq);
			_service_data(buf);
		}
	}

	printf("\n");
	return(errs);
}



//*****************************************************************************
int service_data_rate_phase(int qty, const read_data_t* data, volt_data_t* v_data, int lp)
{
	#define	DEGREES			(12)
	// We're looking for any measurement at or less than about this number
	// of degrees from zero volts. This is the range of highest voltage
	// change in the sine wave input. Looking here will give us the best
	// measure of phase difference between the two boards.

	char	buf[64];
	int		errs		= 0;
	int		i;
	long	index;
	long	ini_d;
	float	pie			= 3.1415926535898;
	long	phase_adc;
	float	phase_deg	= 0;
	float	phase_ns;
	float	phase_rad;
	float	phase_vol;
	float	pp			= v_data[0].vmax - v_data[0].vmin;
	long	range_adc;
	float	range_deg;
	long	range_max;
	long	range_min;
	float	range_rad;
	float	range_vol;
	long	s;
	long	size		= ARRAY_ELEMENTS(data->data);
	long	trg_d;
	int		valid		= 0;
	__u32	value;

	// Compute the range for the input values we can look for.
	range_deg	= DEGREES;
	range_rad	= range_deg * pie / 180;
	range_vol	= (pp / 2) * sin(range_rad);
	range_adc	= range_vol * 65536 / (lp ? 10 : 20);
	range_min	= (65536L / 2) - range_adc;
	range_max	= (65536L / 2) + range_adc;

	// Locate the first suitable value in the channel zero data.

	for (s = EXAMINE_OFFSET; s < size; s++)
	{
		if (data[0].data[s] & IDBR_CHANNEL_TAG_MASK)
		{
			// We only look at channel 0.
			continue;
		}

		value	= data[0].data[s] & 0xFFFF;

		if ((value > range_max) || (value < range_min))
			continue;

		index	= s;
		valid	= 1;
		break;
	}

	// Compute the results for each channel.

	if (valid)
	{
		// Show the results in nano-seconds ---------------
		_service_title		("Phase");
		_service_subtitle	("nano-seconds");

		for (i = 0; i < qty; i++)
		{
			if (i == 0)
			{
				_service_data("Reference");
				continue;
			}

			// Compute the phase difference between the signals.
			ini_d				= data[0].data[index] & 0xFFFF;
			trg_d				= data[i].data[index] & 0xFFFF;
			phase_adc			= labs(ini_d - trg_d);
			phase_vol			= (lp ? 10.0 : 20.0) * phase_adc / 65536;
			phase_rad			= asin(phase_vol * 2 / pp);
			phase_deg			= phase_rad * 180 / pie;
			phase_ns			= phase_deg / v_data[i].freq / 360 * 1000000000L;
			v_data[i].phase_ns	= phase_ns;
			v_data[i].phase_deg	= phase_deg;
			sprintf(buf, "%.0f", v_data[i].phase_ns);
			_service_data(buf);
		}

		printf("\n");

		// Show the results in degrees --------------------
		_service_title		("Phase");
		_service_subtitle	("degrees");

		for (i = 0; i < qty; i++)
		{
			if (i == 0)
			{
				_service_data("Reference");
				continue;
			}

			sprintf(buf, "%.2f", v_data[i].phase_deg);
			_service_data(buf);
		}

		printf("\n");
	}
	else
	{
		_service_title		("Phase");
		_service_subtitle	("Too Little Data");
		errs++;

		for (i = 0; i < qty; i++)
			_service_data("FAIL <---");

		printf("\n");
	}

	return(errs);
}


