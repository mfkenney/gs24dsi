// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.0.1.0_GSC_DN/mbsync/validate.c $
// $Rev: 17650 $
// $Date: 2012-08-20 17:14:06 -0500 (Mon, 20 Aug 2012) $

#include <errno.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/timeb.h>

#include "main.h"



// variables ******************************************************************

static	read_data_t		data[8];
static	volt_data_t		v_data[8];



//*****************************************************************************
int validate_sync(int qty, const int* fd_list, int errs, int lp)
{
	gsc_label ("Validation");
	printf("\n");
	gsc_label_level_inc();

	if (errs)
	{
		printf("\t SKIPPED DUE TO ERRORS.\n");
	}
	else
	{
		errs	+= service_read					(qty, fd_list, data);
		errs	+= service_voltage_range_examine(qty, data, v_data, lp);
		errs	+= service_voltage_range_samples(qty, v_data);
		errs	+= service_voltage_range_vmax	(qty, v_data, lp);
		errs	+= service_voltage_range_vmin	(qty, v_data, lp);
		errs	+= service_data_rate_fsamp		(qty, fd_list, v_data);
		errs	+= service_data_rate_examine	(qty, data, v_data);
		errs	+= service_data_rate_cycles		(qty, v_data);
		errs	+= service_data_rate_period		(qty, v_data);
		errs	+= service_data_rate_frequency	(qty, fd_list, v_data);
		errs	+= service_data_rate_phase		(qty, data, v_data, lp);
	}

	gsc_label_level_dec();
	return(errs);
}


