// This software is covered by the GNU GENERAL PUBLIC LICENSE (GPL).
// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.0.1.0_GSC_DN/driver/24dsi.h $
// $Rev: 21127 $
// $Date: 2013-04-19 13:17:59 -0500 (Fri, 19 Apr 2013) $

#ifndef __24DSI_H__
#define __24DSI_H__

#include <asm/types.h>
#include <linux/ioctl.h>
#include <linux/types.h>

#include "gsc_common.h"
#include "gsc_pci9056.h"
#include "gsc_pci9080.h"



// #defines	*******************************************************************

#define	DSI_BASE_NAME						"24dsi"
#define	DSI_DEV_BASE_NAME					"/dev/" DSI_BASE_NAME

#define	DSI_DIVISOR(ndiv)					((ndiv) ? (1. * (ndiv)) : 0.5)

// Presumes default reference clock of 32.768 MHz
#define	DSI_FGEN_PLL_HZ(fref,nvco,nref)		(((float)(fref)*(nvco))/(nref))
#define	DSI_FSAMP_PLL_SPS(fr,vco,ref,div)	(DSI_FGEN_PLL_HZ((fr),(vco),(ref))\
											/(512.0*DSI_DIVISOR((div))))

#define	DSI_FGEN_PLL_MHZ(fref,nvco,nref)	((((fref) / 1000000.) * (nvco)) / (nref))
#define	DSI_FSAMP_PLL_MSPS(nvco,nref,ndiv)	(DSI_FGEN_PLL_MHZ((fref),(nvco),(nref))\
											/(512.0 * DSI_DIVISOR(ndiv)))

#define	DSI_FGEN_LEGACY_HZ(nrate)			(25600000.*(1.+(((float)(nrate))/100000)))
#define	DSI_FSAMP_LEGACY_SPS(nrate,ndiv)	(DSI_FGEN_LEGACY_HZ((nrate))\
											/(512.0 * DSI_DIVISOR((ndiv))))

#define	DSI_FGEN_LEGACY_MHZ(nrate)			(25.6*(1.+((1.*(nrate))/100000L)))
#define	DSI_FSAMP_LEGACY_MSPS(nrate,ndiv)	(DSI_FGEN_LEGACY_MHZ((nrate))\
											/(512.0 * DSI_DIVISOR((ndiv))))



// IOCTL command codes
#define	DSI_IOCTL_REG_READ					_IOWR(GSC_IOCTL,  0, gsc_reg_t)
#define	DSI_IOCTL_REG_WRITE					_IOWR(GSC_IOCTL,  1, gsc_reg_t)
#define	DSI_IOCTL_REG_MOD					_IOWR(GSC_IOCTL,  2, gsc_reg_t)
#define DSI_IOCTL_QUERY						_IOWR(GSC_IOCTL,  3, __s32)
#define DSI_IOCTL_INITIALIZE				_IO  (GSC_IOCTL,  4)
#define DSI_IOCTL_AUTO_CALIBRATE			_IO  (GSC_IOCTL,  5)
#define DSI_IOCTL_RX_IO_MODE				_IOWR(GSC_IOCTL,  6, __s32)
#define DSI_IOCTL_RX_IO_OVERFLOW			_IOWR(GSC_IOCTL,  7, __s32)
#define DSI_IOCTL_RX_IO_TIMEOUT				_IOWR(GSC_IOCTL,  8, __s32)
#define DSI_IOCTL_RX_IO_UNDERFLOW			_IOWR(GSC_IOCTL,  9, __s32)
#define DSI_IOCTL_AIN_BUF_CLEAR				_IO  (GSC_IOCTL, 10)
#define DSI_IOCTL_AIN_BUF_INPUT				_IOWR(GSC_IOCTL, 11, __s32)
#define DSI_IOCTL_AIN_BUF_OVERFLOW			_IOWR(GSC_IOCTL, 12, __s32)
#define DSI_IOCTL_AIN_BUF_THRESH			_IOWR(GSC_IOCTL, 13, __s32)
#define DSI_IOCTL_AIN_BUF_UNDERFLOW			_IOWR(GSC_IOCTL, 14, __s32)
#define DSI_IOCTL_AIN_FILTER				_IOWR(GSC_IOCTL, 15, __s32)
#define DSI_IOCTL_AIN_FILTER_SEL			_IOWR(GSC_IOCTL, 16, __s32)
#define DSI_IOCTL_AIN_FILTER_00_03			_IOWR(GSC_IOCTL, 17, __s32)
#define DSI_IOCTL_AIN_FILTER_04_07			_IOWR(GSC_IOCTL, 18, __s32)
#define DSI_IOCTL_AIN_FILTER_08_11			_IOWR(GSC_IOCTL, 19, __s32)
#define DSI_IOCTL_AIN_FILTER_12_15			_IOWR(GSC_IOCTL, 20, __s32)
#define DSI_IOCTL_AIN_FILTER_16_19			_IOWR(GSC_IOCTL, 21, __s32)
#define DSI_IOCTL_AIN_FILTER_20_23			_IOWR(GSC_IOCTL, 22, __s32)
#define DSI_IOCTL_AIN_FILTER_24_27			_IOWR(GSC_IOCTL, 23, __s32)
#define DSI_IOCTL_AIN_FILTER_28_31			_IOWR(GSC_IOCTL, 24, __s32)
#define DSI_IOCTL_AIN_MODE					_IOWR(GSC_IOCTL, 25, __s32)
#define DSI_IOCTL_AIN_RANGE					_IOWR(GSC_IOCTL, 26, __s32)
#define DSI_IOCTL_AIN_RANGE_SEL				_IOWR(GSC_IOCTL, 27, __s32)
#define DSI_IOCTL_AIN_RANGE_00_03			_IOWR(GSC_IOCTL, 28, __s32)
#define DSI_IOCTL_AIN_RANGE_04_07			_IOWR(GSC_IOCTL, 29, __s32)
#define DSI_IOCTL_AIN_RANGE_08_11			_IOWR(GSC_IOCTL, 30, __s32)
#define DSI_IOCTL_AIN_RANGE_12_15			_IOWR(GSC_IOCTL, 31, __s32)
#define DSI_IOCTL_AIN_RANGE_16_19			_IOWR(GSC_IOCTL, 32, __s32)
#define DSI_IOCTL_AIN_RANGE_20_23			_IOWR(GSC_IOCTL, 33, __s32)
#define DSI_IOCTL_AIN_RANGE_24_27			_IOWR(GSC_IOCTL, 34, __s32)
#define DSI_IOCTL_AIN_RANGE_28_31			_IOWR(GSC_IOCTL, 35, __s32)
#define DSI_IOCTL_CHANNEL_ORDER				_IOWR(GSC_IOCTL, 36, __s32)
#define DSI_IOCTL_DATA_FORMAT				_IOWR(GSC_IOCTL, 37, __s32)
#define DSI_IOCTL_DATA_WIDTH				_IOWR(GSC_IOCTL, 38, __s32)
#define DSI_IOCTL_INIT_MODE					_IOWR(GSC_IOCTL, 39, __s32)
#define DSI_IOCTL_SW_SYNC					_IO  (GSC_IOCTL, 40)
#define DSI_IOCTL_SW_SYNC_MODE				_IOWR(GSC_IOCTL, 41, __s32)
#define DSI_IOCTL_PLL_REF_FREQ_HZ			_IOR (GSC_IOCTL, 42, __s32)	// PLL
#define DSI_IOCTL_RATE_GEN_A_NVCO			_IOWR(GSC_IOCTL, 43, __s32)	// PLL
#define DSI_IOCTL_RATE_GEN_B_NVCO			_IOWR(GSC_IOCTL, 44, __s32)	// PLL
#define DSI_IOCTL_RATE_GEN_A_NREF			_IOWR(GSC_IOCTL, 45, __s32)	// PLL
#define DSI_IOCTL_RATE_GEN_B_NREF			_IOWR(GSC_IOCTL, 46, __s32)	// PLL
#define DSI_IOCTL_RATE_GEN_A_NRATE			_IOWR(GSC_IOCTL, 47, __s32)	// LEGACY
#define DSI_IOCTL_RATE_GEN_B_NRATE			_IOWR(GSC_IOCTL, 48, __s32)	// LEGACY
#define DSI_IOCTL_RATE_DIV_0_NDIV			_IOWR(GSC_IOCTL, 49, __s32)
#define DSI_IOCTL_RATE_DIV_1_NDIV			_IOWR(GSC_IOCTL, 50, __s32)
#define DSI_IOCTL_CH_GRP_0_SRC				_IOWR(GSC_IOCTL, 51, __s32)
#define DSI_IOCTL_CH_GRP_1_SRC				_IOWR(GSC_IOCTL, 52, __s32)
#define DSI_IOCTL_CH_GRP_2_SRC				_IOWR(GSC_IOCTL, 53, __s32)
#define DSI_IOCTL_CH_GRP_3_SRC				_IOWR(GSC_IOCTL, 54, __s32)
#define DSI_IOCTL_EXT_CLK_SRC				_IOWR(GSC_IOCTL, 55, __s32)
#define DSI_IOCTL_EXT_TRIG					_IOWR(GSC_IOCTL, 56, __s32)
#define DSI_IOCTL_THRES_FLAG_CBL			_IOWR(GSC_IOCTL, 57, __s32)
#define DSI_IOCTL_XCVR_TYPE					_IOWR(GSC_IOCTL, 58, __s32)
#define DSI_IOCTL_GPS_ENABLE				_IOWR(GSC_IOCTL, 59, __s32)	// GPS
#define DSI_IOCTL_GPS_LOCKED				_IOR (GSC_IOCTL, 60, __s32)	// GPS
#define DSI_IOCTL_GPS_POLARITY				_IOWR(GSC_IOCTL, 61, __s32)	// GPS
#define DSI_IOCTL_GPS_RATE_LOCKED			_IOR (GSC_IOCTL, 62, __s32)	// GPS
#define DSI_IOCTL_GPS_TARGET_RATE			_IOWR(GSC_IOCTL, 63, __s32)	// GPS
#define DSI_IOCTL_GPS_TOLERANCE				_IOWR(GSC_IOCTL, 64, __s32)	// GPS
#define DSI_IOCTL_AIN_COUPLING				_IOWR(GSC_IOCTL, 65, __s32)
#define DSI_IOCTL_ADC_RATE_OUT				_IOWR(GSC_IOCTL, 66, __s32)
#define DSI_IOCTL_RX_IO_ABORT				_IOR (GSC_IOCTL, 67, __s32)
#define DSI_IOCTL_IRQ_SEL					_IOWR(GSC_IOCTL, 68, __s32)
#define	DSI_IOCTL_WAIT_EVENT				_IOWR(GSC_IOCTL, 69, gsc_wait_t)
#define	DSI_IOCTL_WAIT_CANCEL				_IOWR(GSC_IOCTL, 70, gsc_wait_t)
#define	DSI_IOCTL_WAIT_STATUS				_IOWR(GSC_IOCTL, 71, gsc_wait_t)
#define DSI_IOCTL_TA_INV_EXT_TRIGGER		_IOWR(GSC_IOCTL, 72, __s32)
#define DSI_IOCTL_COUPLING_MODE_AC			_IOWR(GSC_IOCTL, 73, __s32)
#define DSI_IOCTL_COUPLING_MODE_DC			_IOWR(GSC_IOCTL, 74, __s32)
#define DSI_IOCTL_CHANNELS_READY			_IO  (GSC_IOCTL, 75)

//*****************************************************************************
// DSI_IOCTL_REG_READ
// DSI_IOCTL_REG_WRITE
// DSI_IOCTL_REG_MOD
//
// Parameter:	gsc_reg_t*
//		Please note that not all registers are present on all boards.

// These are common registers.
#define DSI_GSC_BCTLR	GSC_REG_ENCODE(GSC_REG_GSC,4,0x00)	// Board Control Register

#define DSI_GSC_RAR		GSC_REG_ENCODE(GSC_REG_GSC,4,0x0C)	// Rate Assignments Register
#define DSI_GSC_RDR		GSC_REG_ENCODE(GSC_REG_GSC,4,0x10)	// Rate Divisors Register

#define DSI_GSC_PRFR	GSC_REG_ENCODE(GSC_REG_GSC,4,0x18)	// PLL Reference Frequency Register

#define DSI_GSC_IBCR	GSC_REG_ENCODE(GSC_REG_GSC,4,0x20)	// Input Buffer Control Register
#define DSI_GSC_BCFGR	GSC_REG_ENCODE(GSC_REG_GSC,4,0x24)	// Board Configuration Register
#define DSI_GSC_BSR		GSC_REG_ENCODE(GSC_REG_GSC,4,0x28)	// Buffer Size Register
#define DSI_GSC_AVR		GSC_REG_ENCODE(GSC_REG_GSC,4,0x2C)	// Auto Cal Values Register
#define DSI_GSC_IDBR	GSC_REG_ENCODE(GSC_REG_GSC,4,0x30)	// Input Data Buffer Register

// 24DSI12 specific
#define DSI_GSC_RCAR	GSC_REG_ENCODE(GSC_REG_GSC,4,0x04)	// Rate Control A Register
#define DSI_GSC_RCBR	GSC_REG_ENCODE(GSC_REG_GSC,4,0x08)	// Rate Control B Register
#define	DSI_GSC_ICMR	GSC_REG_ENCODE(GSC_REG_GSC,4,0x14)	// Input Coupling Mode Register
#define DSI_GSC_GSR		GSC_REG_ENCODE(GSC_REG_GSC,4,0x1C)	// GPS Synchronization Register

// 24DSI32 specific
#define DSI_GSC_NREFCR	GSC_REG_ENCODE(GSC_REG_GSC,4,0x04)	// Nref Control Register PLL
#define DSI_GSC_NVCOCR	GSC_REG_ENCODE(GSC_REG_GSC,4,0x08)	// Nvco Control Register PLL
#define DSI_GSC_RCR		GSC_REG_ENCODE(GSC_REG_GSC,4,0x08)	// Rate Control Register LEGACY
#define DSI_GSC_RFCR	GSC_REG_ENCODE(GSC_REG_GSC,4,0x1C)	// Range and Filter Control Register

//*****************************************************************************
// DSI_IOCTL_QUERY
//
//	Parameter:	s32
//		Pass in a value from the list below.
//		The value returned is the answer to the query.

typedef enum
{
	DSI_QUERY_AUTO_CAL_MS,		// Max auto-cal period in ms.
	DSI_QUERY_CH_GRP_0_RAR,		// Is the Ch Grp 0 primary in the RAR?
	DSI_QUERY_CHANNEL_GPS,		// Number of channel groups.
	DSI_QUERY_CHANNEL_MAX,		// Maximum number of channels supported.
	DSI_QUERY_CHANNEL_QTY,		// The number of A/D channels.
	DSI_QUERY_COUNT,			// Number of query options.
	DSI_QUERY_D15_PLL,			// BCFGR.D15: For PLL?
	DSI_QUERY_D16_CHANNELS,		// BCFGR.D16: Channel count for bit = 1.
	DSI_QUERY_D16_SYNC_1,		// BCTLR.D16: Is 1 for Synchronous?
	DSI_QUERY_D17_CHANNELS,		// BCFGR.D17: Channel count for bit = 1.
	DSI_QUERY_D18_CF_FILT,		// BCFGR.D18: Custom Freq Filter?
	DSI_QUERY_D18_RIO,			// BCFGR.D18: Rear I/O Panel?
	DSI_QUERY_D19_EXT_TEMP,		// BCFGR.D19: Extended Temperature?
	DSI_QUERY_D20_LOW_POWER,	// BCFGR.D20: Low Power?
	DSI_QUERY_D20_XCVR,			// BCTLR.D20: Xcvr selection?
	DSI_QUERY_D21_EXT_TRIG,		// BCTLR.D21: Arm External Trigger?
	DSI_QUERY_D22_THR_FLAG,		// BCTLR.D22: Threshold Flag Out?
	DSI_QUERY_DEVICE_TYPE,		// Value from gsc_dev_type_t
	DSI_QUERY_FGEN_MAX,			// Rate Generator maximum output rate.
	DSI_QUERY_FGEN_MIN,			// Rate Generator minimum output rate.
	DSI_QUERY_FIFO_SIZE,		// FIFO depth in 32-bit samples
	DSI_QUERY_FREF_DEFAULT,		// The default Fref value for the board.
	DSI_QUERY_FSAMP_DEFAULT,	// The default sample rate.
	DSI_QUERY_FSAMP_MAX,		// The maximum sample rate.
	DSI_QUERY_FSAMP_MIN,		// The minimum sample rate.
	DSI_QUERY_GPS_PRESENT,		// Does the board support GPS?
	DSI_QUERY_INIT_MS,			// Max initialize period in ms.
	DSI_QUERY_LOW_POWER,		// Is this a Low Power board?
	DSI_QUERY_NDIV_MASK,		// Mask of valid Ndiv bits.
	DSI_QUERY_NDIV_MAX,			// Maximum rate divisor Ndiv value.
	DSI_QUERY_NDIV_MIN,			// Maximum rate divisor Ndiv value.
	DSI_QUERY_NRATE_MASK,		// LEGACY: Mask of valid Nrate bits.
	DSI_QUERY_NRATE_MAX,		// LEGACY: Maximum rate generator Nrate value.
	DSI_QUERY_NRATE_MIN,		// LEGACY: Maximum rate generator Nrate value.
	DSI_QUERY_NREF_MASK,		// PLL: Mask of valid Nref bits.
	DSI_QUERY_NREF_MAX,			// PLL: Maximum rate generator Nref value.
	DSI_QUERY_NREF_MIN,			// PLL: Maximum rate generator Nref value.
	DSI_QUERY_NVCO_MASK,		// PLL: Mask of valid Nvco bits.
	DSI_QUERY_NVCO_MAX,			// PLL: Maximum rate generator Nvco value.
	DSI_QUERY_NVCO_MIN,			// PLL: Maximum rate generator Nvco value.
	DSI_QUERY_PLL_PRESENT,		// Does the board support PLL?
	DSI_QUERY_RATE_DIV_QTY,		// The number of rate dividers.
	DSI_QUERY_RATE_GEN_QTY,		// Number of Rate Generatorts.
	DSI_QUERY_RCAR_RCBR,		// Are Rate Control A/B Registrers present?
	DSI_QUERY_RFCR,				// Is the Range and Filter Control Registrer present?

	DSI_QUERY_D0_1_IN_MODE,		// BCTLR.D0/1: Analog Input Mode?
	DSI_QUERY_D2_3_RANGE,		// BCTLR.D2/3: Voltage Range?
	DSI_QUERY_D19_FREQ_FILT,	// BCTLR.D19: Frequency Filter?
	DSI_QUERY_D22_COUPLING,		// BCTLR.D23: Coupling?
	DSI_QUERY_D23_RATE_OUT,		// BCTLR.D23: ADC Rate Out?

	DSI_QUERY_D16_CHANNELS_0,	// BCFGR.D16: Channel count for bit = 0
	DSI_QUERY_D17_CHANNELS_0,	// BCFGR.D17: Channel count for bit = 0
	DSI_QUERY_D17_18_RANGE,		// BCFGR.D17/18: Voltage Range?
	DSI_QUERY_D19_20_FILT,		// BCFGR.D19/20: Filter Frequency?
	DSI_QUERY_D21_EXT_TEMP,		// BCFGR.D21: Extended Temperature?
	DSI_QUERY_D22_24DSI6LN,		// BCFGR.D22: 24DSI6LN?

	DSI_QUERY_LOW_NOISE,		// Is this a Low Noise board?

	DSI_QUERY_D23_INV_EXT_TRIG,	// BCTLR.D23: Invert External Trigger?
	DSI_QUERY_REG_ICMR,			// Input Coupling Mode Regiater?

	DSI_IOCTL_QUERY_LAST

} dsi_query_t;

#define	DSI_IOCTL_QUERY_ERROR				(-1)	// A query request is unknown of invalid.

//*****************************************************************************
// DSI_IOCTL_INITIALIZE						BCTLR D15
//
//	Parameter:	None

//*****************************************************************************
// DSI_IOCTL_AUTO_CALIBRATE					BCTLR D7
//
//	Parameter:	None

//*****************************************************************************
// DSI_IOCTL_RX_IO_MODE
//
//	Parameter:	__s32
//		Pass in any of the gsc_io_mode_t options, or
//		-1 to read the current setting.
#define	DSI_IO_MODE_DEFAULT					GSC_IO_MODE_PIO

//*****************************************************************************
// DSI_IOCTL_RX_IO_OVERFLOW
//
//	Parameter:	__s32
//		Pass in any of the below options, or
//		-1 to read the current setting.
#define	DSI_IO_OVERFLOW_DEFAULT				DSI_IO_OVERFLOW_CHECK
#define	DSI_IO_OVERFLOW_IGNORE				0
#define	DSI_IO_OVERFLOW_CHECK				1

//*****************************************************************************
// DSI_IOCTL_RX_IO_TIMEOUT					(in seconds)
//
//	Parameter:	__s32
//		Pass in any of the below options, or
//		-1 to read the current setting.
#define	DSI_IO_TIMEOUT_DEFAULT				10
#define	DSI_IO_TIMEOUT_NO_SLEEP				0		// Do not sleep to wait for more data.
#define	DSI_IO_TIMEOUT_MIN					0
#define	DSI_IO_TIMEOUT_MAX					3600	// Wait at most one hour for completion.

//*****************************************************************************
// DSI_IOCTL_RX_IO_UNDERFLOW
//
//	Parameter:	__s32
//		Pass in any of the below options, or
//		-1 to read the current setting.
#define	DSI_IO_UNDERFLOW_DEFAULT			DSI_IO_UNDERFLOW_CHECK
#define	DSI_IO_UNDERFLOW_IGNORE				0
#define	DSI_IO_UNDERFLOW_CHECK				1

//*****************************************************************************
// DSI_IOCTL_AIN_BUF_CLEAR					IBCR D19
//
//	Parameter:	None.

//*****************************************************************************
// DSI_IOCTL_AIN_BUF_INPUT					IBCR D18
//
//	Parameter:	__s32
//		Pass in any valid option below, or
//		-1 to read the current setting.
#define	DSI_AIN_BUF_INPUT_DISABLE			1	// Disable input to the buffer
#define	DSI_AIN_BUF_INPUT_ENABLE			0	// Enable input to the buffer

//*****************************************************************************
// DSI_IOCTL_AIN_BUF_OVERFLOW				IBCR D24
//
//	Parameter:	__s32
//		Pass in any of the below options, or
//		-1 to read the current setting.
#define	DSI_AIN_BUF_OVERFLOW_CLEAR			0
#define	DSI_AIN_BUF_OVERFLOW_TEST			(-1)

// The following are the set of value returned values.
#define	DSI_AIN_BUF_OVERFLOW_NO				0
#define	DSI_AIN_BUF_OVERFLOW_YES			1

//*****************************************************************************
// DSI_IOCTL_AIN_BUF_THRESH					IBCR D0-D17
//
//	Parameter:	__s32
//		Pass in any valid threshold level, or
//		-1 to read the current setting.
//		The upper limit to the threshold value is 0x3FFFF.

//*****************************************************************************
// DSI_IOCTL_AIN_BUF_UNDERFLOW				IBCR D25
//
//	Parameter:	__s32
//		Pass in any of the below options, or
//		-1 to read the current setting.
#define	DSI_AIN_BUF_UNDERFLOW_CLEAR			0
#define	DSI_AIN_BUF_UNDERFLOW_TEST			(-1)

// The following are the set of value returned values.
#define	DSI_AIN_BUF_UNDERFLOW_NO			0
#define	DSI_AIN_BUF_UNDERFLOW_YES			1

//*****************************************************************************
// DSI_IOCTL_AIN_FILTER						BCTLR D19
// DSI_IOCTL_AIN_FILTER_00_03				RFCR D0
// DSI_IOCTL_AIN_FILTER_04_07				RFCR D1
// DSI_IOCTL_AIN_FILTER_08_11				RFCR D2
// DSI_IOCTL_AIN_FILTER_12_15				RFCR D3
// DSI_IOCTL_AIN_FILTER_16_19				RFCR D4
// DSI_IOCTL_AIN_FILTER_20_23				RFCR D5
// DSI_IOCTL_AIN_FILTER_24_27				RFCR D6
// DSI_IOCTL_AIN_FILTER_28_31				RFCR D7
//
//	Parameter:	__s32
//		Pass in any of the below options, or
//		-1 to read the current setting.
#define	DSI_AIN_FILTER_HI					0
#define	DSI_AIN_FILTER_LOW					1

//*****************************************************************************
// DSI_IOCTL_AIN_FILTER_SEL					RFCR D8
//
//	Parameter:	__s32
//		Pass in any value between the below inclusive limits, or
//		-1 to read the current setting.
#define	DSI_AIN_FILTER_SEL_DISABLE			0
#define	DSI_AIN_FILTER_SEL_ENABLE			1

//*****************************************************************************
// DSI_IOCTL_AIN_MODE						BCTLR D0-D1
//
//	Parameter:	__s32
//		Pass in any of the below options, or
//		-1 to read the current setting.
#define	DSI_AIN_MODE_DIFF					0	// Differential
#define	DSI_AIN_MODE_ZERO					2	// Zero test
#define	DSI_AIN_MODE_VREF					3	// Vref test

//*****************************************************************************
// DSI_IOCTL_AIN_RANGE						BCTLR D2-D3
// DSI_IOCTL_AIN_RANGE_00_03				RFCR D16-D17
// DSI_IOCTL_AIN_RANGE_04_07				RFCR D18-D19
// DSI_IOCTL_AIN_RANGE_08_11				RFCR D20-D21
// DSI_IOCTL_AIN_RANGE_12_15				RFCR D22-D23
// DSI_IOCTL_AIN_RANGE_16_19				RFCR D24-D25
// DSI_IOCTL_AIN_RANGE_20_23				RFCR D26-D27
// DSI_IOCTL_AIN_RANGE_24_27				RFCR D28-D29
// DSI_IOCTL_AIN_RANGE_28_31				RFCR D30-D31
//
//	Parameter:	__s32
//		Pass in any of the below options, or
//		-1 to read the current setting.
#define	DSI_AIN_RANGE_2_5V					1	// +- 2.5 volts
#define	DSI_AIN_RANGE_5V					2	// +- 5 volts
#define	DSI_AIN_RANGE_10V					3	// +- 10 volts

//*****************************************************************************
// DSI_IOCTL_AIN_RANGE_SEL					RFCR D9
//
//	Parameter:	__s32
//		Pass in any value between the below inclusive limits, or
//		-1 to read the current setting.
#define	DSI_AIN_RANGE_SEL_DISABLE			0
#define	DSI_AIN_RANGE_SEL_ENABLE			1

//*****************************************************************************
// DSI_IOCTL_CHANNEL_ORDER					BCTLR D16
//
//	Parameter:	__s32
//		Pass in any of the below options, or
//		-1 to read the current setting.
//		24DSI6:  BCTLR.D16 = 0 for sync
//		24DSI12: BCTLR.D16 = 0 for sync
//		24DSI32: BCTLR.D16 = 1 for sync
#define	DSI_CHANNEL_ORDER_SYNC				0	// Synchronous channel order
#define	DSI_CHANNEL_ORDER_ASYNC				1	// Asynchronous channel order

//*****************************************************************************
// DSI_IOCTL_DATA_FORMAT					BCTLR D4
//
//	Parameter:	__s32
//		Pass in any of the below options, or
//		-1 to read the current setting.
#define	DSI_DATA_FORMAT_2S_COMP				0	// Twos Compliment
#define	DSI_DATA_FORMAT_OFF_BIN				1	// Offset Binary

//*****************************************************************************
// DSI_IOCTL_DATA_WIDTH						IBCR D20-D21
//
//	Parameter:	__s32
//		Pass in any of the below options, or
//		-1 to read the current setting.
#define	DSI_DATA_WIDTH_16					0
#define	DSI_DATA_WIDTH_18					1
#define	DSI_DATA_WIDTH_20					2
#define	DSI_DATA_WIDTH_24					3

//*****************************************************************************
// DSI_IOCTL_INIT_MODE						BCTLR D5
//
//	Parameter:	__s32
//		Pass in any of the below options, or
//		-1 to read the current setting.
#define	DSI_INIT_MODE_TARGET				0	// Target
#define	DSI_INIT_MODE_INITIATOR				1	// Initiator

//*****************************************************************************
// DSI_IOCTL_SW_SYNC						BCTLR D6
//
//	Parameter:	None.

//*****************************************************************************
// DSI_IOCTL_SW_SYNC_MODE					BCTLR D17
//
//	Parameter:	__s32
//		Pass in any of the below options, or
//		-1 to read the current setting.
#define	DSI_SW_SYNC_MODE_SW_SYNC			0	// Perform a Sync operation
#define	DSI_SW_SYNC_MODE_CLR_BUF			1	// Clear the input buffer/

//*****************************************************************************
// DSI_IOCTL_PLL_REF_FREQ_HZ				PRFR
//
//	Parameter:	__s32
//		The value returned is the reference frequency calculation in hurtz.
//		The vaue is zero if the board doesn't support the PLL.

//*****************************************************************************
// DSI_IOCTL_RATE_GEN_A_NVCO				RCAR D0-D9 or NVCOCR D0-D9
// DSI_IOCTL_RATE_GEN_B_NVCO				RCBR D0-D9
//
//	Parameter:	__s32
//		Pass in any value between the below inclusive limits, or
//		-1 to read the current setting.
//		For all boards at this time the valid range is 30-1000.

//*****************************************************************************
// DSI_IOCTL_RATE_GEN_A_NREF				RCAR D16-D25 or NREFCR D0-D9
// DSI_IOCTL_RATE_GEN_B_NREF				RCBR D16-D25
//	Parameter:	__s32
//		Pass in any value between the below inclusive limits, or
//		-1 to read the current setting.
//		For all boards at this time the valid range is 30-1000.

//*****************************************************************************
// DSI_IOCTL_RATE_GEN_A_NRATE				RCAR D0-D16 or RCR D0-D16
// DSI_IOCTL_RATE_GEN_B_NRATE				RCBR D0-D16
//	Parameter:	__s32
//		Pass in any value between the below inclusive limits, or
//		-1 to read the current setting.
//		For all boards at this time the valid range is 0-100,000.

//*****************************************************************************
// DSI_IOCTL_RATE_DIV_0_NDIV				RDR D0-D7
// DSI_IOCTL_RATE_DIV_1_NDIV				RDR D8-D15
//
//	Parameter:	__s32
//		Pass in any value between the below inclusive limits, or
//		-1 to read the current setting.
//		For some boards the valid range is 0-24.

//*****************************************************************************
// DSI_IOCTL_CH_GRP_0_SRC					RAR D0-D3
// DSI_IOCTL_CH_GRP_1_SRC					RAR D4-D7
// DSI_IOCTL_CH_GRP_2_SRC					RAR D8-D11
// DSI_IOCTL_CH_GRP_3_SRC					RAR D12-D15
//
//	Parameter:	__s32
//		Pass in any valid option below, or
//		-1 to read the current setting.
#define	DSI_CH_GRP_SRC_GEN_A				0	// Valid if supported by board.
#define	DSI_CH_GRP_SRC_GEN_B				1	// Valid if supported by board.
#define	DSI_CH_GRP_SRC_EXTERN				4
#define	DSI_CH_GRP_SRC_DIR_EXTERN			5
#define	DSI_CH_GRP_SRC_DISABLE				6
#define	DSI_CH_GRP_SRC_ENABLE				8	// Some are enable/disable only.

//*****************************************************************************
// DSI_IOCTL_EXT_CLK_SRC					BCTLR D18
//
//	Parameter:	__s32
//		Pass in any of the below options, or
//		-1 to read the current setting.
#define	DSI_EXT_CLK_SRC_GRP_0				0
#define	DSI_EXT_CLK_SRC_GEN_A				1

//*****************************************************************************
// DSI_IOCTL_EXT_TRIG						BCTLR D21
//
//	Parameter:	__s32
//		Pass in any value between the below inclusive limits, or
//		-1 to read the current setting.
#define	DSI_EXT_TRIG_DISARM					0
#define	DSI_EXT_TRIG_ARM					1

//*****************************************************************************
// DSI_IOCTL_THRES_FLAG_CBL					BCTLR D22
//
//	Parameter:	__s32
//		Pass in any value between the below inclusive limits, or
//		-1 to read the current setting.
#define	DSI_THRES_FLAG_CBL_OFF				0
#define	DSI_THRES_FLAG_CBL_OUT				1

//*****************************************************************************
// DSI_IOCTL_XCVR_TYPE						BCTLR D20
//
//	Parameter:	__s32
//		Pass in any of the below options, or
//		-1 to read the current setting.
#define	DSI_XCVR_TYPE_LVDS					0
#define	DSI_XCVR_TYPE_TTL					1

//*****************************************************************************
// DSI_IOCTL_GPS_ENABLE						GSR D20
//
//	Parameter:	__s32
//		Pass in any value between the below inclusive limits, or
//		-1 to read the current setting.
#define	DSI_GPS_ENABLE_NO					0
#define	DSI_GPS_ENABLE_YES					1

//*****************************************************************************
// DSI_IOCTL_GPS_LOCKED						GSR D24
//
//	Parameter:	__s32
//		One if the below valies is returned.
#define	DSI_GPS_LOCKED_NO					0	// The board isn't locked to the GPS signal.
#define	DSI_GPS_LOCKED_YES					1	// The board is locked to the GPS signal.

//*****************************************************************************
// DSI_IOCTL_GPS_POLARITY					GSR D22
//
//	Parameter:	__s32
//		Pass in any of the below options, or
//		-1 to read the current setting.
#define	DSI_GPS_POLARITY_POS				0
#define	DSI_GPS_POLARITY_NEG				1

//*****************************************************************************
// DSI_IOCTL_GPS_RATE_LOCKED				GSR D25
//
//	Parameter:	__s32
//		One if the below valies is returned.
#define	DSI_GPS_RATE_LOCKED_NO				0	// Sampling isn't locked to the GPS signal.
#define	DSI_GPS_RATE_LOCKED_YES				1	// Sampling is locked to the GPS signal.

//*****************************************************************************
// DSI_IOCTL_GPS_TARGET_RATE				GSR D0-D19
//
//	Parameter:	__s32
//		Pass in any valid threshold level, or
//		-1 to read the current setting.
//		The upper limit to the threshold value is 0xFFFFF.

//*****************************************************************************
// DSI_IOCTL_GPS_TOLERANCE					GSR D21
//
//	Parameter:	__s32
//		Pass in any of the below options, or
//		-1 to read the current setting.
#define	DSI_GPS_TOLERANCE_NARROW			0
#define	DSI_GPS_TOLERANCE_WIDE				1

//*****************************************************************************
// DSI_IOCTL_AIN_COUPLING					BCTLR D22
//
//	Parameter:	__s32
//		Pass in any of the below options, or
//		-1 to read the current setting.
#define	DSI_AIN_COUPLING_DC					0
#define	DSI_AIN_COUPLING_AC					1

//*****************************************************************************
// DSI_IOCTL_ADC_RATE_OUT					BCTLR D23
//
//	Parameter:	__s32
//		Pass in any of the below options, or
//		-1 to read the current setting.
#define	DSI_ADC_RATE_OUT_NO					0
#define	DSI_ADC_RATE_OUT_YES				1

//*****************************************************************************
// DSI_IOCTL_RX_IO_ABORT
//
//	Parameter:	__s32*
//		The returned value is one of the below options.
#define	DSI_IO_ABORT_NO						0
#define	DSI_IO_ABORT_YES					1

//*****************************************************************************
// DSI_IOCTL_IRQ_SEL						BCTLR D8-D10
//
//	Parameter:	__s32
//		Pass in any of the below options, or
//		-1 to read the current setting.
#define	DSI_IRQ_INIT_DONE					0
#define	DSI_IRQ_AUTO_CAL_DONE				1
#define	DSI_IRQ_CHAN_READY					2
#define	DSI_IRQ_AIN_BUF_THRESH_L2H			3
#define	DSI_IRQ_AIN_BUF_THRESH_H2L			4

//*****************************************************************************
// DSI_IOCTL_WAIT_EVENT						all fields must be valid
// DSI_IOCTL_WAIT_CANCEL					fields need not be valid
// DSI_IOCTL_WAIT_STATUS					fields need not be valid
//
//	Parameter:	gsc_wait_t*
// gsc_wait_t.flags - see gsc_common.h
// gsc_wait_t.main - see gsc_common.h
// gsc_wait_t.gsc
#define	DSI_WAIT_GSC_INIT_DONE				0x0001
#define	DSI_WAIT_GSC_AUTO_CAL_DONE			0x0002
#define	DSI_WAIT_GSC_CHAN_READY				0x0004
#define	DSI_WAIT_GSC_AIN_BUF_THRESH_L2H		0x0008
#define	DSI_WAIT_GSC_AIN_BUF_THRESH_H2L		0x0010
#define	DSI_WAIT_GSC_ALL					0x001F
// gsc_wait_t.alt flags
#define	DSI_WAIT_ALT_ALL					0x0000
// gsc_wait_t.io - see gsc_common.h

//*****************************************************************************
// DSI_IOCTL_TA_INV_EXT_TRIGGER				BCTLR D23
//
//	Parameter:	__s32
//		Pass in any of the below options, or
//		-1 to read the current setting.
#define	DSI_TA_INV_EXT_TRIGGER_NO			0	// Rising Edge
#define	DSI_TA_INV_EXT_TRIGGER_YES			1	// Falling Edge

//*****************************************************************************
// DSI_IOCTL_COUPLING_MODE_AC				ICMR D0-D11
// DSI_IOCTL_COUPLING_MODE_DC				ICMR D0-D11
//
//	Parameter:	__s32
//		Pass in any valid mask of bits, or
//		-1 to read the current setting.
//		The mask must access only installed channels.

//*****************************************************************************
// DSI_IOCTL_CHANNELS_READY					BCTLR D13
//
//	Parameter:	None.



#endif
