// This software is covered by the GNU GENERAL PUBLIC LICENSE (GPL).
// $URL: http://subversion:8080/svn/gsc/trunk/drivers/gsc_common/linux/driver/gsc_main.h $
// $Rev: 22537 $
// $Date: 2013-07-03 12:32:01 -0500 (Wed, 03 Jul 2013) $

#ifndef __GSC_MAIN_H__
#define __GSC_MAIN_H__

#include <asm/io.h>
#include <asm/irq.h>
#include <asm/page.h>
#include <asm/types.h>
#include <asm/uaccess.h>

#include <linux/delay.h>
#include <linux/interrupt.h>
#include <linux/ioport.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/pci.h>
#include <linux/proc_fs.h>
#include <linux/sched.h>
#include <linux/version.h>
#include <linux/wait.h>

#include "gsc_kernel_2_2.h"
#include "gsc_kernel_2_4.h"
#include "gsc_kernel_2_6.h"
#include "gsc_kernel_3.h"
#include "gsc_common.h"

#ifndef CONFIG_PCI
	#error This driver requires PCI support.
#endif



// #defines	*******************************************************************

// This is the OVERALL version number.
#define GSC_DRIVER_VERSION			DEV_VERSION "." GSC_COMMON_VERSION

// This is for the common code only!
#define	GSC_COMMON_VERSION			"45"
// 45	Fixed a bug in the write() routine.
// 44	Freeing the board interrupt is now conditioned upon it being acquired.
//		Corrected definition of register GSC_PLX_9056_PABTADR.
//		Added support for the DIO24.
//		Added support for the Bus Abort interrupt.
// 43	Updated DMA reads to improve performance.
// 42	Changed how I/O buffers are allocated.
//		Added support for the 16AI64.
// 41	Changed how s8 ... u32 are included.
//		Added support for the 16AO64C.
// 40	Enhanced the IOCTL service code to accommodate unsupported services.
//		Updated the BAR mapping failure messages.
//		Added s8 ... u32 definitions for application use.
//		Updated gsc_dma_destroy() - now zero fills memory when done.
//		Updated gsc_irq_destroy() - does cleanup when done.
// 39	Added support for boards that need additional setup after a DMA channel
//		has been selected.
// 38	Added support for the 18AO8.
//		Made the IOCTL buffer larger - 512 bytes.
// 37	Updated for the 3.x kernel.
// 36	Additional SIO4 porting.
//		Added support (via notes only) for the PCIE104-24DSI12.
// 35	Removed compile warnings from the copy_from_user_ret macro.
// 34	All common driver sources are included even though not all are used.
// 33	Modified the kernel_2_X.h sources for use by the SIO4 driver.
//		The SIO4 support porting is only partially complete at this time.
//		Implemented GSC_DEVS_PER_BOARD macro for multi-device boards.
//		Added support for the PLX PEX 8112 PCI Express Bridge.
// 32	Updated information that distinguishes 16HSDI from 16SDI-HS.
// 31	Fixed bugs in VPD code (Vital Product Data).
// 30	Added support for the PLX PEX 8111 PCI Express Bridge.
//		Added GSC_IRQ_NOT_USED macro to hide interrupt servicing.
//		Added GSC_PCI_SPACE_SIZE macro to support the PEX8111.
// 29	Fixed a bug in the GSC_INTCSR_MAILBOX_INT_ACTIVE macro.
//		Removed a compiler warning under Fedora 15.
// 28	Added support for the 16AI64SSC.
//		Added #defines for D0 through D31.
//		Modified gsc_sem_t to prevent hanging on an uninitialized structure.
//		Moved dev_check_id() functionality to dev_device_create().
//		Added IOCTL support for the 16AI64SSA/C Low Latency Read service.
// 27	Added support for the 20AOF16C500KR.
// 26	Added support for the 16AICS32.
// 25	Added support for deprication of file_ops.ioctl.
//		Added support for the 18AISS6C boards.
//		Started PLX EEPROM access support.
//		Modified so BAR2 can now be memory or I/O mapped.
// 24	Added support for the 25DSI20C500K boards.
// 23	Added support for the 16AI64SSA boards.
//		Corrected the _1M macro.
// 22	Added support for the OPTO32 boards.
//		Added support for the PCI9060ES.
// 21	Removed compiler warning in Fedora 12: reduced module_init stack usage.
// 20	Added support for the 16AIO168.
//		Removed remove_proc_entry() call from proc_start - fix for Fedora 14.
//		Fixed a bug in gsc_ioctl_init().
// 19	Added support for the 24DSI16WRC.
// 18	Added common PIO read and write routines.
//		Changed use of DEV_SUPPORTS_PROC_ID_STR macro.
//		Changed use of DEV_SUPPORTS_READ macro.
//		Changed use of DEV_SUPPORTS_WRITE macro.
//		Added initial support for Vital Product Data.
//		Added support for the 16AI32SSA.
// 17	Added support for the OPTO16X16.
//		Corrected a bug: wait timeouts in jiffy units are negative.
// 16	Added support for aborting active I/O operations.
//		Added support for Auto-Start operations.
//		Added wait options for I/O cancellations.
//		Fixed bugs in the DMA code evaluating lock return status.
//		Added /proc board identifier string support.
//		Added gsc_irq_local_disable and gsc_irq_local_enable;
//		Added failure message when driver doesn't load.
//		Improved timeout handling.
//		Added gsc_time.c.
// 15	Added support for the 14HSAI4 and the HPDI32.
// 14	Added wait event, wait cancel and wait status services.
// 13	Modified the EVENT_WAIT_IRQ_TO() macro for the 2.6 kernel.
//		We now no longer initialize the condition variable in the macro.
//		The condition variable must be initialized outside the macro.
//		Added support for the 16AIO and the 12AIO.
// 12	Added more id information for the 18AI32SSC1M boards.
// 11	Added module count support.
//		Added common gsc_open() and gsc_close() calls.
// 10	Added support for the 12AISS8AO4 and the 16AO16.
// 9	Added support for the 16HSDI4AO4. Fixed bug in gsc_ioctl_init().
// 8	Added support for the 16AISS16AO2.
//		Made various read() and write() support services use same data types.
// 7	Added support for the 16AO20. Implemented write() support.
// 6	Added dev_check_id() for more detailed device identification.
// 5	Fixed DMA engine initialization code.
//		This was previously and incorrectly reported here as a version 4 mod.
// 4	Added support for the 18AI32SSC1M.
//		Modified some PLX register names for consistency.
// 3	Added support for the 16AI32SSC.
// 2	Added support for the 24DSI6.
// 1	Updated the makefile's "clean" code.
//		Added code to expand access rights to makefile.dep.
//		Added 24DSI12/32 types to gsc_common.h
// 0	initial release

#define	ARRAY_ELEMENTS(a)			(sizeof((a))/sizeof((a)[0]))

#define	MS_TO_JIFFIES(m)			(((m) + ((1000 / HZ) - 1)) / (1000 / HZ))
#define	JIFFIES_TO_MS(j)			((((j) * 1000) + (HZ / 2)) / HZ)

#define	_1K							(1024L)
#define	_5K							(_1K * 5)
#define	_30K						(_1K * 30)
#define	_32K						(_1K * 32)
#define	_64K						(_1K * 64)
#define	_220K						(_1K * 220L)
#define	_256K						(_1K * 256L)
#define	_512K						(_1K * 512L)
#define	_1100K						(_1K * 1100L)
#define	_1M							(_1K * _1K)
#define	_8M							(_1M * 8L)

#define	_8MHZ						( 8000000L)
#define	_9_6MHZ						( 9600000L)
#define	_16MHZ						(16000000L)
#define	_19_2MHZ					(19200000L)
#define	_38_4MHZ					(38400000L)

// Bit definitions
#define	D0							0x00000001
#define	D1							0x00000002
#define	D2							0x00000004
#define	D3							0x00000008
#define	D4							0x00000010
#define	D5							0x00000020
#define	D6							0x00000040
#define	D7							0x00000080
#define	D8							0x00000100
#define	D9							0x00000200
#define	D10							0x00000400
#define	D11							0x00000800
#define	D12							0x00001000
#define	D13							0x00002000
#define	D14							0x00004000
#define	D15							0x00008000
#define	D16							0x00010000
#define	D17							0x00020000
#define	D18							0x00040000
#define	D19							0x00080000
#define	D20							0x00100000
#define	D21							0x00200000
#define	D22							0x00400000
#define	D23							0x00800000
#define	D24							0x01000000
#define	D25							0x02000000
#define	D26							0x04000000
#define	D27							0x08000000
#define	D28							0x10000000
#define	D29							0x20000000
#define	D30							0x40000000
#define	D31							0x80000000

// Virtual address items
#define	GSC_VADDR(d,o)				(VADDR_T) (((u8*) (d)->gsc.vaddr) + (o))
#define	PLX_VADDR(d,o)				(VADDR_T) (((u8*) (d)->plx.vaddr) + (o))

// DMA
#define	GSC_DMA_CSR_DISABLE			GSC_FIELD_ENCODE(0,0,0)
#define	GSC_DMA_CSR_ENABLE			GSC_FIELD_ENCODE(1,0,0)
#define	GSC_DMA_CSR_START			GSC_FIELD_ENCODE(1,1,1)
#define	GSC_DMA_CSR_ABORT			GSC_FIELD_ENCODE(1,2,2)
#define	GSC_DMA_CSR_CLEAR			GSC_FIELD_ENCODE(1,3,3)
#define	GSC_DMA_CSR_DONE			GSC_FIELD_ENCODE(1,4,4)

#define	GSC_DMA_CAP_DMA_READ		0x01	// DMA chan can do DMA Rx
#define	GSC_DMA_CAP_DMA_WRITE		0x02	// DMA chan can do DMA Tx
#define	GSC_DMA_CAP_DMDMA_READ		0x04	// DMA chan can do DMDMA Rx
#define	GSC_DMA_CAP_DMDMA_WRITE		0x08	// DMA chan can do DMDMA Tx
#define	GSC_DMA_SEL_STATIC			0x10	// Get the DMA chan and keep it.
#define	GSC_DMA_SEL_DYNAMIC			0x20	// Hold the DMA chan only as needed.

#define	GSC_DMA_MODE_SIZE_8_BITS			GSC_FIELD_ENCODE(0, 1, 0)
#define	GSC_DMA_MODE_SIZE_16_BITS			GSC_FIELD_ENCODE(1, 1, 0)
#define	GSC_DMA_MODE_SIZE_32_BITS			GSC_FIELD_ENCODE(2, 1, 0)
#define	GSC_DMA_MODE_INPUT_ENABLE			GSC_FIELD_ENCODE(1, 6, 6)
#define	GSC_DMA_MODE_BURSTING_LOCAL			GSC_FIELD_ENCODE(1, 8, 8)
#define	GSC_DMA_MODE_INTERRUPT_WHEN_DONE	GSC_FIELD_ENCODE(1,10,10)
#define	GSC_DMA_MODE_LOCAL_ADRESS_CONSTANT	GSC_FIELD_ENCODE(1,11,11)
#define	GSC_DMA_MODE_BLOCK_DMA				GSC_FIELD_ENCODE(0,12,12)	// Non-Demand Mode
#define	GSC_DMA_MODE_DM_DMA					GSC_FIELD_ENCODE(1,12,12)	// Demand Mode
#define	GSC_DMA_MODE_PCI_INTERRUPT_ENABLE	GSC_FIELD_ENCODE(1,17,17)

#define	GSC_DMA_DPR_END_OF_CHAIN			GSC_FIELD_ENCODE(1,1,1)
#define	GSC_DMA_DPR_TERMINAL_COUNT_IRQ		GSC_FIELD_ENCODE(1,2,2)
#define	GSC_DMA_DPR_HOST_TO_BOARD			GSC_FIELD_ENCODE(0,3,3)		// Tx operation
#define	GSC_DMA_DPR_BOARD_TO_HOST			GSC_FIELD_ENCODE(1,3,3)		// Rx operation

// PLX Interrupt Control and Status Register
#define	GSC_INTCSR_MAILBOX_INT_ENABLE		GSC_FIELD_ENCODE(1, 3, 3)
#define	GSC_INTCSR_PCI_INT_ENABLE			GSC_FIELD_ENCODE(1, 8, 8)
#define	GSC_INTCSR_PCI_DOOR_INT_ENABLE		GSC_FIELD_ENCODE(1, 9, 9)
#define	GSC_INTCSR_ABORT_INT_ENABLE			GSC_FIELD_ENCODE(1,10,10)
#define	GSC_INTCSR_LOCAL_INT_ENABLE			GSC_FIELD_ENCODE(1,11,11)
#define	GSC_INTCSR_PCI_DOOR_INT_ACTIVE		GSC_FIELD_ENCODE(1,13,13)
#define	GSC_INTCSR_ABORT_INT_ACTIVE			GSC_FIELD_ENCODE(1,14,14)
#define	GSC_INTCSR_LOCAL_INT_ACTIVE			GSC_FIELD_ENCODE(1,15,15)
#define	GSC_INTCSR_LOC_DOOR_INT_ENABLE		GSC_FIELD_ENCODE(1,17,17)
#define	GSC_INTCSR_DMA_0_INT_ENABLE			GSC_FIELD_ENCODE(1,18,18)
#define	GSC_INTCSR_DMA_1_INT_ENABLE			GSC_FIELD_ENCODE(1,19,19)
#define	GSC_INTCSR_LOC_DOOR_INT_ACTIVE		GSC_FIELD_ENCODE(1,20,20)
#define	GSC_INTCSR_DMA_0_INT_ACTIVE			GSC_FIELD_ENCODE(1,21,21)
#define	GSC_INTCSR_DMA_1_INT_ACTIVE			GSC_FIELD_ENCODE(1,22,22)
#define	GSC_INTCSR_BIST_INT_ACTIVE			GSC_FIELD_ENCODE(1,23,23)
#define	GSC_INTCSR_MAILBOX_INT_ACTIVE		GSC_FIELD_ENCODE(0xF,31,28)

// 32-bit compatibility support.
#define	GSC_IOCTL_32BIT_ERROR				(-1)	// There is a problem.
#define	GSC_IOCTL_32BIT_NONE				0		// Support not in the kernel.
#define	GSC_IOCTL_32BIT_NATIVE				1		// 32-bit support on 32-bit OS
#define	GSC_IOCTL_32BIT_TRANSLATE			2		// globally translated "cmd"s
#define	GSC_IOCTL_32BIT_COMPAT				3		// compat_ioctl service
#define	GSC_IOCTL_32BIT_DISABLED			4		// Support is disabled.

// Data size limits.
#define	S32_MAX								(+2147483647L)

// EEPROM Access
#ifdef DEV_PLX_EEPROM_ACCESS
	#define	GSC_PLX_EEPROM_ACCESS(p)		gsc_plx_eeprom_access((p))
#else
	#define	GSC_PLX_EEPROM_ACCESS(p)		0
#endif

#define	dev_check_id						FAIL FAIL __LINE__ __FILE__

#ifndef	GSC_DEVS_PER_BOARD
	#define	GSC_DEVS_PER_BOARD				1
#endif

#ifndef GSC_ALT_STRUCT_T
	#define	GSC_ALT_STRUCT_T				dev_data_t
#endif

#ifndef GSC_ALT_DEV_GET
	#define	GSC_ALT_DEV_GET(a)				(a)
#endif

#ifndef GSC_ALT_STRUCT_GET
	#define	GSC_ALT_STRUCT_GET(i,d)			(d)
#endif



// data types	***************************************************************

typedef struct _dev_data_t	dev_data_t;	// A device specific structure.
typedef struct _dev_io_t	dev_io_t;	// A device specific structure.

typedef	int					(*gsc_ioctl_service_t)(GSC_ALT_STRUCT_T* alt, void* arg);

typedef	struct
{
	void*					key;	// struct is valid only if key == & of struct
	struct semaphore		sem;
} gsc_sem_t;

typedef struct
{
	int						index;		// BARx
	int						offset;		// Offset of BARx register in PCI space.
	u32						reg;		// Actual BARx register value.
	u32						flags;		// lower register bits
	int						io_mapped;	// Is this an I/O mapped region?
	unsigned long			phys_adrs;	// Physical address of region.
	u32						size;		// Region size in bytes.
	u32						requested;	// Is resource requested from OS?
	void*					vaddr;		// Kernel virtual address.
} gsc_bar_t;

typedef struct
{
	const char*				model;	// NULL model terminates a list of entries.
	u16						vendor;
	u16						device;
	u16						sub_vendor;
	u16						sub_device;
	gsc_dev_type_t			type;
} gsc_dev_id_t;

typedef struct
{
	int						index;
	int						in_use;
	unsigned int			flags;
	int						intcsr_enable;

	struct
	{
		VADDR_T				mode_32;	// DMAMODEx
		VADDR_T				padr_32;	// DMAPADRx
		VADDR_T				ladr_32;	// DMALADRx
		VADDR_T				siz_32;		// DMASIZx
		VADDR_T				dpr_32;		// DMADPRx
		VADDR_T				csr_8;		// DMACSRx
	} vaddr;

} gsc_dma_ch_t;

typedef struct
{
	gsc_sem_t				sem;		// control access
	gsc_dma_ch_t			channel[2];	// Settings and such
} gsc_dma_t;

typedef struct
{
	gsc_sem_t				sem;		// Control access.
	int						acquired;
} gsc_irq_t;

typedef struct
{
	char					built[32];
	dev_data_t*				dev_list[10];
	int						dev_qty;
	int						driver_loaded;
	struct file_operations	fops;
	int						ioctl_32bit;	// IOCTL_32BIT_XXX
	int						major_number;
	int						proc_enabled;
} gsc_global_t;

typedef struct
{
	unsigned long			cmd;	// -1 AND
	gsc_ioctl_service_t		func;	// NULL terminate the list
} gsc_ioctl_t;

typedef struct
{
	u8			loaded;			// Have we tried to load the image?
	u8			eeprom[1025];	// eeprom image is loaded here.
								// Not a multiple of 4 bytes!
	const u8*	head;			// Point to first byte of VPD, if present.
								// If the pointer is NULL when "loaded" is
								// set, then there has been an error.
} gsc_vpd_data_t;

typedef struct _gsc_wait_node_t
{
	gsc_wait_t*					wait;
	WAIT_QUEUE_ENTRY_T			entry;
	WAIT_QUEUE_HEAD_T			queue;
	int							condition;
	struct timeval				tv_start;
	struct _gsc_wait_node_t*	next;
} gsc_wait_node_t;



// variables	***************************************************************

extern	gsc_global_t		gsc_global;



// prototypes	***************************************************************

int		gsc_bar_create(struct pci_dev* pci, int index, gsc_bar_t* bar, int mem, int io);
void	gsc_bar_destroy(gsc_bar_t* bar);

int		gsc_close(struct inode *, struct file *);

int		gsc_ioctl(struct inode* inode, struct file* fp, unsigned int cmd, unsigned long arg);
long	gsc_ioctl_compat(struct file* fp, unsigned int cmd, unsigned long arg);
long	gsc_ioctl_unlocked(struct file* fp, unsigned int cmd, unsigned long arg);

int		gsc_dma_abort_active_xfer(dev_data_t* dev, dev_io_t* io);
void	gsc_dma_close(dev_data_t* dev);
int		gsc_dma_create(dev_data_t* dev, u32 ch0_flags, u32 ch1_flags);
void	gsc_dma_destroy(dev_data_t* dev);
int		gsc_dma_open(dev_data_t* dev);
ssize_t	gsc_dma_perform(GSC_ALT_STRUCT_T*	alt,
						dev_io_t*			io,
						unsigned long		jif_end,
						unsigned int		ability,
						u32					mode,
						u32					dpr,
						void*				buff,
						ssize_t				samples);

int		gsc_io_create(GSC_ALT_STRUCT_T* alt, dev_io_t* gsc, size_t size);
void	gsc_io_destroy(GSC_ALT_STRUCT_T* alt, dev_io_t* gsc);
int		gsc_ioctl_init(void);
void	gsc_ioctl_reset(void);
int		gsc_irq_access_lock(dev_data_t* dev, int isr);
void	gsc_irq_access_unlock(dev_data_t* dev, int isr);
void	gsc_irq_close(dev_data_t* dev);
int		gsc_irq_create(dev_data_t* dev);
void	gsc_irq_destroy(dev_data_t* dev);
void	gsc_irq_intcsr_mod(dev_data_t* dev, u32 value, u32 mask);
int		gsc_irq_isr_common(int irq, void* dev_id);
int		gsc_irq_local_disable(dev_data_t* dev);
int		gsc_irq_local_enable(dev_data_t* dev);
int		gsc_irq_open(dev_data_t* dev);

void	gsc_module_count_dec(void);
int		gsc_module_count_inc(void);

int		gsc_open(struct inode *, struct file *);

int		gsc_plx_eeprom_access(struct pci_dev* pci);
int		gsc_proc_read(char* page, char** start, off_t offset, int count, int* eof, void* data);
int		gsc_proc_start(void);
void	gsc_proc_stop(void);

ssize_t	gsc_read(struct file* filp, char* buf, size_t count, loff_t* offp);
int		gsc_read_abort_active_xfer(GSC_ALT_STRUCT_T* alt);

// Must define GSC_READ_PIO_WORK and/or GSC_READ_PIO_WORK_XX_BIT
ssize_t	gsc_read_pio_work			(GSC_ALT_STRUCT_T* alt, char* buff, ssize_t count, unsigned long jif_end);
ssize_t	gsc_read_pio_work_8_bit		(GSC_ALT_STRUCT_T* alt, char* buff, ssize_t count, unsigned long jif_end);
ssize_t	gsc_read_pio_work_16_bit	(GSC_ALT_STRUCT_T* alt, char* buff, ssize_t count, unsigned long jif_end);
ssize_t	gsc_read_pio_work_32_bit	(GSC_ALT_STRUCT_T* alt, char* buff, ssize_t count, unsigned long jif_end);


void	gsc_reg_mod(GSC_ALT_STRUCT_T* alt, u32 reg, u32 val, u32 mask);	// GSC REGISTERS ONLY!
int		gsc_reg_mod_ioctl(GSC_ALT_STRUCT_T* alt, gsc_reg_t* arg);
u32		gsc_reg_read(GSC_ALT_STRUCT_T* alt, u32 reg);					// GSC REGISTERS ONLY!
int		gsc_reg_read_ioctl(GSC_ALT_STRUCT_T* alt, gsc_reg_t* arg);
void	gsc_reg_write(GSC_ALT_STRUCT_T* alt, u32 reg, u32 val);			// GSC REGISTERS ONLY!
int		gsc_reg_write_ioctl(GSC_ALT_STRUCT_T* alt, gsc_reg_t* arg);

void	gsc_sem_create(gsc_sem_t* sem);
void	gsc_sem_destroy(gsc_sem_t* sem);
int		gsc_sem_lock(gsc_sem_t* sem);
void	gsc_sem_unlock(gsc_sem_t* sem);

long	gsc_time_delta(unsigned long t1, unsigned long t2);

int		gsc_vpd_read_ioctl(dev_data_t* dev, gsc_vpd_t* vpd);

void	gsc_wait_close(GSC_ALT_STRUCT_T* alt);
int		gsc_wait_event(	GSC_ALT_STRUCT_T*	alt,
						gsc_wait_t*			wait,
						int					(*setup)(GSC_ALT_STRUCT_T* alt, unsigned long arg),
						unsigned long		arg,
						gsc_sem_t*			sem);
int		gsc_wait_resume_io(GSC_ALT_STRUCT_T* alt, u32 io);
int		gsc_wait_resume_irq_alt(GSC_ALT_STRUCT_T* alt_t, u32 alt);
int		gsc_wait_resume_irq_gsc(GSC_ALT_STRUCT_T* alt, u32 gsc);
int		gsc_wait_resume_irq_main(GSC_ALT_STRUCT_T* alt, u32 main);
int		gsc_write_abort_active_xfer(GSC_ALT_STRUCT_T* alt);
ssize_t	gsc_write(struct file *filp, const char *buf, size_t count, loff_t * offp);

// Must define GSC_WRITE_PIO_WORK and/or GSC_WRITE_PIO_WORK_XX_BIT
ssize_t	gsc_write_pio_work			(GSC_ALT_STRUCT_T* alt, const char* buff, ssize_t count, unsigned long jif_end);
ssize_t	gsc_write_pio_work_8_bit	(GSC_ALT_STRUCT_T* alt, const char* buff, ssize_t count, unsigned long jif_end);
ssize_t	gsc_write_pio_work_16_bit	(GSC_ALT_STRUCT_T* alt, const char* buff, ssize_t count, unsigned long jif_end);
ssize_t	gsc_write_pio_work_32_bit	(GSC_ALT_STRUCT_T* alt, const char* buff, ssize_t count, unsigned long jif_end);



GSC_ALT_STRUCT_T*	gsc_alt_data_t_locate(struct inode* inode);




// ****************************************************************************
// THESE ARE PROVIDED BY THE DEVICE SPECIFIC CODE

//#define	DEV_MODEL					"XXX"
//#define	DEV_NAME					"xxx"
//#define	DEV_VERSION					"x.x"
//#define	DEV_BAR_SHOW				0 or 1 (1 to show BAR info during init)
//#define	DEV_PCI_ID_SHOW				0 or 1 (1 to show ID info during init)
//#define	DEV_SUPPORTS_READ			define if read() is supported.
//#define	DEV_SUPPORTS_WRITE			define if write() is supported.
//#define	DEV_SUPPORTS_PROC_ID_STR	define if this string is supported.
//#define	DEV_SUPPORTS_VPD			Is Vital Product Data supported?
//#define	DEV_SUPPORTS_VPD_PCI9056	VPD follows PCI9056 implementation.

// If read() is supported, then the following must be provided.
//#define	DEV_PIO_READ_AVAILABLE		xxx
//#define	DEV_PIO_READ_WORK			xxx
//#define	DEV_DMA_READ_AVAILABLE		xxx
//#define	DEV_DMA_READ_WORK			xxx
//#define	DEV_DMDMA_READ_AVAILABLE	xxx
//#define	DEV_DMDMA_READ_WORK			xxx

// If write() is supported, then the following must be provided.
//#define	DEV_PIO_WRITE_AVAILABLE		xxx
//#define	DEV_PIO_WRITE_WORK			xxx
//#define	DEV_DMA_WRITE_AVAILABLE		xxx
//#define	DEV_DMA_WRITE_WORK			xxx
//#define	DEV_DMDMA_WRITE_AVAILABLE	xxx
//#define	DEV_DMDMA_WRITE_WORK		xxx

// Variables
extern	const gsc_dev_id_t	dev_id_list[];
extern	const gsc_ioctl_t	dev_ioctl_list[];

// Functions
int		dev_close					(GSC_ALT_STRUCT_T* alt);
int		dev_device_create			(dev_data_t* dev);
void	dev_device_destroy			(dev_data_t* dev);
void	dev_irq_isr_local_handler	(dev_data_t* dev);
int		dev_open					(GSC_ALT_STRUCT_T* alt);
ssize_t	dev_read_startup			(GSC_ALT_STRUCT_T* alt);
int		dev_reg_mod_alt				(GSC_ALT_STRUCT_T* alt, gsc_reg_t* arg);
int		dev_reg_read_alt			(GSC_ALT_STRUCT_T* alt, gsc_reg_t* arg);
int		dev_reg_write_alt			(GSC_ALT_STRUCT_T* alt, gsc_reg_t* arg);
int		gsc_wait_cancel_ioctl		(GSC_ALT_STRUCT_T* alt, void* arg);
int		gsc_wait_event_ioctl		(GSC_ALT_STRUCT_T* alt, void* arg);
int		gsc_wait_status_ioctl		(GSC_ALT_STRUCT_T* alt, void* arg);
int		dev_write_startup			(GSC_ALT_STRUCT_T* alt);



#endif
