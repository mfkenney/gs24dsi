// This software is covered by the GNU GENERAL PUBLIC LICENSE (GPL).
// $URL: http://subversion:8080/svn/gsc/trunk/drivers/gsc_common/linux/driver/gsc_sem.c $
// $Rev: 21125 $
// $Date: 2013-04-19 13:01:39 -0500 (Fri, 19 Apr 2013) $

#include "main.h"



/******************************************************************************
*
*	Function:	gsc_sem_create
*
*	Purpose:
*
*		Create a semaphore.
*
*	Arguments:
*
*		sem		The OS specific semaphore data is stored here.
*
*		count	The initial count for the semaphore.
*
*	Returned:
*
*		None.
*
******************************************************************************/

void gsc_sem_create(gsc_sem_t* sem)
{
	if (sem)
	{
		sema_init(&sem->sem, 1);
		sem->key	= (void*) sem;
	}
}



/******************************************************************************
*
*	Function:	gsc_sem_destroy
*
*	Purpose:
*
*		Destroy a semaphore.
*
*	Arguments:
*
*		sem		The OS specific semaphore data is stored here.
*
*	Returned:
*
*		None.
*
******************************************************************************/

void gsc_sem_destroy(gsc_sem_t* sem)
{
	if (sem)
		memset(sem, 0, sizeof(gsc_sem_t));
}



/******************************************************************************
*
*	Function:	gsc_sem_lock
*
*	Purpose:
*
*		Acquire a semaphore.
*
*	Arguments:
*
*		sem		The OS specific semaphore data is stored here.
*
*	Returned:
*
*		0		All went well.
*		< 0		The error for the problem.
*
******************************************************************************/

int gsc_sem_lock(gsc_sem_t* sem)
{
	int	ret;

	if ((sem) && (sem->key == sem))
	{
		ret	= down_interruptible(&sem->sem);

		if (ret)
			ret	= -ERESTARTSYS;
	}
	else
	{
		ret	= -EINVAL;
	}

	return(ret);
}



/******************************************************************************
*
*	Function:	gsc_sem_unlock
*
*	Purpose:
*
*		Release a semaphore.
*
*	Arguments:
*
*		sem		The semaphore to acquire.
*
*	Returned:
*
*		None.
*
******************************************************************************/

void gsc_sem_unlock(gsc_sem_t* sem)
{
	if ((sem) && (sem->key == sem))
		up(&sem->sem);
}


