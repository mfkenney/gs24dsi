// This software is covered by the GNU GENERAL PUBLIC LICENSE (GPL).
// $URL: http://subversion:8080/svn/gsc/trunk/drivers/gsc_common/linux/driver/gsc_vpd.c $
// $Rev: 21125 $
// $Date: 2013-04-19 13:01:39 -0500 (Fri, 19 Apr 2013) $

#include "main.h"


#if defined(DEV_SUPPORTS_VPD_PCI9056)
	#define	VPD_TYPE_PCI
#endif


//*****************************************************************************
#if defined(DEV_SUPPORTS_VPD)
#if defined(VPD_TYPE_PCI)
static int _vpd_read_dword(
	dev_data_t*	dev,
	int			adrs_off,
	int			data_off,
	int			adrs,
	u8*			ptr)
{
	int	i	= 0;
	int	ret	= 0;
	u16	status;

	pci_write_config_word(dev->pci, adrs_off, adrs);

	for (i = 1;; i++)
	{
		pci_read_config_word(dev->pci, adrs_off, &status);

		if (status & 0x8000)
			break;

		if (i > 100)
		{
			ret	= -ETIMEDOUT;
			break;
		}

		// Wait one timer tick before checking again.
		SET_CURRENT_STATE(TASK_INTERRUPTIBLE);
		schedule_timeout(1);
		SET_CURRENT_STATE(TASK_RUNNING);
	}

	pci_read_config_byte(dev->pci, data_off + 0, ptr + 0);
	pci_read_config_byte(dev->pci, data_off + 1, ptr + 1);
	pci_read_config_byte(dev->pci, data_off + 2, ptr + 2);
	pci_read_config_byte(dev->pci, data_off + 3, ptr + 3);
	return(ret);
}
#endif
#endif



//*****************************************************************************
#if defined(DEV_SUPPORTS_VPD)
#if defined(VPD_TYPE_PCI)
static int _vpd_load_data_pci(dev_data_t* dev, u8 adrs, u8 data)
{
	int	i;
	int	ret	= 0;
	int	size	= sizeof(dev->vpd.eeprom) / 4;

	for (i = 0; (ret == 0) && (i < size); i += 4)
		ret	= _vpd_read_dword(dev, adrs, data, i, dev->vpd.eeprom + i);

	return(ret);
}
#endif
#endif



//*****************************************************************************
#if defined(DEV_SUPPORTS_VPD)
#if defined(DEV_SUPPORTS_VPD_PCI9056)
static int _vpd_load_9056(dev_data_t* dev)
{
	int	ret	= 0;

	ret	= _vpd_load_data_pci(dev, 0x4E, 0x50);
	return(ret);
}
#endif
#endif



//*****************************************************************************
#if defined(DEV_SUPPORTS_VPD)
static int _vpd_auto_locate(dev_data_t* dev)
{
	int			i;
	const u8*	ptr		= (void*) dev->vpd.eeprom;
	int			size	= sizeof(dev->vpd.eeprom) - 10;

	dev->vpd.head	= NULL;

	for (i = 0; i < size; i++, ptr++)
	{
		if ((ptr[0] == 0x82) &&
			(ptr[3] == 'G')  &&
			(ptr[4] == 'S')  &&
			(ptr[5] == 'C'))
		{
			dev->vpd.head	= (void*) ptr;
			break;
		}
	}

	return(0);
}
#endif



//*****************************************************************************
#if defined(DEV_SUPPORTS_VPD)
static int _vpd_load(dev_data_t* dev)
{
	u16		did;
	u32*	ptr;
	int		ret	= 0;

	if (dev->vpd.loaded == 0)
	{
		memset(&dev->vpd, 0, sizeof(dev->vpd));
		pci_read_config_word(dev->pci, 2, &did);

		switch (did)
		{
			default:

				ret	= -ENOSYS;
				break;

#if defined(DEV_SUPPORTS_VPD_PCI9056)
			case 0x9056:

				ret	= _vpd_load_9056(dev);
				break;
#endif
		}

		dev->vpd.loaded	= 1;
		ptr				= (void*) dev->vpd.eeprom;

		if (ptr[0] == 0xFFFFFFFF)
			dev->vpd.head	= NULL;
		else
			_vpd_auto_locate(dev);
	}

	return(ret);
}
#endif



//*****************************************************************************
#if defined(DEV_SUPPORTS_VPD)
static void _long_string_copy(const u8* src, gsc_vpd_t* vpd)
{
	long	len;
	int		limit	= sizeof(vpd->data);

	len	= src[1] + 256L * src[2];
	len	= (len >= (limit - 1)) ? (limit - 1) : len;
	memcpy(vpd->data, src + 3, len);
	vpd->data[len]	= 0;
}
#endif



//*****************************************************************************
#if defined(DEV_SUPPORTS_VPD)
static void _long_string_skip(const u8** src)
{
	long	len;

	len		= src[0][1] + 256L * src[0][2];
	src[0]	+= len + 3;
}
#endif



//*****************************************************************************
#if defined(DEV_SUPPORTS_VPD)
static int _retrieve_vpd_r_field(const u8* src, u8 c1, u8 c2, gsc_vpd_t* vpd)
{
	int	len;
	int	limit	= sizeof(vpd->data);

	if (src[0] == 0x90)
	{
		// This is a VPD-R record.
		src	+= 3;	// Skip the header.

		for (;;)
		{
			if (src[0] == 0x78)	// End Tag
				break;

			len	= src[2];

			if ((src[0] == c1) && (src[1] == c2))
			{
				len	= (len >= (limit - 1)) ? (limit - 1) : len;
				memcpy(vpd->data, src + 3, len);
				vpd->data[len]	= 0;
			}

			src	+= 3 + len;	// Skip the header and the data.
		}
	}

	return(0);
}
#endif



//*****************************************************************************
#if defined(DEV_SUPPORTS_VPD)
static int _retrieve_model_number(dev_data_t* dev, gsc_vpd_t* vpd)
{
	const u8*	ptr;
	int			ret;

	ret	= _vpd_load(dev);
	vpd->reserved	= 0;
	vpd->data[0]	= 0;
	ptr				= dev->vpd.head;

	if ((ret == 0) && (ptr))
	{
		_long_string_skip(&ptr);
		ret	= _retrieve_vpd_r_field(ptr, 'P', 'N', vpd);
	}

	return(ret);
}
#endif



//*****************************************************************************
#if defined(DEV_SUPPORTS_VPD)
static int _retrieve_name(dev_data_t* dev, gsc_vpd_t* vpd)
{
	const u8*	ptr;
	int			ret;

	ret	= _vpd_load(dev);
	vpd->reserved	= 0;
	vpd->data[0]	= 0;
	ptr				= dev->vpd.head;

	if (ptr)
	{
		_long_string_skip(&ptr);
		_long_string_copy(ptr, vpd);
	}

	return(ret);
}
#endif



//*****************************************************************************
#if defined(DEV_SUPPORTS_VPD)
static int _retrieve_serial_number(dev_data_t* dev, gsc_vpd_t* vpd)
{
	const u8*	ptr;
	int			ret;

	ret	= _vpd_load(dev);
	vpd->reserved	= 0;
	vpd->data[0]	= 0;
	ptr				= dev->vpd.head;

	if ((ret == 0) && (ptr))
	{
		_long_string_skip(&ptr);
		ret	= _retrieve_vpd_r_field(ptr, 'S', 'N', vpd);
	}

	return(ret);
}
#endif



/******************************************************************************
*
*	Function:	gsc_vpd_retrieve
*
*	Purpose:
*
*		Retrieve vital product data from the board.
*
*	Arguments:
*
*		dev		The device data structure.
*
*		vpd		This indicates the specific data desired. This is also where
*				the retrieved data is stored.
*
*	Returned:
*
*		>= 0			This is the number of bytes read.
*		< 0				There was a problem and this is the error status.
*
******************************************************************************/

#if defined(DEV_SUPPORTS_VPD)
int gsc_vpd_read_ioctl(dev_data_t* dev, gsc_vpd_t* vpd)
{
	int	ret;

	ret	= _vpd_load(dev);
	memset(vpd->data, 0, sizeof(vpd->data));

	if (ret == 0)
	{
		switch (vpd->type)
		{
			default:

				vpd->data[0]	= 0;
				ret				= -EINVAL;
				break;

			case GSC_VPD_TYPE_MODEL_NUMBER:

				ret	= _retrieve_model_number(dev, vpd);
				break;

			case GSC_VPD_TYPE_NAME:

				ret	= _retrieve_name(dev, vpd);
				break;

			case GSC_VPD_TYPE_SERIAL_NUMBER:

				ret	= _retrieve_serial_number(dev, vpd);
				break;
		}
	}

	return(ret);
}
#endif


