// This software is covered by the GNU GENERAL PUBLIC LICENSE (GPL).
// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.0.1.0_GSC_DN/driver/irq.c $
// $Rev: 5782 $
// $Date: 2010-06-02 16:43:28 -0500 (Wed, 02 Jun 2010) $

#include "main.h"



//*****************************************************************************
void  dev_irq_isr_local_handler(dev_data_t* dev)
{
	#define	BCR_IRQ_REQUEST	0x800L

	u32	bcr;
	u32	req;

	bcr	= readl(dev->vaddr.gsc_bctlr_32);

	if (bcr & BCR_IRQ_REQUEST)
	{
		// Clear the interrupt.
		bcr	^= BCR_IRQ_REQUEST;
		writel(bcr, dev->vaddr.gsc_bctlr_32);

		// Resume any waiting threads.
		req	= GSC_FIELD_DECODE(bcr, 10, 8);

		switch (req)
		{
			default:

				gsc_wait_resume_irq_main(dev, GSC_WAIT_MAIN_SPURIOUS);
				break;

			case DSI_IRQ_INIT_DONE:

				gsc_wait_resume_irq_gsc(dev, DSI_WAIT_GSC_INIT_DONE);
				break;

			case DSI_IRQ_AUTO_CAL_DONE:

				gsc_wait_resume_irq_gsc(dev, DSI_WAIT_GSC_AUTO_CAL_DONE);
				break;

			case DSI_IRQ_CHAN_READY:

				gsc_wait_resume_irq_gsc(dev, DSI_WAIT_GSC_CHAN_READY);
				break;

			case DSI_IRQ_AIN_BUF_THRESH_L2H:

				gsc_wait_resume_irq_gsc(dev, DSI_WAIT_GSC_AIN_BUF_THRESH_L2H);
				break;

			case DSI_IRQ_AIN_BUF_THRESH_H2L:

				gsc_wait_resume_irq_gsc(dev, DSI_WAIT_GSC_AIN_BUF_THRESH_H2L);
				break;
		}
	}
	else
	{
		// We don't know the source of the interrupt.
		gsc_wait_resume_irq_main(dev, GSC_WAIT_MAIN_SPURIOUS);
	}
}


