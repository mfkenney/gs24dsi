// This software is covered by the GNU GENERAL PUBLIC LICENSE (GPL).
// $URL: http://subversion:8080/svn/gsc/trunk/drivers/gsc_common/linux/driver/gsc_open.c $
// $Rev: 21125 $
// $Date: 2013-04-19 13:01:39 -0500 (Fri, 19 Apr 2013) $

#include "main.h"



//*****************************************************************************
int gsc_open(struct inode *inode, struct file *fp)
{
	#define	_RW_	(FMODE_READ | FMODE_WRITE)

	GSC_ALT_STRUCT_T*	alt;
	int					ret;

	for (;;)	// We'll use a loop for convenience.
	{
		if ((fp->f_mode & _RW_) != _RW_)
		{
			// Read and write access are both required.
			ret	= -EINVAL;
			break;
		}

		alt	= gsc_alt_data_t_locate(inode);

		if (alt == NULL)
		{
			// The referenced device doesn't exist.
			ret	= -ENODEV;
			break;
		}

		ret	= gsc_sem_lock(&alt->sem);

		if (ret)
		{
			// We didn't get the semaphore.
			break;
		}

		if (alt->in_use)
		{
			// The device is already being used.
			ret	= -EBUSY;
			gsc_sem_unlock(&alt->sem);
			break;
		}

		ret	= gsc_module_count_inc();

		if (ret)
		{
			// We coundn't increase the usage count.
			gsc_sem_unlock(&alt->sem);
			break;
		}

		// Perform device specific OPEN actions.
		ret	= dev_open(alt);

		if (ret)
		{
			// There was a problem here.
			dev_close(alt);
			gsc_sem_unlock(&alt->sem);
			gsc_module_count_dec();
			break;
		}

		alt->in_use			= 1;	// Mark as being open/in use.
		fp->private_data	= alt;

		gsc_sem_unlock(&alt->sem);
		break;
	}

	return(ret);
}


