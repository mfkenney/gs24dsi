// This software is covered by the GNU GENERAL PUBLIC LICENSE (GPL).
// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.0.1.0_GSC_DN/driver/ioctl.c $
// $Rev: 17651 $
// $Date: 2012-08-20 17:15:19 -0500 (Mon, 20 Aug 2012) $

#include "main.h"



// variables	******************************************************************

static const s32	_filter[]	=
{
	DSI_AIN_FILTER_HI,
	DSI_AIN_FILTER_LOW,
	-1	// terminate list
};

static const s32	_grp_src_a[]	=
{
	DSI_CH_GRP_SRC_GEN_A,
	DSI_CH_GRP_SRC_EXTERN,
	DSI_CH_GRP_SRC_DIR_EXTERN,
	DSI_CH_GRP_SRC_DISABLE,
	-1	// terminate list
};

static const s32	_grp_src_ab[]	=
{
	DSI_CH_GRP_SRC_GEN_A,
	DSI_CH_GRP_SRC_GEN_B,
	DSI_CH_GRP_SRC_EXTERN,
	DSI_CH_GRP_SRC_DIR_EXTERN,
	DSI_CH_GRP_SRC_DISABLE,
	-1	// terminate list
};

static const s32	_grp_src_alt[]	=
{
	DSI_CH_GRP_SRC_DISABLE,
	5,	// DSI_CH_GRP_SRC_ENABLE
	-1	// terminate list
};

static const s32	_range[]	=
{
	DSI_AIN_RANGE_2_5V,
	DSI_AIN_RANGE_5V,
	DSI_AIN_RANGE_10V,
	-1	// terminate list
};



//*****************************************************************************
static int _arg_s32_list_reg(
	dev_data_t*	dev,
	s32*		arg,
	const s32*	list,
	u32			reg,
	int			begin,
	int			end)
{
	int			i;
	u32			mask;
	u16			offset;
	int			ret		= -EINVAL;
	u32			v;
	VADDR_T		vaddr;

	mask	= GSC_FIELD_ENCODE(0xFFFFFFFF, begin, end);
	offset	= GSC_REG_OFFSET(reg);
	vaddr	= GSC_VADDR(dev, offset);

	if (arg[0] == -1)
	{
		ret	= 0;
		v	= readl(vaddr);
		v	&= mask;
		v	>>= end;

		for (i = 0; list[i] >= 0; i++)
		{
			if (list[i] == v)
			{
				arg[0]	= v;
				break;
			}
		}
	}
	else
	{
		for (i = 0; list[i] >= 0; i++)
		{
			if (list[i] == arg[0])
			{
				ret	= 0;
				v	= readl(vaddr);
				v	&= ~mask;
				v	|= (list[i] << end);
				writel(v, vaddr);
				break;
			}
		}
	}

	return(ret);
}



//*****************************************************************************
static int _arg_s32_list_var(
	dev_data_t*	dev,
	s32*		arg,
	const s32*	list,
	s32*		var)
{
	int	i;
	int	ret	= -EINVAL;

	if (arg[0] == -1)
	{
		ret	= 0;
		arg[0]	= var[0];
	}
	else
	{
		for (i = 0; list[i] >= 0; i++)
		{
			if (list[i] == arg[0])
			{
				ret		= 0;
				var[0]	= arg[0];
				break;
			}
		}
	}

	return(ret);
}



//*****************************************************************************
static int _arg_s32_range_reg(
	dev_data_t*	dev,
	s32*		arg,
	s32			min,
	s32			max,
	u32			reg,
	int			begin,
	int			end)
{
	u32			mask;
	u16			offset;
	int			ret		= -EINVAL;
	u32			v;
	VADDR_T		vaddr;

	mask	= GSC_FIELD_ENCODE(0xFFFFFFFF, begin, end);
	offset	= GSC_REG_OFFSET(reg);
	vaddr	= GSC_VADDR(dev, offset);

	if (arg[0] == -1)
	{
		ret		= 0;
		arg[0]	= readl(vaddr);
		arg[0]	&= mask;
		arg[0]	= (u32) arg[0] >> end;
	}
	else if ((arg[0] >= min) && (arg[0] <= max))
	{
		ret	= 0;
		v	= readl(vaddr);
		v	&= ~mask;
		v	|= (arg[0] << end);
		writel(v, vaddr);
	}

	return(ret);
}



//*****************************************************************************
static int _arg_s32_range_var(
	dev_data_t*	dev,
	s32*		arg,
	s32			min,
	s32			max,
	s32*		var)
{
	int	ret	= -EINVAL;

	if (arg[0] == -1)
	{
		ret		= 0;
		arg[0]	= var[0];
	}
	else if ((arg[0] >= min) && (arg[0] <= max))
	{
		ret		= 0;
		var[0]	= arg[0];
	}

	return(ret);
}



//*****************************************************************************
static void _bctlr_mod(dev_data_t* dev, u32 value, u32 mask)
{
	u32	bctlr;
	int	status;

	status	= gsc_irq_access_lock(dev, 0);
	bctlr	= readl(dev->vaddr.gsc_bctlr_32);
	bctlr	= (bctlr & ~mask) | (value & mask) | 0x800;
	writel(bctlr, dev->vaddr.gsc_bctlr_32);

	if (status == 0)
		gsc_irq_access_unlock(dev, 0);
}



//*****************************************************************************
static int _wait_for_reg_field_value(
	dev_data_t*	dev,
	long		ms_limit,
	VADDR_T		vaddr,
	u32			mask,
	u32			value)
{
	unsigned long	limit;
	u32				reg;
	int				ret;

	limit	= jiffies + MS_TO_JIFFIES(ms_limit);

	for (;;)
	{
		reg	= readl(vaddr);

		if ((reg & mask) == value)
		{
			ret	= 0;
			break;
		}

		if (JIFFIES_TIMEOUT(limit))
		{
			// We've waited long enough.
			ret	= -EIO;
			break;
		}

		// Wait one timer tick before checking again.
		SET_CURRENT_STATE(TASK_INTERRUPTIBLE);
		schedule_timeout(1);
		SET_CURRENT_STATE(TASK_RUNNING);
	}

	return(ret);
}



//*****************************************************************************
static int _wait_for_ready(dev_data_t* dev, long ms_limit)
{
	#define	CHAN_READY	(1L << 13)

	u32				bcr;
	unsigned long	limit;
	int				ret;

	limit	= jiffies + MS_TO_JIFFIES(ms_limit);

	for (;;)
	{
		// Read the register so we can check.
		bcr	= readl(dev->vaddr.gsc_bctlr_32);

		if (bcr & CHAN_READY)
		{
			ret	= 0;
			break;
		}

		if (JIFFIES_TIMEOUT(limit))
		{
			// We've waited long enough.
			ret	= -ETIMEDOUT;
			break;
		}

		// Wait one timer tick before checking again.
		SET_CURRENT_STATE(TASK_INTERRUPTIBLE);
		schedule_timeout(1);
		SET_CURRENT_STATE(TASK_RUNNING);
	}

	return(ret);
}



//*****************************************************************************
static int _query(dev_data_t* dev, s32* arg)
{
	switch (arg[0])
	{
		default:						arg[0]	= DSI_IOCTL_QUERY_ERROR;			break;
		case DSI_QUERY_AUTO_CAL_MS:		arg[0]	= dev->cache.auto_cal_ms;			break;
		case DSI_QUERY_CH_GRP_0_RAR:	arg[0]	= dev->cache.ch_grp_0_rar_pri;		break;
		case DSI_QUERY_CHANNEL_GPS:		arg[0]	= dev->cache.channel_groups;		break;
		case DSI_QUERY_CHANNEL_MAX:		arg[0]	= dev->cache.channels_max;			break;
		case DSI_QUERY_CHANNEL_QTY:		arg[0]	= dev->cache.channel_qty;			break;
		case DSI_QUERY_COUNT:			arg[0]	= DSI_IOCTL_QUERY_LAST + 1;			break;
		case DSI_QUERY_D0_1_IN_MODE:	arg[0]	= dev->cache.bctlr_d0_1_in_mode;	break;
		case DSI_QUERY_D2_3_RANGE:		arg[0]	= dev->cache.bctlr_d2_3_range;		break;
		case DSI_QUERY_D15_PLL:			arg[0]	= dev->cache.bcfgr_d15_pll;			break;
		case DSI_QUERY_D16_CHANNELS:	arg[0]	= dev->cache.bcfgr_d16_chans;		break;
		case DSI_QUERY_D16_CHANNELS_0:	arg[0]	= dev->cache.bcfgr_d16_chans_0;		break;
		case DSI_QUERY_D16_SYNC_1:		arg[0]	= dev->cache.bctlr_d16_sync_1;		break;
		case DSI_QUERY_D17_CHANNELS:	arg[0]	= dev->cache.bcfgr_d17_chans;		break;
		case DSI_QUERY_D17_CHANNELS_0:	arg[0]	= dev->cache.bcfgr_d17_chans_0;		break;
		case DSI_QUERY_D17_18_RANGE:	arg[0]	= dev->cache.bcfgr_d17_18_range;	break;
		case DSI_QUERY_D18_CF_FILT:		arg[0]	= dev->cache.bcfgr_d18_cf_filt;		break;
		case DSI_QUERY_D18_RIO:			arg[0]	= dev->cache.bcfgr_d18_rear_io;		break;
		case DSI_QUERY_D19_EXT_TEMP:	arg[0]	= dev->cache.bcfgr_d19_ext_temp;	break;
		case DSI_QUERY_D19_20_FILT:		arg[0]	= dev->cache.bcfgr_d19_20_filt;		break;
		case DSI_QUERY_D20_LOW_POWER:	arg[0]	= dev->cache.bcfgr_d20_low_pow;		break;
		case DSI_QUERY_D20_XCVR:		arg[0]	= dev->cache.bctlr_d20_xcvr;		break;
		case DSI_QUERY_D21_EXT_TEMP:	arg[0]	= dev->cache.bcfgr_d21_ext_temp;	break;
		case DSI_QUERY_D21_EXT_TRIG:	arg[0]	= dev->cache.bctlr_d21_arm_et;		break;
		case DSI_QUERY_D22_24DSI6LN:	arg[0]	= dev->cache.bcfgr_d22_24dsi6ln;	break;
		case DSI_QUERY_D22_THR_FLAG:	arg[0]	= dev->cache.bctlr_d22_thr_f_out;	break;
		case DSI_QUERY_D19_FREQ_FILT:	arg[0]	= dev->cache.bctlr_d19_freq_filt;	break;
		case DSI_QUERY_D22_COUPLING:	arg[0]	= dev->cache.bctlr_d22_coupling;	break;
		case DSI_QUERY_D23_INV_EXT_TRIG:arg[0]	= dev->cache.bctlr_d23_inv_e_trg;	break;
		case DSI_QUERY_D23_RATE_OUT:	arg[0]	= dev->cache.bctlr_d23_rate_out;	break;
		case DSI_QUERY_DEVICE_TYPE:		arg[0]	= dev->board_type;					break;
		case DSI_QUERY_FGEN_MAX:		arg[0]	= dev->cache.rate_gen_fgen_max;		break;
		case DSI_QUERY_FGEN_MIN:		arg[0]	= dev->cache.rate_gen_fgen_min;		break;
		case DSI_QUERY_FIFO_SIZE:		arg[0]	= dev->cache.fifo_size;				break;
		case DSI_QUERY_FREF_DEFAULT:	arg[0]	= dev->cache.fref_default;			break;	// PLL
		case DSI_QUERY_FSAMP_DEFAULT:	arg[0]	= dev->cache.fsamp_default;			break;
		case DSI_QUERY_FSAMP_MAX:		arg[0]	= dev->cache.fsamp_max;				break;
		case DSI_QUERY_FSAMP_MIN:		arg[0]	= dev->cache.fsamp_min;				break;
		case DSI_QUERY_GPS_PRESENT:		arg[0]	= dev->cache.gps_present;			break;	// GPS
		case DSI_QUERY_INIT_MS:			arg[0]	= dev->cache.initialize_ms;			break;
		case DSI_QUERY_LOW_NOISE:		arg[0]	= dev->cache.low_noise;				break;
		case DSI_QUERY_LOW_POWER:		arg[0]	= dev->cache.low_power;				break;
		case DSI_QUERY_NDIV_MASK:		arg[0]	= dev->cache.rate_div_ndiv_mask;	break;
		case DSI_QUERY_NDIV_MAX:		arg[0]	= dev->cache.rate_div_ndiv_max;		break;
		case DSI_QUERY_NDIV_MIN:		arg[0]	= dev->cache.rate_div_ndiv_min;		break;
		case DSI_QUERY_NRATE_MASK:		arg[0]	= dev->cache.rate_gen_nrate_mask;	break;	// LEGACY
		case DSI_QUERY_NRATE_MAX:		arg[0]	= dev->cache.rate_gen_nrate_max;	break;	// LEGACY
		case DSI_QUERY_NRATE_MIN:		arg[0]	= dev->cache.rate_gen_nrate_min;	break;	// LEGACY
		case DSI_QUERY_NREF_MASK:		arg[0]	= dev->cache.rate_gen_nref_mask;	break;	// PLL
		case DSI_QUERY_NREF_MAX:		arg[0]	= dev->cache.rate_gen_nref_max;		break;	// PLL
		case DSI_QUERY_NREF_MIN:		arg[0]	= dev->cache.rate_gen_nref_min;		break;	// PLL
		case DSI_QUERY_NVCO_MASK:		arg[0]	= dev->cache.rate_gen_nvco_mask;	break;	// PLL
		case DSI_QUERY_NVCO_MAX:		arg[0]	= dev->cache.rate_gen_nvco_max;		break;	// PLL
		case DSI_QUERY_NVCO_MIN:		arg[0]	= dev->cache.rate_gen_nvco_min;		break;	// PLL
		case DSI_QUERY_PLL_PRESENT:		arg[0]	= dev->cache.pll_present;			break;
		case DSI_QUERY_RATE_DIV_QTY:	arg[0]	= dev->cache.rate_div_qty;			break;
		case DSI_QUERY_RATE_GEN_QTY:	arg[0]	= dev->cache.rate_gen_qty;			break;
		case DSI_QUERY_RCAR_RCBR:		arg[0]	= dev->cache.rcar_rcbr;				break;
		case DSI_QUERY_REG_ICMR:		arg[0]	= dev->cache.reg_icmr;				break;
		case DSI_QUERY_RFCR:			arg[0]	= dev->cache.rfcr;					break;
	}

	return(0);
}



//*****************************************************************************
static int _init_start(dev_data_t* dev, unsigned long arg)
{
	#define	BCTLR_INIT_BIT		0x8000

	// Initiate auto-calibration.
	_bctlr_mod(dev, BCTLR_INIT_BIT, BCTLR_INIT_BIT);
	return(0);
}



//*****************************************************************************
int initialize_ioctl(dev_data_t* dev, void* arg)
{
	#define	BCTLR_IRQ_INIT_DONE	0x0000
	#define	BCTLR_IRQ_MASK		0x0700

	int			i;
	long		ms;
	u32			reg;
	int			ret;
	gsc_sem_t	sem;
	gsc_wait_t	wt;

	// Safely select the Initialization Done interrupt.
	_bctlr_mod(dev, BCTLR_IRQ_INIT_DONE, BCTLR_IRQ_MASK);

	// Wait for the local interrupt.
	gsc_sem_create(&sem);	// dummy, required for wait operations.
	ms				= dev->cache.initialize_ms + 5000;
	memset(&wt, 0, sizeof(wt));
	wt.flags		= GSC_WAIT_FLAG_INTERNAL;
	wt.gsc			= DSI_WAIT_GSC_INIT_DONE;
	wt.timeout_ms	= ms;
	ret				= gsc_wait_event(dev, &wt, _init_start, (unsigned long) NULL, &sem);
	gsc_sem_destroy(&sem);

	if (wt.flags & GSC_WAIT_FLAG_TIMEOUT)
	{
		ret	= ret ? ret : -ETIMEDOUT;
		printk("%s: INITIALIZATION TIMED OUT: %ld ms\n", dev->model, ms);
	}

	// Manually wait for completion to insure that initialization completes
	// even if we get a signal, such as ^c.
	i	= _wait_for_reg_field_value(
			dev,
			ms,
			dev->vaddr.gsc_bctlr_32,
			BCTLR_INIT_BIT,
			0);
	ret	= ret ? ret : i;

	// Check the completion status.
	reg	= readl(dev->vaddr.gsc_bctlr_32);

	if (ret)
	{
	}
	else if (reg & BCTLR_INIT_BIT)
	{
		ret	= ret ? ret : -EIO;
		printk(	"%s: INITIALIZATION DID NOT COMPLETE (%ld ms)\n",
				dev->model,
				(long) ms);
	}

	// Initialize the software settings.
	dev->rx.bytes_per_sample	= 4;
	dev->rx.io_mode				= DSI_IO_MODE_DEFAULT;
	dev->rx.overflow_check		= DSI_IO_OVERFLOW_DEFAULT;
	dev->rx.pio_threshold		= 32;
	dev->rx.timeout_s			= DSI_IO_TIMEOUT_DEFAULT;
	dev->rx.underflow_check		= DSI_IO_UNDERFLOW_DEFAULT;
	return(ret);
}



//*****************************************************************************
static int _auto_cal_start(dev_data_t* dev, unsigned long arg)
{
	#define	AUTO_CAL_START	0x0080

	// Initiate auto-calibration.
	_bctlr_mod(dev, AUTO_CAL_START, AUTO_CAL_START);
	return(0);
}



//*****************************************************************************
static int _auto_calibrate(dev_data_t* dev, void* arg)
{
	#define	BCTLR_IRQ_AUTO_CAL_DONE	0x0100
	#define	AUTO_CAL_PASS			0x1000

	int			i;
	long		ms;
	u32			reg;
	int			ret;
	gsc_sem_t	sem;
	gsc_wait_t	wt;

	if (dev->cache.auto_cal_ms)
	{
		// Safely select the Auto-Cal Done interrupt.
		_bctlr_mod(dev, BCTLR_IRQ_AUTO_CAL_DONE, BCTLR_IRQ_MASK);

		// Wait for the local interrupt.
		gsc_sem_create(&sem);	// dummy, required for wait operations.
		ms				= dev->cache.auto_cal_ms + 5000;
		memset(&wt, 0, sizeof(wt));
		wt.flags		= GSC_WAIT_FLAG_INTERNAL;
		wt.gsc			= DSI_WAIT_GSC_AUTO_CAL_DONE;
		wt.timeout_ms	= ms;
		ret				= gsc_wait_event(dev, &wt, _auto_cal_start, (unsigned long) NULL, &sem);
		gsc_sem_destroy(&sem);

		if (wt.flags & GSC_WAIT_FLAG_TIMEOUT)
		{
			ret	= ret ? ret : -ETIMEDOUT;
			printk(	"%s: AUTO-CALIBRATION TIMED OUT (%ld ms).\n",
					dev->model,
					(long) ms);
		}

		// Manually wait for completion to insure that Auto Cal completes
		// even if we got a signal, such as ^c.
		i	= _wait_for_reg_field_value(
				dev,
				ms,
				dev->vaddr.gsc_bctlr_32,
				AUTO_CAL_START,
				0);
		ret	= ret ? ret : i;

		// Check the completion status.
		reg	= readl(dev->vaddr.gsc_bctlr_32);

		if (ret)
		{
		}
		else if (reg & AUTO_CAL_START)
		{
			ret	= ret ? ret : -EIO;
			printk(	"%s: AUTO-CALIBRATION DID NOT COMPLETE (%ld ms)\n",
					dev->model,
					(long) ms);
		}
		else if ((reg & AUTO_CAL_PASS) == 0)
		{
			ret	= ret ? ret : -EIO;
			printk(	"%s: AUTO-CALIBRATION FAILED (%ld ms)\n",
					dev->model,
					(long) ms);
		}
	}
	else
	{
		// Auto-calibration is not supported on this board.
		ret	= -EIO;
	}

	return(ret);
}



//*****************************************************************************
static int _rx_io_mode(dev_data_t* dev, s32* arg)
{
	static const s32	options[]	=
	{
		GSC_IO_MODE_PIO,
		GSC_IO_MODE_DMA,
		GSC_IO_MODE_DMDMA,
		-1	// terminate list
	};

	int	ret;
	s32	var;

	var	= dev->rx.io_mode;
	ret	= _arg_s32_list_var(dev, arg, options, &var);
	dev->rx.io_mode	= var;
	return(ret);
}



//*****************************************************************************
static int _rx_io_overflow(dev_data_t* dev, void* arg)
{
	static const s32	options[]	=
	{
		DSI_IO_OVERFLOW_IGNORE,
		DSI_IO_OVERFLOW_CHECK,
		-1	// terminate list
	};

	int	ret;

	ret	= _arg_s32_list_var(dev, arg, options, &dev->rx.overflow_check);
	return(ret);
}



//*****************************************************************************
static int _rx_io_timeout(dev_data_t* dev, void* arg)
{
	int	ret;

	ret		= _arg_s32_range_var(
				dev,
				arg,
				DSI_IO_TIMEOUT_MIN,
				DSI_IO_TIMEOUT_MAX,
				&dev->rx.timeout_s);
	return(ret);
}



//*****************************************************************************
static int _rx_io_underflow(dev_data_t* dev, void* arg)
{
	static const s32	options[]	=
	{
		DSI_IO_UNDERFLOW_IGNORE,
		DSI_IO_UNDERFLOW_CHECK,
		-1	// terminate list
	};

	int	ret;

	ret	= _arg_s32_list_var(dev, arg, options, &dev->rx.underflow_check);
	return(ret);
}



//*****************************************************************************
static int _adc_rate_out(dev_data_t* dev, void* arg)
{
	static const s32	options[]	=
	{
		DSI_SW_SYNC_MODE_SW_SYNC,
		DSI_SW_SYNC_MODE_CLR_BUF,
		-1	// terminate list
	};

	int	ret;

	if (dev->cache.bctlr_d23_rate_out)
		ret	= _arg_s32_list_reg(dev, arg, options, DSI_GSC_BCTLR, 23, 23);
	else
		ret	= -EINVAL;

	if (ret == 0)
		ret	= _wait_for_ready(dev, 1000);

	return(ret);
}



//*****************************************************************************
static int _ain_buf_clear(dev_data_t* dev, void* arg)
{
	#undef	MASK
	#undef	SHIFT

	u32	mask;
	int	ret;

	mask	= 0x00080000;	// Clear the buffer.
	reg_mem32_mod(dev->vaddr.gsc_ibcr_32, mask, mask);
	mask	|= 0x01000000;	// Clear any overflow.
	mask	|= 0x02000000;	// Clear any underflow.
	reg_mem32_mod(dev->vaddr.gsc_ibcr_32, 0, mask);
	ret		= _wait_for_ready(dev, 1000);
	return(ret);
}



//*****************************************************************************
static int _ain_buf_input(dev_data_t* dev, void* arg)
{
	static const s32	options[]	=
	{
		DSI_AIN_BUF_INPUT_DISABLE,
		DSI_AIN_BUF_INPUT_ENABLE,
		-1	// terminate list
	};

	int	ret;

	ret	= _arg_s32_list_reg(dev, arg, options, DSI_GSC_IBCR, 18, 18);

	if (ret == 0)
		ret	= _wait_for_ready(dev, 1000);

	return(ret);
}



//*****************************************************************************
static int _ain_buf_overflow(dev_data_t* dev, void* arg)
{
	static const s32	options[]	=
	{
		DSI_AIN_BUF_OVERFLOW_NO,
		DSI_AIN_BUF_OVERFLOW_YES,
		-1	// terminate list
	};

	int	ret;

	ret	= _arg_s32_list_reg(dev, arg, options, DSI_GSC_IBCR, 24, 24);

	if (ret == 0)
		ret	= _wait_for_ready(dev, 1000);

	return(ret);
}



//*****************************************************************************
static int _ain_buf_thresh(dev_data_t* dev, s32* arg)
{
	int	ret;

	ret	= _arg_s32_range_reg(dev, arg, 0, 0x3FFFF, DSI_GSC_IBCR, 17, 0);

	if (ret == 0)
		ret	= _wait_for_ready(dev, 1000);

	return(ret);
}



//*****************************************************************************
static int _ain_buf_underflow(dev_data_t* dev, s32* arg)
{
	static const s32	options[]	=
	{
		DSI_AIN_BUF_UNDERFLOW_NO,
		DSI_AIN_BUF_UNDERFLOW_YES,
		-1	// terminate list
	};

	int	ret;

	ret	= _arg_s32_list_reg(dev, arg, options, DSI_GSC_IBCR, 25, 25);
	return(ret);
}



//*****************************************************************************
static int _ain_coupling(dev_data_t* dev, void* arg)
{
	static const s32	options[]	=
	{
		DSI_AIN_COUPLING_AC,
		DSI_AIN_COUPLING_DC,
		-1	// terminate list
	};

	int	ret;

	if (dev->cache.bctlr_d22_coupling)
		ret	= _arg_s32_list_reg(dev, arg, options, DSI_GSC_BCTLR, 22, 22);
	else
		ret	= -EINVAL;

	if (ret == 0)
		ret	= _wait_for_ready(dev, 1000);

	return(ret);
}



//*****************************************************************************
static int _ain_filter(dev_data_t* dev, s32* arg)
{
	int	ret	= 0;

	if (dev->cache.bctlr_d19_freq_filt == 0)
		ret	= -EINVAL;
	else if (arg[0] == -1)
		ret	= _arg_s32_list_reg(dev, arg, _filter, DSI_GSC_BCTLR, 19, 19);
	else if (arg[0] == DSI_AIN_FILTER_HI)
		_bctlr_mod(dev, 0, D19);
	else if (arg[0] == DSI_AIN_FILTER_LOW)
		_bctlr_mod(dev, D19, D19);
	else
		ret	= -EINVAL;

	if (ret == 0)
		ret	= _wait_for_ready(dev, 1000);

	return(ret);
}



//*****************************************************************************
static int _ain_filter_sel(dev_data_t* dev, void* arg)
{
	static const s32	options[]	=
	{
		DSI_AIN_FILTER_SEL_DISABLE,
		DSI_AIN_FILTER_SEL_ENABLE,
		-1	// terminate list
	};

	int	ret;

	if (dev->cache.rfcr)
		ret	= _arg_s32_list_reg(dev, arg, options, DSI_GSC_RFCR, 8, 8);
	else
		ret	= -EINVAL;

	return(ret);
}



//*****************************************************************************
static int _ain_filter_00_03(dev_data_t* dev, void* arg)
{
	int	ret;

	if ((dev->cache.rfcr == 0) || (dev->cache.channel_qty <= 3))
		ret	= -EINVAL;
	else
		ret	= _arg_s32_list_reg(dev, arg, _filter, DSI_GSC_RFCR, 0, 0);

	return(ret);
}



//*****************************************************************************
static int _ain_filter_04_07(dev_data_t* dev, void* arg)
{
	int	ret;

	if ((dev->cache.rfcr == 0) || (dev->cache.channel_qty <= 7))
		ret	= -EINVAL;
	else
		ret	= _arg_s32_list_reg(dev, arg, _filter, DSI_GSC_RFCR, 1, 1);

	return(ret);
}



//*****************************************************************************
static int _ain_filter_08_11(dev_data_t* dev, void* arg)
{
	int	ret;

	if ((dev->cache.rfcr == 0) || (dev->cache.channel_qty <= 11))
		ret	= -EINVAL;
	else
		ret	= _arg_s32_list_reg(dev, arg, _filter, DSI_GSC_RFCR, 2, 2);

	return(ret);
}



//*****************************************************************************
static int _ain_filter_12_15(dev_data_t* dev, void* arg)
{
	int	ret;

	if ((dev->cache.rfcr == 0) || (dev->cache.channel_qty <= 15))
		ret	= -EINVAL;
	else
		ret	= _arg_s32_list_reg(dev, arg, _filter, DSI_GSC_RFCR, 3, 3);

	return(ret);
}



//*****************************************************************************
static int _ain_filter_16_19(dev_data_t* dev, void* arg)
{
	int	ret;

	if ((dev->cache.rfcr == 0) || (dev->cache.channel_qty <= 19))
		ret	= -EINVAL;
	else
		ret	= _arg_s32_list_reg(dev, arg, _filter, DSI_GSC_RFCR, 4, 4);

	return(ret);
}



//*****************************************************************************
static int _ain_filter_20_23(dev_data_t* dev, void* arg)
{
	int	ret;

	if ((dev->cache.rfcr == 0) || (dev->cache.channel_qty <= 23))
		ret	= -EINVAL;
	else
		ret	= _arg_s32_list_reg(dev, arg, _filter, DSI_GSC_RFCR, 5, 5);

	return(ret);
}



//*****************************************************************************
static int _ain_filter_24_27(dev_data_t* dev, void* arg)
{
	int	ret;

	if ((dev->cache.rfcr == 0) || (dev->cache.channel_qty <= 27))
		ret	= -EINVAL;
	else
		ret	= _arg_s32_list_reg(dev, arg, _filter, DSI_GSC_RFCR, 6, 6);

	return(ret);
}



//*****************************************************************************
static int _ain_filter_28_31(dev_data_t* dev, void* arg)
{
	int	ret;

	if ((dev->cache.rfcr == 0) || (dev->cache.channel_qty <= 31))
		ret	= -EINVAL;
	else
		ret	= _arg_s32_list_reg(dev, arg, _filter, DSI_GSC_RFCR, 7, 7);

	return(ret);
}



//*****************************************************************************
static int _ain_mode(dev_data_t* dev, s32* arg)
{
	static const s32	options[]	=
	{
		DSI_AIN_MODE_DIFF,
		DSI_AIN_MODE_ZERO,
		DSI_AIN_MODE_VREF,
		-1	// terminate list
	};

	int	ret	= 0;

	if (dev->cache.bctlr_d0_1_in_mode == 0)
	{
		ret	= -EINVAL;
	}
	else
	{
		switch (arg[0])
		{
			case -1:

				ret	= _arg_s32_list_reg(dev, arg, options, DSI_GSC_BCTLR, 1, 0);
				break;

			default:				ret	= -EINVAL;				break;
			case DSI_AIN_MODE_DIFF:	_bctlr_mod(dev, 0, 0x3);	break;
			case DSI_AIN_MODE_ZERO:	_bctlr_mod(dev, 2, 0x3);	break;
			case DSI_AIN_MODE_VREF:	_bctlr_mod(dev, 3, 0x3);	break;
		}
	}

	return(ret);
}



//*****************************************************************************
static int _ain_range(dev_data_t* dev, s32* arg)
{
	static const s32	_range_lp[]	=
	{
		DSI_AIN_RANGE_2_5V,
		DSI_AIN_RANGE_5V,
		-1	// terminate list
	};

	int	ret	= 0;

	if (dev->cache.bctlr_d2_3_range == 0)
	{
		ret	= -EINVAL;
	}
	else if (dev->cache.low_power)
	{
		switch (arg[0])
		{
			case -1:

				ret	= _arg_s32_list_reg(dev, arg, _range_lp, DSI_GSC_BCTLR, 3, 2);
				break;

			default:					ret	= -EINVAL;				break;
			case DSI_AIN_RANGE_2_5V:	_bctlr_mod(dev, 0x4, 0xC);	break;
			case DSI_AIN_RANGE_5V:		_bctlr_mod(dev, 0x8, 0xC);	break;
		}
	}
	else
	{
		switch (arg[0])
		{
			case -1:

				ret	= _arg_s32_list_reg(dev, arg, _range, DSI_GSC_BCTLR, 3, 2);
				break;

			default:					ret	= -EINVAL;				break;
			case DSI_AIN_RANGE_2_5V:	_bctlr_mod(dev, 0x4, 0xC);	break;
			case DSI_AIN_RANGE_5V:		_bctlr_mod(dev, 0x8, 0xC);	break;
			case DSI_AIN_RANGE_10V:		_bctlr_mod(dev, 0xC, 0xC);	break;
		}
	}

	return(ret);
}



//*****************************************************************************
static int _ain_range_sel(dev_data_t* dev, void* arg)
{
	static const s32	options[]	=
	{
		DSI_AIN_RANGE_SEL_DISABLE,
		DSI_AIN_RANGE_SEL_ENABLE,
		-1	// terminate list
	};

	int	ret;

	if (dev->cache.rfcr)
		ret	= _arg_s32_list_reg(dev, arg, options, DSI_GSC_RFCR, 9, 9);
	else
		ret	= -EINVAL;

	return(ret);
}



//*****************************************************************************
static int _ain_range_00_03(dev_data_t* dev, void* arg)
{
	int	ret;

	if ((dev->cache.rfcr == 0) || (dev->cache.channel_qty <= 3))
		ret	= -EINVAL;
	else
		ret	= _arg_s32_list_reg(dev, arg, _range, DSI_GSC_RFCR, 17, 16);

	return(ret);
}



//*****************************************************************************
static int _ain_range_04_07(dev_data_t* dev, void* arg)
{
	int	ret;

	if ((dev->cache.rfcr == 0) || (dev->cache.channel_qty <= 7))
		ret	= -EINVAL;
	else
		ret	= _arg_s32_list_reg(dev, arg, _range, DSI_GSC_RFCR, 19, 18);

	return(ret);
}



//*****************************************************************************
static int _ain_range_08_11(dev_data_t* dev, void* arg)
{
	int	ret;

	if ((dev->cache.rfcr == 0) || (dev->cache.channel_qty <= 11))
		ret	= -EINVAL;
	else
		ret	= _arg_s32_list_reg(dev, arg, _range, DSI_GSC_RFCR, 21, 20);

	return(ret);
}



//*****************************************************************************
static int _ain_range_12_15(dev_data_t* dev, void* arg)
{
	int	ret;

	if ((dev->cache.rfcr == 0) || (dev->cache.channel_qty <= 15))
		ret	= -EINVAL;
	else
		ret	= _arg_s32_list_reg(dev, arg, _range, DSI_GSC_RFCR, 23, 22);

	return(ret);
}



//*****************************************************************************
static int _ain_range_16_19(dev_data_t* dev, void* arg)
{
	int	ret;

	if ((dev->cache.rfcr == 0) || (dev->cache.channel_qty <= 19))
		ret	= -EINVAL;
	else
		ret	= _arg_s32_list_reg(dev, arg, _range, DSI_GSC_RFCR, 25, 24);

	return(ret);
}



//*****************************************************************************
static int _ain_range_20_23(dev_data_t* dev, void* arg)
{
	int	ret;

	if ((dev->cache.rfcr == 0) || (dev->cache.channel_qty <= 23))
		ret	= -EINVAL;
	else
		ret	= _arg_s32_list_reg(dev, arg, _range, DSI_GSC_RFCR, 27, 26);

	return(ret);
}



//*****************************************************************************
static int _ain_range_24_27(dev_data_t* dev, void* arg)
{
	int	ret;

	if ((dev->cache.rfcr == 0) || (dev->cache.channel_qty <= 27))
		ret	= -EINVAL;
	else
		ret	= _arg_s32_list_reg(dev, arg, _range, DSI_GSC_RFCR, 29, 28);

	return(ret);
}



//*****************************************************************************
static int _ain_range_28_31(dev_data_t* dev, void* arg)
{
	int	ret;

	if ((dev->cache.rfcr == 0) || (dev->cache.channel_qty <= 31))
		ret	= -EINVAL;
	else
		ret	= _arg_s32_list_reg(dev, arg, _range, DSI_GSC_RFCR, 31, 30);

	return(ret);
}



//*****************************************************************************
static int _channel_order(dev_data_t* dev, s32* arg)
{
	#define	MASK 0x10000

	u32	reg;
	int	ret	= 0;

	switch (arg[0])
	{
		default:

			ret	= -EINVAL;
			break;

		case -1:

			reg	= readl(dev->vaddr.gsc_bctlr_32);

			if (dev->cache.bctlr_d16_sync_1)
				arg[0]	= (reg & MASK) ? DSI_CHANNEL_ORDER_SYNC : DSI_CHANNEL_ORDER_ASYNC;
			else
				arg[0]	= (reg & MASK) ? DSI_CHANNEL_ORDER_ASYNC : DSI_CHANNEL_ORDER_SYNC;

			break;

		case DSI_CHANNEL_ORDER_ASYNC:

			if (dev->cache.bctlr_d16_sync_1)
				_bctlr_mod(dev, 0, MASK);
			else
				_bctlr_mod(dev, MASK, MASK);

			break;

		case DSI_CHANNEL_ORDER_SYNC:

			if (dev->cache.bctlr_d16_sync_1)
			{
				_bctlr_mod(dev, MASK, MASK);
			}
			else
			{
				_bctlr_mod(dev, MASK, MASK);
				_bctlr_mod(dev, 0, MASK);
			}

			_wait_for_ready(dev, 1000);
			break;
	}

	return(ret);
}



//*****************************************************************************
static int _data_format(dev_data_t* dev, s32* arg)
{
	static const s32	options[]	=
	{
		DSI_DATA_FORMAT_2S_COMP,
		DSI_DATA_FORMAT_OFF_BIN,
		-1	// terminate list
	};

	int	ret	= 0;

	switch (arg[0])
	{
		case -1:

			ret	= _arg_s32_list_reg(dev, arg, options, DSI_GSC_BCTLR, 4, 4);
			break;

		default:						ret	= -EINVAL;					break;
		case DSI_DATA_FORMAT_2S_COMP:	_bctlr_mod(dev, 0x00, 0x10);	break;
		case DSI_DATA_FORMAT_OFF_BIN:	_bctlr_mod(dev, 0x10, 0x10);	break;
	}

	return(ret);
}



//*****************************************************************************
static int _data_width(dev_data_t* dev, void* arg)
{
	static const s32	options[]	=
	{
		DSI_DATA_WIDTH_16,
		DSI_DATA_WIDTH_18,
		DSI_DATA_WIDTH_20,
		DSI_DATA_WIDTH_24,
		-1	// terminate list
	};

	int	ret;

	ret	= _arg_s32_list_reg(dev, arg, options, DSI_GSC_IBCR, 21, 20);
	return(ret);
}



//*****************************************************************************
static int _init_mode(dev_data_t* dev, s32* arg)
{
	static const s32	options[]	=
	{
		DSI_INIT_MODE_TARGET,
		DSI_INIT_MODE_INITIATOR,
		-1	// terminate list
	};

	int	ret	= 0;

	switch (arg[0])
	{
		case -1:

			ret	= _arg_s32_list_reg(dev, arg, options, DSI_GSC_BCTLR, 5, 5);
			break;

		default:						ret	= -EINVAL;					break;
		case DSI_INIT_MODE_TARGET:		_bctlr_mod(dev, 0x00, 0x20);	break;
		case DSI_INIT_MODE_INITIATOR:	_bctlr_mod(dev, 0x20, 0x20);	break;
	}

	return(ret);
}



//*****************************************************************************
static int _sw_sync(dev_data_t* dev, void* arg)
{
	_bctlr_mod(dev, 0x40, 0x40);
	return(0);
}



//*****************************************************************************
static int _sw_sync_mode(dev_data_t* dev, s32* arg)
{
	static const s32	options[]	=
	{
		DSI_SW_SYNC_MODE_SW_SYNC,
		DSI_SW_SYNC_MODE_CLR_BUF,
		-1	// terminate list
	};

	int	ret	= 0;

	if (arg[0] == -1)
		ret	= _arg_s32_list_reg(dev, arg, options, DSI_GSC_BCTLR, 17, 17);
	else if (arg[0] == DSI_SW_SYNC_MODE_SW_SYNC)
		_bctlr_mod(dev, 0, D17);
	else if (arg[0] == DSI_SW_SYNC_MODE_CLR_BUF)
		_bctlr_mod(dev, D17, D17);
	else
		ret	= -EINVAL;

	return(ret);
}



//*****************************************************************************
static int _pll_ref_freq_hz(dev_data_t* dev, s32* arg)
{
	arg[0]	= readl(dev->vaddr.gsc_prfr_32);
	return(0);
}



//*****************************************************************************
static int _rate_gen_a_nvco(dev_data_t* dev, s32* arg)
{
	int	ret;

	if (dev->cache.pll_present == 0)
		ret	= -EINVAL;
	else if (dev->cache.rcar_rcbr)
		ret	= _arg_s32_range_reg(dev, arg, 30, 1000, DSI_GSC_RCAR, 9, 0);
	else
		ret	= _arg_s32_range_reg(dev, arg, 30, 1000, DSI_GSC_NVCOCR, 9, 0);

	return(ret);
}



//*****************************************************************************
static int _rate_gen_b_nvco(dev_data_t* dev, void* arg)
{
	int	ret;

	if (dev->cache.pll_present == 0)
		ret	= -EINVAL;
	else if (dev->cache.rate_gen_qty < 2)
		ret	= -EINVAL;
	else if (dev->cache.rcar_rcbr)
		ret	= _arg_s32_range_reg(dev, arg, 30, 1000, DSI_GSC_RCBR, 9, 0);
	else
		ret	= -EINVAL;

	return(ret);
}



//*****************************************************************************
static int _rate_gen_a_nref(dev_data_t* dev, void* arg)
{
	int	ret;

	if (dev->cache.pll_present == 0)
		ret	= -EINVAL;
	else if (dev->cache.rcar_rcbr)
		ret	= _arg_s32_range_reg(dev, arg, 30, 1000, DSI_GSC_RCAR, 26, 16);
	else
		ret	= _arg_s32_range_reg(dev, arg, 30, 1000, DSI_GSC_NREFCR, 9, 0);

	return(ret);
}



//*****************************************************************************
static int _rate_gen_b_nref(dev_data_t* dev, void* arg)
{
	int	ret;

	if (dev->cache.pll_present == 0)
		ret	= -EINVAL;
	else if (dev->cache.rate_gen_qty < 2)
		ret	= -EINVAL;
	else if (dev->cache.rcar_rcbr)
		ret	= _arg_s32_range_reg(dev, arg, 30, 1000, DSI_GSC_RCBR, 26, 16);
	else
		ret	= -EINVAL;

	return(ret);
}



//*****************************************************************************
static int _rate_gen_a_nrate(dev_data_t* dev, void* arg)
{
	int	ret;

	if (dev->cache.pll_present)
	{
		ret	= -EINVAL;
	}
	else if (dev->cache.rcar_rcbr)
	{
		ret	= _arg_s32_range_reg(
				dev,
				arg,
				dev->cache.rate_gen_nrate_min,
				dev->cache.rate_gen_nrate_max,
				DSI_GSC_RCAR,
				16,
				0);
	}
	else
	{
		ret	= _arg_s32_range_reg(
				dev,
				arg,
				dev->cache.rate_gen_nrate_min,
				dev->cache.rate_gen_nrate_max,
				DSI_GSC_RCR,
				16,
				0);
	}

	return(ret);
}



//*****************************************************************************
static int _rate_gen_b_nrate(dev_data_t* dev, void* arg)
{
	int	ret;

	if (dev->cache.pll_present)
	{
		ret	= -EINVAL;
	}
	else if (dev->cache.rate_gen_qty < 2)
	{
		ret	= -EINVAL;
	}
	else if (dev->cache.rcar_rcbr)
	{
		ret	= _arg_s32_range_reg(
				dev,
				arg,
				dev->cache.rate_gen_nrate_min,
				dev->cache.rate_gen_nrate_max,
				DSI_GSC_RCBR,
				16,
				0);
	}
	else
	{
		ret	= -EINVAL;
	}

	return(ret);
}



//*****************************************************************************
static int _rate_div_0_ndiv(dev_data_t* dev, void* arg)
{
	int		ret;

	if (dev->cache.rate_div_qty < 1)
	{
		ret	= -EINVAL;
	}
	else
	{
		ret	= _arg_s32_range_reg(
				dev,
				arg,
				dev->cache.rate_div_ndiv_min,
				dev->cache.rate_div_ndiv_max,
				DSI_GSC_RDR,
				7,
				0);
	}

	return(ret);
}



//*****************************************************************************
static int _rate_div_1_ndiv(dev_data_t* dev, void* arg)
{
	int		ret;

	if (dev->cache.rate_div_qty < 2)
	{
		ret	= -EINVAL;
	}
	else
	{
		ret	= _arg_s32_range_reg(
				dev,
				arg,
				dev->cache.rate_div_ndiv_min,
				dev->cache.rate_div_ndiv_max,
				DSI_GSC_RDR,
				15,
				8);
	}

	return(ret);
}



//*****************************************************************************
static int _ch_grp_0_src(dev_data_t* dev, void* arg)
{
	int	ret;

	if (dev->cache.channel_groups < 1)
		ret	= -EINVAL;
	else if (dev->cache.rate_gen_qty > 1)
		ret	= _arg_s32_list_reg(dev, arg, _grp_src_ab, DSI_GSC_RAR, 3, 0);
	else
		ret	= _arg_s32_list_reg(dev, arg, _grp_src_a, DSI_GSC_RAR, 3, 0);

	return(ret);
}



//*****************************************************************************
static int _ch_grp_1_src(dev_data_t* dev, s32* arg)
{
	int	ret;

	if (dev->cache.channel_groups < 2)
	{
		ret	= -EINVAL;
	}
	else if (dev->cache.ch_grp_0_rar_pri)
	{
		arg[0]	= (arg[0] == DSI_CH_GRP_SRC_ENABLE) ? 5 : arg[0];
		ret		= _arg_s32_list_reg(dev, arg, _grp_src_alt, DSI_GSC_RAR, 7, 4);
		arg[0]	= (arg[0] == 5) ? DSI_CH_GRP_SRC_ENABLE : arg[0];
	}
	else if (dev->cache.rate_gen_qty > 1)
	{
		ret	= _arg_s32_list_reg(dev, arg, _grp_src_ab, DSI_GSC_RAR, 7, 4);
	}
	else
	{
		ret	= _arg_s32_list_reg(dev, arg, _grp_src_a, DSI_GSC_RAR, 7, 4);
	}

	return(ret);
}



//*****************************************************************************
static int _ch_grp_2_src(dev_data_t* dev, s32* arg)
{
	int	ret;

	if (dev->cache.channel_groups < 3)
	{
		ret	= -EINVAL;
	}
	else if (dev->cache.ch_grp_0_rar_pri)
	{
		arg[0]	= (arg[0] == DSI_CH_GRP_SRC_ENABLE) ? 5 : arg[0];
		ret		= _arg_s32_list_reg(dev, arg, _grp_src_alt, DSI_GSC_RAR, 11, 8);
		arg[0]	= (arg[0] == 5) ? DSI_CH_GRP_SRC_ENABLE : arg[0];
	}
	else if (dev->cache.rate_gen_qty > 1)
	{
		ret	= _arg_s32_list_reg(dev, arg, _grp_src_ab, DSI_GSC_RAR, 11, 8);
	}
	else
	{
		ret	= _arg_s32_list_reg(dev, arg, _grp_src_a, DSI_GSC_RAR, 11, 8);
	}

	return(ret);
}



//*****************************************************************************
static int _ch_grp_3_src(dev_data_t* dev, s32* arg)
{
	int	ret;

	if (dev->cache.channel_groups < 4)
	{
		ret	= -EINVAL;
	}
	else if (dev->cache.ch_grp_0_rar_pri)
	{
		arg[0]	= (arg[0] == DSI_CH_GRP_SRC_ENABLE) ? 5 : arg[0];
		ret		= _arg_s32_list_reg(dev, arg, _grp_src_alt, DSI_GSC_RAR, 15, 12);
		arg[0]	= (arg[0] == 5) ? DSI_CH_GRP_SRC_ENABLE : arg[0];
	}
	else if (dev->cache.rate_gen_qty > 1)
	{
		ret	= _arg_s32_list_reg(dev, arg, _grp_src_ab, DSI_GSC_RAR, 15, 12);
	}
	else
	{
		ret	= _arg_s32_list_reg(dev, arg, _grp_src_a, DSI_GSC_RAR, 15, 12);
	}

	return(ret);
}



//*****************************************************************************
static int _ext_clk_src(dev_data_t* dev, s32* arg)
{
	static const s32	options[]	=
	{
		DSI_EXT_CLK_SRC_GRP_0,
		DSI_EXT_CLK_SRC_GEN_A,
		-1	// terminate list
	};

	int	ret	= 0;

	if (arg[0] == -1)
		ret	= _arg_s32_list_reg(dev, arg, options, DSI_GSC_BCTLR, 18, 18);
	else if (arg[0] == DSI_EXT_CLK_SRC_GRP_0)
		_bctlr_mod(dev, 0, D18);
	else if (arg[0] == DSI_EXT_CLK_SRC_GEN_A)
		_bctlr_mod(dev, D18, D18);
	else
		ret	= -EINVAL;

	return(ret);
}



//*****************************************************************************
static int _ext_trig(dev_data_t* dev, s32* arg)
{
	static const s32	options[]	=
	{
		DSI_EXT_TRIG_DISARM,
		DSI_EXT_TRIG_ARM,
		-1	// terminate list
	};

	int	ret	= 0;

	if (dev->cache.bctlr_d21_arm_et == 0)
		ret	= -EINVAL;
	else if (arg[0] == -1)
		ret	= _arg_s32_list_reg(dev, arg, options, DSI_GSC_BCTLR, 21, 21);
	else if (arg[0] == DSI_EXT_TRIG_DISARM)
		_bctlr_mod(dev, 0, D21);
	else if (arg[0] == DSI_EXT_TRIG_ARM)
		_bctlr_mod(dev, D21, D21);

	if (ret == 0)
		ret	= _wait_for_ready(dev, 1000);

	return(ret);
}



//*****************************************************************************
static int _thres_flag_cbl(dev_data_t* dev, void* arg)
{
	static const s32	options[]	=
	{
		DSI_THRES_FLAG_CBL_OFF,
		DSI_THRES_FLAG_CBL_OUT,
		-1	// terminate list
	};

	int	ret;

	if (dev->cache.bctlr_d22_thr_f_out)
		ret	= _arg_s32_list_reg(dev, arg, options, DSI_GSC_BCTLR, 22, 22);
	else
		ret	= -EINVAL;

	if (ret == 0)
		ret	= _wait_for_ready(dev, 1000);

	return(ret);
}



//*****************************************************************************
static int _xcvr_type(dev_data_t* dev, s32* arg)
{
	static const s32	options[]	=
	{
		DSI_XCVR_TYPE_LVDS,
		DSI_XCVR_TYPE_TTL,
		-1	// terminate list
	};

	int	ret	= 0;

	if (dev->cache.bctlr_d20_xcvr == 0)
		ret	= -EINVAL;
	else if (arg[0] == -1)
		ret	= _arg_s32_list_reg(dev, arg, options, DSI_GSC_BCTLR, 20, 20);
	else if (arg[0] == DSI_XCVR_TYPE_LVDS)
		_bctlr_mod(dev, 0, D20);
	else if (arg[0] == DSI_XCVR_TYPE_TTL)
		_bctlr_mod(dev, D20, D20);
	else
		ret	= -EINVAL;

	if (ret == 0)
		ret	= _wait_for_ready(dev, 1000);

	return(ret);
}



//*****************************************************************************
static int _gps_enable(dev_data_t* dev, void* arg)
{
	static const s32	options[]	=
	{
		DSI_GPS_ENABLE_NO,
		DSI_GPS_ENABLE_YES,
		-1	// terminate list
	};

	int	ret;

	if (dev->cache.gps_present)
		ret	= _arg_s32_list_reg(dev, arg, options, DSI_GSC_GSR, 20, 20);
	else
		ret	= -EINVAL;

	return(ret);
}



//*****************************************************************************
static int _gps_locked(dev_data_t* dev, s32* arg)
{
	static const s32	options[]	=
	{
		DSI_GPS_LOCKED_NO,
		DSI_GPS_LOCKED_YES,
		-1	// terminate list
	};

	int	ret;

	arg[0]	= -1;

	if (dev->cache.gps_present)
		ret	= _arg_s32_list_reg(dev, arg, options, DSI_GSC_GSR, 24, 24);
	else
		ret	= -EINVAL;

	return(ret);
}



//*****************************************************************************
static int _gps_polarity(dev_data_t* dev, void* arg)
{
	static const s32	options[]	=
	{
		DSI_GPS_POLARITY_POS,
		DSI_GPS_POLARITY_NEG,
		-1	// terminate list
	};

	int	ret;

	if (dev->cache.gps_present)
		ret	= _arg_s32_list_reg(dev, arg, options, DSI_GSC_GSR, 22, 22);
	else
		ret	= -EINVAL;

	return(ret);
}



//*****************************************************************************
static int _gps_rate_locked(dev_data_t* dev, s32* arg)
{
	static const s32	options[]	=
	{
		DSI_GPS_RATE_LOCKED_NO,
		DSI_GPS_RATE_LOCKED_YES,
		-1	// terminate list
	};

	int	ret;

	arg[0]	= -1;

	if (dev->cache.gps_present)
		ret	= _arg_s32_list_reg(dev, arg, options, DSI_GSC_GSR, 25, 25);
	else
		ret	= -EINVAL;

	return(ret);
}



//*****************************************************************************
static int _gps_target_rate(dev_data_t* dev, s32* arg)
{
	int	ret;

	if (dev->cache.gps_present)
		ret	= _arg_s32_range_reg(dev, arg, 0, 0xFFFFF, DSI_GSC_GSR, 19, 0);
	else
		ret	= -EINVAL;

	return(ret);
}



//*****************************************************************************
static int _gps_tolerance(dev_data_t* dev, s32* arg)
{
	static const s32	options[]	=
	{
		DSI_GPS_TOLERANCE_NARROW,
		DSI_GPS_TOLERANCE_WIDE,
		-1	// terminate list
	};

	int	ret;

	if (dev->cache.gps_present)
		ret	= _arg_s32_list_reg(dev, arg, options, DSI_GSC_GSR, 21, 21);
	else
		ret	= -EINVAL;

	return(ret);
}



//*****************************************************************************
static int _rx_io_abort(dev_data_t* dev, s32* arg)
{
	arg[0]	= gsc_read_abort_active_xfer(dev);
	return(0);
}



//*****************************************************************************
static int _irq_sel(dev_data_t* dev, s32* arg)
{
	static const s32	options[]	=
	{
		DSI_IRQ_INIT_DONE,
		DSI_IRQ_AUTO_CAL_DONE,
		DSI_IRQ_CHAN_READY,
		DSI_IRQ_AIN_BUF_THRESH_L2H,
		DSI_IRQ_AIN_BUF_THRESH_H2L,
		-1	// terminate list
	};

	int	ret	= 0;

	switch (arg[0])
	{
		case -1:

			ret	= _arg_s32_list_reg(dev, arg, options, DSI_GSC_BCTLR, 10, 8);
			break;

		default:							ret	= -EINVAL;					break;
		case DSI_IRQ_INIT_DONE:				_bctlr_mod(dev, 0x000, 0x700);	break;
		case DSI_IRQ_AUTO_CAL_DONE:			_bctlr_mod(dev, 0x100, 0x700);	break;
		case DSI_IRQ_CHAN_READY:			_bctlr_mod(dev, 0x200, 0x700);	break;
		case DSI_IRQ_AIN_BUF_THRESH_L2H:	_bctlr_mod(dev, 0x300, 0x700);	break;
		case DSI_IRQ_AIN_BUF_THRESH_H2L:	_bctlr_mod(dev, 0x400, 0x700);	break;
	}

	return(ret);
}



//*****************************************************************************
static int _ta_inv_ext_trigger(dev_data_t* dev, s32* arg)
{
	static const s32	options[]	=
	{
		DSI_TA_INV_EXT_TRIGGER_NO,
		DSI_TA_INV_EXT_TRIGGER_YES,
		-1	// terminate list
	};

	int	ret	= 0;

	if (dev->cache.bctlr_d23_inv_e_trg == 0)
		ret	= -EINVAL;
	else if (arg[0] == -1)
		ret	= _arg_s32_list_reg(dev, arg, options, DSI_GSC_BCTLR, 23, 23);
	else if (arg[0] == DSI_TA_INV_EXT_TRIGGER_NO)
		_bctlr_mod(dev, 0, D23);
	else if (arg[0] == DSI_TA_INV_EXT_TRIGGER_YES)
		_bctlr_mod(dev, D23, D23);
	else
		ret	= -EINVAL;

	return(ret);
}



//*****************************************************************************
static int _coupling_mode_ac(dev_data_t* dev, void* arg)
{
	int	ret;

	if (dev->cache.reg_icmr == 0)
		ret	= -EINVAL;
	else if (dev->cache.channel_qty == 12)
		ret	= _arg_s32_range_reg(dev, arg, 0, 0xFFF, DSI_GSC_ICMR, 11, 0);
	else if (dev->cache.channel_qty == 8)
		ret	= _arg_s32_range_reg(dev, arg, 0, 0xFF, DSI_GSC_ICMR, 7, 0);
	else
		ret	= _arg_s32_range_reg(dev, arg, 0, 0xF, DSI_GSC_ICMR, 3, 0);

	return(ret);
}



//*****************************************************************************
static int _coupling_mode_dc(dev_data_t* dev, s32* arg)
{
	int	ret;

	if (dev->cache.reg_icmr == 0)
	{
		ret	= -EINVAL;
	}
	else if (arg[0] == -1)
	{
		if (dev->cache.channel_qty == 12)
		{
			ret		= _arg_s32_range_reg(dev, arg, 0, 0xFFF, DSI_GSC_ICMR, 11, 0);
			arg[0]	= ~arg[0] & 0xFFF;
		}
		else if (dev->cache.channel_qty == 8)
		{
			ret		= _arg_s32_range_reg(dev, arg, 0, 0xFF, DSI_GSC_ICMR, 7, 0);
			arg[0]	= ~arg[0] & 0xFF;
		}
		else
		{
			ret		= _arg_s32_range_reg(dev, arg, 0, 0xF, DSI_GSC_ICMR, 3, 0);
			arg[0]	= ~arg[0] & 0xF;
		}
	}
	else
	{
		if ((dev->cache.channel_qty == 12) && ((arg[0] & 0xFFF) == arg[0]))
		{
			arg[0]	= ~arg[0] & 0xFFF;
			ret		= _arg_s32_range_reg(dev, arg, 0, 0xFFF, DSI_GSC_ICMR, 11, 0);
		}
		else if ((dev->cache.channel_qty == 8) && ((arg[0] & 0xFF) == arg[0]))
		{
			arg[0]	= ~arg[0] & 0xFF;
			ret		= _arg_s32_range_reg(dev, arg, 0, 0xFF, DSI_GSC_ICMR, 7, 0);
		}
		else if ((dev->cache.channel_qty == 4) && ((arg[0] & 0xF) == arg[0]))
		{
			arg[0]	= ~arg[0] & 0xF;
			ret		= _arg_s32_range_reg(dev, arg, 0, 0xF, DSI_GSC_ICMR, 3, 0);
		}
		else
		{
			ret	= -EINVAL;
		}
	}

	return(ret);
}



//*****************************************************************************
static int _channels_ready(dev_data_t* dev, void* arg)
{
	int	ret;

	ret	= _wait_for_ready(dev, dev->rx.timeout_s * 1000);
	return(ret);
}



// variables	***************************************************************

const gsc_ioctl_t	dev_ioctl_list[]	=
{
	{ DSI_IOCTL_REG_READ,			(void*) gsc_reg_read_ioctl		},
	{ DSI_IOCTL_REG_WRITE,			(void*) gsc_reg_write_ioctl		},
	{ DSI_IOCTL_REG_MOD,			(void*) gsc_reg_mod_ioctl		},
	{ DSI_IOCTL_QUERY,				(void*) _query					},
	{ DSI_IOCTL_INITIALIZE,			(void*) initialize_ioctl		},
	{ DSI_IOCTL_AUTO_CALIBRATE,		(void*) _auto_calibrate			},
	{ DSI_IOCTL_RX_IO_MODE,			(void*) _rx_io_mode				},
	{ DSI_IOCTL_RX_IO_OVERFLOW,		(void*) _rx_io_overflow			},
	{ DSI_IOCTL_RX_IO_TIMEOUT,		(void*) _rx_io_timeout			},
	{ DSI_IOCTL_RX_IO_UNDERFLOW,	(void*) _rx_io_underflow		},
	{ DSI_IOCTL_AIN_BUF_CLEAR,		(void*) _ain_buf_clear			},
	{ DSI_IOCTL_AIN_BUF_INPUT,		(void*) _ain_buf_input			},
	{ DSI_IOCTL_AIN_BUF_OVERFLOW,	(void*) _ain_buf_overflow		},
	{ DSI_IOCTL_AIN_BUF_THRESH,		(void*) _ain_buf_thresh			},
	{ DSI_IOCTL_AIN_BUF_UNDERFLOW,	(void*) _ain_buf_underflow		},
	{ DSI_IOCTL_AIN_FILTER,			(void*) _ain_filter				},
	{ DSI_IOCTL_AIN_FILTER_SEL,		(void*) _ain_filter_sel			},
	{ DSI_IOCTL_AIN_FILTER_00_03,	(void*) _ain_filter_00_03		},
	{ DSI_IOCTL_AIN_FILTER_04_07,	(void*) _ain_filter_04_07		},
	{ DSI_IOCTL_AIN_FILTER_08_11,	(void*) _ain_filter_08_11		},
	{ DSI_IOCTL_AIN_FILTER_12_15,	(void*) _ain_filter_12_15		},
	{ DSI_IOCTL_AIN_FILTER_16_19,	(void*) _ain_filter_16_19		},
	{ DSI_IOCTL_AIN_FILTER_20_23,	(void*) _ain_filter_20_23		},
	{ DSI_IOCTL_AIN_FILTER_24_27,	(void*) _ain_filter_24_27		},
	{ DSI_IOCTL_AIN_FILTER_28_31,	(void*) _ain_filter_28_31		},
	{ DSI_IOCTL_AIN_MODE,			(void*) _ain_mode				},
	{ DSI_IOCTL_AIN_RANGE,			(void*) _ain_range				},
	{ DSI_IOCTL_AIN_RANGE_SEL,		(void*) _ain_range_sel			},
	{ DSI_IOCTL_AIN_RANGE_00_03,	(void*) _ain_range_00_03		},
	{ DSI_IOCTL_AIN_RANGE_04_07,	(void*) _ain_range_04_07		},
	{ DSI_IOCTL_AIN_RANGE_08_11,	(void*) _ain_range_08_11		},
	{ DSI_IOCTL_AIN_RANGE_12_15,	(void*) _ain_range_12_15		},
	{ DSI_IOCTL_AIN_RANGE_16_19,	(void*) _ain_range_16_19		},
	{ DSI_IOCTL_AIN_RANGE_20_23,	(void*) _ain_range_20_23		},
	{ DSI_IOCTL_AIN_RANGE_24_27,	(void*) _ain_range_24_27		},
	{ DSI_IOCTL_AIN_RANGE_28_31,	(void*) _ain_range_28_31		},
	{ DSI_IOCTL_CHANNEL_ORDER,		(void*) _channel_order			},
	{ DSI_IOCTL_DATA_FORMAT,		(void*) _data_format			},
	{ DSI_IOCTL_DATA_WIDTH,			(void*) _data_width				},
	{ DSI_IOCTL_INIT_MODE,			(void*) _init_mode				},
	{ DSI_IOCTL_SW_SYNC,			(void*) _sw_sync				},
	{ DSI_IOCTL_SW_SYNC_MODE,		(void*) _sw_sync_mode			},
	{ DSI_IOCTL_PLL_REF_FREQ_HZ,	(void*) _pll_ref_freq_hz		},
	{ DSI_IOCTL_RATE_GEN_A_NVCO,	(void*) _rate_gen_a_nvco		},
	{ DSI_IOCTL_RATE_GEN_B_NVCO,	(void*) _rate_gen_b_nvco		},
	{ DSI_IOCTL_RATE_GEN_A_NREF,	(void*) _rate_gen_a_nref		},
	{ DSI_IOCTL_RATE_GEN_B_NREF,	(void*) _rate_gen_b_nref		},
	{ DSI_IOCTL_RATE_GEN_A_NRATE,	(void*) _rate_gen_a_nrate		},
	{ DSI_IOCTL_RATE_GEN_B_NRATE,	(void*) _rate_gen_b_nrate		},
	{ DSI_IOCTL_RATE_DIV_0_NDIV,	(void*) _rate_div_0_ndiv		},
	{ DSI_IOCTL_RATE_DIV_1_NDIV,	(void*) _rate_div_1_ndiv		},
	{ DSI_IOCTL_CH_GRP_0_SRC,		(void*) _ch_grp_0_src			},
	{ DSI_IOCTL_CH_GRP_1_SRC,		(void*) _ch_grp_1_src			},
	{ DSI_IOCTL_CH_GRP_2_SRC,		(void*) _ch_grp_2_src			},
	{ DSI_IOCTL_CH_GRP_3_SRC,		(void*) _ch_grp_3_src			},
	{ DSI_IOCTL_EXT_CLK_SRC,		(void*) _ext_clk_src			},
	{ DSI_IOCTL_EXT_TRIG,			(void*) _ext_trig				},
	{ DSI_IOCTL_THRES_FLAG_CBL,		(void*) _thres_flag_cbl			},
	{ DSI_IOCTL_XCVR_TYPE,			(void*) _xcvr_type				},
	{ DSI_IOCTL_GPS_ENABLE,			(void*) _gps_enable				},
	{ DSI_IOCTL_GPS_LOCKED,			(void*) _gps_locked				},
	{ DSI_IOCTL_GPS_POLARITY,		(void*) _gps_polarity			},
	{ DSI_IOCTL_GPS_RATE_LOCKED,	(void*) _gps_rate_locked		},
	{ DSI_IOCTL_GPS_TARGET_RATE,	(void*) _gps_target_rate		},
	{ DSI_IOCTL_GPS_TOLERANCE,		(void*) _gps_tolerance			},
	{ DSI_IOCTL_AIN_COUPLING,		(void*) _ain_coupling			},
	{ DSI_IOCTL_ADC_RATE_OUT,		(void*) _adc_rate_out			},
	{ DSI_IOCTL_RX_IO_ABORT,		(void*) _rx_io_abort			},
	{ DSI_IOCTL_IRQ_SEL,			(void*) _irq_sel				},
	{ DSI_IOCTL_WAIT_EVENT,			(void*) gsc_wait_event_ioctl	},
	{ DSI_IOCTL_WAIT_CANCEL,		(void*) gsc_wait_cancel_ioctl	},
	{ DSI_IOCTL_WAIT_STATUS,		(void*) gsc_wait_status_ioctl	},
	{ DSI_IOCTL_TA_INV_EXT_TRIGGER,	(void*) _ta_inv_ext_trigger		},
	{ DSI_IOCTL_COUPLING_MODE_AC,	(void*) _coupling_mode_ac		},
	{ DSI_IOCTL_COUPLING_MODE_DC,	(void*) _coupling_mode_dc		},
	{ DSI_IOCTL_CHANNELS_READY,		(void*) _channels_ready			},
	{ -1, NULL }
};


