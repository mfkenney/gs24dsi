// This software is covered by the GNU GENERAL PUBLIC LICENSE (GPL).
// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.0.1.0_GSC_DN/driver/ioctl.c $
// $Rev: 3177 $
// $Date: 2009-08-31 11:08:14 -0500 (Mon, 31 Aug 2009) $

#include "main.h"



//*****************************************************************************
static int _wait_for_local_interrupt(dev_data_t* dev, long ms_limit)
{
	u32		intcsr;
	ssize_t	limit;
	int		ret;

	// The ISR will disable the local interrupt when it occurs.
	// This is our indication that the desired interrupt has triggered.
	limit	= jiffies + MS_TO_JIFFIES(ms_limit);

	for (;;)
	{
		// Read the register so we can check again.
		intcsr	= readl(dev->vaddr.plx_intcsr_32);

		if ((intcsr & GSC_INTCSR_LOCAL_INT_ENABLE) == 0)
		{
			ret	= 0;
			break;
		}

		if (jiffies >= limit)
		{
			// We've waited long enough.
			ret	= -EIO;
			break;
		}

		// Wait one timer tick before checking again.
		SET_CURRENT_STATE(TASK_INTERRUPTIBLE);
		schedule_timeout(1);
		SET_CURRENT_STATE(TASK_RUNNING);
	}

	return(ret);
}



//*****************************************************************************
static int _wait_for_ready(dev_data_t* dev, long ms_limit)
{
	#define	CHAN_READY	(1L << 13)

	u32		bcr;
	ssize_t	limit;
	int		ret;

	limit	= jiffies + MS_TO_JIFFIES(ms_limit);

	for (;;)
	{
		// Read the register so we can check.
		bcr	= readl(dev->vaddr.gsc_bctlr_32);

		if (bcr & CHAN_READY)
		{
			ret	= 0;
			break;
		}

		if (jiffies >= limit)
		{
			// We've waited long enough.
			ret	= -EIO;
			break;
		}

		// Wait one timer tick before checking again.
		SET_CURRENT_STATE(TASK_INTERRUPTIBLE);
		schedule_timeout(1);
		SET_CURRENT_STATE(TASK_RUNNING);
	}

	return(ret);
}



//*****************************************************************************
static int _query(dev_data_t* dev, s32* arg)
{
	switch (arg[0])
	{
		default:						arg[0]	= DSI_IOCTL_QUERY_ERROR;			break;
		case DSI_QUERY_AUTO_CAL_MS:		arg[0]	= dev->cache.auto_cal_ms;			break;
		case DSI_QUERY_CH_GRP_0_RAR:	arg[0]	= dev->cache.ch_grp_0_rar_pri;		break;
		case DSI_QUERY_CHANNEL_GPS:		arg[0]	= dev->cache.channel_groups;		break;
		case DSI_QUERY_CHANNEL_MAX:		arg[0]	= dev->cache.channels_max;			break;
		case DSI_QUERY_CHANNEL_QTY:		arg[0]	= dev->cache.channel_qty;			break;
		case DSI_QUERY_COUNT:			arg[0]	= DSI_IOCTL_QUERY_LAST + 1;			break;
		case DSI_QUERY_D0_1_IN_MODE:	arg[0]	= dev->cache.bctlr_d0_1_in_mode;	break;
		case DSI_QUERY_D2_3_RANGE:		arg[0]	= dev->cache.bctlr_d2_3_range;		break;
		case DSI_QUERY_D15_PLL:			arg[0]	= dev->cache.bcfgr_d15_pll;			break;
		case DSI_QUERY_D16_CHANNELS:	arg[0]	= dev->cache.bcfgr_d16_chans;		break;
		case DSI_QUERY_D16_CHANNELS_0:	arg[0]	= dev->cache.bcfgr_d16_chans_0;		break;
		case DSI_QUERY_D16_SYNC_1:		arg[0]	= dev->cache.bctlr_d16_sync_1;		break;
		case DSI_QUERY_D17_CHANNELS:	arg[0]	= dev->cache.bcfgr_d17_chans;		break;
		case DSI_QUERY_D17_CHANNELS_0:	arg[0]	= dev->cache.bcfgr_d17_chans_0;		break;
		case DSI_QUERY_D17_18_RANGE:	arg[0]	= dev->cache.bctlr_d17_18_range;	break;
		case DSI_QUERY_D18_CF_FILT:		arg[0]	= dev->cache.bcfgr_d18_cf_filt;		break;
		case DSI_QUERY_D18_RIO:			arg[0]	= dev->cache.bcfgr_d18_rear_io;		break;
		case DSI_QUERY_D19_EXT_TEMP:	arg[0]	= dev->cache.bcfgr_d19_ext_temp;	break;
		case DSI_QUERY_D19_20_FILT:		arg[0]	= dev->cache.bctlr_d19_20_filt;		break;
		case DSI_QUERY_D20_LOW_POWER:	arg[0]	= dev->cache.bcfgr_d20_low_pow;		break;
		case DSI_QUERY_D20_XCVR:		arg[0]	= dev->cache.bctlr_d20_xcvr;		break;
		case DSI_QUERY_D21_EXT_TEMP:	arg[0]	= dev->cache.bcfgr_d21_ext_temp;	break;
		case DSI_QUERY_D21_EXT_TRIG:	arg[0]	= dev->cache.bctlr_d21_arm_et;		break;
		case DSI_QUERY_D22_24DSI6LN:	arg[0]	= dev->cache.bcfgr_d22_24dsi6ln;	break;
		case DSI_QUERY_D22_THR_FLAG:	arg[0]	= dev->cache.bctlr_d22_thr_f_out;	break;
		case DSI_QUERY_D19_FREQ_FILT:	arg[0]	= dev->cache.bctlr_d19_freq_filt;	break;
		case DSI_QUERY_D22_COUPLING:	arg[0]	= dev->cache.bctlr_d22_coupling;	break;
		case DSI_QUERY_D23_RATE_OUT:	arg[0]	= dev->cache.bctlr_d23_rate_out;	break;
		case DSI_QUERY_DEVICE_TYPE:		arg[0]	= dev->board_type;					break;
		case DSI_QUERY_FGEN_MAX:		arg[0]	= dev->cache.rate_gen_fgen_max;		break;
		case DSI_QUERY_FGEN_MIN:		arg[0]	= dev->cache.rate_gen_fgen_min;		break;
		case DSI_QUERY_FIFO_SIZE:		arg[0]	= dev->cache.fifo_size;				break;
		case DSI_QUERY_FREF_DEFAULT:	arg[0]	= dev->cache.fref_default;			break;	// PLL
		case DSI_QUERY_FSAMP_DEFAULT:	arg[0]	= dev->cache.fsamp_default;			break;
		case DSI_QUERY_FSAMP_MAX:		arg[0]	= dev->cache.fsamp_max;				break;
		case DSI_QUERY_FSAMP_MIN:		arg[0]	= dev->cache.fsamp_min;				break;
		case DSI_QUERY_GPS_PRESENT:		arg[0]	= dev->cache.gps_present;			break;	// GPS
		case DSI_QUERY_INIT_MS:			arg[0]	= dev->cache.initialize_ms;			break;
		case DSI_QUERY_LOW_NOISE:		arg[0]	= dev->cache.low_noise;				break;
		case DSI_QUERY_LOW_POWER:		arg[0]	= dev->cache.low_power;				break;
		case DSI_QUERY_NDIV_MASK:		arg[0]	= dev->cache.rate_div_ndiv_mask;	break;
		case DSI_QUERY_NDIV_MAX:		arg[0]	= dev->cache.rate_div_ndiv_max;		break;
		case DSI_QUERY_NDIV_MIN:		arg[0]	= dev->cache.rate_div_ndiv_min;		break;
		case DSI_QUERY_NRATE_MASK:		arg[0]	= dev->cache.rate_gen_nrate_mask;	break;	// LEGACY
		case DSI_QUERY_NRATE_MAX:		arg[0]	= dev->cache.rate_gen_nrate_max;	break;	// LEGACY
		case DSI_QUERY_NRATE_MIN:		arg[0]	= dev->cache.rate_gen_nrate_min;	break;	// LEGACY
		case DSI_QUERY_NREF_MASK:		arg[0]	= dev->cache.rate_gen_nref_mask;	break;	// PLL
		case DSI_QUERY_NREF_MAX:		arg[0]	= dev->cache.rate_gen_nref_max;		break;	// PLL
		case DSI_QUERY_NREF_MIN:		arg[0]	= dev->cache.rate_gen_nref_min;		break;	// PLL
		case DSI_QUERY_NVCO_MASK:		arg[0]	= dev->cache.rate_gen_nvco_mask;	break;	// PLL
		case DSI_QUERY_NVCO_MAX:		arg[0]	= dev->cache.rate_gen_nvco_max;		break;	// PLL
		case DSI_QUERY_NVCO_MIN:		arg[0]	= dev->cache.rate_gen_nvco_min;		break;	// PLL
		case DSI_QUERY_PLL_PRESENT:		arg[0]	= dev->cache.pll_present;			break;
		case DSI_QUERY_RATE_DIV_QTY:	arg[0]	= dev->cache.rate_div_qty;			break;
		case DSI_QUERY_RATE_GEN_QTY:	arg[0]	= dev->cache.rate_gen_qty;			break;
		case DSI_QUERY_RCAR_RCBR:		arg[0]	= dev->cache.rcar_rcbr;				break;
		case DSI_QUERY_RFCR:			arg[0]	= dev->cache.rfcr;					break;
	}

	return(0);
}



//*****************************************************************************
static int _initialize(dev_data_t* dev, void* arg)
{
	u32		bcr;
	int		i;
	long	ms;
	int		ret;

	// Enable the local interrupt.
	gsc_irq_intcsr_mod(dev, GSC_INTCSR_LOCAL_INT_ENABLE, GSC_INTCSR_LOCAL_INT_ENABLE);

	// Setup the firmware interrupt option.
	bcr	= readl(dev->vaddr.gsc_bctlr_32);
	bcr &= ~BCTLR_IRQ_MASK;		// Clear the interrupt option.
	bcr	|= BCTLR_IRQ_INIT_DONE;	// Interrupt upon initialization done.
	bcr	&= ~BCTLR_IRQ_ACTIVE;	// Clear any existing interrupt.
	writel(bcr, dev->vaddr.gsc_bctlr_32);

	bcr	|= BCTLR_INIT_BIT;		// Initiate initialization
	writel(bcr, dev->vaddr.gsc_bctlr_32);

	// Wait for the local interrupt. At times this has taken a long time.
	ms	= dev->cache.initialize_ms + 25000;
	ret	= _wait_for_local_interrupt(dev, ms);

	// Clear the firmware interrupt.
	bcr	= readl(dev->vaddr.gsc_bctlr_32);
	bcr	&= ~BCTLR_IRQ_ACTIVE;	// Clear any existing interrupt.
	writel(bcr, dev->vaddr.gsc_bctlr_32);

	// Disable the local interrupt, just in case.
	gsc_irq_intcsr_mod(dev, 0, GSC_INTCSR_LOCAL_INT_ENABLE);

	// Now wait for the Initialize bit to clear.

	for (i = 0; i < HZ; i++)
	{
		bcr	= readl(dev->vaddr.gsc_bctlr_32);

		if ((bcr & BCTLR_INIT_BIT) == 0)
			break;

		// Wait one timer tick before checking again.
		SET_CURRENT_STATE(TASK_INTERRUPTIBLE);
		schedule_timeout(1);
		SET_CURRENT_STATE(TASK_RUNNING);
	}

	if (bcr & BCTLR_INIT_BIT)
	{
		ret	= ret ? ret : -EIO;
		printk(	"%s: Initialization did not complete (%ld ms).\n",
				dev->model,
				(long) ms);
	}

	return(ret);
}


//*****************************************************************************
static int _auto_calibrate(dev_data_t* dev, void* arg)
{
	u32		bcr;
	long	ms;
	int		ret;

	if (dev->cache.auto_cal_ms)
	{
		// Enable the local interrupt.
		gsc_irq_intcsr_mod(dev, GSC_INTCSR_LOCAL_INT_ENABLE, GSC_INTCSR_LOCAL_INT_ENABLE);

		// Setup the firmware interrupt option.
		bcr	= readl(dev->vaddr.gsc_bctlr_32);
		bcr &= ~BCTLR_IRQ_MASK;		// Clear the interrupt option.
		bcr	|= BCTLR_IRQ_AUTO_CAL;	// Interrupt upon calibration done.
		bcr	&= ~BCTLR_IRQ_ACTIVE;	// Clear any existing interrupt.
		writel(bcr, dev->vaddr.gsc_bctlr_32);

		bcr	|= BCTLR_AUTO_CAL_START;	// Initiate initialization
		writel(bcr, dev->vaddr.gsc_bctlr_32);

		// Wait for the local interrupt. At times this has taken a long time.
		ms	= dev->cache.auto_cal_ms + 25000;
		ret	= _wait_for_local_interrupt(dev, ms);

		// Clear the firmware interrupt.
		bcr	= readl(dev->vaddr.gsc_bctlr_32);
		bcr	&= ~BCTLR_IRQ_ACTIVE;	// Clear any existing interrupt.
		writel(bcr, dev->vaddr.gsc_bctlr_32);

		// Now check the status.
		bcr	= readl(dev->vaddr.gsc_bctlr_32);

		if (bcr & BCTLR_AUTO_CAL_START)
		{
			ret	= ret ? ret : -EIO;		// Auto-cal did not complete
			printk(	"%s: Auto-calibration did not complete (%ld ms).\n",
					dev->model,
					(long) ms);
		}
		else if ((bcr & BCTLR_AUTO_CAL_PASS) == 0)
		{
			ret	= ret ? ret : -EIO;		// Auto-cal failed.
			printk(	"%s: Auto-calibration failed.\n", dev->model);
		}

		// Disable the local interrupt, just in case.
		gsc_irq_intcsr_mod(dev, 0, GSC_INTCSR_LOCAL_INT_ENABLE);
	}
	else
	{
		// Auto-calibration is not supported on this board.
		ret	= -EIO;
	}

	return(ret);
}



//*****************************************************************************
static int _rx_io_mode(dev_data_t* dev, void* arg)
{
	s32*	parm	= (s32*) arg;
	int		ret;

	switch (parm[0])
	{
		default:

			ret	= -EINVAL;
			break;

		case -1:

			ret		= 0;
			parm[0]	= dev->rx.io_mode;
			break;

		case GSC_IO_MODE_PIO:
		case GSC_IO_MODE_DMA:
		case GSC_IO_MODE_DMDMA:

			ret				= 0;
			dev->rx.io_mode	= parm[0];
			break;
	}

	return(ret);
}



//*****************************************************************************
static int _rx_io_overflow(dev_data_t* dev, void* arg)
{
	s32*	parm	= (s32*) arg;
	int		ret;

	switch (parm[0])
	{
		default:

			ret	= -EINVAL;
			break;

		case -1:

			ret		= 0;
			parm[0]	= dev->rx.overflow_check;
			break;

		case DSI_IO_OVERFLOW_IGNORE:
		case DSI_IO_OVERFLOW_CHECK:

			ret	= 0;
			dev->rx.overflow_check	= parm[0];
			break;
	}

	return(ret);
}



//*****************************************************************************
static int _rx_io_timeout(dev_data_t* dev, void* arg)
{
	s32*	parm	= (s32*) arg;
	int		ret;

	if (parm[0] == -1)
	{
		ret		= 0;
		parm[0]	= dev->rx.timeout_s;
	}
	else if (	(parm[0] >= DSI_IO_TIMEOUT_MIN) &&
				(parm[0] <= DSI_IO_TIMEOUT_MAX))
	{
		ret					= 0;
		dev->rx.timeout_s	= parm[0];
	}
	else
	{
		ret	= -EINVAL;
	}

	return(ret);
}



//*****************************************************************************
static int _rx_io_underflow(dev_data_t* dev, void* arg)
{
	s32*	parm	= (s32*) arg;
	int		ret;

	switch (parm[0])
	{
		default:

			ret	= -EINVAL;
			break;

		case -1:

			ret		= 0;
			parm[0]	= dev->rx.underflow_check;
			break;

		case DSI_IO_UNDERFLOW_IGNORE:
		case DSI_IO_UNDERFLOW_CHECK:

			ret	= 0;
			dev->rx.underflow_check	= parm[0];
			break;
	}

	return(ret);
}



//*****************************************************************************
static int _adc_rate_out(dev_data_t* dev, void* arg)
{
	#undef	MASK
	#undef	SHIFT
	#define	MASK	(0x1L << SHIFT)
	#define	SHIFT	23

	s32*	parm	= (s32*) arg;
	int		ret;
	u32		v;

	if (dev->cache.bctlr_d23_rate_out)
	{
		switch (parm[0])
		{
			default:

				ret	= -EINVAL;
				break;

			case -1:

				ret		= 0;
				parm[0]	= readl(dev->vaddr.gsc_bctlr_32);
				parm[0]	&= MASK;
				parm[0]	= parm[0] ? 1 : 0;
				break;

			case DSI_ADC_RATE_OUT_NO:
			case DSI_ADC_RATE_OUT_YES:

				v	= parm[0] ? MASK : 0;
				reg_mem32_mod(dev->vaddr.gsc_bctlr_32, v, MASK);
				ret	= _wait_for_ready(dev, 1000);
				break;
		}
	}
	else
	{
		// This feature isn't supported.
		ret	= -EIO;
	}

	return(ret);
}



//*****************************************************************************
static int _ain_buf_clear(dev_data_t* dev, void* arg)
{
	#undef	MASK
	#undef	SHIFT

	u32	mask;
	int	ret;

	mask	= 0x00080000;	// Clear the buffer.
	reg_mem32_mod(dev->vaddr.gsc_ibcr_32, mask, mask);
	mask	|= 0x01000000;	// Clear any overflow.
	mask	|= 0x02000000;	// Clear any underflow.
	reg_mem32_mod(dev->vaddr.gsc_ibcr_32, 0, mask);
	ret		= _wait_for_ready(dev, 1000);

	return(ret);
}



//*****************************************************************************
static int _ain_buf_input(dev_data_t* dev, void* arg)
{
	#undef	MASK
	#undef	SHIFT
	#define	MASK	(0x1L << SHIFT)
	#define	SHIFT	18

	s32*	parm	= (s32*) arg;
	int		ret;
	u32		v;

	switch (parm[0])
	{
		default:

			ret	= -EINVAL;
			break;

		case -1:

			ret		= 0;
			parm[0]	= readl(dev->vaddr.gsc_ibcr_32);
			parm[0]	&= MASK;
			parm[0]	>>= SHIFT;
			parm[0]	= parm[0]
					? DSI_AIN_BUF_INPUT_DISABLE
					: DSI_AIN_BUF_INPUT_ENABLE;
			break;

		case DSI_AIN_BUF_INPUT_DISABLE:
		case DSI_AIN_BUF_INPUT_ENABLE:

			v	= (!parm[0]) << SHIFT;
			reg_mem32_mod(dev->vaddr.gsc_ibcr_32, v, MASK);
			ret	= _wait_for_ready(dev, 1000);
			break;
	}

	return(ret);
}



//*****************************************************************************
static int _ain_buf_overflow(dev_data_t* dev, void* arg)
{
	#undef	MASK
	#undef	SHIFT
	#define	MASK	(0x1L << SHIFT)
	#define	SHIFT	24

	s32*	parm	= (s32*) arg;
	int		ret;
	u32		v;

	switch (parm[0])
	{
		default:

			ret	= -EINVAL;
			break;

		case -1:

			ret		= 0;
			parm[0]	= readl(dev->vaddr.gsc_ibcr_32);
			parm[0]	&= MASK;
			parm[0]	= parm[0] ? 1 : 0;
			break;

		case DSI_AIN_BUF_OVERFLOW_CLEAR:

			v	= 0;
			reg_mem32_mod(dev->vaddr.gsc_ibcr_32, v, MASK);
			ret	= _wait_for_ready(dev, 1000);
			break;
	}

	return(ret);
}



//*****************************************************************************
static int _ain_buf_thresh(dev_data_t* dev, void* arg)
{
	#undef	MASK
	#undef	SHIFT
	#define	MASK	0x3FFFF
	#define	SHIFT	0

	s32*	parm	= (s32*) arg;
	int		ret;
	u32		v;

	if (parm[0] == -1)
	{
		ret		= 0;
		parm[0]	= readl(dev->vaddr.gsc_ibcr_32);
		parm[0]	&= MASK;
		parm[0]	>>= SHIFT;
	}
	else if ((parm[0] >= 0) && (parm[0] <= MASK))
	{
		v	= parm[0] << SHIFT;
		reg_mem32_mod(dev->vaddr.gsc_ibcr_32, v, MASK);
		ret	= _wait_for_ready(dev, 1000);
	}
	else
	{
		ret	= -EINVAL;
	}

	return(ret);
}



//*****************************************************************************
static int _ain_buf_underflow(dev_data_t* dev, void* arg)
{
	#undef	MASK
	#undef	SHIFT
	#define	MASK	(0x1L << SHIFT)
	#define	SHIFT	25

	s32*	parm	= (s32*) arg;
	int		ret;
	u32		v;

	switch (parm[0])
	{
		default:

			ret	= -EINVAL;
			break;

		case -1:

			ret		= 0;
			parm[0]	= readl(dev->vaddr.gsc_ibcr_32);
			parm[0]	&= MASK;
			parm[0]	= parm[0] ? 1 : 0;
			break;

		case DSI_AIN_BUF_UNDERFLOW_CLEAR:

			v	= 0;
			reg_mem32_mod(dev->vaddr.gsc_ibcr_32, v, MASK);
			ret	= _wait_for_ready(dev, 1000);
			break;
	}

	return(ret);
}



//*****************************************************************************
static int _ain_coupling(dev_data_t* dev, void* arg)
{
	#undef	MASK
	#undef	SHIFT
	#define	MASK	(0x1L << SHIFT)
	#define	SHIFT	22

	s32*	parm	= (s32*) arg;
	int		ret;
	u32		v;

	if (dev->cache.bctlr_d22_coupling)
	{
		switch (parm[0])
		{
			default:

				ret	= -EINVAL;
				break;

			case -1:

				ret		= 0;
				parm[0]	= readl(dev->vaddr.gsc_bctlr_32);
				parm[0]	&= MASK;
				parm[0]	= parm[0] ? 1 : 0;
				break;

			case DSI_AIN_COUPLING_AC:
			case DSI_AIN_COUPLING_DC:

				v	= parm[0] ? MASK : 0;
				reg_mem32_mod(dev->vaddr.gsc_bctlr_32, v, MASK);
				ret	= _wait_for_ready(dev, 1000);
				break;
		}
	}
	else
	{
		// This feature isn't supported.
		ret	= -EIO;
	}

	return(ret);
}



//*****************************************************************************
static int _ain_filter(dev_data_t* dev, void* arg)
{
	#undef	MASK
	#undef	SHIFT
	#define	MASK	(0x1L << SHIFT)
	#define	SHIFT	19

	s32*	parm	= (s32*) arg;
	int		ret;
	u32		v;

	switch (parm[0])
	{
		default:

			ret	= -EINVAL;
			break;

		case -1:

			ret		= 0;
			parm[0]	= readl(dev->vaddr.gsc_bctlr_32);
			parm[0]	&= MASK;
			parm[0]	>>= SHIFT;
			break;

		case DSI_AIN_FILTER_HI:
		case DSI_AIN_FILTER_LOW:

			v	= parm[0] << SHIFT;
			reg_mem32_mod(dev->vaddr.gsc_bctlr_32, v, MASK);
			ret	= _wait_for_ready(dev, 1000);
			break;
	}

	return(ret);
}



//*****************************************************************************
static int _ain_filter_sel(dev_data_t* dev, void* arg)
{
	#undef	MASK
	#undef	SHIFT
	#define	MASK	(0x1L << SHIFT)
	#define	SHIFT	8

	s32*	parm	= (s32*) arg;
	int		ret;
	u32		v;

	switch (parm[0])
	{
		default:

			ret	= -EINVAL;
			break;

		case -1:

			if (dev->cache.rfcr)
			{
				ret		= 0;
				parm[0]	= readl(dev->vaddr.gsc_rfcr_32);
				parm[0]	&= MASK;
				parm[0]	>>= SHIFT;
			}
			else
			{
				ret	= -EINVAL;
			}

			break;

		case DSI_AIN_FILTER_SEL_DISABLE:
		case DSI_AIN_FILTER_SEL_ENABLE:

			if (dev->cache.rfcr)
			{
				v	= parm[0] << SHIFT;
				reg_mem32_mod(dev->vaddr.gsc_rfcr_32, v, MASK);
				ret	= _wait_for_ready(dev, 1000);
			}
			else
			{
				ret	= -EINVAL;
			}

			break;
	}

	return(ret);
}



//*****************************************************************************
static int _ain_filter_xx_xx(dev_data_t* dev, void* arg, int chan)
{
	#undef	MASK
	#undef	SHIFT

	u32		mask;
	s32*	parm	= (s32*) arg;
	int		ret;
	int		shift;
	u32		v;

	shift	= chan / 4;
	mask	= 0x1 << shift;

	switch (parm[0])
	{
		default:

			ret	= -EINVAL;
			break;

		case -1:

			if ((dev->cache.rfcr == 0) || (chan >= dev->cache.channel_qty))
			{
				ret	= -EINVAL;
			}
			else
			{
				ret		= 0;
				parm[0]	= readl(dev->vaddr.gsc_rfcr_32);
				parm[0]	&= mask;
				parm[0]	>>= shift;
			}

			break;

		case DSI_AIN_FILTER_HI:
		case DSI_AIN_FILTER_LOW:

			if ((dev->cache.rfcr == 0) || (chan >= dev->cache.channel_qty))
			{
				ret	= -EINVAL;
			}
			else
			{
				v	= parm[0] << shift;
				reg_mem32_mod(dev->vaddr.gsc_rfcr_32, v, mask);
				ret	= _wait_for_ready(dev, 1000);
			}

			break;
	}

	return(ret);
}



//*****************************************************************************
static int _ain_filter_00_03(dev_data_t* dev, void* arg)
{
	int	ret;

	ret	= _ain_filter_xx_xx(dev, arg, 0);
	return(ret);
}



//*****************************************************************************
static int _ain_filter_04_07(dev_data_t* dev, void* arg)
{
	int	ret;

	ret	= _ain_filter_xx_xx(dev, arg, 4);
	return(ret);
}



//*****************************************************************************
static int _ain_filter_08_11(dev_data_t* dev, void* arg)
{
	int	ret;

	ret	= _ain_filter_xx_xx(dev, arg, 8);
	return(ret);
}



//*****************************************************************************
static int _ain_filter_12_15(dev_data_t* dev, void* arg)
{
	int	ret;

	ret	= _ain_filter_xx_xx(dev, arg, 12);
	return(ret);
}



//*****************************************************************************
static int _ain_filter_16_19(dev_data_t* dev, void* arg)
{
	int	ret;

	ret	= _ain_filter_xx_xx(dev, arg, 16);
	return(ret);
}



//*****************************************************************************
static int _ain_filter_20_23(dev_data_t* dev, void* arg)
{
	int	ret;

	ret	= _ain_filter_xx_xx(dev, arg, 20);
	return(ret);
}



//*****************************************************************************
static int _ain_filter_24_27(dev_data_t* dev, void* arg)
{
	int	ret;

	ret	= _ain_filter_xx_xx(dev, arg, 24);
	return(ret);
}



//*****************************************************************************
static int _ain_filter_28_31(dev_data_t* dev, void* arg)
{
	int	ret;

	ret	= _ain_filter_xx_xx(dev, arg, 28);
	return(ret);
}



//*****************************************************************************
static int _ain_mode(dev_data_t* dev, void* arg)
{
	#undef	MASK
	#undef	SHIFT
	#define	MASK	(0x3L << SHIFT)
	#define	SHIFT	0

	s32*	parm	= (s32*) arg;
	int		ret;
	u32		v;

	switch (parm[0])
	{
		default:

			ret	= -EINVAL;
			break;

		case -1:

			if (dev->cache.bctlr_d2_3_range)
			{
				// Read the setting from the Board Control Register.
				parm[0]	= readl(dev->vaddr.gsc_bctlr_32);
				parm[0]	&= MASK;
				parm[0]	>>= SHIFT;
			}
			else
			{
				// The option is fixed.
				parm[0]	= DSI_AIN_MODE_DIFF;
			}

			ret		= 0;
			break;

		case DSI_AIN_MODE_DIFF:
		case DSI_AIN_MODE_ZERO:
		case DSI_AIN_MODE_VREF:

			if (dev->cache.bctlr_d0_1_in_mode)
			{
				// The input mode IS software configurable.
				v	= parm[0] << SHIFT;
				reg_mem32_mod(dev->vaddr.gsc_bctlr_32, v, MASK);
				ret	= _wait_for_ready(dev, 1000);
			}
			else
			{
				// The input mode is NOT software configurable.
				ret	= -EIO;
			}


			break;
	}

	return(ret);
}



//*****************************************************************************
static int _ain_range(dev_data_t* dev, void* arg)
{
	#undef	MASK
	#undef	SHIFT
	#define	MASK	(0x3L << SHIFT)
	#define	SHIFT	2

	s32*	parm	= (s32*) arg;
	int		ret;
	u32		v;

	switch (parm[0])
	{
		default:

			ret	= -EINVAL;
			break;

		case -1:

			if (dev->cache.bctlr_d2_3_range)
			{
				// Read the setting from the Board Control Register.
				parm[0]	= readl(dev->vaddr.gsc_bctlr_32);
				parm[0]	&= MASK;
				parm[0]	>>= SHIFT;
				parm[0]	= parm[0] ? parm[0] : 1;
			}
			else
			{
				// Read the setting from the Board Configuration Register.

				switch ((dev->cache.gsc_bcfgr_32 >> 17) & 0x3)
				{
					default:	ret	= -EIO;									break;
					case 0:		ret	= 0;	parm[0]	= DSI_AIN_RANGE_10V;	break;
					case 1:		ret	= 0;	parm[0]	= DSI_AIN_RANGE_5V;		break;
					case 2:		ret	= 0;	parm[0]	= DSI_AIN_RANGE_2_5V;	break;
				}
			}

			ret		= 0;
			break;

		case DSI_AIN_RANGE_2_5V:
		case DSI_AIN_RANGE_5V:
		case DSI_AIN_RANGE_10V:

			if (dev->cache.bctlr_d2_3_range)
			{
				// The voltage range IS software configurable.
				v	= parm[0] << SHIFT;
				reg_mem32_mod(dev->vaddr.gsc_bctlr_32, v, MASK);
				ret	= _wait_for_ready(dev, 1000);
			}
			else
			{
				// The voltage range is NOT software configurable.
				ret	= -EIO;
			}

			break;
	}

	return(ret);
}



//*****************************************************************************
static int _ain_range_sel(dev_data_t* dev, void* arg)
{
	#undef	MASK
	#undef	SHIFT
	#define	MASK	(0x1L << SHIFT)
	#define	SHIFT	9

	s32*	parm	= (s32*) arg;
	int		ret;
	u32		v;

	switch (parm[0])
	{
		default:

			ret	= -EINVAL;
			break;

		case -1:

			if (dev->cache.rfcr)
			{
				ret		= 0;
				parm[0]	= readl(dev->vaddr.gsc_rfcr_32);
				parm[0]	&= MASK;
				parm[0]	>>= SHIFT;
			}
			else
			{
				ret	= -EINVAL;
			}

			break;

		case DSI_AIN_RANGE_SEL_DISABLE:
		case DSI_AIN_RANGE_SEL_ENABLE:

			if (dev->cache.rfcr)
			{
				v	= parm[0] << SHIFT;
				reg_mem32_mod(dev->vaddr.gsc_rfcr_32, v, MASK);
				ret	= _wait_for_ready(dev, 1000);
			}
			else
			{
				ret	= -EINVAL;
			}

			break;
	}

	return(ret);
}



//*****************************************************************************
static int _ain_range_xx_xx(dev_data_t* dev, void* arg, int chan)
{
	#undef	MASK
	#undef	SHIFT

	u32		mask;
	s32*	parm	= (s32*) arg;
	int		ret;
	int		shift;
	u32		v;

	shift	= 17 + ((chan / 4) * 2);
	mask	= 0x3 << shift;

	switch (parm[0])
	{
		default:

			ret	= -EINVAL;
			break;

		case -1:

			if ((dev->cache.rfcr == 0) || (chan >= dev->cache.channel_qty))
			{
				ret	= -EINVAL;
			}
			else
			{
				ret		= 0;
				parm[0]	= readl(dev->vaddr.gsc_rfcr_32);
				parm[0]	&= mask;
				parm[0]	>>= shift;
				parm[0] = parm[0] ? parm[0] : DSI_AIN_RANGE_2_5V;
			}

			break;

		case DSI_AIN_RANGE_2_5V:
		case DSI_AIN_RANGE_5V:
		case DSI_AIN_RANGE_10V:

			if ((dev->cache.rfcr == 0) || (chan >= dev->cache.channel_qty))
			{
				ret	= -EINVAL;
			}
			else
			{
				v	= parm[0] << shift;
				reg_mem32_mod(dev->vaddr.gsc_rfcr_32, v, mask);
				ret	= _wait_for_ready(dev, 1000);
			}

			break;
	}

	return(ret);
}



//*****************************************************************************
static int _ain_range_00_03(dev_data_t* dev, void* arg)
{
	int	ret;

	ret	= _ain_range_xx_xx(dev, arg, 0);
	return(ret);
}



//*****************************************************************************
static int _ain_range_04_07(dev_data_t* dev, void* arg)
{
	int	ret;

	ret	= _ain_range_xx_xx(dev, arg, 4);
	return(ret);
}



//*****************************************************************************
static int _ain_range_08_11(dev_data_t* dev, void* arg)
{
	int	ret;

	ret	= _ain_range_xx_xx(dev, arg, 8);
	return(ret);
}



//*****************************************************************************
static int _ain_range_12_15(dev_data_t* dev, void* arg)
{
	int	ret;

	ret	= _ain_range_xx_xx(dev, arg, 12);
	return(ret);
}



//*****************************************************************************
static int _ain_range_16_19(dev_data_t* dev, void* arg)
{
	int	ret;

	ret	= _ain_range_xx_xx(dev, arg, 16);
	return(ret);
}



//*****************************************************************************
static int _ain_range_20_23(dev_data_t* dev, void* arg)
{
	int	ret;

	ret	= _ain_range_xx_xx(dev, arg, 20);
	return(ret);
}



//*****************************************************************************
static int _ain_range_24_27(dev_data_t* dev, void* arg)
{
	int	ret;

	ret	= _ain_range_xx_xx(dev, arg, 24);
	return(ret);
}



//*****************************************************************************
static int _ain_range_28_31(dev_data_t* dev, void* arg)
{
	int	ret;

	ret	= _ain_range_xx_xx(dev, arg, 28);
	return(ret);
}



//*****************************************************************************
static int _channel_order(dev_data_t* dev, void* arg)
{
	#undef	MASK
	#undef	SHIFT
	#define	MASK	(0x1L << SHIFT)
	#define	SHIFT	16

	s32*	parm	= (s32*) arg;
	int		ret;
	u32		v;

	switch (parm[0])
	{
		default:

			ret	= -EINVAL;
			break;

		case -1:

			ret		= 0;
			parm[0]	= readl(dev->vaddr.gsc_bctlr_32);
			parm[0]	&= MASK;
			parm[0]	>>= SHIFT;

			if (dev->cache.bctlr_d16_sync_1)
				parm[0]	= parm[0] ? DSI_CHANNEL_ORDER_SYNC : DSI_CHANNEL_ORDER_ASYNC;
			else
				parm[0]	= parm[0] ? DSI_CHANNEL_ORDER_ASYNC : DSI_CHANNEL_ORDER_SYNC;

			break;

		case DSI_CHANNEL_ORDER_ASYNC:

			if (dev->cache.bctlr_d16_sync_1)
				v	= 0;
			else
				v	= MASK;

			reg_mem32_mod(dev->vaddr.gsc_bctlr_32, v, MASK);
			ret	= _wait_for_ready(dev, 1000);
			break;

		case DSI_CHANNEL_ORDER_SYNC:

			if (dev->cache.bctlr_d16_sync_1)
				v	= MASK;
			else
				v	= 0;

			reg_mem32_mod(dev->vaddr.gsc_bctlr_32, v, MASK);
			ret	= _wait_for_ready(dev, 1000);
			break;
	}

	return(ret);
}



//*****************************************************************************
static int _data_format(dev_data_t* dev, void* arg)
{
	#undef	MASK
	#undef	SHIFT
	#define	MASK	(0x1L << SHIFT)
	#define	SHIFT	4

	s32*	parm	= (s32*) arg;
	int		ret;
	u32		v;

	switch (parm[0])
	{
		default:

			ret	= -EINVAL;
			break;

		case -1:

			ret		= 0;
			parm[0]	= readl(dev->vaddr.gsc_bctlr_32);
			parm[0]	&= MASK;
			parm[0]	>>= SHIFT;
			break;

		case DSI_DATA_FORMAT_2S_COMP:
		case DSI_DATA_FORMAT_OFF_BIN:

			v	= parm[0] << SHIFT;
			reg_mem32_mod(dev->vaddr.gsc_bctlr_32, v, MASK);
			ret	= _wait_for_ready(dev, 1000);
			break;
	}

	return(ret);
}



//*****************************************************************************
static int _data_width(dev_data_t* dev, void* arg)
{
	#undef	MASK
	#undef	SHIFT
	#define	MASK	(0x3L << SHIFT)
	#define	SHIFT	20

	s32*	parm	= (s32*) arg;
	int		ret;
	u32		v;

	switch (parm[0])
	{
		default:

			ret	= -EINVAL;
			break;

		case -1:

			ret		= 0;
			parm[0]	= readl(dev->vaddr.gsc_ibcr_32);
			parm[0]	&= MASK;
			parm[0]	>>= SHIFT;
			break;

		case DSI_DATA_WIDTH_16:
		case DSI_DATA_WIDTH_18:
		case DSI_DATA_WIDTH_20:
		case DSI_DATA_WIDTH_24:

			v	= parm[0] << SHIFT;
			reg_mem32_mod(dev->vaddr.gsc_ibcr_32, v, MASK);
			ret	= _wait_for_ready(dev, 1000);
			break;
	}

	return(ret);
}



//*****************************************************************************
static int _init_mode(dev_data_t* dev, void* arg)
{
	#undef	MASK
	#undef	SHIFT
	#define	MASK	(0x1L << SHIFT)
	#define	SHIFT	5

	s32*	parm	= (s32*) arg;
	int		ret;
	u32		v;

	switch (parm[0])
	{
		default:

			ret	= -EINVAL;
			break;

		case -1:

			ret		= 0;
			parm[0]	= readl(dev->vaddr.gsc_bctlr_32);
			parm[0]	&= MASK;
			parm[0]	>>= SHIFT;
			break;

		case DSI_INIT_MODE_TARGET:
		case DSI_INIT_MODE_INITIATOR:

			v	= parm[0] << SHIFT;
			reg_mem32_mod(dev->vaddr.gsc_bctlr_32, v, MASK);
			ret	= _wait_for_ready(dev, 1000);
			break;
	}

	return(ret);
}



//*****************************************************************************
static int _sw_sync(dev_data_t* dev, void* arg)
{
	#undef	MASK
	#undef	SHIFT
	#define	MASK	(0x1L << SHIFT)
	#define	SHIFT	6

	int	ret;
	u32	v;

	v	= 0x1 << SHIFT;
	reg_mem32_mod(dev->vaddr.gsc_bctlr_32, v, MASK);
	ret	= _wait_for_ready(dev, 1000);
	return(ret);
}



//*****************************************************************************
static int _sw_sync_mode(dev_data_t* dev, void* arg)
{
	#undef	MASK
	#undef	SHIFT
	#define	MASK	(0x1L << SHIFT)
	#define	SHIFT	17

	s32*	parm	= (s32*) arg;
	int		ret;
	u32		v;

	switch (parm[0])
	{
		default:

			ret	= -EINVAL;
			break;

		case -1:

			ret		= 0;
			parm[0]	= readl(dev->vaddr.gsc_bctlr_32);
			parm[0]	&= MASK;
			parm[0]	>>= SHIFT;
			break;

		case DSI_SW_SYNC_MODE_SW_SYNC:
		case DSI_SW_SYNC_MODE_CLR_BUF:

			v	= parm[0] << SHIFT;
			reg_mem32_mod(dev->vaddr.gsc_bctlr_32, v, MASK);
			ret	= _wait_for_ready(dev, 1000);
			break;
	}

	return(ret);
}



//*****************************************************************************
static int _pll_ref_freq_hz(dev_data_t* dev, void* arg)
{
	s32*	parm	= (s32*) arg;

	parm[0]	= readl(dev->vaddr.gsc_prfr_32);
	return(0);
}



//*****************************************************************************
static int _rate_gen_x_nvco(
	dev_data_t*	dev,
	void*		arg,
	int			rate_gen,
	VADDR_T		vaddr)
{
	#undef	MASK
	#define	MASK	0x3FF
	#undef	SHIFT

	s32*	parm	= (s32*) arg;
	int		ret;

	if (dev->cache.rcar_rcbr == 0)
		vaddr	= dev->vaddr.gsc_nvcocr_32;

	if (dev->cache.pll_present == 0)
	{
		ret	= -EINVAL;
	}
	else if (parm[0] == -1)
	{
		ret		= 0;
		parm[0]	= readl(vaddr);
		parm[0]	&= MASK;
	}
	else if (	(rate_gen < 0)								||
				(rate_gen >= dev->cache.rate_gen_qty)		||
				(parm[0] < dev->cache.rate_gen_nvco_min)	||
				(parm[0] > dev->cache.rate_gen_nvco_max))
	{
		ret	= -EINVAL;
	}
	else
	{
		reg_mem32_mod(vaddr, parm[0], MASK);
		ret	= _wait_for_ready(dev, 1000);
	}

	return(ret);
}



//*****************************************************************************
static int _rate_gen_a_nvco(dev_data_t* dev, void* arg)
{
	int		ret;

	ret	= _rate_gen_x_nvco(dev, arg, 0, dev->vaddr.gsc_rcar_32);
	return(ret);
}



//*****************************************************************************
static int _rate_gen_b_nvco(dev_data_t* dev, void* arg)
{
	int		ret;

	ret	= _rate_gen_x_nvco(dev, arg, 1, dev->vaddr.gsc_rcbr_32);
	return(ret);
}



//*****************************************************************************
static int _rate_gen_x_nref(
	dev_data_t*	dev,
	void*		arg,
	int			rate_gen,
	VADDR_T		vaddr)
{
	#undef	MASK
	#define	MASK	(0x3FF << SHIFT)

	u32		mask;
	s32*	parm	= (s32*) arg;
	int		ret;
	int		shift;
	__u32	v;

	if (dev->cache.rcar_rcbr)
	{
		shift	= 16;
		mask	= 0x3FF << shift;
	}
	else
	{
		shift	= 0;
		mask	= 0x3FF;
		vaddr	= dev->vaddr.gsc_nrefcr_32;
	}

	if (dev->cache.pll_present == 0)
	{
		ret	= -EINVAL;
	}
	else if (parm[0] == -1)
	{
		ret		= 0;
		parm[0]	= readl(vaddr);
		parm[0]	&= mask;
		parm[0]	>>= shift;
	}
	else if (	(rate_gen < 0)								||
				(rate_gen >= dev->cache.rate_gen_qty)		||
				(parm[0] < dev->cache.rate_gen_nref_min)	||
				(parm[0] > dev->cache.rate_gen_nref_max))
	{
		ret	= -EINVAL;
	}
	else
	{
		v	= parm[0];
		v	<<= shift;
		reg_mem32_mod(vaddr, v, mask);
		ret	= _wait_for_ready(dev, 1000);
	}

	return(ret);
}



//*****************************************************************************
static int _rate_gen_a_nref(dev_data_t* dev, void* arg)
{
	int		ret;

	ret	= _rate_gen_x_nref(dev, arg, 0, dev->vaddr.gsc_rcar_32);
	return(ret);
}



//*****************************************************************************
static int _rate_gen_b_nref(dev_data_t* dev, void* arg)
{
	int		ret;

	ret	= _rate_gen_x_nref(dev, arg, 1, dev->vaddr.gsc_rcbr_32);
	return(ret);
}



//*****************************************************************************
static int _rate_gen_x_nrate(
	dev_data_t*	dev,
	void*		arg,
	int			rate_gen,
	VADDR_T		vaddr)
{
	#undef	MASK
	#undef	SHIFT
	#define	MASK	0x1FF

	s32*	parm	= (s32*) arg;
	int		ret;
	u32		v;

	if (dev->cache.rcar_rcbr == 0)
		vaddr	= dev->vaddr.gsc_rcr_32;

	if (dev->cache.pll_present)
	{
		ret	= -EINVAL;
	}
	else if (parm[0] == -1)
	{
		ret		= 0;
		parm[0]	= readl(vaddr);
		parm[0]	&= MASK;
	}
	else if (	(rate_gen < 0)								||
				(rate_gen >= dev->cache.rate_gen_qty)		||
				(parm[0] < dev->cache.rate_gen_nrate_min)	||
				(parm[0] > dev->cache.rate_gen_nrate_max))
	{
		ret	= -EINVAL;
	}
	else
	{
		v	= parm[0];
		reg_mem32_mod(vaddr, v, MASK);
		ret	= _wait_for_ready(dev, 1000);
	}

	return(ret);

}



//*****************************************************************************
static int _rate_gen_a_nrate(dev_data_t* dev, void* arg)
{
	int		ret;

	ret	= _rate_gen_x_nrate(dev, arg, 0, dev->vaddr.gsc_rcar_32);
	return(ret);
}



//*****************************************************************************
static int _rate_gen_b_nrate(dev_data_t* dev, void* arg)
{
	int		ret;

	ret	= _rate_gen_x_nrate(dev, arg, 1, dev->vaddr.gsc_rcbr_32);
	return(ret);
}



//*****************************************************************************
static int _rate_div_x_ndiv(
	dev_data_t*	dev,
	void*		arg,
	int			rate_div,
	u32			shift)
{
	#undef	MASK
	#undef	SHIFT

	s32		mask	= dev->cache.rate_div_ndiv_mask << shift;
	s32*	parm	= (s32*) arg;
	int		ret;
	u32		v;

	if ((rate_div < 0) || (rate_div >= dev->cache.rate_div_qty))
	{
		ret	= -EINVAL;
	}
	else if (parm[0] == -1)
	{
		ret		= 0;
		parm[0]	= readl(dev->vaddr.gsc_rdr_32);
		parm[0]	&= mask;
		parm[0]	>>= shift;
	}
	else if (	(parm[0] < dev->cache.rate_div_ndiv_min)	||
				(parm[0] > dev->cache.rate_div_ndiv_max))
	{
		ret	= -EINVAL;
	}
	else
	{
		v	= parm[0] << shift;
		reg_mem32_mod(dev->vaddr.gsc_rdr_32, v, mask);
		ret	= _wait_for_ready(dev, 1000);
	}

	return(ret);
}



//*****************************************************************************
static int _rate_div_0_ndiv(dev_data_t* dev, void* arg)
{
	int		ret;

	ret	= _rate_div_x_ndiv(dev, arg, 0, 0);
	return(ret);
}



//*****************************************************************************
static int _rate_div_1_ndiv(dev_data_t* dev, void* arg)
{
	int		ret;

	ret	= _rate_div_x_ndiv(dev, arg, 1, 8);
	return(ret);
}



//*****************************************************************************
static int _ch_grp_x_src(
	dev_data_t*	dev,
	void*		arg,
	int			group,
	u32			mask,
	u32			shift)
{
	#undef	MASK
	#undef	SHIFT

	s32*	parm	= (s32*) arg;
	int		ret;
	u32		v;

	if (group >= dev->cache.channel_groups)
	{
		ret	= -EINVAL;
	}
	else if ((group) && (dev->cache.ch_grp_0_rar_pri))
	{
		// The rate group can only be enabled or disabled.

		switch (parm[0])
		{
			default:

				ret	= -EINVAL;
				break;

			case -1:

				ret		= 0;
				parm[0]	= readl(dev->vaddr.gsc_rar_32);
				parm[0]	&= mask;
				parm[0]	>>= shift;

				switch (parm[0])
				{
					default:

						parm[0]	= -1;
						break;

					case 0:
					case 4:
					case 5:

						parm[0]	= DSI_CH_GRP_SRC_ENABLE;
						break;

					case 6:
					case 7:

						parm[0]	= DSI_CH_GRP_SRC_DISABLE;
						break;
				}

				break;

			case DSI_CH_GRP_SRC_DISABLE:

				v	= 6 << shift;
				reg_mem32_mod(dev->vaddr.gsc_rar_32, v, mask);
				ret	= 0;	// Don't wait for ready.
				break;

			case DSI_CH_GRP_SRC_ENABLE:

				v	= 4 << shift;
				reg_mem32_mod(dev->vaddr.gsc_rar_32, v, mask);
				ret	= 0;	// Don't wait for ready.
				break;
		}
	}
	else
	{
		switch (parm[0])
		{
			default:

				ret	= -EINVAL;
				break;

			case -1:

				ret		= 0;
				parm[0]	= readl(dev->vaddr.gsc_rar_32);
				parm[0]	&= mask;
				parm[0]	>>= shift;

				if (parm[0] > DSI_CH_GRP_SRC_DISABLE)
					parm[0]	= DSI_CH_GRP_SRC_DISABLE;

				break;

			case DSI_CH_GRP_SRC_GEN_B:

				if (dev->cache.rate_gen_qty <= 1)
				{
					ret	= -EINVAL;
					break;
				}

			case DSI_CH_GRP_SRC_GEN_A:
			case DSI_CH_GRP_SRC_EXTERN:
			case DSI_CH_GRP_SRC_DIR_EXTERN:
			case DSI_CH_GRP_SRC_DISABLE:

				v	= parm[0] << shift;
				reg_mem32_mod(dev->vaddr.gsc_rar_32, v, mask);
				ret	= 0;	// Don't wait for ready.
				break;
		}
	}

	return(ret);
}



//*****************************************************************************
static int _ch_grp_0_src(dev_data_t* dev, void* arg)
{
	int		ret;

	ret	= _ch_grp_x_src(dev, arg, 0, 0xF, 0);
	return(ret);
}



//*****************************************************************************
static int _ch_grp_1_src(dev_data_t* dev, void* arg)
{
	int		ret;

	ret	= _ch_grp_x_src(dev, arg, 1, 0xF0, 4);
	return(ret);
}



//*****************************************************************************
static int _ch_grp_2_src(dev_data_t* dev, void* arg)
{
	int		ret;

	ret	= _ch_grp_x_src(dev, arg, 2, 0xF00, 8);
	return(ret);
}



//*****************************************************************************
static int _ch_grp_3_src(dev_data_t* dev, void* arg)
{
	int		ret;

	ret	= _ch_grp_x_src(dev, arg, 3, 0xF000, 12);
	return(ret);
}



//*****************************************************************************
static int _ext_clk_src(dev_data_t* dev, void* arg)
{
	#undef	MASK
	#undef	SHIFT
	#define	MASK	(0x1L << SHIFT)
	#define	SHIFT	18

	s32*	parm	= (s32*) arg;
	int		ret;
	u32		v;

	switch (parm[0])
	{
		default:

			ret	= -EINVAL;
			break;

		case -1:

			ret		= 0;
			parm[0]	= readl(dev->vaddr.gsc_bctlr_32);
			parm[0]	&= MASK;
			parm[0]	>>= SHIFT;
			break;

		case DSI_EXT_CLK_SRC_GRP_0:
		case DSI_EXT_CLK_SRC_GEN_A:

			v	= parm[0] << SHIFT;
			reg_mem32_mod(dev->vaddr.gsc_bctlr_32, v, MASK);
			ret	= _wait_for_ready(dev, 1000);
			break;
	}

	return(ret);
}



//*****************************************************************************
static int _ext_trg(dev_data_t* dev, void* arg)
{
	#undef	MASK
	#undef	SHIFT
	#define	MASK	(0x1L << SHIFT)
	#define	SHIFT	21

	s32*	parm	= (s32*) arg;
	int		ret;
	u32		v;

	switch (parm[0])
	{
		default:

			ret	= -EINVAL;
			break;

		case -1:

			if (dev->cache.bctlr_d21_arm_et)
			{
				ret		= 0;
				parm[0]	= readl(dev->vaddr.gsc_bctlr_32);
				parm[0]	&= MASK;
				parm[0]	>>= SHIFT;
			}
			else
			{
				ret	= -EINVAL;
			}

			break;

		case DSI_EXT_TRIG_DISARM:
		case DSI_EXT_TRIG_ARM:

			if (dev->cache.bctlr_d21_arm_et)
			{
				v	= parm[0] << SHIFT;
				reg_mem32_mod(dev->vaddr.gsc_bctlr_32, v, MASK);
				ret	= _wait_for_ready(dev, 1000);
			}
			else
			{
				ret	= -EINVAL;
			}

			break;
	}

	return(ret);
}



//*****************************************************************************
static int _thresh_flag_cbl(dev_data_t* dev, void* arg)
{
	#undef	MASK
	#undef	SHIFT
	#define	MASK	(0x1L << SHIFT)
	#define	SHIFT	22

	s32*	parm	= (s32*) arg;
	int		ret;
	u32		v;

	switch (parm[0])
	{
		default:

			ret	= -EINVAL;
			break;

		case -1:

			if (dev->cache.bctlr_d22_thr_f_out)
			{
				ret		= 0;
				parm[0]	= readl(dev->vaddr.gsc_bctlr_32);
				parm[0]	&= MASK;
				parm[0]	>>= SHIFT;
			}
			else
			{
				ret	= -EINVAL;
			}

			break;

		case DSI_THRES_FLAG_CBL_OFF:
		case DSI_THRES_FLAG_CBL_OUT:

			if (dev->cache.bctlr_d22_thr_f_out)
			{
				v	= parm[0] << SHIFT;
				reg_mem32_mod(dev->vaddr.gsc_bctlr_32, v, MASK);
				ret	= _wait_for_ready(dev, 1000);
			}
			else
			{
				ret	= -EINVAL;
			}

			break;
	}

	return(ret);
}



//*****************************************************************************
static int _xcvr_type(dev_data_t* dev, void* arg)
{
	#undef	MASK
	#undef	SHIFT
	#define	MASK	(0x1L << SHIFT)
	#define	SHIFT	20

	s32*	parm	= (s32*) arg;
	int		ret;
	u32		v;

	switch (parm[0])
	{
		default:

			ret	= -EINVAL;
			break;

		case -1:

			if (dev->cache.bctlr_d20_xcvr)
			{
				ret		= 0;
				parm[0]	= readl(dev->vaddr.gsc_bctlr_32);
				parm[0]	&= MASK;
				parm[0]	>>= SHIFT;
			}
			else
			{
				ret	= -EINVAL;
			}

			break;

		case DSI_XCVR_TYPE_LVDS:
		case DSI_XCVR_TYPE_TTL:

			if (dev->cache.bctlr_d20_xcvr)
			{
				v	= parm[0] << SHIFT;
				reg_mem32_mod(dev->vaddr.gsc_bctlr_32, v, MASK);
				ret	= _wait_for_ready(dev, 1000);
			}
			else
			{
				ret	= -EINVAL;
			}

			break;
	}

	return(ret);
}



//*****************************************************************************
static int _gps_enable(dev_data_t* dev, void* arg)
{
	#undef	MASK
	#undef	SHIFT
	#define	MASK	(0x1L << SHIFT)
	#define	SHIFT	20

	s32*	parm	= (s32*) arg;
	int		ret;
	u32		v;

	switch (parm[0])
	{
		default:

			ret	= -EINVAL;
			break;

		case -1:

			ret		= 0;
			parm[0]	= readl(dev->vaddr.gsc_gsr_32);
			parm[0]	&= MASK;
			parm[0]	>>= SHIFT;
			break;

		case DSI_GPS_ENABLE_NO:
		case DSI_GPS_ENABLE_YES:

			ret	= 0;
			v	= parm[0] << SHIFT;;
			reg_mem32_mod(dev->vaddr.gsc_gsr_32, v, MASK);
			break;
	}

	return(ret);
}



//*****************************************************************************
static int _gps_locked(dev_data_t* dev, void* arg)
{
	#undef	MASK
	#undef	SHIFT
	#define	MASK	(0x1L << SHIFT)
	#define	SHIFT	24

	s32*	parm	= (s32*) arg;

	parm[0]	= readl(dev->vaddr.gsc_gsr_32);
	parm[0]	&= MASK;
	parm[0]	>>= SHIFT;
	return(0);
}



//*****************************************************************************
static int _gps_polarity(dev_data_t* dev, void* arg)
{
	#undef	MASK
	#undef	SHIFT
	#define	MASK	(0x1L << SHIFT)
	#define	SHIFT	22

	s32*	parm	= (s32*) arg;
	int		ret;
	u32		v;

	switch (parm[0])
	{
		default:

			ret	= -EINVAL;
			break;

		case -1:

			ret		= 0;
			parm[0]	= readl(dev->vaddr.gsc_gsr_32);
			parm[0]	&= MASK;
			parm[0]	>>= SHIFT;
			break;

		case DSI_GPS_POLARITY_POS:
		case DSI_GPS_POLARITY_NEG:

			ret	= 0;
			v	= parm[0] << SHIFT;;
			reg_mem32_mod(dev->vaddr.gsc_gsr_32, v, MASK);
			break;
	}

	return(ret);
}



//*****************************************************************************
static int _gps_rate_locked(dev_data_t* dev, void* arg)
{
	#undef	MASK
	#undef	SHIFT
	#define	MASK	(0x1L << SHIFT)
	#define	SHIFT	25

	s32*	parm	= (s32*) arg;

	parm[0]	= readl(dev->vaddr.gsc_gsr_32);
	parm[0]	&= MASK;
	parm[0]	>>= SHIFT;
	return(0);
}



//*****************************************************************************
static int _gps_target_rate(dev_data_t* dev, void* arg)
{
	#undef	MASK
	#undef	SHIFT
	#define	MASK	(0xFFFFFL << SHIFT)
	#define	SHIFT	0

	s32*	parm	= (s32*) arg;
	int		ret;
	u32		v;

	if (parm[0] == -1)
	{
		ret		= 0;
		parm[0]	= readl(dev->vaddr.gsc_gsr_32);
		parm[0]	&= MASK;
		parm[0]	>>= SHIFT;
	}
	else if ((parm[0] < 0) || (parm[0] > 0xFFFFF))
	{
		ret	= -EINVAL;
	}
	else
	{
		ret	= 0;
		v	= parm[0] << SHIFT;
		reg_mem32_mod(dev->vaddr.gsc_gsr_32, v, MASK);
	}

	return(ret);
}



//*****************************************************************************
static int _gps_tolerance(dev_data_t* dev, void* arg)
{
	#undef	MASK
	#undef	SHIFT
	#define	MASK	(0x1L << SHIFT)
	#define	SHIFT	21

	s32*	parm	= (s32*) arg;
	int		ret;
	u32		v;

	switch (parm[0])
	{
		default:

			ret	= -EINVAL;
			break;

		case -1:

			ret		= 0;
			parm[0]	= readl(dev->vaddr.gsc_gsr_32);
			parm[0]	&= MASK;
			parm[0]	>>= SHIFT;
			break;

		case DSI_GPS_TOLERANCE_NARROW:
		case DSI_GPS_TOLERANCE_WIDE:

			ret	= 0;
			v	= parm[0] << SHIFT;;
			reg_mem32_mod(dev->vaddr.gsc_gsr_32, v, MASK);
			break;
	}

	return(ret);
}



// variables	***************************************************************

const gsc_ioctl_t	dev_ioctl_list[]	=
{
	{ DSI_IOCTL_REG_READ,			(void*) gsc_reg_read_ioctl		},
	{ DSI_IOCTL_REG_WRITE,			(void*) gsc_reg_write_ioctl		},
	{ DSI_IOCTL_REG_MOD,			(void*) gsc_reg_mod_ioctl		},
	{ DSI_IOCTL_QUERY,				(void*) _query					},
	{ DSI_IOCTL_INITIALIZE,			(void*) _initialize				},
	{ DSI_IOCTL_AUTO_CALIBRATE,		(void*) _auto_calibrate			},
	{ DSI_IOCTL_RX_IO_MODE,			(void*) _rx_io_mode				},
	{ DSI_IOCTL_RX_IO_OVERFLOW,		(void*) _rx_io_overflow			},
	{ DSI_IOCTL_RX_IO_TIMEOUT,		(void*) _rx_io_timeout			},
	{ DSI_IOCTL_RX_IO_UNDERFLOW,	(void*) _rx_io_underflow		},
	{ DSI_IOCTL_AIN_BUF_CLEAR,		(void*) _ain_buf_clear			},
	{ DSI_IOCTL_AIN_BUF_INPUT,		(void*) _ain_buf_input			},
	{ DSI_IOCTL_AIN_BUF_OVERFLOW,	(void*) _ain_buf_overflow		},
	{ DSI_IOCTL_AIN_BUF_THRESH,		(void*) _ain_buf_thresh			},
	{ DSI_IOCTL_AIN_BUF_UNDERFLOW,	(void*) _ain_buf_underflow		},
	{ DSI_IOCTL_AIN_FILTER,			(void*) _ain_filter				},
	{ DSI_IOCTL_AIN_FILTER_SEL,		(void*) _ain_filter_sel			},
	{ DSI_IOCTL_AIN_FILTER_00_03,	(void*) _ain_filter_00_03		},
	{ DSI_IOCTL_AIN_FILTER_04_07,	(void*) _ain_filter_04_07		},
	{ DSI_IOCTL_AIN_FILTER_08_11,	(void*) _ain_filter_08_11		},
	{ DSI_IOCTL_AIN_FILTER_12_15,	(void*) _ain_filter_12_15		},
	{ DSI_IOCTL_AIN_FILTER_16_19,	(void*) _ain_filter_16_19		},
	{ DSI_IOCTL_AIN_FILTER_20_23,	(void*) _ain_filter_20_23		},
	{ DSI_IOCTL_AIN_FILTER_24_27,	(void*) _ain_filter_24_27		},
	{ DSI_IOCTL_AIN_FILTER_28_31,	(void*) _ain_filter_28_31		},
	{ DSI_IOCTL_AIN_MODE,			(void*) _ain_mode				},
	{ DSI_IOCTL_AIN_RANGE,			(void*) _ain_range				},
	{ DSI_IOCTL_AIN_RANGE_SEL,		(void*) _ain_range_sel			},
	{ DSI_IOCTL_AIN_RANGE_00_03,	(void*) _ain_range_00_03		},
	{ DSI_IOCTL_AIN_RANGE_04_07,	(void*) _ain_range_04_07		},
	{ DSI_IOCTL_AIN_RANGE_08_11,	(void*) _ain_range_08_11		},
	{ DSI_IOCTL_AIN_RANGE_12_15,	(void*) _ain_range_12_15		},
	{ DSI_IOCTL_AIN_RANGE_16_19,	(void*) _ain_range_16_19		},
	{ DSI_IOCTL_AIN_RANGE_20_23,	(void*) _ain_range_20_23		},
	{ DSI_IOCTL_AIN_RANGE_24_27,	(void*) _ain_range_24_27		},
	{ DSI_IOCTL_AIN_RANGE_28_31,	(void*) _ain_range_28_31		},
	{ DSI_IOCTL_CHANNEL_ORDER,		(void*) _channel_order			},
	{ DSI_IOCTL_DATA_FORMAT,		(void*) _data_format			},
	{ DSI_IOCTL_DATA_WIDTH,			(void*) _data_width				},
	{ DSI_IOCTL_INIT_MODE,			(void*) _init_mode				},
	{ DSI_IOCTL_SW_SYNC,			(void*) _sw_sync				},
	{ DSI_IOCTL_SW_SYNC_MODE,		(void*) _sw_sync_mode			},
	{ DSI_IOCTL_PLL_REF_FREQ_HZ,	(void*) _pll_ref_freq_hz		},
	{ DSI_IOCTL_RATE_GEN_A_NVCO,	(void*) _rate_gen_a_nvco		},
	{ DSI_IOCTL_RATE_GEN_B_NVCO,	(void*) _rate_gen_b_nvco		},
	{ DSI_IOCTL_RATE_GEN_A_NREF,	(void*) _rate_gen_a_nref		},
	{ DSI_IOCTL_RATE_GEN_B_NREF,	(void*) _rate_gen_b_nref		},
	{ DSI_IOCTL_RATE_GEN_A_NRATE,	(void*) _rate_gen_a_nrate		},
	{ DSI_IOCTL_RATE_GEN_B_NRATE,	(void*) _rate_gen_b_nrate		},
	{ DSI_IOCTL_RATE_DIV_0_NDIV,	(void*) _rate_div_0_ndiv		},
	{ DSI_IOCTL_RATE_DIV_1_NDIV,	(void*) _rate_div_1_ndiv		},
	{ DSI_IOCTL_CH_GRP_0_SRC,		(void*) _ch_grp_0_src			},
	{ DSI_IOCTL_CH_GRP_1_SRC,		(void*) _ch_grp_1_src			},
	{ DSI_IOCTL_CH_GRP_2_SRC,		(void*) _ch_grp_2_src			},
	{ DSI_IOCTL_CH_GRP_3_SRC,		(void*) _ch_grp_3_src			},
	{ DSI_IOCTL_EXT_CLK_SRC,		(void*) _ext_clk_src			},
	{ DSI_IOCTL_EXT_TRIG,			(void*) _ext_trg				},
	{ DSI_IOCTL_THRES_FLAG_CBL,		(void*) _thresh_flag_cbl		},
	{ DSI_IOCTL_XCVR_TYPE,			(void*) _xcvr_type				},
	{ DSI_IOCTL_GPS_ENABLE,			(void*) _gps_enable				},
	{ DSI_IOCTL_GPS_LOCKED,			(void*) _gps_locked				},
	{ DSI_IOCTL_GPS_POLARITY,		(void*) _gps_polarity			},
	{ DSI_IOCTL_GPS_RATE_LOCKED,	(void*) _gps_rate_locked		},
	{ DSI_IOCTL_GPS_TARGET_RATE,	(void*) _gps_target_rate		},
	{ DSI_IOCTL_GPS_TOLERANCE,		(void*) _gps_tolerance			},
	{ DSI_IOCTL_AIN_COUPLING,		(void*) _ain_coupling			},
	{ DSI_IOCTL_ADC_RATE_OUT,		(void*) _adc_rate_out			},

	{ -1, NULL }
};


