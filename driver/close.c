// This software is covered by the GNU GENERAL PUBLIC LICENSE (GPL).
// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.0.1.0_GSC_DN/driver/close.c $
// $Rev: 5782 $
// $Date: 2010-06-02 16:43:28 -0500 (Wed, 02 Jun 2010) $

#include "main.h"



//*****************************************************************************
int dev_close(dev_data_t* dev)
{
	u32	reg;

	reg	= readl(dev->vaddr.gsc_bctlr_32);
	reg	&= 0xFFFFF0FF;	// Clear the status bit, init the IRQ selection.
	writel(reg, dev->vaddr.gsc_bctlr_32);

	gsc_dma_close(dev);
	io_close(dev);
	gsc_irq_close(dev);
	return(0);
}



