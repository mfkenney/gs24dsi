// This software is covered by the GNU GENERAL PUBLIC LICENSE (GPL).
// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.0.1.0_GSC_DN/driver/reg.c $
// $Rev: 3177 $
// $Date: 2009-08-31 11:08:14 -0500 (Mon, 31 Aug 2009) $

#include "main.h"



//*****************************************************************************
int dev_reg_mod_alt(dev_data_t* dev, gsc_reg_t* arg)
{
	// No alternate register encodings are defined for this driver.
	return(-EINVAL);
}



//*****************************************************************************
int dev_reg_read_alt(dev_data_t* dev, gsc_reg_t* arg)
{
	// No alternate register encodings are defined for this driver.
	return(-EINVAL);
}



//*****************************************************************************
int dev_reg_write_alt(dev_data_t* dev, gsc_reg_t* arg)
{
	// No alternate register encodings are defined for this driver.
	return(-EINVAL);
}



//*****************************************************************************
void reg_mem32_mod(VADDR_T vaddr, u32 value, u32 mask)
{
	u32	v;

	v	= readl(vaddr);
	v	&= ~mask;
	v	|= (value & mask);
	writel(v, vaddr);
}


