// This software is covered by the GNU GENERAL PUBLIC LICENSE (GPL).
// $URL: http://subversion:8080/svn/gsc/trunk/drivers/gsc_common/linux/driver/gsc_close.c $
// $Rev: 22531 $
// $Date: 2013-07-02 11:42:04 -0500 (Tue, 02 Jul 2013) $

#include "main.h"



//*****************************************************************************
int gsc_close(struct inode *inode, struct file *fp)
{
	GSC_ALT_STRUCT_T*	alt;
	int					ret;

	for (;;)	// A convenience loop.
	{
		alt	= (GSC_ALT_STRUCT_T*) fp->private_data;

		if (alt == NULL)
		{
			// The referenced device doesn't exist.
			ret	= -ENODEV;
			break;
		}

		ret	= gsc_sem_lock(&alt->sem);

		if (ret)
		{
			// We didn't get the semaphore.
			break;
		}

		// Perform device specific CLOSE actions.
		ret	= dev_close(alt);

		// Complete
		alt->in_use			= 0;
		fp->private_data	= NULL;
		gsc_module_count_dec();
		gsc_sem_unlock(&alt->sem);
		break;
	}

	return(ret);
}//lint !e715



