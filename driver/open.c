// This software is covered by the GNU GENERAL PUBLIC LICENSE (GPL).
// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.0.1.0_GSC_DN/driver/open.c $
// $Rev: 11338 $
// $Date: 2011-07-27 14:25:04 -0500 (Wed, 27 Jul 2011) $

#include "main.h"



//*****************************************************************************
int dev_open(dev_data_t* dev)
{
	int	ret;

	ret	= io_open(dev);

	if (ret == 0)
		ret	= gsc_dma_open(dev);

	if (ret == 0)
		ret	= gsc_irq_open(dev);

	if (ret == 0)
		ret	= initialize_ioctl(dev, NULL);

	if (ret)
		dev_close(dev);

	return(ret);
}


