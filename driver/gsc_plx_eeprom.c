// This software is covered by the GNU GENERAL PUBLIC LICENSE (GPL).
// $URL: http://subversion:8080/svn/gsc/trunk/drivers/gsc_common/linux/driver/gsc_plx_eeprom.c $
// $Rev: 21125 $
// $Date: 2013-04-19 13:01:39 -0500 (Fri, 19 Apr 2013) $

#include "main.h"
#include <linux/moduleparam.h>



#ifdef DEV_PLX_EEPROM_ACCESS

//*****************************************************************************

static	gsc_bar_t	_plx;	// BAR0

int					eeprom_read	= 0;
module_param(		eeprom_read, int, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP);
MODULE_PARM_DESC(	eeprom_read, "Read and display EEPROM? 0=No, !0=Yes (negates programming");




//*****************************************************************************
static int _plx_eeprom_start(struct pci_dev* pci)
{
	int			errs	= 0;

	errs	= gsc_bar_create(pci, 0, &_plx,  1, 0);	// memory
	return(errs);
}



//*****************************************************************************
static int _plx_eeprom_stop(void)
{
	int	errs	= 0;

	gsc_bar_destroy(&_plx);
	return(errs);
}



//*****************************************************************************
int gsc_plx_eeprom_access(struct pci_dev* pci)
{
	int	errs	= 0;

	printk("%s: EEPROM Access Services\n", DEV_NAME);
	errs	+= _plx_eeprom_start(pci);

	if ((errs == 0) && (eeprom_read))
	{
		printk("%s: EEPROM Access Services: Read\n", DEV_NAME);
		errs	= 1;	// status indicator
//		_plx_eeprom_read_all();
	}

	errs	+= _plx_eeprom_stop();

	return(errs);
}

#endif



#if 0
//*****************************************************************************
static void _plx_eeprom_read_all(void)
//	unsigned int*	read_config_array_ptr)
{
	int lc;
	unsigned char eeprom_control_byte;
	unsigned char * eeprom_control_byte_ptr = &eeprom_control_byte;

	unsigned long PLX_io_base_address;
	unsigned int eeprom_control_io_address = 2;
	read_a_pci_config_register(5,&PLX_io_base_address);

	 // This is so damn cute it ain't even Funny.
	 // Control_io_address = 2, assigned above in the declaration .
	 // io_base_address is in the format xxx1 , because it's an I/O address
	 // Therefore 0x06c + 2 + xxx1 = 0x06f
	 // which is what the hell it should be.
	 // DAMN.
  eeprom_control_io_address += ((unsigned int) PLX_io_base_address +  0x6C);

 outportb(eeprom_control_io_address,0x90);

 *eeprom_control_byte_ptr = inportb(eeprom_control_io_address);

 for(lc=0; lc < 34; lc++)
	 {
	 read_a_word_from_the_eeprom (eeprom_control_byte_ptr,
								 eeprom_control_io_address,
								 lc,
								 read_config_array_ptr );
	 read_config_array_ptr++ ; // update the pointer
	 }

 // I have no Idea why he's doing this????
 outportb(eeprom_control_io_address,0x90);

//  outportb(eeprom_control_io_address,0xB0); // Re-Load Configuration Registers
// I Don't think so.

} // end of procedure read_the_plx_config_eeprom



//#ifndef SERIAL_1KBIT_EEPROM
//#define SERIAL_1KBIT_EEPROM
//#include<dos.h>
//#include<stdio.h>


//void write_a_word_to_the_eeprom (
//				 unsigned char * eeprom_control_byte_ptr,
//				 unsigned int io_address,
//				 unsigned char eeprom_address,
//				 unsigned int * eeprom_register_data_ptr
//				 );

//void send_a_byte_of_data_to_the_eeprom
//				 (
//				 unsigned char * eeprom_control_byte_ptr,
//				 unsigned int io_address,
//				 unsigned char byte_to_write
//				 );

//void send_write_enable_cmmd_to_eeprom
//				 (
//				 unsigned char * eeprom_control_byte_ptr,
//				 unsigned int io_address
//				 );

//void send_write_disable_cmmd_to_eeprom
//				 (
//				 unsigned char * eeprom_control_byte_ptr,
//				 unsigned int io_address
//				 );

//void clk_a_bit_into_the_eeprom
//				 (
//				 unsigned char * eeprom_control_byte_ptr,
//				 unsigned int io_address
//				 );

//void set_eeprom_clk_bit_to_a_state
//				  ( unsigned char * eeprom_control_byte_ptr,
//				    unsigned char state_control_byte
//				  );

//void set_eeprom_cs_bit_to_a_state
//				  ( unsigned char * eeprom_control_byte_ptr,
//				    unsigned char state_control_byte
//				  );

//void set_eeprom_wr_bit_to_a_state
//				  ( unsigned char * eeprom_control_byte_ptr,
//				    unsigned char state_control_byte
//				  );

//void program_the_plx_config_eeprom(void);

//int verify_the_plx_config_eeprom(void) ;


// for verify to work, the data array must be a global, and
// permanent.
 unsigned int plx_config_data_array[34] = {   // !!!!!!! LOCAL !!!!! EEPROM OFFSET
	0x906E,	// 0x0 device ID
	0x10B5,	// 0x2 vendor ID
	0x0780,	// 0x8 class code register
	0x0002,	// 0xA revision level
	0x0000,	// 0x3c Latency
    0x0100,	// 0x3e Interrupt Info.
    0x0004,	// 0xc0 MSW Mailbox 0 EE prom revision
    0x0003,	// 0xc2 LSW Mailbox 0 PLD revision
    0x0004,	// 0xc4 MSW Mailbox 1 Board Assembly Level
    0x0000,	// 0xc6 LSW Mailbox 1
    0xFFFF,	// 0x80 MSW range PCI to Local
    0xFFC0,	// 0x82 LSW range PCI to Local
	0x0000,	// 0x84 MSW Local Base Address (remap)
	0x0001,	// 0x86 LSW Local Base Address (remap)
    0x0000,	// 0x88 MSW Local Arbitration register
    0x0000,	// 0x8a LSW Local Arbitration register
    0x0000,	// 0x8c MSW Big / little indian descriptor
    0x0000,	// 0x8e LSW Big / little indian descriptor
    0x0000,	// 0x90 MSW range for PCI to local expansion ROM
    0x0000,	// 0x92 LSW range for PCI to local expansion ROM
    0x0000,	// 0x94 MSW Local Base Address Remap PCI to Local Expansion ROM
    0x0000,	// 0x96 LSW Local Base Address Remap PCI to Local Expansion ROM
	0x4000,	// 0x98 MSW Bus region descriptors for PCI to Local
	0x0300,	// 0x9a LSW Bus region descriptors for PCI to Local
    0x0000,	// 0x9c MSW Range for direct Master to PCI
    0x0000,	// 0x9e LSW Range for direct Master to PCI
    0x0000,	// 0xa0 MSW Local base address direct Master to PCI
    0x0000,	// 0xa2 LSW Local base address direct Master to PCI
    0x0000,	// 0xa4 MSW Local base address direct Master to PCI IO/CFG
    0x0000,	// 0xa6 LSW Local base address direct Master to PCI IO/CFG
    0x0000,	// 0xa8 MSW PCI base address Remap for direct Master to PCI
	0x0000,	// 0xaa LSW PCI base address Remap for direct Master to PCI
    0x0000,	// 0xac MSW PCI Configuration Address register for direct Master to PCI IO/CFG
    0x0000,	// 0xae LSW PCI Configuration Address register for direct Master to PCI IO/CFG
};



//*****************************************************************************
void set_eeprom_clk_bit_to_a_state(
	unsigned char*	eeprom_control_byte_ptr,
	unsigned char	state_control_byte)
{
	switch(state_control_byte)
	{
		case 0x00:	*eeprom_control_byte_ptr &= 0xFE;	break;
		case 0x01:	*eeprom_control_byte_ptr |= 0x01;	break;
	}
}

//*****************************************************************************
void set_eeprom_cs_bit_to_a_state(
	unsigned char*	eeprom_control_byte_ptr,
	unsigned char	state_control_byte)
{
	switch(state_control_byte)
	{
		case 0x00:	*eeprom_control_byte_ptr &= 0xFD;	break;
		case 0x01:	*eeprom_control_byte_ptr |= 0x02;	break;
	}
}

//*****************************************************************************
void set_eeprom_wr_bit_to_a_state(
	unsigned char*	eeprom_control_byte_ptr,
	unsigned char	state_control_byte)
{
	switch(state_control_byte)
	{
		case 0x00:	*eeprom_control_byte_ptr &= 0xFB;	break;
		case 0x01:	*eeprom_control_byte_ptr |= 0x04;	break;
	}
}

//*****************************************************************************
void clk_a_bit_into_the_eeprom(
	unsigned char*	eeprom_control_byte_ptr,
	unsigned int	eeprom_control_io_address)
{
	int	delay_loop ;

	set_eeprom_clk_bit_to_a_state(eeprom_control_byte_ptr, 0x00);
	outportb(eeprom_control_io_address, *eeprom_control_byte_ptr);

	for( delay_loop = 0; delay_loop < 64 ; delay_loop++);

	set_eeprom_clk_bit_to_a_state(eeprom_control_byte_ptr, 0x01);
	outportb(eeprom_control_io_address, *eeprom_control_byte_ptr);

	for( delay_loop = 0; delay_loop < 64 ; delay_loop++);

	set_eeprom_clk_bit_to_a_state(eeprom_control_byte_ptr, 0x00);
	outportb(eeprom_control_io_address, *eeprom_control_byte_ptr);

	for( delay_loop = 0; delay_loop < 64 ; delay_loop++);
}

//*****************************************************************************
void send_a_byte_of_data_to_the_eeprom(
	unsigned char*	eeprom_control_byte_ptr,
	unsigned int	eeprom_control_io_address,
	unsigned char	byte_to_write)
{
	unsigned char	bit_mask;
	int				lc;

	for (lc = 0; lc < 8; lc++)
	{
		switch (lc)
		{
			case 0x00:	bit_mask = 0x80;	break;
			case 0x01:	bit_mask = 0x40;	break;
			case 0x02:	bit_mask = 0x20;	break;
			case 0x03:	bit_mask = 0x10;	break;
			case 0x04:	bit_mask = 0x08;	break;
			case 0x05:	bit_mask = 0x04;	break;
			case 0x06:	bit_mask = 0x02;	break;
			case 0x07:	bit_mask = 0x01;	break;
		}

		switch(byte_to_write & bit_mask)
		{
			default:

				set_eeprom_wr_bit_to_a_state(eeprom_control_byte_ptr, 0x01);
				break;

			case 0x00:

				set_eeprom_wr_bit_to_a_state(eeprom_control_byte_ptr, 0x00);
				break;
		}

		clk_a_bit_into_the_eeprom(eeprom_control_byte_ptr, eeprom_control_io_address);
	}
}

//*****************************************************************************
void send_write_enable_cmmd_to_eeprom(
	unsigned char*	eeprom_control_byte_ptr,
	unsigned int	eeprom_control_io_address)
{
	set_eeprom_clk_bit_to_a_state(eeprom_control_byte_ptr, 0x00);
	outportb(eeprom_control_io_address, *eeprom_control_byte_ptr);
	delay(1);

	set_eeprom_cs_bit_to_a_state(eeprom_control_byte_ptr, 0x01);
	set_eeprom_wr_bit_to_a_state(eeprom_control_byte_ptr, 0x00);
	outportb(eeprom_control_io_address, *eeprom_control_byte_ptr);
	delay(1);

	// send the start bit
	set_eeprom_wr_bit_to_a_state(eeprom_control_byte_ptr, 0x01);
	clk_a_bit_into_the_eeprom(eeprom_control_byte_ptr, eeprom_control_io_address);

	// send write enable command with the correct address
	send_a_byte_of_data_to_the_eeprom(	eeprom_control_byte_ptr,
										eeprom_control_io_address,
										0x3F);

	// set cs low after byte data is written.
	set_eeprom_clk_bit_to_a_state(eeprom_control_byte_ptr, 0x00);
	outportb(eeprom_control_io_address, *eeprom_control_byte_ptr);
	set_eeprom_cs_bit_to_a_state(eeprom_control_byte_ptr, 0x00);
	outportb(eeprom_control_io_address, *eeprom_control_byte_ptr);
	delay(1);
}

//*****************************************************************************
void send_write_disable_cmmd_to_eeprom(
	unsigned char*	eeprom_control_byte_ptr,
	unsigned int	eeprom_control_io_address)
{
	set_eeprom_clk_bit_to_a_state(eeprom_control_byte_ptr, 0x00);
	outportb(eeprom_control_io_address, *eeprom_control_byte_ptr);
	delay(1);

	set_eeprom_cs_bit_to_a_state(eeprom_control_byte_ptr, 0x01);
	set_eeprom_wr_bit_to_a_state(eeprom_control_byte_ptr, 0x00);
	outportb(eeprom_control_io_address, *eeprom_control_byte_ptr);
	delay(1);

	// send the start bit
	set_eeprom_wr_bit_to_a_state(eeprom_control_byte_ptr, 0x01);
	clk_a_bit_into_the_eeprom(eeprom_control_byte_ptr, eeprom_control_io_address);

	// send write enable command with the correct address
	send_a_byte_of_data_to_the_eeprom(
		eeprom_control_byte_ptr,
		eeprom_control_io_address,
		0x0F);

	// set cs low after byte data is written.
	set_eeprom_clk_bit_to_a_state(eeprom_control_byte_ptr, 0x00);
	outportb(eeprom_control_io_address, *eeprom_control_byte_ptr);
	set_eeprom_cs_bit_to_a_state(eeprom_control_byte_ptr, 0x00);
	outportb(eeprom_control_io_address, *eeprom_control_byte_ptr);
	delay(1);
}

//*****************************************************************************
void write_a_word_to_the_eeprom (
				 unsigned char * eeprom_control_byte_ptr,
				 unsigned int eeprom_control_io_address,
				 unsigned char eeprom_address,
				 unsigned int * eeprom_register_data_ptr
				 )
{
 unsigned char write_cmmd_and_addr;
 unsigned char * byte_wide_data_ptr;
 unsigned int   delay_counter;
 unsigned char test_results;

// setup eeprom input signal to a starting value:
// cs -> high  (chip select)
// di -> low   (data in eeprom)
// sk -> low   (clock signal)

  set_eeprom_clk_bit_to_a_state( eeprom_control_byte_ptr,0x00 );
  outportb(eeprom_control_io_address,*eeprom_control_byte_ptr);
  delay(1);

  set_eeprom_cs_bit_to_a_state ( eeprom_control_byte_ptr,0x01 );
  set_eeprom_wr_bit_to_a_state ( eeprom_control_byte_ptr,0x00 );
  outportb(eeprom_control_io_address,*eeprom_control_byte_ptr);
  delay(1);



// send the start bit
  set_eeprom_wr_bit_to_a_state ( eeprom_control_byte_ptr,0x01 );
  clk_a_bit_into_the_eeprom ( eeprom_control_byte_ptr,eeprom_control_io_address );

// build the write command code into the eeprom register address
   write_cmmd_and_addr =  eeprom_address & 0x3F;
   write_cmmd_and_addr |= 0x40;


// send the write command along with the address to the eeprom
  send_a_byte_of_data_to_the_eeprom
				 (
				 eeprom_control_byte_ptr,
				 eeprom_control_io_address,
				 write_cmmd_and_addr
				 );

 byte_wide_data_ptr = (unsigned char far *) eeprom_register_data_ptr;


   send_a_byte_of_data_to_the_eeprom
				 (
				 eeprom_control_byte_ptr,
				 eeprom_control_io_address,
				 *(byte_wide_data_ptr+1)
				 );


   send_a_byte_of_data_to_the_eeprom
				 (
				 eeprom_control_byte_ptr,
				 eeprom_control_io_address,
				 *(byte_wide_data_ptr)
				 );

// set cs low after byte data is written.

    set_eeprom_clk_bit_to_a_state( eeprom_control_byte_ptr,0x00 );
    outportb(eeprom_control_io_address,*eeprom_control_byte_ptr);


    set_eeprom_cs_bit_to_a_state ( eeprom_control_byte_ptr,0x00 );
// drive cs low and wait approx a micro second.
    outportb(eeprom_control_io_address,*eeprom_control_byte_ptr);
    outportb(eeprom_control_io_address,*eeprom_control_byte_ptr);
    outportb(eeprom_control_io_address,*eeprom_control_byte_ptr);

    for (delay_counter = 0; delay_counter <= 32766 ; delay_counter++ ) ;

	 set_eeprom_cs_bit_to_a_state ( eeprom_control_byte_ptr,0x01 );
    outportb(eeprom_control_io_address,*eeprom_control_byte_ptr);
    delay(1);

    delay_counter = 0;
	do{
		test_results =  inportb(eeprom_control_io_address) & 0x08;
		delay(1) ;
		delay_counter++;

// ???????   } while(test_results & (delay_counter < 32767) );
	} while( (test_results != 0)  && (delay_counter <= 32) );


	delay_counter = 0;
	do{
		test_results =  inportb(eeprom_control_io_address) & 0x08;
		delay(1) ;
		delay_counter++;

// ???????   } while(!test_results & (delay_counter <=32));
	} while( (test_results == 0)  && (delay_counter <= 32) );


  set_eeprom_cs_bit_to_a_state ( eeprom_control_byte_ptr,0x00 );
  outportb(eeprom_control_io_address,*eeprom_control_byte_ptr);
  delay(1);
}

//*****************************************************************************
void program_the_plx_config_eeprom(void)
{ int lc;
  unsigned char eeprom_control_byte;
  unsigned char * eeprom_control_byte_ptr = &eeprom_control_byte;

  unsigned long PLX_io_base_address;
  unsigned int eeprom_control_io_address = 2;
  read_a_pci_config_register(5,&PLX_io_base_address);

    // This is so damn cute it ain't even Funny.
    // control_io_address = 2, assigned above in the declaration .
    // io_base_address is in the format xxx1 , because it's an I/O address
    // Therefore 0x06c + 2 + xxx1 = 0x06f
    // which is what the hell it should be.
    // DAMN.
  eeprom_control_io_address += ((unsigned int) PLX_io_base_address +  0x6C);

 outportb(eeprom_control_io_address,0x90);

 *eeprom_control_byte_ptr = inportb(eeprom_control_io_address);
 delay(1);
 send_write_enable_cmmd_to_eeprom(eeprom_control_byte_ptr,
                                  eeprom_control_io_address);

 for(lc=0; lc < 34; lc++)
	 write_a_word_to_the_eeprom (eeprom_control_byte_ptr,
                 		        eeprom_control_io_address,
                  		        lc,
                  		        &plx_config_data_array[lc]);

 // I have no IDEA why he's doing this????
 outportb(eeprom_control_io_address,0x90);

 send_write_disable_cmmd_to_eeprom(eeprom_control_byte_ptr,
											  eeprom_control_io_address
                           		 );
 // Force him to re-Load from the EEPROM
  outportb(eeprom_control_io_address,0xB0);
 // Fine if it worked, Not fine if it didn't work??

}

// mark

//*****************************************************************************
void read_a_word_from_the_eeprom (
				 unsigned char * eeprom_control_byte_ptr,
				 unsigned int eeprom_control_io_address,
				 unsigned char eeprom_address,
				 unsigned int * eeprom_register_data_ptr
				 )
{
    unsigned char write_cmmd_and_addr;
	unsigned int   delay_counter;
    int lc;
    unsigned int word_read = 0 ;
    unsigned char test_results = 0 ;

// setup eeprom input signal to a starting value:
// cs -> high  (chip select)
// di -> low   (data in eeprom)
// sk -> low   (clock signal)


   set_eeprom_cs_bit_to_a_state ( eeprom_control_byte_ptr,0x00 );
   set_eeprom_clk_bit_to_a_state( eeprom_control_byte_ptr,0x00 );
   outportb(eeprom_control_io_address,*eeprom_control_byte_ptr);
   delay(1);

   set_eeprom_cs_bit_to_a_state ( eeprom_control_byte_ptr,0x01 );
   set_eeprom_wr_bit_to_a_state ( eeprom_control_byte_ptr,0x00 );
   outportb(eeprom_control_io_address,*eeprom_control_byte_ptr);
   delay(1);

// send the start bit
   set_eeprom_wr_bit_to_a_state ( eeprom_control_byte_ptr,0x01 );
   clk_a_bit_into_the_eeprom ( eeprom_control_byte_ptr,eeprom_control_io_address );

// build the read command code into the eeprom register address
   write_cmmd_and_addr =  eeprom_address & 0x3F;
   write_cmmd_and_addr |= 0x80;

// send the read command along with the address to the eeprom
   send_a_byte_of_data_to_the_eeprom
				 (
				 eeprom_control_byte_ptr,
				 eeprom_control_io_address,
				 write_cmmd_and_addr
				 );

// read_a_word_of_data_from_the_eeprom

    test_results = 0 ;
    word_read = 0 ;

    set_eeprom_wr_bit_to_a_state(eeprom_control_byte_ptr,0x00);

    // for loop to read all 16 bits, MSB first, so shift up.
	for (lc = 0; lc < 16; lc++)
       {
		 // clock to get the next bit out
		 clk_a_bit_into_the_eeprom ( eeprom_control_byte_ptr,eeprom_control_io_address );

		 word_read <<= 1 ; // shift up
		 test_results =  inportb(eeprom_control_io_address) & 0x08;
         if( test_results != 0)
             word_read |= 0x01 ;


       }// for loop, get all 16 bits

    *eeprom_register_data_ptr = word_read ;

// set cs low after word is read.

    set_eeprom_clk_bit_to_a_state( eeprom_control_byte_ptr,0x00 );
    set_eeprom_wr_bit_to_a_state(eeprom_control_byte_ptr,0x00);
	set_eeprom_cs_bit_to_a_state ( eeprom_control_byte_ptr,0x00 );

    outportb(eeprom_control_io_address,*eeprom_control_byte_ptr);

    clk_a_bit_into_the_eeprom ( eeprom_control_byte_ptr,eeprom_control_io_address );

// drive cs low and wait approx a micro second.
    outportb(eeprom_control_io_address,*eeprom_control_byte_ptr);
    outportb(eeprom_control_io_address,*eeprom_control_byte_ptr);
	outportb(eeprom_control_io_address,*eeprom_control_byte_ptr);

    for (delay_counter = 0; delay_counter <= 32766 ; delay_counter++ ) ;

} // end of procedure read_a_word_from_the_eeprom

//*****************************************************************************
int verify_the_plx_config_eeprom(void)
{
	int loop_control ;
	unsigned int plx_read_config_data_array[34] ;
    int failed ;
	 // initialize the array I'm reading data into
	for(loop_control=0; loop_control < 34 ; loop_control++ )
		  plx_read_config_data_array[loop_control] = 0xdead + loop_control ;

	read_the_plx_config_eeprom(plx_read_config_data_array);

	 // What the Hell, Might as well print it out
	for(loop_control=0; loop_control < 34 ; loop_control += 2 )
	    {
		printf("\n EEPROM: %2d, return value   %4X , %4X ",
					 loop_control,
					 plx_read_config_data_array[loop_control] ,
					 plx_read_config_data_array[loop_control+1] );
        if(( plx_read_config_data_array[loop_control]   != plx_config_data_array[loop_control] )
        || ( plx_read_config_data_array[loop_control+1] != plx_config_data_array[loop_control+1]) )
                {
                printf(" FAILED - Expected %4X , %4x " ,
				    	 plx_config_data_array[loop_control] ,
					     plx_config_data_array[loop_control+1] );
                failed++ ;
                }
		}

    return failed ;
}

#endif