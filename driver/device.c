// This software is covered by the GNU GENERAL PUBLIC LICENSE (GPL).
// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.0.1.0_GSC_DN/driver/device.c $
// $Rev: 15406 $
// $Date: 2012-03-20 17:27:06 -0500 (Tue, 20 Mar 2012) $

#include "main.h"



// variables	***************************************************************

const gsc_dev_id_t	dev_id_list[]	=
{
	// model		Vendor	Device	SubVen	SubDev	type

	{ "24DSI12",	0x10B5, 0x9080, 0x10B5, 0x3100,	GSC_DEV_TYPE_24DSI12	},
	{ "24DSI12",	0x10B5, 0x9056, 0x10B5, 0x3540,	GSC_DEV_TYPE_24DSI12	},
	{ "24DSI32",	0x10B5, 0x9080, 0x10B5, 0x2974,	GSC_DEV_TYPE_24DSI32	},

	// The 24DSI6 has the same ID values as the 24DSI12. What distinguishes
	// these boards is bit D22 in the Board Config Register.
	// That bit is "1" for the 24DSI6 and "0" for the 24DSI12.
	{ "24DSI6",		0x10B5, 0x9080, 0x10B5, 0x3100,	GSC_DEV_TYPE_24DSI6		},
	{ NULL }
};



//*****************************************************************************
static int _board_type_error(dev_data_t* dev, int line)
{
	printk(	"%s: INTERNAL ERROR: file %s, line %d, id %d\n",
			DEV_NAME,
			__FILE__,
			line,
			(int) dev->board_type);
	return(1);
}



//*****************************************************************************
static int _stricmp(const char* str1, const char* str2)
{
	int	c1;
	int	c2;
	int	ret;

	if ((str1 == NULL) && (str2 == NULL))
	{
		ret	= 0;
	}
	else if (str1 == NULL)
	{
		ret	= 1;
	}
	else if (str2 == NULL)
	{
		ret	= -1;
	}
	else
	{
		for (ret = 0;; str1++, str2++)
		{
			c1	= str1[0];
			c1	= (c1 < 'A') ? c1 : ((c1 > 'Z') ? c1 : c1 - 'A' + 'a');
			c2	= str2[0];
			c2	= (c2 < 'A') ? c2 : ((c2 > 'Z') ? c2 : c2 - 'A' + 'a');
			ret	= c2 - c1;

			if ((ret == 0) || (c1 == 0))
				break;
		}
	}

	return(ret);
}



//*****************************************************************************
static int _auto_calibrate_compute(dev_data_t* dev)
{
	int	errs	= 0;
	u16	sdidr;

	switch (dev->board_type)
	{
		default:

			errs	= _board_type_error(dev, __LINE__);
			break;

		case GSC_DEV_TYPE_24DSI6:

			// Auto-cal is not supported.
			dev->cache.auto_cal_ms	= 0;
			break;

		case GSC_DEV_TYPE_24DSI12:

			pci_read_config_word(dev->pci, 0x2E, &sdidr);

			if (sdidr == 0x3540)
				dev->cache.auto_cal_ms	= 5 * 1000;
			else
				dev->cache.auto_cal_ms	= 8 * 1000;

			break;

		case GSC_DEV_TYPE_24DSI32:

			dev->cache.auto_cal_ms	= 9 * 1000;
			break;
	}

	return(errs);
}



//*****************************************************************************
static int _registers_compute(dev_data_t* dev)
{
	int	errs	= 0;
	u16	sdidr;

	switch (dev->board_type)
	{
		default:

			errs	= _board_type_error(dev, __LINE__);
			break;

		case GSC_DEV_TYPE_24DSI6:

			dev->cache.rcar_rcbr	= 1;
			dev->cache.reg_icmr		= 0;
			dev->cache.rfcr			= 0;
			break;


		case GSC_DEV_TYPE_24DSI12:

			pci_read_config_word(dev->pci, 0x2E, &sdidr);

			if (sdidr == 0x3540)
				dev->cache.reg_icmr	= 1;
			else
				dev->cache.reg_icmr	= 0;

			dev->cache.rcar_rcbr	= 1;
			dev->cache.rfcr			= 0;
			break;

		case GSC_DEV_TYPE_24DSI32:

			dev->cache.rcar_rcbr	= 0;
			dev->cache.reg_icmr		= 0;

			if ((dev->cache.gsc_bcfgr_32 & 0xF00) == 0x700)
				dev->cache.rfcr	= 1;
			else
				dev->cache.rfcr	= 0;

			break;
	}

	return(errs);
}



//*****************************************************************************
static int _bctlr_d16_compute(dev_data_t* dev)
{
	int	errs	= 0;

	switch (dev->board_type)
	{
		default:

			errs	= _board_type_error(dev, __LINE__);
			break;

		case GSC_DEV_TYPE_24DSI6:
		case GSC_DEV_TYPE_24DSI12:

			dev->cache.bctlr_d16_sync_1	= 0;
			break;

		case GSC_DEV_TYPE_24DSI32:

			dev->cache.bctlr_d16_sync_1	= 1;
			break;
	}

	return(errs);
}



//*****************************************************************************
static int _bctlr_d19_compute(dev_data_t* dev)
{
	int	errs	= 0;

	switch (dev->board_type)
	{
		default:

			errs	= _board_type_error(dev, __LINE__);
			break;

		case GSC_DEV_TYPE_24DSI6:

			dev->cache.bctlr_d19_freq_filt	= 0;
			break;

		case GSC_DEV_TYPE_24DSI12:
		case GSC_DEV_TYPE_24DSI32:

			dev->cache.bctlr_d19_freq_filt	= 1;
			break;
	}

	return(errs);
}



//*****************************************************************************
static int _bctlr_d20_compute(dev_data_t* dev)
{
	int	errs	= 0;

	switch (dev->board_type)
	{
		default:

			errs	= _board_type_error(dev, __LINE__);
			break;

		case GSC_DEV_TYPE_24DSI6:
		case GSC_DEV_TYPE_24DSI12:

			dev->cache.bctlr_d20_xcvr	= 1;
			break;

		case GSC_DEV_TYPE_24DSI32:

			dev->cache.bctlr_d20_xcvr	= 0;
			break;
	}

	return(errs);
}



//*****************************************************************************
static int _bctlr_d21_compute(dev_data_t* dev)
{
	int	errs	= 0;
	u16	sdidr;

	switch (dev->board_type)
	{
		default:

			errs	= _board_type_error(dev, __LINE__);
			break;

		case GSC_DEV_TYPE_24DSI6:

			dev->cache.bctlr_d21_arm_et	= 0;
			break;

		case GSC_DEV_TYPE_24DSI12:

			pci_read_config_word(dev->pci, 0x2E, &sdidr);

			if (sdidr == 0x3540)
				dev->cache.bctlr_d21_arm_et	= 1;
			else
				dev->cache.bctlr_d21_arm_et	= 0;

			break;

		case GSC_DEV_TYPE_24DSI32:

			if ((dev->cache.gsc_bcfgr_32 & 0xFFF) < 0x502)
				dev->cache.bctlr_d21_arm_et	= 0;
			else if ((dev->cache.gsc_bcfgr_32 & 0xFFF) > 0x5FF)
				dev->cache.bctlr_d21_arm_et	= 0;
			else
				dev->cache.bctlr_d21_arm_et	= 1;

			break;
	}

	return(errs);
}



//*****************************************************************************
static int _bctlr_d22_compute(dev_data_t* dev)
{
	int	errs	= 0;

	switch (dev->board_type)
	{
		default:

			errs	= _board_type_error(dev, __LINE__);
			break;

		case GSC_DEV_TYPE_24DSI6:

			dev->cache.bctlr_d22_coupling	= 1;
			dev->cache.bctlr_d22_thr_f_out	= 0;
			break;

		case GSC_DEV_TYPE_24DSI12:

			dev->cache.bctlr_d22_coupling	= 0;
			dev->cache.bctlr_d22_thr_f_out	= 0;
			break;

		case GSC_DEV_TYPE_24DSI32:

			if ((dev->cache.gsc_bcfgr_32 & 0xFFF) < 0x502)
				dev->cache.bctlr_d22_thr_f_out	= 0;
			else if ((dev->cache.gsc_bcfgr_32 & 0xFFF) > 0x5FF)
				dev->cache.bctlr_d22_thr_f_out	= 0;
			else
				dev->cache.bctlr_d22_thr_f_out	= 1;

			dev->cache.bctlr_d22_coupling	= 0;
			break;
	}

	return(errs);
}



//*****************************************************************************
static int _bctlr_d23_compute(dev_data_t* dev)
{
	int	errs	= 0;

	switch (dev->board_type)
	{
		default:

			errs	= _board_type_error(dev, __LINE__);
			break;

		case GSC_DEV_TYPE_24DSI6:

			if ((dev->cache.gsc_bcfgr_32 & 0xF00) >= 0x900)
				dev->cache.bctlr_d23_rate_out	= 1;
			else
				dev->cache.bctlr_d23_rate_out	= 0;

			dev->cache.bctlr_d23_inv_e_trg	= 0;
			break;

		case GSC_DEV_TYPE_24DSI12:

			if (dev->cache.pll_present)
				dev->cache.bctlr_d23_inv_e_trg	= 1;
			else
				dev->cache.bctlr_d23_inv_e_trg	= 0;

			dev->cache.bctlr_d23_rate_out	= 0;
			break;

		case GSC_DEV_TYPE_24DSI32:

			dev->cache.bctlr_d23_inv_e_trg	= 0;
			dev->cache.bctlr_d23_rate_out	= 0;
			break;
	}

	return(errs);
}



//*****************************************************************************
static int _bcfgr_d15_compute(dev_data_t* dev)
{
	int	errs	= 0;

	switch (dev->board_type)
	{
		default:

			errs	= _board_type_error(dev, __LINE__);
			break;

		case GSC_DEV_TYPE_24DSI12:

			dev->cache.bcfgr_d15_pll	= 1;
			break;

		case GSC_DEV_TYPE_24DSI6:
		case GSC_DEV_TYPE_24DSI32:

			dev->cache.bcfgr_d15_pll	= 0;
			break;
	}

	return(errs);
}



//*****************************************************************************
static int _bcfgr_d17_d18_compute(dev_data_t* dev)
{
	int	errs	= 0;

	switch (dev->board_type)
	{
		default:

			errs	= _board_type_error(dev, __LINE__);
			break;

		case GSC_DEV_TYPE_24DSI6:

			dev->cache.bcfgr_d17_18_range	= 1;
			break;

		case GSC_DEV_TYPE_24DSI12:
		case GSC_DEV_TYPE_24DSI32:

			dev->cache.bcfgr_d17_18_range	= 0;
			break;
	}

	return(errs);
}



//*****************************************************************************
static int _bcfgr_d18_compute(dev_data_t* dev)
{
	int	errs	= 0;

	switch (dev->board_type)
	{
		default:

			errs	= _board_type_error(dev, __LINE__);
			break;

		case GSC_DEV_TYPE_24DSI6:

			dev->cache.bcfgr_d18_cf_filt	= 0;
			dev->cache.bcfgr_d18_rear_io	= 0;
			break;

		case GSC_DEV_TYPE_24DSI12:

			dev->cache.bcfgr_d18_cf_filt	= 1;
			dev->cache.bcfgr_d18_rear_io	= 0;
			break;

		case GSC_DEV_TYPE_24DSI32:

			if ((dev->cache.gsc_bcfgr_32 & 0xF00) == 0x700)
				dev->cache.bcfgr_d18_rear_io	= 1;
			else
				dev->cache.bcfgr_d18_rear_io	= 0;

			dev->cache.bcfgr_d18_cf_filt	= 0;
			break;
	}

	return(errs);
}



//*****************************************************************************
static int _bcfgr_d19_compute(dev_data_t* dev)
{
	int	errs	= 0;

	switch (dev->board_type)
	{
		default:

			errs	= _board_type_error(dev, __LINE__);
			break;

		case GSC_DEV_TYPE_24DSI12:

			dev->cache.bcfgr_d19_ext_temp	= 1;
			break;

		case GSC_DEV_TYPE_24DSI6:
		case GSC_DEV_TYPE_24DSI32:

			dev->cache.bcfgr_d19_ext_temp	= 0;
			break;
	}

	return(errs);
}



//*****************************************************************************
static int _bcfgr_d19_d20_compute(dev_data_t* dev)
{
	int	errs	= 0;

	switch (dev->board_type)
	{
		default:

			errs	= _board_type_error(dev, __LINE__);
			break;

		case GSC_DEV_TYPE_24DSI6:

			dev->cache.bcfgr_d19_20_filt	= 1;
			break;

		case GSC_DEV_TYPE_24DSI12:
		case GSC_DEV_TYPE_24DSI32:

			dev->cache.bcfgr_d19_20_filt	= 0;
			break;
	}

	return(errs);
}



//*****************************************************************************
static int _bcfgr_d20_compute(dev_data_t* dev)
{
	int	errs	= 0;
	u16	sdidr;

	switch (dev->board_type)
	{
		default:

			errs	= _board_type_error(dev, __LINE__);
			break;

		case GSC_DEV_TYPE_24DSI12:

			pci_read_config_word(dev->pci, 0x2E, &sdidr);

			if (sdidr == 0x3540)
				dev->cache.bcfgr_d20_low_pow	= 0;
			else
				dev->cache.bcfgr_d20_low_pow	= 1;

			if (dev->cache.bcfgr_d20_low_pow == 0)
				dev->cache.low_power	= 0;
			else if (dev->cache.gsc_bcfgr_32 & D20)
				dev->cache.low_power	= 1;
			else
				dev->cache.low_power	= 0;

			break;

		case GSC_DEV_TYPE_24DSI6:
		case GSC_DEV_TYPE_24DSI32:

			dev->cache.bcfgr_d20_low_pow	= 0;
			dev->cache.low_power			= 0;
			break;
	}

	return(errs);
}



//*****************************************************************************
static int _bcfgr_d21_compute(dev_data_t* dev)
{
	int	errs	= 0;

	switch (dev->board_type)
	{
		default:

			errs	= _board_type_error(dev, __LINE__);
			break;

		case GSC_DEV_TYPE_24DSI6:

			dev->cache.bcfgr_d21_ext_temp	= 1;
			break;

		case GSC_DEV_TYPE_24DSI12:
		case GSC_DEV_TYPE_24DSI32:

			dev->cache.bcfgr_d21_ext_temp	= 0;
			break;
	}

	return(errs);
}



//*****************************************************************************
static int _bcfgr_d22_compute(dev_data_t* dev)
{
	int	errs	= 0;

	switch (dev->board_type)
	{
		default:

			errs	= _board_type_error(dev, __LINE__);
			break;

		case GSC_DEV_TYPE_24DSI6:

			dev->cache.bcfgr_d22_24dsi6ln	= 1;
			dev->cache.low_noise			= 1;
			break;

		case GSC_DEV_TYPE_24DSI12:
		case GSC_DEV_TYPE_24DSI32:

			dev->cache.bcfgr_d22_24dsi6ln	= 0;
			dev->cache.low_noise			= 0;
			break;
	}

	return(errs);
}



//*****************************************************************************
static int _channel_groups_compute(dev_data_t* dev)
{
	int	errs	= 0;

	switch (dev->board_type)
	{
		default:

			errs	= _board_type_error(dev, __LINE__);
			break;

		case GSC_DEV_TYPE_24DSI6:
		case GSC_DEV_TYPE_24DSI12:

			dev->cache.channel_groups	= 2;
			dev->cache.ch_grp_0_rar_pri	= 0;
			break;

		case GSC_DEV_TYPE_24DSI32:

			dev->cache.channel_groups	= 4;
			dev->cache.ch_grp_0_rar_pri	= 1;
			break;
	}

	return(errs);
}



//*****************************************************************************
static int _channels_compute(dev_data_t* dev)
{
	int	errs	= 0;

	switch (dev->board_type)
	{
		default:

			errs	= _board_type_error(dev, __LINE__);
			break;

		case GSC_DEV_TYPE_24DSI6:

			if (dev->cache.gsc_bcfgr_32 & 0x10000)
				dev->cache.channel_qty	= 6;
			else
				dev->cache.channel_qty	= 4;

			dev->cache.channels_max			= 6;
			dev->cache.bcfgr_d16_chans		= 6;
			dev->cache.bcfgr_d16_chans_0	= 4;
			dev->cache.bcfgr_d17_chans		= 0;
			dev->cache.bcfgr_d17_chans_0	= 0;
			break;

		case GSC_DEV_TYPE_24DSI12:

			if (dev->cache.gsc_bcfgr_32 & D17)
				dev->cache.channel_qty	= 4;
			else if (dev->cache.gsc_bcfgr_32 & D16)
				dev->cache.channel_qty	= 8;
			else
				dev->cache.channel_qty	= 12;

			dev->cache.channels_max			= 12;
			dev->cache.bcfgr_d16_chans		= 8;
			dev->cache.bcfgr_d16_chans_0	= dev->cache.channel_qty;
			dev->cache.bcfgr_d17_chans		= 4;
			dev->cache.bcfgr_d17_chans_0	= dev->cache.channel_qty;
			break;

		case GSC_DEV_TYPE_24DSI32:

			if (dev->cache.gsc_bcfgr_32 & 0x20000)
				dev->cache.channel_qty	= 8;
			else if (dev->cache.gsc_bcfgr_32 & 0x10000)
				dev->cache.channel_qty	= 16;
			else
				dev->cache.channel_qty	= 32;

			dev->cache.channels_max			= 32;
			dev->cache.bcfgr_d16_chans		= 16;
			dev->cache.bcfgr_d16_chans_0	= dev->cache.channel_qty;
			dev->cache.bcfgr_d17_chans		= 8;
			dev->cache.bcfgr_d17_chans_0	= dev->cache.channel_qty;
			break;
	}

	return(errs);
}



//*****************************************************************************
static int _fref_compute(dev_data_t* dev)
{
	if (dev->cache.pll_present)
	{
		// For PLL support.
		dev->cache.fref_default	= 32768000;
	}
	else
	{
		// For legacy support.
		dev->cache.fref_default	= 0;
	}

	return(0);
}



//*****************************************************************************
static int _fifo_size_compute(dev_data_t* dev)
{
	dev->cache.fifo_size		= _256K;
	return(0);
}



//*****************************************************************************
static int _model_compute(dev_data_t* dev)
{
	int	errs	= 0;
	int	i;
	u16	sdidr;

	switch (dev->board_type)
	{
		default:

			errs	= _board_type_error(dev, __LINE__);
			break;

		case GSC_DEV_TYPE_24DSI12:

			pci_read_config_word(dev->pci, 0x2E, &sdidr);

			if (sdidr == 0x3540)	// already got positive id
				break;

		case GSC_DEV_TYPE_24DSI6:

			if (dev->cache.gsc_bcfgr_32 & 0x00400000)
				dev->board_type	= GSC_DEV_TYPE_24DSI6;
			else
				dev->board_type	= GSC_DEV_TYPE_24DSI12;

			for (i = 0; dev_id_list[i].model; i++)
			{
				if (dev->board_type == dev_id_list[i].type)
				{
					dev->model	= dev_id_list[i].model;
					break;
				}
			}

			break;

		case GSC_DEV_TYPE_24DSI32:

			break;
	}

	return(errs);
}



//*****************************************************************************
static int _rate_divisors_compute(dev_data_t* dev)
{
	int	errs	= 0;

	switch (dev->board_type)
	{
		default:

			errs	= _board_type_error(dev, __LINE__);
			break;

		case GSC_DEV_TYPE_24DSI6:
		case GSC_DEV_TYPE_24DSI12:
		case GSC_DEV_TYPE_24DSI32:

			dev->cache.rate_div_ndiv_min	= 0;
			dev->cache.rate_div_ndiv_max	= 25;
			dev->cache.rate_div_ndiv_mask	= 0xFF;
			break;
	}

	switch (dev->board_type)
	{
		default:

			errs	= _board_type_error(dev, __LINE__);
			break;

		case GSC_DEV_TYPE_24DSI6:
		case GSC_DEV_TYPE_24DSI12:

			dev->cache.rate_div_qty	= 2;
			break;

		case GSC_DEV_TYPE_24DSI32:

			dev->cache.rate_div_qty	= 1;
			break;
	}

	return(errs);
}



//*****************************************************************************
static int _gps_present_compute(dev_data_t* dev)
{
	int	errs	= 0;

	switch (dev->board_type)
	{
		default:

			errs	= _board_type_error(dev, __LINE__);
			break;

		case GSC_DEV_TYPE_24DSI6:

			if ((dev->cache.gsc_bcfgr_32 & 0x8FFF) >= 0x8103)
				dev->cache.gps_present	= 1;
			else
				dev->cache.gps_present	= 0;

			break;

		case GSC_DEV_TYPE_24DSI12:

			if ((dev->cache.gsc_bcfgr_32 & 0xFFFF) >= 0x8103)
				dev->cache.gps_present	= 1;
			else
				dev->cache.gps_present	= 0;

			break;

		case GSC_DEV_TYPE_24DSI32:

			dev->cache.gps_present	= 0;
			break;
	}

	return(errs);
}



//*****************************************************************************
static int _pll_present_compute(dev_data_t* dev)
{
	int	errs	= 0;

	switch (dev->board_type)
	{
		default:

			errs	= _board_type_error(dev, __LINE__);
			break;

		case GSC_DEV_TYPE_24DSI6:

			dev->cache.pll_present	= 1;
			break;

		case GSC_DEV_TYPE_24DSI12:

			if (dev->cache.gsc_bcfgr_32 & 0x8000)
				dev->cache.pll_present	= 1;
			else
				dev->cache.pll_present	= 0;

			break;

		case GSC_DEV_TYPE_24DSI32:

			if ((dev->cache.gsc_bcfgr_32 & 0xF00) >= 0x500)
				dev->cache.pll_present	= 1;
			else
				dev->cache.pll_present	= 0;

			break;
	}

	return(errs);
}



//*****************************************************************************
static int _in_mode_compute(dev_data_t* dev)
{
	int	errs	= 0;

	switch (dev->board_type)
	{
		default:

			errs	= _board_type_error(dev, __LINE__);
			break;

		case GSC_DEV_TYPE_24DSI6:

			dev->cache.bctlr_d0_1_in_mode	= 0;
			break;

		case GSC_DEV_TYPE_24DSI12:
		case GSC_DEV_TYPE_24DSI32:

			dev->cache.bctlr_d0_1_in_mode	= 1;
			break;
	}

	return(errs);
}



//*****************************************************************************
static int _range_compute(dev_data_t* dev)
{
	int	errs	= 0;

	switch (dev->board_type)
	{
		default:

			errs	= _board_type_error(dev, __LINE__);
			break;

		case GSC_DEV_TYPE_24DSI6:

			dev->cache.bctlr_d2_3_range	= 0;
			break;

		case GSC_DEV_TYPE_24DSI12:
		case GSC_DEV_TYPE_24DSI32:

			dev->cache.bctlr_d2_3_range	= 1;
			break;
	}

	return(errs);
}



//*****************************************************************************
static int _sample_rates_compute(dev_data_t* dev)
{
	int	errs;

	dev->cache.fsamp_max	= 200000L;
	dev->cache.fsamp_min	=   2000L;

	switch (dev->board_type)
	{
		default:

			errs	= _board_type_error(dev, __LINE__);
			break;

		case GSC_DEV_TYPE_24DSI6:
		case GSC_DEV_TYPE_24DSI12:

			errs						= 0;
			dev->cache.fsamp_default	= 10000;
			break;

		case GSC_DEV_TYPE_24DSI32:

			errs						= 0;
			dev->cache.fsamp_default	= 12800;
			break;
	}

	return(errs);
}



//*****************************************************************************
static int _initialize_compute(dev_data_t* dev)
{
	// It is documented as 5 seconds, but longer periods have been observed.
	dev->cache.initialize_ms	= 6000;
	return(0);
}



//*****************************************************************************
static int _rate_generators_compute(dev_data_t* dev)
{
	int	errs	= 0;

	if (dev->cache.pll_present)
	{
		// For PLL support.
		dev->cache.rate_gen_nref_min	= 30;
		dev->cache.rate_gen_nref_max	= 1000;
		dev->cache.rate_gen_nref_mask	= 0x3FF;

		dev->cache.rate_gen_nvco_min	= 30;
		dev->cache.rate_gen_nvco_max	= 1000;
		dev->cache.rate_gen_nvco_mask	= 0x3FF;
	}
	else
	{
		// For legacy support.
		dev->cache.rate_gen_nrate_min	= 0;
		dev->cache.rate_gen_nrate_max	= 100000;
		dev->cache.rate_gen_nrate_mask	= 0x1FFFF;
	}

	switch (dev->board_type)
	{
		default:

			errs	= _board_type_error(dev, __LINE__);
			break;

		case GSC_DEV_TYPE_24DSI6:
		case GSC_DEV_TYPE_24DSI12:

			dev->cache.rate_gen_qty			= 2;
			dev->cache.rate_gen_fgen_max	= 51200000L;
			dev->cache.rate_gen_fgen_min	= 25600000L;
			break;

		case GSC_DEV_TYPE_24DSI32:

			dev->cache.rate_gen_qty	= 1;

			if (dev->cache.pll_present)
			{
				dev->cache.rate_gen_fgen_max	= 55000000L;
				dev->cache.rate_gen_fgen_min	= 20000000L;
			}
			else
			{
				dev->cache.rate_gen_fgen_max	= 51200000L;
				dev->cache.rate_gen_fgen_min	= 25600000L;
			}

			break;
	}

	return(errs);
}



/******************************************************************************
*
*	Function:	dev_device_create
*
*	Purpose:
*
*		Do everything needed to setup and use the given device.
*
*	Arguments:
*
*		dev		The structure to initialize.
*
*	Returned:
*
*		>=0		The number of errors seen.
*
******************************************************************************/

int dev_device_create(dev_data_t* dev)
{
	u32	bcr;
	u32	dma;
	int	errs	= 0;

	for (;;)	// A convenience loop.
	{
		// Verify some macro contents.

		if (strcmp(DEV_NAME, DSI_BASE_NAME))
		{
			errs	= 1;
			printk(	"%s: dev_device_create, line %d:"
					" DEV_NAME and DSI_BASE_NAME are different.\n",
					DEV_NAME,
					__LINE__);
			printk(	"%s: dev_device_create, line %d:"
					" DEV_NAME is '%s' from main.h\n",
					DEV_NAME,
					__LINE__,
					DEV_NAME);
			printk(	"%s: dev_device_create, line %d:"
					" DSI_BASE_NAME is '%s' from 24dsi.h\n",
					DEV_NAME,
					__LINE__,
					DSI_BASE_NAME);
			break;
		}

		if (_stricmp(DEV_NAME, DEV_MODEL))
		{
			errs	= 1;
			printk(	"%s: dev_device_create, line %d:"
					" DEV_NAME and DEV_MODEL do not agree.\n",
					DEV_NAME,
					__LINE__);
			printk(	"%s: dev_device_create, line %d:"
					" DEV_NAME is '%s' from main.h\n",
					DEV_NAME,
					__LINE__,
					DEV_NAME);
			printk(	"%s: dev_device_create, line %d:"
					" DEV_MODEL is '%s' from main.h\n",
					DEV_NAME,
					__LINE__,
					DEV_MODEL);
			break;
		}

		// Enable the PCI device.
		errs	= PCI_ENABLE_DEVICE(dev->pci);

		if (errs)
		{
			printk(	"%s: dev_device_create, line %d:"
					" PCI_ENABLE_DEVICE error\n",
					DEV_NAME,
					__LINE__);
			break;
		}

		pci_set_master(dev->pci);

		// Control accessto the device structure.
		gsc_sem_create(&dev->sem);

		// Access the BAR regions.
		errs	+= gsc_bar_create(dev->pci, 0, &dev->plx,  1, 0);	// memory
		errs	+= gsc_bar_create(dev->pci, 2, &dev->gsc,  1, 0);	// memory

		if (errs)
			break;

		dev->vaddr.plx_intcsr_32	= PLX_VADDR(dev, 0x68);
		dev->vaddr.plx_dmaarb_32	= PLX_VADDR(dev, 0xAC);
		dev->vaddr.plx_dmathr_32	= PLX_VADDR(dev, 0xB0);

		dev->vaddr.gsc_bctlr_32		= GSC_VADDR(dev, 0x00);
		dev->vaddr.gsc_rar_32		= GSC_VADDR(dev, 0x0C);
		dev->vaddr.gsc_rdr_32		= GSC_VADDR(dev, 0x10);
		dev->vaddr.gsc_prfr_32		= GSC_VADDR(dev, 0x18);
		dev->vaddr.gsc_gsr_32		= GSC_VADDR(dev, 0x1C);
		dev->vaddr.gsc_ibcr_32		= GSC_VADDR(dev, 0x20);
		dev->vaddr.gsc_bcfgr_32		= GSC_VADDR(dev, 0x24);
		dev->vaddr.gsc_bsr_32		= GSC_VADDR(dev, 0x28);
		dev->vaddr.gsc_idbr_32		= GSC_VADDR(dev, 0x30);

		// 24DSI12 specific
		dev->vaddr.gsc_rcar_32		= GSC_VADDR(dev, 0x04);
		dev->vaddr.gsc_rcbr_32		= GSC_VADDR(dev, 0x08);

		// 24DSI32 specific
		dev->vaddr.gsc_nrefcr_32	= GSC_VADDR(dev, 0x04);
		dev->vaddr.gsc_nvcocr_32	= GSC_VADDR(dev, 0x08);
		dev->vaddr.gsc_rcr_32		= GSC_VADDR(dev, 0x08);
		dev->vaddr.gsc_rfcr_32		= GSC_VADDR(dev, 0x1C);

		// Start the cache.
		dev->cache.gsc_bcfgr_32		= readl(dev->vaddr.gsc_bcfgr_32);

		errs	+= gsc_irq_create(dev);

		if (errs)
			break;

		// Clear any existing firmware interrupt.
		bcr	= readl(dev->vaddr.gsc_bctlr_32);
		bcr	&= ~BCTLR_IRQ_ACTIVE;
		writel(bcr, dev->vaddr.gsc_bctlr_32);

		// Cache various pieces of information.
		errs	+= _model_compute(dev);			// Must be 1st.
		errs	+= _pll_present_compute(dev);	// Must be 2nd.
		errs	+= _gps_present_compute(dev);
		errs	+= _initialize_compute(dev);
		errs	+= _auto_calibrate_compute(dev);

		errs	+= _registers_compute(dev);

		errs	+= _bctlr_d16_compute(dev);
		errs	+= _bctlr_d19_compute(dev);
		errs	+= _bctlr_d20_compute(dev);
		errs	+= _bctlr_d21_compute(dev);
		errs	+= _bctlr_d22_compute(dev);
		errs	+= _bctlr_d23_compute(dev);

		errs	+= _bcfgr_d15_compute(dev);
		errs	+= _bcfgr_d17_d18_compute(dev);
		errs	+= _bcfgr_d18_compute(dev);
		errs	+= _bcfgr_d19_compute(dev);
		errs	+= _bcfgr_d19_d20_compute(dev);
		errs	+= _bcfgr_d20_compute(dev);
		errs	+= _bcfgr_d21_compute(dev);
		errs	+= _bcfgr_d22_compute(dev);

		errs	+= _channels_compute(dev);
		errs	+= _channel_groups_compute(dev);
		errs	+= _fifo_size_compute(dev);
		errs	+= _in_mode_compute(dev);
		errs	+= _range_compute(dev);
		errs	+= _sample_rates_compute(dev);
		errs	+= _rate_divisors_compute(dev);
		errs	+= _rate_generators_compute(dev);	// After PLL.
		errs	+= _fref_compute(dev);				// After PLL.

		errs	+= io_create(dev);

		dma		= GSC_DMA_SEL_STATIC
				| GSC_DMA_CAP_DMA_READ
				| GSC_DMA_CAP_DMDMA_READ;
		errs	+= gsc_dma_create(dev, dma, dma);

		break;
	}

	return(errs);
}



/******************************************************************************
*
*	Function:	dev_device_destroy
*
*	Purpose:
*
*		Do everything needed to release the referenced device.
*
*	Arguments:
*
*		dev		The partial data for the device of interest.
*
*	Returned:
*
*		None.
*
******************************************************************************/

void dev_device_destroy(dev_data_t* dev)
{
	if (dev)
	{
		gsc_dma_destroy(dev);
		io_destroy(dev);
		gsc_irq_destroy(dev);

		// Tear down the BAR regions.
		gsc_bar_destroy(&dev->plx);
		gsc_bar_destroy(&dev->gsc);

		gsc_sem_destroy(&dev->sem);

		if (dev->pci)
			PCI_DISABLE_DEVICE(dev->pci);
	}
}


