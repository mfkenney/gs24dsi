// This software is covered by the GNU GENERAL PUBLIC LICENSE (GPL).
// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.0.1.0_GSC_DN/driver/main.h $
// $Rev: 17651 $
// $Date: 2012-08-20 17:15:19 -0500 (Mon, 20 Aug 2012) $

#ifndef __MAIN_H__
#define __MAIN_H__

#define	DEV_BAR_SHOW		0
#define	DEV_PCI_ID_SHOW		0

#include "24dsi.h"

#include "gsc_common.h"
#include "gsc_main.h"



// #defines	*******************************************************************

#define	DEV_MODEL					"24DSI"	// Upper case form of the below.
#define	DEV_NAME					"24dsi"	// MUST AGREE WITH DSI_BASE_NAME

#define DEV_VERSION					"3.14"	// FOR DEVICE SPECIFIC CODE ONLY!
// 3.14	Correct IOCTL services that access BCTRL - they disable interrupt
//		Modified the Init service; wait for irq, then waits for bit to clear
//		Added the IOCTL service DSI_IOCTL_CHANNELS_READY.
//		Updated the IOCTL service DSI_IOCTL_EXT_CLK_SRC.
//		Updated the IOCTL service DSI_IOCTL_CHANNEL_ORDER.
// 3.13	Corrected timeout error with jiffies rollover.
// 3.12	Updated for the 3.x kernel.
// 3.11	Corrected problems with feature support calculations.
// 3.10	Add support for the PCIE104-24DSI12.
// 3.9	BAR0 and BAR2 are now the only BAR regions used.
//		Include all common source, though not all are used.
//		Fixed bugs in the Nrate services.
// 3.8	Overhauled IOCTL based local interrupt implementation.
//		Implemented support for the common PIO I/O routines.
//		Corrected I/O timeout handling.
//		Improved non-blocking I/O handling.
//		IOCTL services which timeout now return ETIMEOUT rather than EIO.
//		Changed use of DEV_SUPPORTS_READ macro.
//		Changed use of DEV_SUPPORTS_WRITE macro.
//		Changed use of DEV_IO_AUTO_START macro.
//		Eliminated the global dev_check_id() routine.
//		Corrected Auto-Calibrate IOCTL service.
// 3.7	Overhauled IOCTL based local interrupt implementation.
//		Updated per changes to the common source code.
//		Added DEV_IO_AUTO_START macro - not support here.
//		Implemented IOCTL based I/O Abort support.
//		Implemented IOCTL based event/interrupt wait support.
//		Updated to use the common gsc_open() and gsc_close() services.
//		Implemented explicit local interrupt support.
// 3.6	Made various read support services use the same argument data types.
//		Modified code to use the common open and close services.
// 3.5	Any existing FW interrupt is now cleared at startup.
// 3.4	Allotted more time for init and Auto Cal IOCTL services.
//		Added dev_check_id() for common GSC code.
// 3.3	Fixed bug in DSI_IOCTL_CH_GRP_1/2/3_SRC ioctl services.
//		Made init and auto-cal services more robust.
// 3.2	Fixed GPS feature detection.
// 3.1	Added support for the 24DSI6LN board.
// 3.0	Initial release of the new driver supporting all 24DSI boards.

#ifndef	TRUE
	#define	TRUE		1
#endif

#ifndef	FALSE
	#define	FALSE		0
#endif

// I/O services
#define	DEV_PIO_READ_AVAILABLE		pio_read_available
#define	DEV_PIO_READ_WORK			gsc_read_pio_work_32_bit
#define	DEV_DMA_READ_AVAILABLE		dma_read_available
#define	DEV_DMA_READ_WORK			dma_read_work
#define	DEV_DMDMA_READ_AVAILABLE	dmdma_read_available
#define	DEV_DMDMA_READ_WORK			dmdma_read_work

#define	DEV_SUPPORTS_READ
#define	GSC_READ_PIO_WORK_32_BIT

// WAIT services
#define	DEV_WAIT_GSC_ALL			DSI_WAIT_GSC_ALL
#define	DEV_WAIT_ALT_ALL			DSI_WAIT_ALT_ALL

// Register macros.
#define	BCTLR_IRQ_ACTIVE			0x0800



// data types	***************************************************************

struct _dev_io_t
{
	gsc_sem_t				sem;				// Only one Tx or Rx at a time.

	int						abort;				// Abort an active I/O operation.
	int						bytes_per_sample;	// Sample size in bytes.
	gsc_dma_ch_t*			dma_channel;		// Use this channel for DMA.
	gsc_io_mode_t			io_mode;			// PIO, DMA, DMDMA
	u32						io_reg_offset;		// Offset of board's I/O FIFO.
	VADDR_T					io_reg_vaddr;		// Address of board's I/O FIFO.
	int						non_blocking;		// Is this non-blocking I/O?
	s32						overflow_check;		// Check overflow when reading?
	s32						pio_threshold;		// Use PIO if samples <= this.
	s32						timeout_s;			// I/O timeout in seconds.
	s32						underflow_check;	// Check underflow when reading?

	// Memory allocations.
	unsigned long			adrs;				// From get free pages.
	void*					buf;				// Page aligned.
	s32						bytes;				// Rounded to page boundary.
	u8						order;				// Order of the page request.

	// Thread sleep and resume fields.
	WAIT_QUEUE_HEAD_T		queue;
	int						condition;			// 2.6 kernel
};

struct _dev_data_t
{
	struct pci_dev*		pci;			// the kernel PCI device descriptor
	gsc_sem_t			sem;			// Comtrol access to structure
	int					in_use;			// Only one user at a time.
	int					board_index;	// Corresponds to minor number.
	const char*			model;			// i.e. "24DSI12"
	gsc_dev_type_t		board_type;		// Corresponds to the model string.

	gsc_bar_t			plx;			// PLX registers in memory space.
	gsc_bar_t			gsc;			// GSC registers in memory space.

	gsc_dma_t			dma;			// For DMA based I/O.

	gsc_irq_t			irq;			// For interrut support.

	dev_io_t			rx;				// For read operations.

	gsc_wait_node_t*	wait_list;

	struct
	{
		VADDR_T			plx_intcsr_32;	// Interrupt Control/Status Register
		VADDR_T			plx_dmaarb_32;	// DMA Arbitration Register
		VADDR_T			plx_dmathr_32;	// DMA Threshold Register

		VADDR_T			gsc_bctlr_32;	// 0x00 Board Control Register
		VADDR_T			gsc_rar_32;		// 0x0C Rate Assignment Register
		VADDR_T			gsc_rdr_32;		// 0x10 Rate Divisor Register
		VADDR_T			gsc_prfr_32;	// 0x18	PLL Reference Frequency Register
		VADDR_T			gsc_gsr_32;		// 0x1C GPS Synchronization Register
		VADDR_T			gsc_ibcr_32;	// 0x20 Input Buffer Control Register
		VADDR_T			gsc_bcfgr_32;	// 0x24 Board Configuration Register
		VADDR_T			gsc_bsr_32;		// 0x28 Buffer Size Register
		VADDR_T			gsc_idbr_32;	// 0x30 Input Data Buffer Register

		// 24DSI12 specific
		VADDR_T			gsc_rcar_32;	// 0x04 Rate Control A Register
		VADDR_T			gsc_rcbr_32;	// 0x08 Rate Control B Register

		// 24DSI32 specific
		VADDR_T			gsc_nrefcr_32;	// 0x04 Nref Control Register
		VADDR_T			gsc_nvcocr_32;	// 0x08 Nvco Control Register
		VADDR_T			gsc_rcr_32;		// 0x08 Nvco Control Register
		VADDR_T			gsc_rfcr_32;	// 0x1C Range and Filter Control Register
	} vaddr;

	struct
	{
		u32				gsc_bcfgr_32;		// Board Configuration Register
		int				bcfgr_d15_pll;		// Is BCFGR D15 for PLL status?
		int				bcfgr_d16_chans;	// Channel count if BCFGR D16 is set.
		int				bcfgr_d16_chans_0;	// Channel count if BCFGR D16 is clear.
		int				bcfgr_d17_chans;	// Channel count if BCFGR D17 is set.
		int				bcfgr_d17_chans_0;	// Channel count if BCFGR D17 is clear.
		int				bcfgr_d17_18_range;	// Are BCFGR.D17/18 for Voltage Range?
		int				bcfgr_d18_cf_filt;	// Is BCFGR D18 for Custom Frequency Filter?
		int				bcfgr_d18_rear_io;	// Is BCFGR D18 for Rear I/O Panel?
		int				bcfgr_d19_ext_temp;	// Is BCFGR D19 for Extended Temperature?
		int				bcfgr_d19_20_filt;	// Are BCFGR.D19/20 for Filter Frequency?
		int				bcfgr_d20_low_pow;	// Is BCFGR D20 for Low Power?
		int				bcfgr_d21_ext_temp;	// Is BCFGR D21 for Extended Temperature?
		int				bcfgr_d22_24dsi6ln;	// Is BCFGR.D22 for 24DSI6LN?

		int				bctlr_d0_1_in_mode;	// BCTLR.D0/1: Analog Input Mode?
		int				bctlr_d2_3_range;	// BCTLR.D2/3: Voltage Range?
		int				bctlr_d16_sync_1;	// BCTLR.D16: Is 1 for Synchronous?
		int				bctlr_d19_freq_filt;// BCTLR.D19: Frequency Filter?
		int				bctlr_d20_xcvr;		// BCTLR.D20: Xcvr selection?
		int				bctlr_d21_arm_et;	// BCTLR.D21: Arm External Trigger?
		int				bctlr_d22_thr_f_out;// BCTLR.D22: Is  Threshold Flag Out?
		int				bctlr_d22_coupling;	// BCTLR.D23: Coupling?
		int				bctlr_d23_rate_out;	// BCTLR.D23: ADC Rate Out?
		int				bctlr_d23_inv_e_trg;// BCTLR.D23: Invert External Trigger?

		int				channel_groups;		// The number of channel groups on the board.
		int				ch_grp_0_rar_pri;	// Is Chanel Group 0 Rate Assignment primary?
		int				channel_qty;		// The number of channels on the board.
		int				channels_max;		// Maximum channels supported by model.

		int				range_2p5;			// Is the 2.5 volt range supported?
		int				range_5;			// Is the 5 volt range supported?
		int				range_10;			// Is the 10 volt range supported?

		int				gps_present;		// Does the board have GPS support?

		int				pll_present;		// Does the board have PLL support?
		u32				fref_default;		// The default Fref clock rate.
		u32				fsamp_default;		// The default Fsamp rate.
		u32				fsamp_max;			// The maximum Fsamp rate supported.
		u32				fsamp_min;			// The minimum Fsamp rate supported.

		u32				fifo_size;			// Size of FIFO - not the fill level.
		int				low_noise;			// Is this a low noise board?
		int				low_power;			// Is this a low power board?
		int				rate_gen_qty;		// The number of rate generators on board.

		int				rate_gen_fgen_max;	// Rate Generator maximum output rate.
		int				rate_gen_fgen_min;	// Rate Generator minimum output rate.

		int				rate_div_qty;		// Number of rate dividers.
		u32				rate_div_ndiv_mask;	// Mask of valid Nrate bits.
		int				rate_div_ndiv_max;	// Minimum Nrate value.
		int				rate_div_ndiv_min;	// Maximum Nrate value.

		int				rate_gen_nref_mask;	// PLL: Mask of valid Nref bits.
		int				rate_gen_nref_max;	// PLL: Minimum Nref value.
		int				rate_gen_nref_min;	// PLL: Maximum Nref value.

		int				rate_gen_nvco_mask;	// PLL: Mask of valid Nvco bits.
		int				rate_gen_nvco_max;	// PLL: Minimum Nvco value.
		int				rate_gen_nvco_min;	// PLL: Maximum Nvco value.

		int				rate_gen_nrate_mask;// LEGACY: Mask of valid Nrate bits.
		int				rate_gen_nrate_max;	// LEGACY: Minimum Nrate value.
		int				rate_gen_nrate_min;	// LEGACY: Maximum Nrate value.

		int				rcar_rcbr;			// Are RCAR and RCBR present?
		int				rfcr;				// Is the RFCR present.
		int				reg_icmr;			// Input Coupling Mode Register?

		int				auto_cal_ms;		// Maximum ms for auto-cal
		int				initialize_ms;		// Maximum ms for initialize

	} cache;
};



// prototypes	***************************************************************

ssize_t		dma_read_available(dev_data_t* dev, ssize_t samples);
ssize_t		dma_read_work(dev_data_t* dev, char* buff, ssize_t samples, unsigned long jif_end);
ssize_t		dmdma_read_available(dev_data_t* dev, ssize_t samples);
ssize_t		dmdma_read_work(dev_data_t* dev, char* buff, ssize_t samples, unsigned long jif_end);

int			initialize_ioctl(dev_data_t* dev, void* arg);
void		io_close(dev_data_t* dev);
int			io_create(dev_data_t* dev);
void		io_destroy(dev_data_t* dev);
int			io_open(dev_data_t* dev);

ssize_t		pio_read_available(dev_data_t* dev, ssize_t samples);

void		reg_mem32_mod(VADDR_T vaddr, u32 value, u32 mask);



#endif
