#
# Makefile to build the 24DSI utility programs
#

PROGDIRS = mbsync billion fsamp id regs rxrate savedata sbtest signals sw_sync
LIBDIRS = docsrc utils
SUBDIRS := $(LIBDIRS) $(PROGDIRS)

default: all

all clean::
	for dir in $(SUBDIRS); do\
	    $(MAKE) -C $$dir $@ || exit 1;\
	done

install::
	for dir in $(PROGDIRS); do\
	    $(MAKE) -C $$dir $@ || exit 1;\
	done

install-lib:
	for dir in $(LIBDIRS); do\
	    $(MAKE) -C $$dir install || exit 1;\
	done

